// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_FINITE_ELEMENT_H
#define NARWAL_FINITE_ELEMENT_H

#include <memory>
#include <dolfin/fem/FiniteElement.h>

namespace ufc
{
  class finite_element;
}

namespace dolfin
{
  class Point;
}

namespace narwal
{

  /// This class provides dolfin::FiniteElement::space_dimension().
  /// It is needed, so that eval functions allocate sufficient memory.

  class FiniteElement : public dolfin::FiniteElement
  {
  public:

    /// Constructor
    FiniteElement(std::shared_ptr<const dolfin::FiniteElement> element,
                  std::size_t dim)
      : dolfin::FiniteElement(element->ufc_element()), _dim(dim)
    {

    }

    /// Destructor
    virtual ~FiniteElement() {}

    /// Return the dimension of the finite element function space
    std::size_t space_dimension() const
    { return _dim; }

    // FIXME: make pure virtual
    /// Evaluate basis function i at given point in cell
    virtual void
      evaluate_basis(std::size_t i,
                     std::vector<double>& values,
                     const dolfin::Point& x,
                     const dolfin::Cell& cell,
                     const std::vector<double>& vertex_coordinates) const
    {
      dolfin::error("narwal::FiniteElement not implemented by base class");
    }

  private:

    const std::size_t _dim;

  };

}


#endif
