// Copyright (C) 2016 Chris Richardson
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.
//

#ifndef NARWAL_SURFACE_REFINEMENT_ND_H
#define NARWAL_SURFACE_REFINEMENT_ND_H

#include <memory>

namespace dolfin
{
  class Mesh;
}

namespace narwal
{
  class GenericSurface;

  /// Redefines a mesh by incorporating a surface

  class SurfaceRefinementND
  {
  public:

    static void refine(dolfin::Mesh& newmesh,
                       std::shared_ptr<const dolfin::Mesh> mesh,
                       const GenericSurface& surface);

    static std::shared_ptr<dolfin::Mesh>
    refine(std::shared_ptr<const dolfin::Mesh> mesh,
           const GenericSurface& surface);

  private:

    // Get the longest edge of each face (using local mesh index)
    //static std::vector<std::size_t> face_long_edge
    //  (const dolfin::Mesh& mesh,
    //   const dolfin::MeshFunction<bool>& marker);

  };

}

#endif
