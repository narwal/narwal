// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_EVALUATOR_H
#define NARWAL_EVALUATOR_H

#include <memory>
#include <vector>
#include <dolfin/common/Array.h>
#include <dolfin/fem/DirichletBC.h>


namespace dolfin
{
  class FiniteElement;
  class DirichletBC;
  class GenericDofMap;
  class GenericMatrix;
  class GenericVector;
  class Point;
  class Mesh;
  class Cell;
  class Function;
}

namespace ufc
{
  class cell;
}

namespace narwal
{

  class DofMap;
  class XFEMForms;
  class GenericSurface;
  class DomainManager;

  /// This class evaluates function values
  // it has eval() functions similar to dolfin::Function
  class Evaluator
  {
  public:

    /// define, how discontinuous meshes are evaluated in Evaluator
    enum Mode {
      domain,           ///< assume point is in the domain away from discontinuity
      interface_full,   ///< evaluate size of discontinuity, e.g. full displacement vector
      interface_normal  ///< evaluate normal part of discontinuity, if applicable
    };

    /// Constructor
    Evaluator(
      const std::shared_ptr<const dolfin::Function> function,
      const std::shared_ptr<const narwal::DofMap> dofmap,
      const std::shared_ptr<const narwal::DomainManager> domain_manager,
      const std::shared_ptr<const narwal::XFEMForms> forms
      );

    /// Assemble matrix and RHS vector
    void assemble(dolfin::GenericMatrix& A, dolfin::GenericVector& b);

    /// Evaluate function along a given surface mesh defined along a discontinuity
    ///
    /// *Arguments*
    ///     values (_Array_ <double>)
    ///         The values.
    ///     x (_Array_ <double>)
    ///         The coordinates.
    std::unique_ptr<dolfin::Function> evaluate_discontinuity(
      std::shared_ptr<const dolfin::Mesh> surface_mesh,
      const int surface_index
      ) const;

    /// Evaluate function along a discontinuity at given coordinates
    ///
    /// *Arguments*
    ///     values (_Array_ <double>)
    ///         The values.
    ///     x (_Array_ <double>)
    ///         The coordinates.
    void evaluate_discontinuity(
        dolfin::Array<double>& values,
        const dolfin::Array<double>& x,
        const Mode evalMode,
        const int surface_index
        ) const;

    /// Evaluate function at given coordinates
    ///
    /// *Arguments*
    ///     values (_Array_ <double>)
    ///         The values.
    ///     x (_Array_ <double>)
    ///         The coordinates.
    void eval(dolfin::Array<double>& values,
              const dolfin::Array<double>& x) const;

    /// Evaluate function at given coordinates
    ///
    /// *Arguments*
    ///     values (_Array_ <double>)
    ///         The values.
    ///     x_eval (_Array_ <double>)
    ///         The coordinates.
    ///     x_eval_heaviside (_Array_ <double>)
    ///         The coordinates of a point where to evaluate the heaviside function
    void eval(dolfin::Array<double>& values,
              const dolfin::Array<double>& x_eval,
              const dolfin::Array<double>& x_eval_heaviside) const;

    /// Evaluate function at given coordinates
    ///
    /// *Arguments*
    ///     values (_Array_ <double>)
    ///         The values.
    ///     x (_Array_ <double>)
    ///         The coordinates.
    void eval(dolfin::Array<double>& values,
              const dolfin::Array<double>& x_eval,
              const dolfin::Array<double>& x_eval_heaviside,
              const Mode evalMode,
              const int surface_index) const;

    /// Evaluate function at given coordinates in given cell
    ///
    /// *Arguments*
    ///     values (_Array_ <double>)
    ///         The values.
    ///     x (_Array_ <double>)
    ///         The coordinates.
    ///     dolfin_cell (_Cell_)
    ///         The cell.
    ///     ufc_cell (ufc::cell)
    ///         The ufc::cell.
    void eval(dolfin::Array<double>& values,
              const dolfin::Array<double>& x_eval,
              const dolfin::Array<double>& x_eval_heaviside,
              const dolfin::Cell& dolfin_cell,
              const ufc::cell& ufc_cell,
              const Mode evalMode,
              const int surface_index) const;

    /// Evaluate at given point in given cell
    ///
    /// *Arguments*
    ///     values (_Array_ <double>)
    ///         The values at the point.
    ///     x (_Array_ <double>)
    ///         The coordinates of the point.
    ///     cell (ufc::cell)
    ///         The cell which contains the given point.
    virtual void eval(dolfin::Array<double>& values,
                      const dolfin::Array<double>& x_eval,
                      const dolfin::Array<double>& x_eval_heaviside,
                      const ufc::cell& cell,
                      const Mode evalMode,
                      const int surface_index) const;

  private:

    void evaluate_basis(
      const dolfin::FiniteElement& element,
      const dolfin::Cell &c,
      std::size_t i, double* values, const double* x,
      const double* vertex_coordinates,
      int cell_orientation) const;

    const std::shared_ptr<const dolfin::Function> _function;
    const std::shared_ptr<const narwal::DofMap> _dofmap;
    const std::shared_ptr<const narwal::DomainManager> _domain;
    const std::shared_ptr<const narwal::XFEMForms> _forms;

  };


}
#endif
