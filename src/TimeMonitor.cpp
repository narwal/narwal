// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "TimeMonitor.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <boost/timer/timer.hpp>


//------------------------------------------------------------------------------
narwal::Time::Time(const std::string& name) : _name(name)
{
  _timer.start();
}
//------------------------------------------------------------------------------
narwal::Time::~Time()
{
  stopAndRegisterTime();
}
//------------------------------------------------------------------------------
void narwal::Time::stopAndRegisterTime()
{
  _timer.stop();
  TimeMonitor::instance().addTime(_name, _timer.elapsed());
  // reset to zero to avoid double counting when stopAndRegisterTime is called directly
  _timer = boost::timer::cpu_timer();
}
//------------------------------------------------------------------------------
void narwal::TimeMonitor::plotResultsToStream(const std::size_t namecol,
                                              const std::size_t timecolWall,
                                              const std::size_t timecolUser,
                                              const std::size_t timecolSyst,
                                              const std::size_t countcol,
                                              std::ostream& os) const
{
  if (times.empty())
    return;

  const std::size_t allcol = namecol + timecolWall + timecolUser + timecolSyst
    + countcol;

  os << std::string(allcol, '=') << std::endl;
  os << std::left << " TimeMonitor Summary" << std::endl;
  os << std::string(allcol, '-') << std::endl;

  os.width(namecol);
  os.fill(' ');
  os << std::left << " Timer name";

  os.width(timecolWall);
  os.fill(' ');
  os << std::right << "Wall clock [s] ";

  os.width(timecolUser);
  os.fill(' ');
  os << std::right << "User clock [s] ";

  os.width(timecolSyst);
  os.fill(' ');
  os << std::right << "Syst clock [s] ";

  os.width(countcol);
  os.fill(' ');
  os << std::right << "# calls " << std::endl;

  os << std::string(allcol, '-') << std::endl;

//  for (std::vector<std::string>::const_iterator it =
//      counternames.begin();
//      it != counternames.end(); ++it)
//  {
//    const std::string &name = *it;
//    std::cout << name << std::endl;
//    const boost::timer::cpu_times &ts = times.find(name)->second;
  for (TimeMap_Type::const_iterator it = times.begin(); it != times.end(); ++it)
  {
    const std::string& name = it->first;
    const boost::timer::cpu_times &ts = it->second;
    if (name.size() > namecol)
    {
      os << std::setw(namecol) << std::left
         << (" " + name.substr(0, namecol - 5) + "...");
    }
    else
    {
      os << std::setw(namecol) << std::left << (" " + name);
    }
    os << std::setw(timecolWall - 2) << std::right << std::fixed
       << std::setprecision(3) << double(ts.wall) / 1.0e9;
    os << std::setw(timecolUser) << std::right << std::fixed
       << std::setprecision(3) << double(ts.user) / 1.0e9;
    os << std::setw(timecolSyst) << std::right << std::fixed
       << std::setprecision(3) << double(ts.system) / 1.0e9;
    os << std::setw(countcol) << std::right << counts.find(name)->second
       << std::endl;
  }
  os << std::string(allcol, '=') << std::endl;
}
//------------------------------------------------------------------------------
narwal::TimeMonitor& narwal::TimeMonitor::instance()
{
  static TimeMonitor _instance;
  return _instance;
}
//------------------------------------------------------------------------------
narwal::TimeMonitor::TimeMonitor() : running(false)
{
  // Do nothing
}
//------------------------------------------------------------------------------
narwal::TimeMonitor::~TimeMonitor()
{
  summarize("timings.log");
}
//------------------------------------------------------------------------------
void narwal::TimeMonitor::summarize(const std::string& filename)
{
  // write to console
  plotResultsToStream(60, 16, 16, 16, 12, std::cout);

  // write to log file
  std::ofstream ofs(filename);
  plotResultsToStream(120, 16, 16, 16, 12, ofs);

  // delete timing data
  reset();
  running = false;
}
//------------------------------------------------------------------------------
void narwal::TimeMonitor::start()
{
  reset();
  running = true;
}
//------------------------------------------------------------------------------
void narwal::TimeMonitor::reset()
{
  times.clear();
  counts.clear();
//  counternames.clear();
//  running = false;
}
//------------------------------------------------------------------------------
void narwal::TimeMonitor::addTime(const std::string &name,
                                  const boost::timer::cpu_times& t)
{
  if (!running)
    return;

  TimeMap_Type::const_iterator it = times.find(name);
  if (it == times.end())
  {
    times[name] = boost::timer::cpu_times();
    counts[name] = 0;
    //counternames.push_back(name);
  }
  boost::timer::cpu_times& ts = times[name];
  ts.wall += t.wall;
  ts.user += t.user;
  ts.system += t.system;
  counts[name] = counts[name] + 1;
}
//------------------------------------------------------------------------------
std::shared_ptr<narwal::Time>
narwal::TimeMonitor::createTimer(const std::string& name)
{
  return std::make_shared<narwal::Time>(name);
}
//------------------------------------------------------------------------------
