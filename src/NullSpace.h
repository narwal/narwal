// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_NULLSPACE_H
#define NARWAL_NULLSPACE_H

#include <memory>

namespace dolfin
{
  class FunctionSpace;
  class GenericVector;
  class Mesh;
  class VectorSpaceBasis;
}

namespace narwal
{
  class DofMap;
}

namespace narwal
{

  // FIXME (GNW): This class could be made simpler and clearer.

  /// <b> Extended Algebraic Multigrid </b>

  namespace xamg
  {
    /// Base class for NullSpaces

    class INullSpace
    {
    public:
      virtual ~INullSpace(){}
      virtual std::unique_ptr<dolfin::VectorSpaceBasis> build() const = 0;

    };

    /// Builds nullspace for displacements in 3D (3 translations, 3
    /// rotations)

    class NullSpace3DDisplacement : public INullSpace
    {
      const std::shared_ptr<const dolfin::FunctionSpace> V;
      const std::shared_ptr<const dolfin::GenericVector> b;

      // FIXME: Why store a mesh when a FunctionSpace is stored?
      const std::shared_ptr<const dolfin::Mesh> mesh;

    public:

      NullSpace3DDisplacement(
        const std::shared_ptr<const dolfin::FunctionSpace> V_,
        const std::shared_ptr<const dolfin::GenericVector> b_,
        const std::shared_ptr<const dolfin::Mesh> mesh_)
        : V(V_), b(b_), mesh(mesh_) {}
      virtual ~NullSpace3DDisplacement(){}

      /// Build nullspace
      std::unique_ptr<dolfin::VectorSpaceBasis> build() const;
    };

    /// Builds nullspace for displacements in 3D (3 translations, 3
    /// rotations)

    class NullSpace3DDisplacement2 : public INullSpace
    {
      const std::shared_ptr<const dolfin::FunctionSpace> V;
      const std::shared_ptr<const narwal::DofMap>        dofmap;
      const std::shared_ptr<const dolfin::GenericVector> b;
      const std::shared_ptr<const dolfin::Mesh>          mesh;

    public:

      NullSpace3DDisplacement2(
        const std::shared_ptr<const dolfin::FunctionSpace> V_,
        const std::shared_ptr<const narwal::DofMap>        dofmap_,
        const std::shared_ptr<const dolfin::GenericVector> b_,
        const std::shared_ptr<const dolfin::Mesh> mesh_)
        : V(V_), dofmap(dofmap_), b(b_), mesh(mesh_) {}
      virtual ~NullSpace3DDisplacement2(){}

      /// Build nullspace
      std::unique_ptr<dolfin::VectorSpaceBasis> build() const;
    };

    /// Builds nullspace for displacements in 2D (2 translations, 1
    /// rotation)

    class NullSpace2DDisplacement : public INullSpace
    {
      const std::shared_ptr<const dolfin::FunctionSpace> V;
      const std::shared_ptr<const dolfin::GenericVector> b;
      const std::shared_ptr<const dolfin::Mesh>          mesh;
    public:

      NullSpace2DDisplacement(
        const std::shared_ptr<const dolfin::FunctionSpace> V_,
        const std::shared_ptr<const dolfin::GenericVector> b_,
        const std::shared_ptr<const dolfin::Mesh>          mesh_)
        : V(V_), b(b_), mesh(mesh_) {}
      virtual ~NullSpace2DDisplacement(){}

      /// Build nullspace
      std::unique_ptr<dolfin::VectorSpaceBasis> build() const;
    };

    /// Builds nullspace for scalar (1 offset)

    class NullSpace3DScalarSolution : public INullSpace
    {
      const std::shared_ptr<const dolfin::FunctionSpace> V;
      const std::shared_ptr<const dolfin::GenericVector> b;

    public:

    NullSpace3DScalarSolution(
      const std::shared_ptr<const dolfin::FunctionSpace> V_,
      const std::shared_ptr<const dolfin::GenericVector> b_)
      : V(V_), b(b_) {}
    virtual ~NullSpace3DScalarSolution(){}

      /// Build nullspace
      std::unique_ptr<dolfin::VectorSpaceBasis> build() const;
    };

  }



}

#endif
