// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_SOLVER_XAMG_H
#define NARWAL_SOLVER_XAMG_H

#include <memory>
#include <vector>
#include <string>

#ifdef HAVE_MPI
#include <mpi.h>
#endif

namespace dolfin
{
  class Mesh;
  class FiniteElement;
  class Function;
  class FunctionSpace;
  class GenericLinearSolver;
  class GenericMatrix;
  class GenericPreconditioner;
  class GenericVector;
  class VectorSpaceBasis;
}

namespace narwal
{
  class DofMap;
  class DomainManager;
  class XFEMForms;
}

namespace narwal
{

  namespace la
  {

    /// Solves a XFEM system via multigrid using a transformation approach

    /*
     *
     *
     * Instead of solving:
     *
     *   A * u = b
     *   --> u = A^-1 * b
     *
     * we solve:
     *
     *   Gt * A G* G^-1 * u = Gt  *b
     *                --> u = G * (Gt*A*G)^-1 * Gt * b
     *
     */

    // For doxygen
    /**
     *
     * Instead of solving:
     *
     *   \b A u = b
     *
     *   &rarr; u = \b A<sup>-1</sup> b
     *
     * we solve:
     *
     *   \b G<sup>T</sup>  \b A \b G \b G<sup>-1</sup> u = \b G<sup>T</sup> b
     *
     *   &rarr; u = \b G (\b G<sup>T</sup>\b A \b G)<sup>-1</sup> \b G<sup>T</sup> b
     *
     *
     */

    class SolverXAMG
    {

    public:

      SolverXAMG(
        std::shared_ptr<const dolfin::GenericMatrix> A,
        std::shared_ptr<const dolfin::GenericVector> b,
        std::shared_ptr<dolfin::Function> u,
        std::shared_ptr<const dolfin::FunctionSpace> V,
        std::shared_ptr<const narwal::DomainManager> domainManager,
        std::shared_ptr<const narwal::DofMap> dofmap,
        std::shared_ptr<const narwal::XFEMForms> forms,
        const bool applyTransformation_=true,
        const bool debugOutput_=false,
        const std::string &debugFilenamePrefix=std::string(""));

      ~SolverXAMG();

      void setupDefaultPreCondAndSolver(const std::string& amg_lib="petsc_amg");

      /// solve
      std::size_t solve();

      /// create transformation matrix from shifted to Heaviside basis
      static std::unique_ptr<dolfin::GenericMatrix>
      buildTransformationMatrixG(
        const narwal::DomainManager& domainManager,
        const narwal::DofMap&        dofmap,
        const dolfin::FiniteElement& element,
        const narwal::XFEMForms&     forms);

    private:

      void transformSystem();

      void inverseTransformSystem();

      std::shared_ptr<const narwal::DomainManager> domainManager;

      std::shared_ptr<const narwal::DofMap> dofmap;

      std::shared_ptr<const dolfin::FiniteElement> element;

      std::shared_ptr<const narwal::XFEMForms> forms;

      /// system matrix
      std::shared_ptr<const dolfin::GenericMatrix> A;

      /// rhs
      std::shared_ptr<const dolfin::GenericVector> b;

      /// solution
      std::shared_ptr<dolfin::Function> u;

    public:

      /// null space
      std::shared_ptr<dolfin::VectorSpaceBasis> null_space;

    private:

      /// preconditioner object
      std::shared_ptr<dolfin::GenericPreconditioner> pc;

      /// solver object
      std::shared_ptr<dolfin::GenericLinearSolver> solver;

      // transformation matrix
      std::shared_ptr<dolfin::GenericMatrix> G;

      // transformed system matrix
      std::shared_ptr<const dolfin::GenericMatrix> GtAG;

      // transformed rhs
      std::shared_ptr<const dolfin::GenericVector> Gtb;

      // transformed solution function
      std::shared_ptr<dolfin::Function> Ginv_u;

      // transformed null space
      std::shared_ptr<dolfin::VectorSpaceBasis> Ginv_null_space;

      // control output of ASCII matrix dump and other debug output
      const bool applyTransformation;

      // if no AMG is used, turn off some AMG specific computations
      bool enableAMG;

      // control output of ASCII matrix dump and other debug output
      const bool debugOutput;

      const std::string debugFilenamePrefix;

    };

  }

}

#endif
