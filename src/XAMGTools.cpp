// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "XAMGTools.h"

#include <fstream>
#include <iomanip>

#include <dolfin/la/PETScKrylovSolver.h>
#include <dolfin/la/PETScLUSolver.h>
#include <dolfin/la/PETScMatrix.h>
#include <dolfin/la/PETScVector.h>
#include <dolfin/la/VectorSpaceBasis.h>
#include <dolfin/parameter/GlobalParameters.h>

//-----------------------------------------------------------------------------
std::unique_ptr<dolfin::GenericMatrix> narwal::la::initSystemMatrix()
{
  const std::string lab = dolfin::parameters["linear_algebra_backend"];
  std::unique_ptr<dolfin::GenericMatrix> A;
  if (lab == "PETSc")
  {
    #ifdef HAS_PETSC
    if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
      dolfin::info("Create PETSc system matrix...");
    A.reset(new dolfin::PETScMatrix);
    #endif
  }
  else
  {
    if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
      dolfin::warning("unknown linear algebra backend...");
  }

  return A;
}
//-----------------------------------------------------------------------------
std::unique_ptr<dolfin::GenericVector> narwal::la::initSystemVector()
{
  const std::string lab = dolfin::parameters["linear_algebra_backend"];
  std::unique_ptr<dolfin::GenericVector> b;
  if (lab == "PETSc")
  {
    #ifdef HAS_PETSC
    if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
      dolfin::info("Create PETSc system vector...");
    b.reset(new dolfin::PETScVector);
    #endif
  }
  else
  {
    if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
      dolfin::warning("unknown linear algebra backend...");
  }

  return b;
}
//-----------------------------------------------------------------------------
std::unique_ptr<dolfin::GenericMatrix>
narwal::la::MatPtAP(const dolfin::GenericMatrix &G,
                    const dolfin::GenericMatrix &A)
{
  if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
    dolfin::info("MatPtAP");

  std::unique_ptr<dolfin::GenericMatrix> GtAG;

  const std::string lab = dolfin::parameters["linear_algebra_backend"];
  if (lab == "PETSc")
  {
    #ifdef HAS_PETSC
    Mat NativeA = A.down_cast<dolfin::PETScMatrix>().mat();
    Mat NativeG = G.down_cast<dolfin::PETScMatrix>().mat();
    Mat NativeGtAG;
    PetscErrorCode error =
        MatPtAP(NativeA, NativeG, MAT_INITIAL_MATRIX, 1.0, &NativeGtAG);
    // NativeGtAG PETSc reference count is now 1

    if (error) {
      dolfin::error("MatPtAP call failed...");
    }

    GtAG.reset(new dolfin::PETScMatrix(NativeGtAG));
    // GtAG constructor has increased Reference count for NativeGtAG object to 2
    // Lets reduce by 1 to make sure, GtAG dolfin destructor deletes the object
    MatDestroy(&NativeGtAG);
    // PETSc matrix is still allocated and will be finally
    // cleaned up by GtAG destructor
    #endif
  }
  else
  {
    if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
      dolfin::warning("unknown linear algebra backend...");
  }
  return GtAG;
}
//-----------------------------------------------------------------------------
std::unique_ptr<dolfin::VectorSpaceBasis>
narwal::la::transformGinvN(const std::shared_ptr<const dolfin::GenericMatrix> G,
                           const dolfin::VectorSpaceBasis &null_space,
                           const bool approx)
{
  dolfin::info("Inverse-transform Nullspace...");
  std::unique_ptr<dolfin::GenericLinearSolver> linSolver;

  const std::string lab = dolfin::parameters["linear_algebra_backend"];
  if (lab == "PETSc")
  {
    #ifdef HAS_PETSC
    if (approx) {
      linSolver.reset(new dolfin::PETScKrylovSolver("gmres", "jacobi"));
    } else {
#ifdef PETSC_HAVE_MUMPS
      std::cout << "Mumps to the rescue..." << std::endl;
      linSolver.reset(new dolfin::PETScLUSolver("mumps"));
#else
      linSolver.reset(new dolfin::PETScLUSolver("umfpack"));
#endif
    }
    #endif
  }
  else
  {
    if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
      dolfin::warning("unknown linear algebra backend...");
  }

  std::vector<std::shared_ptr<dolfin::GenericVector> > Ginv_null_space_basis;
  linSolver->set_operator(G);
  linSolver->parameters["report"] = false;

  if (approx) {
    linSolver->parameters["relative_tolerance"] = 1.0e-9;
    linSolver->parameters["monitor_convergence"] = false;
  }
  for (std::size_t i = 0; i < null_space.dim(); ++i)
  {
    Ginv_null_space_basis.push_back(null_space[i]->copy());
    linSolver->solve(*Ginv_null_space_basis.back(), *(null_space[i]));
  }
  std::unique_ptr<dolfin::VectorSpaceBasis>
    Ginv_null_space(new dolfin::VectorSpaceBasis(Ginv_null_space_basis));

  return Ginv_null_space;
}
//-----------------------------------------------------------------------------
void narwal::la::outputNullSpace(const dolfin::VectorSpaceBasis &null_space,
                                 std::ostream &os)
{
  for (std::size_t i = 0; i < null_space[0]->size(); ++i) {
    for (std::size_t iDim = 0; iDim < null_space.dim(); ++iDim) {
      const dolfin::GenericVector &a = *null_space[iDim];
      const std::size_t w = (iDim >=3 ? 10 : 3);
      os << std::setw(w) << a[i];
    }
    os << std::endl;
  }
}
//-----------------------------------------------------------------------------
void narwal::la::toMatrixMarketFile(const dolfin::GenericMatrix& A,
                                    const std::string &filenameroot)
{
  const std::string lab = dolfin::parameters["linear_algebra_backend"];
  const std::string filename = filenameroot+"_"+lab+".txt";
  if (lab == "PETSc")
  {
    #ifdef HAS_PETSC
    if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
      dolfin::info("write PETSc Matrix to Matrix Market file: "+filename);
    Mat NativeA = A.down_cast<dolfin::PETScMatrix>().mat();
    // get number of rows and columns
    PetscInt nrow, ncol;
    MatGetSize(NativeA, &nrow, &ncol);

    // get number of non zeros (is there a better native PETSc call)
    PetscInt numNZ = 0;
    for (PetscInt irow=0; irow < nrow; irow++)
    {
      PetscInt numcols;
      const PetscInt* cols;
      const PetscScalar* vals;
      MatGetRow(NativeA, irow, &numcols, PETSC_NULL, PETSC_NULL);
      for (PetscInt icol=0; icol < numcols; icol++)
        numNZ += numcols;
      MatRestoreRow(NativeA, irow, &numcols, &cols, &vals);
    }

    // write non zeros to file
    std::ofstream f;
    f.open(filename.c_str());
    f << "%%MatrixMarket matrix coordinate real general\n";
    f << nrow << " " << ncol << " " << numNZ << std::endl;
    for (PetscInt irow=0; irow < nrow; irow++)
    {
      PetscInt numcols;
      const PetscInt* cols;
      const PetscScalar* vals;
      MatGetRow(NativeA, irow, &numcols, &cols, &vals);
      for (PetscInt icol=0; icol < numcols; icol++)
      {
        f << irow+1 << " " << cols[icol]+1 << " " << std::scientific
          << vals[icol] << "\n";
      }
      MatRestoreRow(NativeA, irow, &numcols, &cols, &vals);
    }
    f.close();
    #endif
  }
  else
  {
    if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
      dolfin::warning("unknown linear algebra backend...");
  }
}
//-----------------------------------------------------------------------------
