// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "ImplicitSurface.h"

#include <dolfin/mesh/Cell.h>
#include <dolfin/mesh/Edge.h>
#include <dolfin/mesh/MeshEntity.h>
#include <dolfin/mesh/Vertex.h>

#include "SubTriangulation.h"

using namespace narwal;

//-----------------------------------------------------------------------------
ImplicitSurface::ImplicitSurface(const dolfin::Point& center,
                                 const double radius)
{
  // Do nothing

  // FIXME: store center and radius
}
//-----------------------------------------------------------------------------
ImplicitSurface::~ImplicitSurface()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
bool ImplicitSurface::entity_intersects_f0(const dolfin::MeshEntity& cell) const
{
  bool plus_side = false;
  bool minus_side = false;
  for (dolfin::VertexIterator v(cell); !v.end(); ++v)
  {
    const dolfin::Point p = v->point();
    const double d = f0(p);
    if(d > 0)
      plus_side = true;
    else
      minus_side = true;
  }
  return (plus_side && minus_side);
}
//-----------------------------------------------------------------------------
std::pair<bool, bool>
ImplicitSurface::entity_intersection(const dolfin::MeshEntity& e) const
{
  bool inside = false;
  bool outside = false;
  if (e.dim() == 1)
  {
    // Edge
    if (entity_intersects_f0(e))
    {
      const dolfin::Edge edge(e.mesh(), e.index());
      const dolfin::Point p = compute_intersection(edge);
      if (f1(p) < 0.0)
        inside = true;
    }
  }
  else
  {
    // FIXME: for cell, this involves 12 evaluations of f0,
    // when 4 would be enough
    for (dolfin::EdgeIterator edge(e); !edge.end(); ++edge)
    {
      // Check if this edge intersects f0
      if (entity_intersects_f0(*edge))
      {
        // Find the intersect with f0, and check f1 is negative
        const dolfin::Point p = compute_intersection(*edge);
        if (f1(p) < 0.0)
          inside = true;
        else
          outside = true;
      }
    }
  }

  return std::make_pair(inside, outside);
}
//-----------------------------------------------------------------------------
bool ImplicitSurface::contains_boundary(const dolfin::Cell& cell) const
{
  const std::pair<bool, bool> result = entity_intersection(cell);
  return (result.first && result.second);
}
//-----------------------------------------------------------------------------
dolfin::Point ImplicitSurface::compute_intersection(const dolfin::Edge& edge,
                                                    const double eps) const
{
  // Get vertices of edge
  const dolfin::Vertex v0(edge.mesh(), edge.entities(0)[0]);
  const dolfin::Vertex v1(edge.mesh(), edge.entities(0)[1]);
  dolfin::Point p0 = v0.point();
  dolfin::Point p1 = v1.point();

  double s0 = f0(p0);
  double s1 = f0(p1);

  // Ensure points are either side of surface
  if (s0*s1 > 0.0)
    dolfin::error("In compute_intersection(), edge does not cut surface.");

  dolfin::Point p;
  double s = 1.0;
  unsigned int it_count = 0;

  while (std::abs(s) > eps)
  {
    p = (s0/(s0 - s1)) * p1 - (s1/(s0 - s1)) * p0;
    s = f0(p);

    // Copy s to whichever has the same sign
    if (std::signbit(s) == std::signbit(s0))
    {
      s0 = s;
      p0 = p;
    }
    else
    {
      s1 = s;
      p1 = p;
    }

    if (it_count++ > 100)
      dolfin::error("In compute_intersection(), iterations exceeded.");
  }

  return p;
}
//-----------------------------------------------------------------------------
SubTriangulation ImplicitSurface::triangulate(const dolfin::Cell& cell) const
{
  return SubTriangulation(cell, *this);
}
//-----------------------------------------------------------------------------
int ImplicitSurface::side(const dolfin::Point p, double eps) const
{
  const double d = f0(p);
  if (d > eps)
    return 1;
  else if (d < -eps)
    return -1;
  else
    return 0;
}
//-----------------------------------------------------------------------------
std::vector<double> ImplicitSurface::normal(const dolfin::Point /*p*/,
                                            double /*eps*/) const
{
  dolfin::error("normal vector computation not implemented for this surface");
  return std::vector<double>(0);
}
//-----------------------------------------------------------------------------
