// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "DofMap.h"

#include <algorithm>
#include <array>
#include <unordered_set>
#include <vector>

#include <dolfin/fem/GenericDofMap.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/mesh/Cell.h>
#include <dolfin/mesh/Mesh.h>
#include <dolfin/mesh/Vertex.h>

#include "DomainManager.h"
#include "XFEMTools.h"

//-----------------------------------------------------------------------------
narwal::DofMap::DofMap(const dolfin::FunctionSpace& V,
                       const DomainManager& domain)
  : _dolfin_dofmap(V.dofmap()),  _mesh(V.mesh()), _max_cell_dimension(0)
{
  // Get Mesh and regular dofmap
  dolfin_assert(_mesh);
  dolfin_assert(V.dofmap());
  const dolfin::Mesh& mesh = *_mesh;
  const dolfin::GenericDofMap& dofmap = *V.dofmap();

  // Copy regular part of dofmap
  for (dolfin::CellIterator c(mesh); !c.end(); ++c)
  {
    auto dofs = dofmap.cell_dofs(c->index());
    _dofmap.push_back(std::vector<dolfin::la_index>(dofs.begin(), dofs.end()));
  }

  // Compute enrichment marker for each regular dof, and mark cells
  // affected by enrichment
  std::vector<std::set<EnrLabelArray>> enriched_dof_markers;
  std::vector<std::set<EnrLabelArray>> enriched_cells;
  narwal::XFEMTools::compute_enriched_dof_markers(enriched_dof_markers,
                                                  enriched_cells,
                                                  V, domain);
  dolfin_assert(enriched_dof_markers.size() == dofmap.global_dimension());

  // std::set for counting enriched dofs
  std::set<dolfin::la_index> enriched_dof_set;

  // Number (assign index to) enriched dofs and build maps
  // (regular_dof->enriched_dof, enriched_dof->surface_side,
  // enriched_dofs->regular_dof)
  std::vector<std::set<std::pair<std::size_t, EnrLabelArray>>>
    regular_to_enriched(dofmap.global_dimension());
  std::size_t enriched_dof_index = regular_to_enriched.size();
  for (std::size_t reg_dof_index = 0;
       reg_dof_index < enriched_dof_markers.size(); ++reg_dof_index)
  {
    const std::set<EnrLabelArray>&
      edofs = enriched_dof_markers[reg_dof_index];
    if (!edofs.empty())
    {
      for (auto& edof : edofs)
      {
        // Store array map from regular dof index to enriched dof index
        regular_to_enriched[reg_dof_index].insert({enriched_dof_index, edof});

        // Insert enriched dof index into set
        enriched_dof_set.insert(enriched_dof_index);

        // Increment enriched dof index
        ++enriched_dof_index;
      }
    }
  }

  // Add enriched dofs to cell dofmap for enriched cells
  _max_cell_dimension = _dolfin_dofmap->max_cell_dimension();
  _cell_enriched_dof_indices.resize(_dofmap.size());
  for (std::size_t c = 0; c < _dofmap.size(); ++c)
  {
    if (!enriched_cells[c].empty())
    {
      // Get cell dofmap
      std::vector<dolfin::la_index>& cell_dofs = _dofmap[c];

      // Get number of regular dofs
      const std::size_t num_regular_dofs = cell_dofs.size();

      // Initialise cell-wise regular -> enriched dofs
      _cell_enriched_dof_indices[c].resize(num_regular_dofs);

      // Extend dofmap for enriched cells
      for (std::size_t i = 0; i < num_regular_dofs; ++i)
      {
        const dolfin::la_index regular_dof_index = cell_dofs[i];
        for (auto edof : regular_to_enriched[regular_dof_index])
        {
          // FIXME: Dof for i-j surface interactions need to extend to
          //        neighbouring cells
          // Add dof if active on this cell
          //if (enriched_cells[c].count(edof.second) != 0)
          {
            std::size_t edof_index = edof.first;
            _cell_enriched_dof_indices[c][i].push_back({cell_dofs.size(),
                  edof.second});
            cell_dofs.push_back(edof_index);
          }
        }
      }
      _max_cell_dimension = std::max(cell_dofs.size(), _max_cell_dimension);
    }
  }

  // Set range
  const std::size_t range = dofmap.global_dimension() + enriched_dof_set.size();
  _ownership_range = std::pair<std::size_t, std::size_t>(0, range);

  // Initialise index map
  _index_map = std::make_shared<dolfin::IndexMap>(MPI_COMM_WORLD, range, 1);
}
//-----------------------------------------------------------------------------
void narwal::DofMap::set(dolfin::GenericVector& x, double value) const
{
  // FIXME: Assumes only one enriched dof at a regular dof

  dolfin::warning("EXPERIMENTAL: narwal::Dofmap::set(..)");
  std::vector<double> _value;
  std::vector<std::vector<dolfin::la_index> >::const_iterator cell_dofs;
  for (cell_dofs = _dofmap.begin(); cell_dofs != _dofmap.end(); ++cell_dofs)
  {
    _value.resize(cell_dofs->size(), value);
    x.set(_value.data(), cell_dofs->size(), cell_dofs->data());
  }

  // XFEM part - begin
  std::unordered_set<dolfin::la_index> enriched_dof_set;
  for (std::size_t c = 0; c < _cell_enriched_dof_indices.size(); ++c)
  {
    for (auto edofs : _cell_enriched_dof_indices[c])
    {
      if (!edofs.empty())
      {
        dolfin_assert(edofs.size() == 1);
        const std::size_t dof = edofs.begin()->first;
        enriched_dof_set.insert(_dofmap[c][dof]);
      }
    }
  }
  const std::vector<dolfin::la_index> edofs(enriched_dof_set.begin(),
                                            enriched_dof_set.end());
  const std::vector<double> zeros(edofs.size(), 0.0);
  x.set(zeros.data(), edofs.size(), edofs.data());
  // XFEM part - end

  x.apply("insert");
}
//-----------------------------------------------------------------------------
void narwal::DofMap::set(dolfin::GenericVector& x, std::size_t dim,
                         double value) const
{
  dolfin::warning("EXPERIMENTAL: narwal::Dofmap::set(.., dim, ..)");

  // Extract regular sub-dofmap
  dolfin_assert(_dolfin_dofmap);
  dolfin_assert(_mesh);
  std::vector<std::size_t> component(1, dim);
  std::shared_ptr<GenericDofMap> submap
    = _dolfin_dofmap->extract_sub_dofmap(component, *_mesh);

  // Dof markers
  const std::size_t regular_size
    = ownership_range().second - ownership_range().first;
  std::vector<bool> regular_subdofs(regular_size, false);

  // Set regular dofs
  std::vector<double> _value;
  for (dolfin::CellIterator c(*_mesh); !c.end(); ++c)
  {
    dolfin::ArrayView<const dolfin::la_index> cell_dofs
      = submap->cell_dofs(c->index());
    for (std::size_t i = 0; i < cell_dofs.size(); ++i)
      regular_subdofs[cell_dofs[i]] = true;
    _value.resize(cell_dofs.size(), value);
    x.set(_value.data(), cell_dofs.size(), cell_dofs.data());
  }

  for (dolfin::CellIterator c(*_mesh); !c.end(); ++c)
  {
    // set enriched dofs value to zero
    if (is_enriched(c->index()))
    {
      const std::size_t numRegDofs
        = _dolfin_dofmap->num_element_dofs(c->index());

      const dolfin::ArrayView<const dolfin::la_index> cell_dofs
        = submap->cell_dofs(c->index());

      std::vector<dolfin::la_index> enriched_dofs;
      for (std::size_t iRegDof = numRegDofs; iRegDof < cell_dofs.size();
           ++iRegDof)
      {
        enriched_dofs.push_back(cell_dofs[iRegDof]);
      }
      const std::vector<double> _zeros(enriched_dofs.size(), 0.0);
      x.set(_zeros.data(), enriched_dofs.size(), enriched_dofs.data());
    }
  }

  // Apply
  x.apply("insert");
}
//-----------------------------------------------------------------------------
void narwal::DofMap::set_x(dolfin::GenericVector& x, double value,
                           std::size_t component,
                           const dolfin::Mesh& mesh,
                           const dolfin::FiniteElement& element) const
{
  dolfin::warning("EXPERIMENTAL: narwal::DofMap::set_x(..)");
  std::vector<double> x_values;
  boost::multi_array<double, 2> coordinates;
  std::vector<double> vertex_coordinates;
  for (dolfin::CellIterator cell(mesh); !cell.end(); ++cell)
  {
    // Update UFC cell
    cell->get_vertex_coordinates(vertex_coordinates);

    // Get local-to-global map
    dolfin::ArrayView<const dolfin::la_index> dofs = cell_dofs(cell->index());

    // Tabulate dof coordinates
    element.tabulate_dof_coordinates(coordinates, vertex_coordinates, *cell);
    dolfin_assert(coordinates.shape()[0] == dofs.size());
    dolfin_assert(component < coordinates.shape()[1]);

    // Copy coordinate (it may be possible to avoid this)
    x_values.resize(dofs.size());
    for (std::size_t i = 0; i < coordinates.shape()[0]; ++i)
      x_values[i] = value*coordinates[i][component];

    // Set x[component] values in vector
    x.set(x_values.data(), dofs.size(), dofs.data());
  }
}
//-----------------------------------------------------------------------------
void narwal::DofMap::set_x(dolfin::GenericVector& x, std::size_t dim,
                           double value, std::size_t x_component,
                           const dolfin::Mesh& mesh,
                           const dolfin::FiniteElement& element) const
{
  dolfin::warning("EXPERIMENTAL: narwal::DofMap::set_x(.., dim, ..)");

  // Extract regular sub-dofmap
  dolfin_assert(_dolfin_dofmap);
  std::vector<std::size_t> component(1, dim);
  std::shared_ptr<GenericDofMap> submap
    = _dolfin_dofmap->extract_sub_dofmap(component, mesh);

  // Dof markers
  const std::size_t regular_size
    = ownership_range().second - ownership_range().first;
  std::vector<bool> regular_subdof(regular_size, false);

  // Mark regular dofs that belong to sub-map
  for (dolfin::CellIterator c(mesh); !c.end(); ++c)
  {
    dolfin::ArrayView<const dolfin::la_index> cell_dofs
      = submap->cell_dofs(c->index());
    for (std::size_t i = 0; i < cell_dofs.size(); ++i)
      regular_subdof[cell_dofs[i]] = true;
  }

  // Build regular-to-enriched map (for sub-map)
//  std::vector<int> regular_to_enriched(regular_size, -1);
//  for (auto it = _enriched_to_regular.begin(); it != _enriched_to_regular.end();
//       ++it)
//  {
//    if (regular_subdof[it->second])
//      regular_to_enriched[it->second] = it->first;
//  }

  std::vector<double> x_values;
  boost::multi_array<double, 2> coordinates;
  std::vector<double> vertex_coordinates;
  for (dolfin::CellIterator cell(mesh); !cell.end(); ++cell)
  {
    std::vector<double> x_values_enriched;
    std::vector<dolfin::la_index> enriched_dofs;

    // Update UFC cell
    cell->get_vertex_coordinates(vertex_coordinates);

    // Get local-to-global map
    dolfin::ArrayView<const dolfin::la_index> dofs
      = submap->cell_dofs(cell->index());

    // Tabulate regular dof coordinates
    element.tabulate_dof_coordinates(coordinates, vertex_coordinates, *cell);
    dolfin_assert(coordinates.shape()[0] == dofs.size());
    dolfin_assert(x_component < coordinates.shape()[1]);

    // Copy coordinate for regular coordinate (it may be possible to
    // avoid this)
    x_values.resize(dofs.size());
    for (std::size_t i = 0; i < coordinates.shape()[0]; ++i)
    {
      x_values[i] = value*coordinates[i][x_component];
//      if (regular_to_enriched[dofs[i]] >= 0)
//      {
//        x_values_enriched.push_back(x_values[i]);
//        enriched_dofs.push_back(regular_to_enriched[dofs[i]]);
//      }
    }

    // Set x[x_component] values in vector
    x.set(x_values.data(), dofs.size(), dofs.data());
//    x.set(x_values_enriched.data(), enriched_dofs.size(), enriched_dofs.data());
  }
}
//-----------------------------------------------------------------------------
std::size_t narwal::DofMap::global_dimension() const
{
  return  _ownership_range.second;
}
//-----------------------------------------------------------------------------
std::vector<dolfin::la_index> narwal::DofMap::dofs() const
{
  // Ownership range
  const std::size_t r0 = _ownership_range.first;
  const std::size_t r1 = _ownership_range.second;

  // Create vector to hold dofs
  std::vector<dolfin::la_index> _dofs;
  _dofs.reserve(_dofmap.size()*max_cell_dimension());

  // Insert all dofs into a vector (will contain duplicates)
  std::vector<std::vector<dolfin::la_index> >::const_iterator cell_dofs;
  for (cell_dofs = _dofmap.begin(); cell_dofs != _dofmap.end(); ++cell_dofs)
  {
    for (std::size_t i = 0; i < cell_dofs->size(); ++i)
    {
      const std::size_t dof = (*cell_dofs)[i];
      if (dof >= r0 && dof < r1)
        _dofs.push_back(dof);
    }
  }

  // Sort dofs (required to later remove duplicates)
  std::sort(_dofs.begin(), _dofs.end());

  // Remove duplicates
  _dofs.erase(std::unique(_dofs.begin(), _dofs.end() ), _dofs.end());

  return _dofs;
}
//-----------------------------------------------------------------------------
