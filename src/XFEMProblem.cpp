// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "XFEMProblem.h"

#include <dolfin/fem/GenericDofMap.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/la/GenericMatrix.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/la/TensorLayout.h>

#include "Assembler.h"
#include "TimeMonitor.h"
#include "SparsityTools.h"
#include "XFEMForms.h"
#include "XFEMFunctionSpace.h"

using namespace narwal;

//-----------------------------------------------------------------------------
XFEMProblem::XFEMProblem(
  std::shared_ptr<const narwal::XFEMForms> forms,
  std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs)
  : _forms(forms), _bcs(bcs)
{
  // Do nothing
}
//-----------------------------------------------------------------------------
XFEMProblem::~XFEMProblem()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
void
XFEMProblem::assemble(dolfin::GenericMatrix& A, dolfin::GenericVector& b) const
{
  narwal::Time t("narwal::XFEMProblem::assemble()");
  // Assemble system
  dolfin_assert(_forms);
  narwal::Assembler assembler(_bcs, _forms);
  assembler.assemble(A, b);
}
//-----------------------------------------------------------------------------
