// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_CONFORMING_MESH_GENERATOR_H
#define NARWAL_CONFORMING_MESH_GENERATOR_H

#include <memory>

namespace dolfin
{
  class Mesh;
}

namespace narwal
{
  class DomainManager;
  class GenericSurface;

  /// A class for generating meshes incorporating given surfaces

  class ConformingMeshGenerator
  {
  public:

    /// Generate a mesh that conforms to the crack surface
    /// Cells which are fully intersected by the surface are split
    /// by adding two new vertices where the surface intersects each edge,
    /// which join the mesh on either side of the surface.
    /// Cells which are partially intersected by the surface generate one
    /// new vertex on each intesected edge, which are connected to both sides
    /// of the crack.
    static std::shared_ptr<dolfin::Mesh>
    build(std::shared_ptr<const dolfin::Mesh> mesh,
          const GenericSurface& surface);

    /// Generate a mesh that conforms to the crack surfaces
    static std::shared_ptr<dolfin::Mesh> build(const DomainManager& domain);

    /// Generate a mesh of a surface (intended for
    /// visualisation). Based on individual surface intersections of
    /// the SubTriangulations
    static std::shared_ptr<dolfin::Mesh>
      build_surface(const dolfin::Mesh& mesh,
                    const GenericSurface& surface);

  };

}

#endif
