// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "NullSpace.h"
#include "DofMap.h"

#include <dolfin/function/FunctionSpace.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/la/VectorSpaceBasis.h>
#include <memory>

//-----------------------------------------------------------------------------
std::unique_ptr<dolfin::VectorSpaceBasis>
narwal::xamg::NullSpace3DDisplacement::build() const
{
  // Create null space
  std::shared_ptr<dolfin::GenericVector> null_space0(b->copy());
  null_space0->zero();
  std::shared_ptr<dolfin::GenericVector> null_space1(null_space0->copy());
  std::shared_ptr<dolfin::GenericVector> null_space2(null_space0->copy());
  std::shared_ptr<dolfin::GenericVector> null_space3(null_space0->copy());
  std::shared_ptr<dolfin::GenericVector> null_space4(null_space0->copy());
  std::shared_ptr<dolfin::GenericVector> null_space5(null_space0->copy());

  // Get subspaces - used to construct null space basis
  auto V0 = V->sub(0);
  auto V1 = V->sub(1);
  auto V2 = V->sub(2);

  // x0, x1, x2 translations
  V0->dofmap()->set(*null_space0, 1.0);
  V1->dofmap()->set(*null_space1, 1.0);
  V2->dofmap()->set(*null_space2, 1.0);

  // Rotations
  V0->set_x(*null_space3, -1.0, 1);
  V1->set_x(*null_space3,  1.0, 0);
  V0->set_x(*null_space4,  1.0, 2);
  V2->set_x(*null_space4, -1.0, 0);
  V2->set_x(*null_space5,  1.0, 1);
  V1->set_x(*null_space5, -1.0, 2);

  // Apply
  null_space0->apply("add");
  null_space1->apply("add");
  null_space2->apply("add");
  null_space3->apply("add");
  null_space4->apply("add");
  null_space5->apply("add");

  std::vector<std::shared_ptr<dolfin::GenericVector> > null_space_basis;
  null_space_basis.push_back(null_space0);
  null_space_basis.push_back(null_space1);
  null_space_basis.push_back(null_space2);
  null_space_basis.push_back(null_space3);
  null_space_basis.push_back(null_space4);
  null_space_basis.push_back(null_space5);

  auto null_space = std::unique_ptr<dolfin::VectorSpaceBasis>(
    new dolfin::VectorSpaceBasis(null_space_basis));

  return null_space;
}
//-----------------------------------------------------------------------------
std::unique_ptr<dolfin::VectorSpaceBasis>
narwal::xamg::NullSpace3DDisplacement2::build() const
{
  // Extract element
  dolfin_assert(V);
  auto element_master = V->element();
  dolfin_assert(element_master);
  dolfin_assert(element_master->num_sub_elements() == 3);
  auto element = element_master->create_sub_element(0);
  dolfin_assert(element);

  // Create null space
  std::shared_ptr<dolfin::GenericVector> null_space0(b->copy());
  null_space0->zero();
  std::shared_ptr<dolfin::GenericVector> null_space1(null_space0->copy());
  std::shared_ptr<dolfin::GenericVector> null_space2(null_space0->copy());
  std::shared_ptr<dolfin::GenericVector> null_space3(null_space0->copy());
  std::shared_ptr<dolfin::GenericVector> null_space4(null_space0->copy());
  std::shared_ptr<dolfin::GenericVector> null_space5(null_space0->copy());

  // x0, x1, x2 translations
  dofmap->set(*null_space0, 0, 1.0);
  dofmap->set(*null_space1, 1, 1.0);
  dofmap->set(*null_space2, 2, 1.0);

  // Rotations
  dofmap->set_x(*null_space3, 0, -1.0, 1, *mesh, *element);
  dofmap->set_x(*null_space3, 1,  1.0, 0, *mesh, *element);
  dofmap->set_x(*null_space4, 0,  1.0, 2, *mesh, *element);
  dofmap->set_x(*null_space4, 2, -1.0, 0, *mesh, *element);
  dofmap->set_x(*null_space5, 2,  1.0, 1, *mesh, *element);
  dofmap->set_x(*null_space5, 1, -1.0, 2, *mesh, *element);

  // Apply
  null_space0->apply("add");
  null_space1->apply("add");
  null_space2->apply("add");
  null_space3->apply("add");
  null_space4->apply("add");
  null_space5->apply("add");

  std::vector<std::shared_ptr<dolfin::GenericVector> > null_space_basis;
  null_space_basis.push_back(null_space0);
  null_space_basis.push_back(null_space1);
  null_space_basis.push_back(null_space2);
  null_space_basis.push_back(null_space3);
  null_space_basis.push_back(null_space4);
  null_space_basis.push_back(null_space5);

  auto null_space = std::unique_ptr<dolfin::VectorSpaceBasis>(
      new dolfin::VectorSpaceBasis(null_space_basis));

  return null_space;
}
//-----------------------------------------------------------------------------
std::unique_ptr<dolfin::VectorSpaceBasis>
narwal::xamg::NullSpace2DDisplacement::build() const
{

  // Create null space
  std::shared_ptr<dolfin::GenericVector> null_space0(b->copy());
  null_space0->zero();
  std::shared_ptr<dolfin::GenericVector> null_space1(null_space0->copy());
  std::shared_ptr<dolfin::GenericVector> null_space3(null_space0->copy());

  // Get subspaces - used to construct null space basis
  auto V0 = V->sub(0);
  auto V1 = V->sub(1);


  // x0, x1, x2 translations
  V0->dofmap()->set(*null_space0, 1.0);
  V1->dofmap()->set(*null_space1, 1.0);

  // Rotations
  V0->set_x(*null_space3, -1.0, 1);
  V1->set_x(*null_space3,  1.0, 0);

  // Apply
  null_space0->apply("add");
  null_space1->apply("add");
  null_space3->apply("add");

  std::vector<std::shared_ptr<dolfin::GenericVector> > null_space_basis;

  null_space_basis.push_back(null_space0);
  null_space_basis.push_back(null_space1);
  null_space_basis.push_back(null_space3);

  auto null_space = std::unique_ptr<dolfin::VectorSpaceBasis>(
      new dolfin::VectorSpaceBasis(null_space_basis));

  return null_space;
}
//-----------------------------------------------------------------------------
std::unique_ptr<dolfin::VectorSpaceBasis>
narwal::xamg::NullSpace3DScalarSolution::build() const
{

  // Create null space
  std::shared_ptr<dolfin::GenericVector> null_space0(b->copy());
  null_space0->zero();

  // constant field
  V->dofmap()->set(*null_space0, 1.0);

  // Apply
  null_space0->apply("add");

  std::vector<std::shared_ptr<dolfin::GenericVector> > null_space_basis;

  null_space_basis.push_back(null_space0);

  auto null_space = std::unique_ptr<dolfin::VectorSpaceBasis>(
      new dolfin::VectorSpaceBasis(null_space_basis));

  return null_space;
}
//-----------------------------------------------------------------------------
