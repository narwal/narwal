// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "XFEMFunctionSpace.h"

#include <ufc.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/la/GenericMatrix.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/la/TensorLayout.h>

#include "DofMap.h"
#include "DomainManager.h"
#include "FiniteElement.h"
#include "ImplicitSurface.h"


using namespace narwal;

//-----------------------------------------------------------------------------
XFEMFunctionSpace::XFEMFunctionSpace(
std::shared_ptr<const dolfin::FunctionSpace> Vs,
std::shared_ptr<const narwal::DomainManager> domain,
std::size_t max_element_dim)
  : _Vs(Vs), _domain_manager(domain)
{
  // Build XFEM dofmap
  dolfin_assert(Vs);
  dolfin_assert(domain);
  auto dofmap = std::make_shared<narwal::DofMap>(*Vs, *domain);

  // FIXME: remove integer argument
  // Create UFC element
  auto element =
      std::make_shared<narwal::FiniteElement>(Vs->element(), max_element_dim);

  // Create enriched function space
  _Ve.reset(new dolfin::FunctionSpace(Vs->mesh(), element, dofmap));
}
//-----------------------------------------------------------------------------
XFEMFunctionSpace::~XFEMFunctionSpace()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
std::shared_ptr<const dolfin::FunctionSpace>
XFEMFunctionSpace::function_space() const
{
  return _Ve;
}
//-----------------------------------------------------------------------------
std::shared_ptr<dolfin::FunctionSpace> XFEMFunctionSpace::function_space()
{
  return _Ve;
}
//-----------------------------------------------------------------------------
std::shared_ptr<const narwal::DomainManager> XFEMFunctionSpace::domain() const
{
  return _domain_manager;
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
std::shared_ptr<const narwal::DofMap> XFEMFunctionSpace::dofmap() const
{
  dolfin_assert(_Ve);
  std::shared_ptr<const narwal::DofMap> _dofmap
    = std::static_pointer_cast<const narwal::DofMap>(_Ve->dofmap());
  return _dofmap;
}
//-----------------------------------------------------------------------------
std::size_t XFEMFunctionSpace::dim() const
{
  dolfin_assert(_Ve);
  return _Ve->dim();
}
//-----------------------------------------------------------------------------
