// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

// Narwal interface
#ifndef NARWAL_H
#define NARWAL_H

#include <Assembler.h>
#include <ConformingMeshGenerator.h>
#include <DofKey.h>
#include <DofMap.h>
#include <DomainManager.h>
#include <ElasticityFunction.h>
#include <Evaluator.h>
#include <FiniteElement.h>
#include <GenericFunctionBuilder.h>
#include <GenericSurface.h>
#include <GeometryTools.h>
#include <ImplicitSurface.h>
#include <NullSpace.h>
#include <SolverXAMG.h>
#include <SubTriangulation.h>
#include <SurfaceRefinementND.h>
#include <SparsityTools.h>
#include <TestingTools.h>
#include <TimeMonitor.h>
#include <XAMGTools.h>
#include <XFEMFunctionSpace.h>
#include <XFEMProblem.h>
#include <XFEMTools.h>


#endif
