// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2013-09-14
// Last changed:

#ifndef NARWAL_DOF_MAP_H
#define NARWAL_DOF_MAP_H

#include <array>
#include <string>
#include <utility>
#include <vector>
#include <dolfin/common/ArrayView.h>
#include <dolfin/common/types.h>
#include <dolfin/fem/DofMap.h>
#include <dolfin/fem/GenericDofMap.h>

#include "DofKey.h"

namespace ufc
{
  class cell;
}

namespace dolfin
{
  class Cell;
  class FiniteElement;
  class FunctionSpace;
  class GenericVector;
  class Mesh;
  class Restriction;
  class SubDomain;
}

namespace narwal
{
  class DomainManager;

  /// This class implements a dofmap with XFEM-type enrichment

  class DofMap : public dolfin::GenericDofMap
  {
  public:

    // -- narwal::DofMap interface

    /// Constructor
    DofMap(const dolfin::FunctionSpace& V, const DomainManager& surface);

    // -- dolfin::GenericDofMap interface

    /// True if dof map is a view into another map (is a sub-dofmap)
    bool is_view() const
    { return false; }

    /// Return the dimension of the global finite element function
    /// space
    std::size_t global_dimension() const;

    /// Return number of owned (type="owned"), unowned
    /// (type="unowned"), or all (type="all") dofs in the map on this
    /// process
    std::size_t local_dimension(std::string type) const
    { return global_dimension(); }

    /// Return the dimension of the local finite element function
    /// space on a cell
    std::size_t num_element_dofs(std::size_t cell_index) const
    {
      dolfin_assert(cell_index < _dofmap.size());
      return  _dofmap[cell_index].size();
    }

    /// Return the maximum dimension of the local finite element
    /// function space
    std::size_t max_element_dofs() const
    { return _max_cell_dimension; }

    /// Return the number of dofs for a given entity dimension
    std::size_t num_entity_dofs(std::size_t dim) const
    {
      dolfin::error("Not implemented (num_entity_dofs)");
      return 0;
    }

    /// Return number of facet dofs
    std::size_t num_facet_dofs() const
    {
      dolfin::error("Not implemented (num_facet_dofs");
      return 0;
    }

    /// Return the ownership range (dofs in this range are owned by
    /// this process)
    std::pair<std::size_t, std::size_t> ownership_range() const
    { return _ownership_range; }

    /// Return map from nonlocal-dofs (that appear in local dof map)
    /// to owning process
    const std::vector<int>& off_process_owner() const
    { return _vec_int; }

    /// Local-to-global mapping of dofs on a cell
    dolfin::ArrayView<const dolfin::la_index>
      cell_dofs(std::size_t cell_index) const
    {
      dolfin_assert(cell_index < _dofmap.size());
      return dolfin::ArrayView<const dolfin::la_index>(_dofmap[cell_index]);
    }

    /// Tabulate local-local facet dofs
    void tabulate_facet_dofs(std::vector<std::size_t>& dofs,
                                     std::size_t local_facet) const
    { dolfin::error("Not implemented 3"); }

    /// Tabulate the local-to-local mapping of dofs on entity
    /// (dim, local_entity)
    void tabulate_entity_dofs(std::vector<std::size_t>& dofs,
                              std::size_t dim,
                              std::size_t local_entity) const
    {  dolfin::error("Not implemented 4"); }

    /// Return a map between vertices and dofs
    std::vector<dolfin::la_index>
      dof_to_vertex_map(const dolfin::Mesh& mesh) const
    {
      dolfin::error("Not implemented 5");
      return std::vector<dolfin::la_index>();
    }

    /// Return a map between vertices and dofs
    std::vector<std::size_t> vertex_to_dof_map(const dolfin::Mesh& mesh) const
    {
      dolfin::error("Not implemented 6");
      return std::vector<std::size_t>();
    }

    /// Create a copy of the dof map
    std::shared_ptr<dolfin::GenericDofMap> copy() const
    {
      dolfin::error("Not implemented 9");
      return  std::shared_ptr<dolfin::GenericDofMap>();
    }

    /// Create a new dof map on new mesh
    std::shared_ptr<dolfin::GenericDofMap>
      create(const dolfin::Mesh& new_mesh) const
    {
      dolfin::error("Not implemented 10");
      return  std::shared_ptr<dolfin::GenericDofMap>();
    }

    /// Extract sub dofmap component
    std::shared_ptr<dolfin::GenericDofMap>
      extract_sub_dofmap(const std::vector<std::size_t>& component,
                         const dolfin::Mesh& mesh) const
    {
      dolfin::error("Not implemented 11");
      return  std::shared_ptr<dolfin::GenericDofMap>();
    }

    /// Create a "collapsed" a dofmap (collapses from a sub-dofmap
    /// view)
    std::shared_ptr<dolfin::GenericDofMap>
      collapse(std::unordered_map<std::size_t, std::size_t>& collapsed_map,
               const dolfin::Mesh& mesh) const
    {
      dolfin::error("Not implemented 12");
      return  std::shared_ptr<dolfin::GenericDofMap>();
    }

    /// Return list of dof indices on this process that belong to mesh
    /// entities of dimension dim
    std::vector<dolfin::la_index> dofs(const dolfin::Mesh& mesh,
                                       std::size_t dim) const
    {
      dolfin::error("Not implemented (DofMap::dofs(mesh, dim))");
      return  std::vector<dolfin::la_index>();
    }


    /// Return list of global dof indices on this process
    std::vector<dolfin::la_index> dofs() const;

    /// Set dof entries in vector to a specified value. Parallel
    /// layout of vector must be consistent with dof map range.
    void set(dolfin::GenericVector& x, double value) const;

    /// Set dof entries in vector to a specified value
    void set(dolfin::GenericVector& x, std::size_t dim, double value) const;

    /// Set dof entries in vector to the value*x[i], where x[i] is the
    /// spatial coordinate of the dof. Parallel layout of vector must
    /// be consistent with dof map range.
    void set_x(dolfin::GenericVector& x, double value,
               std::size_t component, const dolfin::Mesh& mesh,
               const dolfin::FiniteElement& element) const;

    /// Set dof entries in vector to the value*x[i], where x[i] is the
    /// spatial coordinate of the dof. Parallel layout of vector must
    /// be consistent with dof map range.
    void set_x(dolfin::GenericVector& x, std::size_t dim, double value,
               std::size_t x_component, const dolfin::Mesh& mesh,
               const dolfin::FiniteElement& element) const;

    /// Return map from shared dofs to the processes (not including
    /// the current process) that share it.
    const std::unordered_map<std::size_t,
                             std::vector<unsigned int> >& shared_dofs() const
    {
      dolfin::error("Not implemented 14");
      return _map_vector;
    }

    /// Return set of processes that share dofs with the this process
    const std::set<int>& neighbours() const
    {
      dolfin::error("Not implemented 15");
      return _set_int;
    }

    /// Return informal string representation (pretty-print)
    std::string str(bool verbose) const
    { dolfin::error("Not implemented 16"); return "-";}

    void clear_sub_map_data()
    { dolfin::error("narwal::DofMap::clear_sub_map_data not implemented."); }

    void tabulate_local_to_global_dofs(std::vector<std::size_t>&
                                       local_to_global_map) const
    {
      local_to_global_map.resize(ownership_range().second);
      for (std::size_t i = 0; i < local_to_global_map.size(); ++i)
        local_to_global_map[i] = i;
    }

    /// Return global dof index corresponding to a given local index
    std::size_t local_to_global_index(int local_index) const
    { return local_index; }

    const std::unordered_map<int, std::vector<int> >& shared_nodes() const
    {
      dolfin::error("narwal::DofMap::shared_nodes not implemented");
      return _map_int_vecint;
    }

    const std::vector<std::size_t>& local_to_global_unowned() const
    { return _vec_size_t; }

    // --- narwal::DofMap interface

    /// Enriched dof data at a regular dof. For given cells and
    /// regular dof index, returns list [enriched dof index,
    /// (surface0, surface1)]
    const std::vector<std::pair<int, EnrLabelArray>>&
      enriched_dofs(std::size_t cell_index, std::size_t reg_dof_index) const
    {
      dolfin_assert(cell_index < _cell_enriched_dof_indices.size());
      dolfin_assert(reg_dof_index
                    < _cell_enriched_dof_indices[cell_index].size());
      return _cell_enriched_dof_indices[cell_index][reg_dof_index];
    }

    /// Return true, if cell has enriched DOF
    bool is_enriched(std::size_t cell_index) const
    {
      dolfin_assert(cell_index < _cell_enriched_dof_indices.size());
      return !_cell_enriched_dof_indices[cell_index].empty();
    }


    /// Get block size
    int block_size() const
    { return 1; }

    /// Index map (const access)
    std::shared_ptr<const dolfin::IndexMap> index_map() const
    { return _index_map; }

  private:

    // Object containing information about dof distribution across
    // processes
    std::shared_ptr<dolfin::IndexMap> _index_map;

    // FIXME: document data layout
    // Mark regular dofs that have an associated enrichment for a
    // given cell. Empty for cells that are not enriched.
    std::vector<std::vector<
      std::vector<std::pair<int, EnrLabelArray>>>>
      _cell_enriched_dof_indices;

    // FIXME: Keeping a pointer to original dofmap because we can't
    //        access the underlying UFC dofmap
    // DOLFIN dof map
    std::shared_ptr<const dolfin::GenericDofMap> _dolfin_dofmap;

    // FIXME: this should be removed
    // The Mesh
    std::shared_ptr<const dolfin::Mesh> _mesh;

    // Local-to-global dof map (dofs for cell dofmap[i])
    std::vector<std::vector<dolfin::la_index> > _dofmap;

    std::size_t _max_cell_dimension;

    std::pair<std::size_t, std::size_t> _ownership_range;

    // Temp data structures used during development
    std::unordered_map<std::size_t, std::vector<unsigned int> > _map_vector;
    std::unordered_map<std::size_t, unsigned int> _map_uint;
    std::unordered_map<int, std::vector<int> > _map_int_vecint;
    std::vector<dolfin::la_index> _vec_index;
    std::vector<int> _vec_int;
    std::vector<std::size_t> _vec_size_t;
    std::set<int> _set_int;

  };

}

#endif
