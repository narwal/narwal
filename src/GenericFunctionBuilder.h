// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_GENERIC_FUNCTION_BUILDER_H
#define NARWAL_GENERIC_FUNCTION_BUILDER_H

#include <memory>

#include <dolfin/function/GenericFunction.h>
#include <dolfin/function/FunctionSpace.h>


namespace dolfin
{
//  class GenericFunction;
  class Mesh;
  class Function;
//  class FunctionSpace;
}

namespace narwal
{
  class ImplicitSurface;

  /// FIXME: briefly describe class

  class GenericFunctionFactory
  {
  public:
    GenericFunctionFactory() {}
    /// Destructor
    virtual ~GenericFunctionFactory() {}

    /// Tabulate tensor
    virtual std::shared_ptr<dolfin::GenericFunction>
      build(const dolfin::Mesh& mesh, const dolfin::Function& u,
          const narwal::ImplicitSurface& surface) const = 0;

  };

  /// FIXME: briefly describe class

  class GenericDolfinFunctionSpaceFactory
  {
  public:
    GenericDolfinFunctionSpaceFactory() {}
    /// Destructor
    virtual ~GenericDolfinFunctionSpaceFactory() {}

    /// Tabulate tensor
    virtual std::shared_ptr<dolfin::FunctionSpace>
      build(const dolfin::Mesh& mesh) const = 0;

  };

}


#endif
