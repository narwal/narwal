// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "ElasticityFunction.h"

#include <dolfin/common/ArrayView.h>
#include <dolfin/function/Function.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/geometry/BoundingBoxTree.h>
#include <dolfin/geometry/Point.h>
#include <dolfin/mesh/Cell.h>
#include <dolfin/mesh/Mesh.h>
#include <dolfin/mesh/Vertex.h>

#include "DofMap.h"
#include "DomainManager.h"
#include "Elasticity3D_P1.h"
#include "Elasticity3D_P2.h"
#include "ImplicitSurface.h"
#include "SubTriangulation.h"
#include "XFEMForms.h"
#include "XFEMFunctionSpace.h"
#include "XFEMTools.h"

using namespace narwal;

//-----------------------------------------------------------------------------
ElasticFunction::ElasticFunction(const dolfin::Mesh& mesh,
                                 const dolfin::Function& u,
                                 const DomainManager& domain,
                                 const int polynomialDegree)
  : _conforming_mesh(mesh), _u_enriched(u), _domain(domain),
    _polynomialOrder(polynomialDegree), _useEvaluator(false)
{


  // Do nothing
}
//-----------------------------------------------------------------------------
ElasticFunction::ElasticFunction(
  const std::shared_ptr<const dolfin::Mesh> conforming_mesh,
  const std::shared_ptr<const dolfin::Function> u_enriched,
  const std::shared_ptr<const narwal::DomainManager> domain,
  const std::shared_ptr<const narwal::XFEMForms> forms)
  : _conforming_mesh(*conforming_mesh), _u_enriched(*u_enriched), _domain(*domain),
    _polynomialOrder(forms->polynomial_order()),
    _evaluator(std::make_shared<narwal::Evaluator>(
        u_enriched, forms->function_space()->dofmap(), domain, forms)),
    _useEvaluator(true)
{
  // Do nothing
}
//-----------------------------------------------------------------------------
ElasticFunction::~ElasticFunction()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
std::size_t ElasticFunction::value_rank() const
{
  return 1;
}
//-----------------------------------------------------------------------------
std::size_t ElasticFunction::value_dimension(std::size_t i) const
{
  return 3;
}
//-----------------------------------------------------------------------------
void ElasticFunction::eval(dolfin::Array<double>& values,
                           const dolfin::Array<double>& x,
                           const ufc::cell& ufc_cell) const
{
  // Get cell of 'cut' mesh (mesh conforms to crack surface)
  const dolfin::Cell cell_cut(_conforming_mesh, ufc_cell.index);

  const dolfin::Point centroid = cell_cut.midpoint();

  if (_useEvaluator) {
    // evaluate heaviside at centroid of subgrid cell
    dolfin::Array<double> x_heaviside(3);
    x_heaviside[0] = centroid.x();
    x_heaviside[1] = centroid.y();
    x_heaviside[2] = centroid.z();
    _evaluator->eval(values, x, x_heaviside);
  } else {

  // Get parent (enriched/xfem) mesh
  const dolfin::Mesh& mesh_enriched = *_u_enriched.function_space()->mesh();

  // Geometric dimension
  const std::size_t gdim = _conforming_mesh.geometry().dim();

  // Find index of parent cell in XFEM mesh (cell that contains x)
  const dolfin::Point x_point(gdim, x.data());

  const dolfin::Point x_point0
    = x_point + 0.02*cell_cut.diameter()*(centroid - x_point);

  const unsigned int id = mesh_enriched.bounding_box_tree()->compute_first_entity_collision(x_point0);

  // Get parent cell on XFEM mesh
  const dolfin::Cell cell_enriched(mesh_enriched, id);

  // Get dof map on enriched (XFEM) cell
  dolfin_assert(_u_enriched.function_space()->dofmap());
  std::shared_ptr<const narwal::DofMap> dofmap_enriched
    = std::static_pointer_cast<const narwal::DofMap>(_u_enriched.function_space()->dofmap());

  // Get dof map for cell
  dolfin_assert(dofmap_enriched);
  dolfin::ArrayView<const dolfin::la_index> dofs_enriched
    = dofmap_enriched->cell_dofs(cell_enriched.index());

  // Get number of element 'nodes'
  const std::size_t num_nodes = XFEMForms::getNumElemNodes(_polynomialOrder);

  // Get number of regular dofs (regular element dim)
  const std::size_t element_dim_regular = num_nodes*gdim;

  // Handle case of enriche cell
  if (dofs_enriched.size() == element_dim_regular)
  {
    // FIXME: Pass cell to speed-up eval, see
    // https://bitbucket.org/narwal/narwal/issue/18

    // Use standard eval
    _u_enriched.eval(values, x);
  }
  else
  {
    // FIXME: remove surface reference (work through domain manager)
    // Check if XFEM cell is intersected by surface
    const int surface_index = _domain.intersecting_surface(cell_enriched);
    dolfin_assert(surface_index != -1);

    // Determine jump function (need to iterate over all vertices to
    // handle case of conforming mesh and curved cracks)
    double f0_sum = 0.0;
    for(dolfin::VertexIterator v(cell_cut); !v.end(); ++v)
      f0_sum += _domain.surface_side(v->point(), surface_index);
    const double H_point = f0_sum > 0.0 ? 1.0 : -1.0;

    // Get dofs from solution vector (of XFEM problem)
    std::vector<double> w(dofs_enriched.size());
    _u_enriched.vector()->get_local(w.data(), dofs_enriched.size(),
                                    dofs_enriched.data());

    // Get cell vertex coordinates (re-allocated inside function for
    // thread safety)
    std::vector<double> vertex_coordinates;
    cell_enriched.get_vertex_coordinates(vertex_coordinates);

    // Convenience reference to FiniteElement for XFEM problem
    dolfin_assert(_u_enriched.function_space()->element());
    const dolfin::FiniteElement& element
      = *(_u_enriched.function_space()->element());

    // Get dof coordinates for all regular dofs on XFEM element
    boost::multi_array<double, 2> enriched_dof_coordinates;
    element.tabulate_dof_coordinates(enriched_dof_coordinates,
                                     vertex_coordinates,
                                     cell_enriched);

    // Zero function values
    std::fill(values.data(), values.data() + gdim, 0.0);

    // Get basis from finite element (on enriched cell) and
    // evaluate (compute linear combination)
    std::vector<double> basis(gdim);
    for (std::size_t i = 0; i < element_dim_regular; ++i)
    {
      // Get local index of enriched dof corresponding to regular dof.
      //dolfin_assert(dofmap_enriched->_cell_enriched_dof_indices.size()
      //              > cell_enriched.index());
      //const std::vector<int>& reg_to_enriched_dof
      //  = dofmap_enriched->_cell_enriched_dof_indices[cell_enriched.index()];
      //const std::vector<std::vector<std::pair<int,EnrLabelArray>>>&
      //  reg_to_enriched_dof
      //  = dofmap_enriched->_cell_enriched_dof_indices[cell_enriched.index()];
      //dolfin_assert(reg_to_enriched_dof.size() == element_dim_regular);
      //const int enriched_dof_index = reg_to_enriched_dof[i];

      // Evaluate FE basis at point
      element.evaluate_basis(i, basis.data(), x.data(),
                             vertex_coordinates.data(),
                             ufc_cell.orientation);

      // Standard dof contribution
      for (std::size_t j = 0; j < gdim; ++j)
        values[j] += w[i]*basis[j];

      // Add contribution of enriched dof, if present
      const std::vector<std::pair<int, EnrLabelArray>>&
        enriched_dofs
        = dofmap_enriched->enriched_dofs(cell_enriched.index(), i);
      if (!enriched_dofs.empty())
      {
        // FIXME:
        dolfin_assert(enriched_dofs.size() == 1);
        const int enriched_dof_index = enriched_dofs.begin()->first;

        // Spatial point of the enriched cell dof
        const dolfin::Point ni_point(enriched_dof_coordinates[i][0],
                                     enriched_dof_coordinates[i][1],
                                     enriched_dof_coordinates[i][2]);

        // Heaviside function at dof node (i)
        const double H_node = _domain.surface_side(ni_point, surface_index);

        // Jump function for node at quadrature point
        const double H = 0.5*std::abs(H_node - H_point);

        // Enriched dof contribution
        for (std::size_t j = 0; j < 3; ++j)
          values[j] += w[enriched_dof_index]*basis[j]*H;
      }
    }
  }
  }
}
//-----------------------------------------------------------------------------
void ElasticFunction::eval(dolfin::Array<double>& values,
                           const dolfin::Array<double>& x) const
{
  dolfin::error("eval not supported");
}
//-----------------------------------------------------------------------------
void ElasticFunction::restrict(double* w,
                               const dolfin::FiniteElement& element,
                               const dolfin::Cell& dolfin_cell,
                               const double* vertex_coordinates,
                               const ufc::cell& ufc_cell) const
{
  restrict_as_ufc_function(w, element, dolfin_cell, vertex_coordinates,
                           ufc_cell);
}
//-----------------------------------------------------------------------------
void ElasticFunction::compute_vertex_values(std::vector<double>& vertex_values,
                                            const dolfin::Mesh& mesh) const
{
  dolfin::error("Compute vertex values not implemented");
}
//-----------------------------------------------------------------------------
