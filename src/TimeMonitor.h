// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_TIME_MONITOR_H
#define NARWAL_TIME_MONITOR_H

#include <string>
#include <vector>
#include <map>
#include <memory>
#include <boost/timer/timer.hpp>

namespace narwal
{
  class Time
  {
  public:

    explicit Time(const std::string& name);
    ~Time();
    Time(const Time& t_other);

  private:

    void stopAndRegisterTime();
    const std::string _name;
    boost::timer::cpu_timer _timer;
  };

  // Singleton class for collecting times does not work in parallel,
  // yet
  class TimeMonitor
  {
  public:
    // only access to the TimeMonitor object
    static TimeMonitor& instance();
    ~TimeMonitor();

    std::shared_ptr<narwal::Time> createTimer(const std::string& name);
    // write all collected times to file and screen
    void summarize(const std::string &filename);
    // start collecting times, thereby reseting all previous timings
    void start();

  private:

    // friend Time can add times to TimeMonitor, but no one else
    friend class Time;
    void addTime(const std::string &name, const boost::timer::cpu_times& t);

    // hide constructors, only above instance call is allowed
    TimeMonitor();
    TimeMonitor(const TimeMonitor&);
    TimeMonitor & operator =(const TimeMonitor &);

    bool running;

    // reset all collected times and start all over
    void reset();

    typedef std::map<std::string, boost::timer::cpu_times> TimeMap_Type;
    typedef std::map<std::string, std::size_t> CountMap_Type;
    TimeMap_Type times;
    CountMap_Type counts;

    //std::vector<std::string> counternames;

    void plotResultsToStream(
      const std::size_t namecol,
      const std::size_t timecol1,
      const std::size_t timecol2,
      const std::size_t timecol3,
      const std::size_t countcol,
      std::ostream& os) const;
  };
}

#endif
