// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_XFEMPROBLEM_H
#define NARWAL_XFEMPROBLEM_H

#include <memory>
#include <vector>

namespace dolfin
{
  //class Cell;
  class DirichletBC;
  //class Form;
  class GenericMatrix;
  class GenericVector;
}

namespace ufc
{
  //class cell;
  //class cell_integral;
}

namespace narwal
{
  class Form;
  class XFEMForms;

  /// Variational problem with additional XFEM dofs

  class XFEMProblem
  {

  public:

    XFEMProblem(std::shared_ptr<const narwal::XFEMForms> forms,
                std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs);

    /// Destructor
    virtual ~XFEMProblem();

    /// Assemble lhs matrix and rhs vector
    void assemble(dolfin::GenericMatrix& A, dolfin::GenericVector& b) const;

  private:

    // The XFEM forms
    std::shared_ptr<const narwal::XFEMForms> _forms;

    // Dirichlet boundary conditions
    std::vector<std::shared_ptr<const dolfin::DirichletBC>> _bcs;

  };

}

#endif
