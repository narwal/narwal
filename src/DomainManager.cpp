// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2014-07-03
// Last changed:

#include "DomainManager.h"

#include <memory>
#include <dolfin/geometry/Point.h>
#include <dolfin/mesh/Cell.h>
#include <dolfin/mesh/Mesh.h>
#include <dolfin/mesh/MeshEntity.h>
#include <dolfin/mesh/Edge.h>

#include "GenericSurface.h"
#include "ImplicitSurface.h"
#include "XFEMTools.h"

using namespace narwal;

//-----------------------------------------------------------------------------
DomainManager::DomainManager(std::shared_ptr<const dolfin::Mesh> mesh)
  : _mesh(mesh)
{
  // Do nothing
}
//-----------------------------------------------------------------------------
narwal::DomainManager::~DomainManager()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
void DomainManager::register_surface
(std::shared_ptr<const GenericSurface> surface, std::string name)
{
  // FIXME: check for double registration of surfaces (compare memory
  // addresses)

  // Register surface
  _surfaces.push_back(std::make_pair(name, surface));
}
//-----------------------------------------------------------------------------
std::shared_ptr<const dolfin::Mesh> DomainManager::mesh() const
{
  return _mesh;
}
//-----------------------------------------------------------------------------
std::size_t DomainManager::num_surfaces() const
{
  return _surfaces.size();
}
//-----------------------------------------------------------------------------
int DomainManager::intersecting_surface(const dolfin::MeshEntity& e) const
{
  for (auto s = _surfaces.begin(); s != _surfaces.end(); ++s)
  {
    const std::pair<bool, bool> intersect = s->second->entity_intersection(e);
    if (intersect.first)
      return std::distance(_surfaces.begin(), s);
  }

  return -1;
}
//-----------------------------------------------------------------------------
std::pair<bool, bool>
DomainManager::entity_intersection(const dolfin::MeshEntity& e,
                                   std::size_t surface_index) const
{
  dolfin_assert(surface_index < _surfaces.size());
  dolfin_assert(_surfaces[surface_index].second);
  return _surfaces[surface_index].second->entity_intersection(e);
}
//-----------------------------------------------------------------------------
narwal::SubTriangulation
DomainManager::triangulate(const dolfin::Cell& c,
                           std::size_t surface_index) const
{
  dolfin_assert(surface_index < _surfaces.size());
  dolfin_assert(_surfaces[surface_index].second);
  return SubTriangulation(c, *_surfaces[surface_index].second);
}
//-----------------------------------------------------------------------------
int DomainManager::surface_side(const dolfin::Point& p,
                                SurfaceId surface_index,
                                double eps) const
{
  dolfin_assert((std::size_t) surface_index < _surfaces.size());
  dolfin_assert(_surfaces[surface_index].second);
  return _surfaces[surface_index].second->side(p, eps);
}
//-----------------------------------------------------------------------------
