// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_SUBTRIANGULATION_H
#define NARWAL_SUBTRIANGULATION_H

#include <boost/multi_array.hpp>

#include <dolfin/mesh/Mesh.h>

namespace dolfin
{
  class Cell;
}

namespace narwal
{

  class GenericSurface;

  /// Implements the sub-division of a Cell or Mesh by a surface

  class SubTriangulation
  {
  public:

    /// Build a SubTriangulation on a Cell, as it is intersected
    /// by the given surface
    SubTriangulation(const dolfin::Cell& cell,
                     const narwal::GenericSurface& surface);

    /// Build a SubTriangulation on a Cell, as it is intersected
    /// by a set of surfaces
    SubTriangulation(const dolfin::Cell& cell,
                     const std::vector<
                     std::shared_ptr<const narwal::GenericSurface>>& surfaces);

    /// Build a SubTriangulation on an entire Mesh, based on
    /// the intersection with the given surface
    SubTriangulation(const dolfin::Mesh& mesh,
                     const narwal::GenericSurface& surface);

    /// Destructor
    ~SubTriangulation() {}

    // FIXME: Rename function to make more meaningful
    /// Small Mesh which is formed by cutting the divided cell into
    /// tetrahedral subcells
    const dolfin::Mesh& mesh() const
    { return _submesh; }

    /// Two-dimensional surface of intersection, consisting of either
    /// one or two triangles
    std::shared_ptr<const dolfin::Mesh> surface_mesh() const
    { return _surfmesh.back(); }

    /// Number of surfaces intersecting mesh
    std::size_t num_surface_meshes() const
    { return _surfmesh.size(); }

    /// Two-dimensional surface of intersection, of surface i
    /// with the original mesh
    std::shared_ptr<const dolfin::Mesh> surface_mesh(std::size_t i) const
    {
      dolfin_assert(i < _surfmesh.size());
      return _surfmesh[i];
    }

    /// Determine if a given vertex index lies on surface 'i'
    bool vertex_on_surface(std::size_t vertex_index,
                           std::size_t surface_index);

  private:

    // Volume mesh including all intersection points with surfaces
    dolfin::Mesh _submesh;

    // Boundary meshes between +/- side of each cut surface
    std::vector<std::shared_ptr<dolfin::Mesh>> _surfmesh;

    // Indicates ranges of vertices assigned to each surface
    // Original vertices have indices in 0:_ranges[0]
    // vertices associated with the first cut _ranges[0]:_ranges[1]
    std::vector<std::size_t> _ranges;

    // Mapping for vertices which are in multiple (max 2) surfaces
    // This indicates which other surface outside the range (see above)
    // this vertex is part of.
    std::map<std::size_t, std::size_t> _vertex_to_surface;

    // Rebuild subtriangulation with new surface
    void build(const narwal::GenericSurface& surface);

    // Pull out surface mesh 'i' from submesh, and store in
    // _surfmesh[i]
    void extract_surface_mesh(std::size_t surface_index);

    // Generate volume or surface mesh
    void create_mesh(dolfin::Mesh& mesh,
                     const boost::multi_array<double, 2>& geometry,
                     const std::vector<std::size_t>& topology,
                     std::size_t tdim);

  };

}


#endif
