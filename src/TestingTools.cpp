// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "TestingTools.h"

#include <map>
#include <set>
#include <vector>
#include <boost/multi_array.hpp>

#include <dolfin/fem/GenericDofMap.h>
#include <dolfin/function/Function.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/geometry/Point.h>
#include <dolfin/la/GenericMatrix.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/la/PETScMatrix.h>
#include <dolfin/la/PETScVector.h>
#include <dolfin/mesh/Mesh.h>

#include "GenericSurface.h"

using namespace narwal;

//-----------------------------------------------------------------------------
void TestingTools::test_enriched_dofs(const dolfin::Function& u,
                                      const GenericSurface& surface)
{
  // Get mesh
  dolfin_assert(u.function_space()->mesh());
  const dolfin::Mesh& mesh = *(u.function_space()->mesh());


  // Get dofmap
  dolfin_assert(u.function_space()->dofmap());
  const dolfin::GenericDofMap& dofmap = *(u.function_space()->dofmap());
  std::shared_ptr<const dolfin::FiniteElement> element
    = u.function_space()->element();

  std::map<std::size_t, dolfin::Point> dof_data;

  // Loop over cells
  std::vector<double> vertex_coordinates;
  boost::multi_array<double, 2> coordinates;
  for (dolfin::CellIterator cell(mesh); !cell.end(); ++cell)
  {
    // Check if cell is intersected
    std::pair<bool, bool> intersected
      = surface.entity_intersection(*cell);

    if (intersected.first)
    {
      // Check if we have a cell that contains surface boundary
      //const bool edge_cell = intersected.second;

      // Get vertex coordinates
      cell->get_vertex_coordinates(vertex_coordinates);

      // Cell index (local to process)
      const std::size_t cell_index = cell->index();

      // Tabulate coordinates of all (regular) dofs on this cell
      element->tabulate_dof_coordinates(coordinates, vertex_coordinates, *cell);

      if ( dofmap.cell_dofs(cell_index).size() == 24)
      {
        // Loop over enriched degrees of freedom
        const std::size_t num_regular_dofs = 12;
        for (std::size_t edof = num_regular_dofs;
             edof < num_regular_dofs + 4; ++edof)
        {
          // Enriched dof index
          const std::size_t dof_index = dofmap.cell_dofs(cell_index)[edof];

          // Construct DOLFIN Point for dof
          const dolfin::Point p(coordinates.shape()[1],
                                &(coordinates[edof - num_regular_dofs][0]));

          // Check surface side
          //const int side = surface.side(p);

          dof_data.insert(std::make_pair(dof_index, p));
        }
      }
    }
  }

  std::vector<dolfin::la_index> dof_index;
  std::vector<dolfin::Point> dof_point;
  for (auto it = dof_data.begin(); it != dof_data.end(); ++it)
  {
    //std::cout << "Dof index, coord: " << it->first << ", " << it->second
    //          << std::endl;
    dof_index.push_back(it->first);
    dof_point.push_back(it->second);
  }

  // Get dof values from vector
  std::vector<double> dof_values(dof_index.size());
  dolfin_assert(u.vector());
  u.vector()->get_local(dof_values.data(), dof_values.size(), dof_index.data());
}
//-----------------------------------------------------------------------------
void TestingTools::extract_matrix_diagonal(dolfin::GenericVector& x,
                                           const dolfin::GenericMatrix& A)
{
  // Downcast to PETSc objects
  dolfin::PETScVector& _x = x.down_cast<dolfin::PETScVector>();
  const dolfin::PETScMatrix& _A = A.down_cast<const dolfin::PETScMatrix>();

  // Initialise vector
  _A.init_vector(_x, 1);

  // Get diagonal
  MatGetDiagonal(_A.mat(), _x.vec());
}
//-----------------------------------------------------------------------------
void TestingTools::check_dof_count(const dolfin::GenericDofMap& dofmap)
{
  const std::vector<dolfin::la_index> dof_list = dofmap.dofs();
  const std::set<dolfin::la_index> dof_set(dof_list.begin(), dof_list.end());

  std::cout << "Number of dofs in vector: " << dof_list.size() << std::endl;
  std::cout << "Number of dofs in set:    " << dof_set.size() << std::endl;
  std::cout << "Max dof index in set:     " << *dof_set.rbegin() << std::endl;
}
//-----------------------------------------------------------------------------
