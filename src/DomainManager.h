// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2014-07-03
// Last changed:

#ifndef NARWAL_DOMAINMANAGER_H
#define NARWAL_DOMAINMANAGER_H

#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "DofKey.h"
#include "SubTriangulation.h"

namespace narwal
{
  class GenericSurface;
}

namespace dolfin
{
  class Cell;
  class GenericSurface;
  class Mesh;
  class MeshEntity;
  class Point;
}

namespace narwal
{

  /// A container for surfaces, which also manages their interactions with the mesh

  class DomainManager
  {
  public:

    /// Constructor
    DomainManager(const std::shared_ptr<const dolfin::Mesh> mesh);

    /// Destructor
    ~DomainManager();

    /// Register surface (with name)
    void register_surface(std::shared_ptr<const GenericSurface> surface,
                          std::string name);

    /// Return mesh
    std::shared_ptr<const dolfin::Mesh> mesh() const;

    /// Number of surface
    std::size_t num_surfaces() const;

    /// Return index of the intersecting surface (picks first
    /// surface). Returns -1 if entity is not intersected
    int intersecting_surface(const dolfin::MeshEntity& e) const;

    /// Returns a pair denoting the type of entity intersection with
    /// the surface
    /// <inside, outside>
    /// <true, false> - intersects fully inside surface
    /// <true, true> - intersects at boundary of surface
    /// <false, true> - intersects f0, but not f1.
    /// <false, false> - no intersection
    std::pair<bool, bool>
      entity_intersection(const dolfin::MeshEntity& e,
                          std::size_t surface_index) const;

    /// Sub-triangulate cell with a particular surface
    SubTriangulation triangulate(const dolfin::Cell& c,
                                 std::size_t surface_index) const;

    /// Determine side of surface (+1/-1) above/below (zero on
    /// surface)
    int surface_side(const dolfin::Point& p, SurfaceId surface_index,
                     double eps=1.0e-7) const;

    /// Return ith surface. This function should not generally be used.
    std::shared_ptr<const narwal::GenericSurface> surface(std::size_t i) const
    {
      dolfin_assert(i < _surfaces.size());
      return _surfaces[i].second;
    }

    /// Return surfaces
    std::vector<std::shared_ptr<const narwal::GenericSurface>> surfaces() const
    {
      std::vector<std::shared_ptr<const narwal::GenericSurface>> s;
      for (auto surface : _surfaces)
        s.push_back(surface.second);
      return s;
    }

  private:

    // Collection of surfaces
    std::vector<
      std::pair<std::string,
      std::shared_ptr<const narwal::GenericSurface>>> _surfaces;

    // The mesh
    std::shared_ptr<const dolfin::Mesh> _mesh;

  };
}

#endif
