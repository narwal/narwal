// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "XFEMTools.h"

#include <map>
#include <utility>
#include <vector>
#include <boost/multi_array.hpp>

#include <dolfin/fem/GenericDofMap.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/log/log.h>
#include <dolfin/io/File.h>
#include <dolfin/mesh/Cell.h>
#include <dolfin/mesh/Facet.h>
#include <dolfin/mesh/Mesh.h>
#include <dolfin/mesh/MeshEditor.h>
#include <dolfin/mesh/Edge.h>
#include <dolfin/mesh/Vertex.h>

#include "DomainManager.h"
#include "GenericSurface.h"
#include "GeometryTools.h"
#include "ImplicitSurface.h"
#include "SubTriangulation.h"
#include "SurfaceRefinementND.h"

using namespace narwal;

//-----------------------------------------------------------------------------
void XFEMTools::compute_enriched_dof_markers(
  std::vector<std::set<EnrLabelArray>>& enriched_dofs,
  std::vector<std::set<EnrLabelArray>>& enriched_cells,
  const dolfin::FunctionSpace& V,
  const DomainManager& domain)
{
  // Get Mesh
  dolfin_assert(V.mesh());
  auto mesh = V.mesh();

  // Initialise edges
  mesh->init(1);

  // Get dofmap
  dolfin_assert(V.dofmap());
  const dolfin::GenericDofMap& dofmap = *(V.dofmap());
  std::shared_ptr<const dolfin::FiniteElement> element = V.element();

  // Get surfaces
  const auto surfaces = domain.surfaces();

  // Get list of 'tip' dofs
  const std::vector<std::set<int>> tip_dofs = compute_tip_dofs(mesh, domain, dofmap);

  // Initialise markers for each regular dof
  enriched_dofs.clear();
  enriched_dofs.resize(dofmap.global_dimension());

  // Initialise cell markers
  enriched_cells.clear();
  enriched_cells.resize(mesh->num_cells());

  // Loop over cells
  std::vector<double> vertex_coordinates;
  std::vector<std::pair<bool, bool>> intersections(surfaces.size());
  for (dolfin::CellIterator cell(*mesh); !cell.end(); ++cell)
  {
    // Compute intersections
    std::size_t counter = 0;
    bool cell_intersected = false;
    for (auto surface : surfaces)
    {
      intersections[counter] = surface->entity_intersection(*cell);
      if (!cell_intersected)
      {
        if (intersections[counter].first && !intersections[counter].second)
          cell_intersected = true;
      }
      ++counter;
    }

    // Check if cell is intersected, if not skip
    if (!cell_intersected)
      continue;

    // Get cell index (local to process)
    const std::size_t cell_index = cell->index();

    // Update UFC cell wrapper
    cell->get_vertex_coordinates(vertex_coordinates);

    // Tabulate coordinates of all dofs on this cell
    boost::multi_array<double, 2> coordinates;
    element->tabulate_dof_coordinates(coordinates, vertex_coordinates, *cell);

    // Regular dofs
    const auto regular_dofs= dofmap.cell_dofs(cell_index);

    // Loop over degrees of freedom
    for (std::size_t dof = 0; dof < coordinates.shape()[0]; ++dof)
    {
      // Construct DOLFIN Point for dof
      const dolfin::Point p(coordinates.shape()[1], &(coordinates[dof][0]));

      // Get regular dof index
      dolfin_assert(dof < regular_dofs.size());
      const std::size_t regular_dof_index = regular_dofs[dof];

      // Add markers if not a tip dof
      const std::set<int>& tip_surfaces = tip_dofs[regular_dof_index];
      for (std::size_t i = 0; i < intersections.size(); ++i)
      {
        if (intersections[i].first && !intersections[i].second)
        {
          if (tip_surfaces.find(i) == tip_surfaces.end())
          {
            // 'Self-self interaction case'
            enriched_dofs[regular_dof_index].insert({{{SurfaceId(i), SurfaceId(i), SurfaceId(i)}}});
            enriched_cells[cell_index].insert(      {{{SurfaceId(i), SurfaceId(i), SurfaceId(i)}}});

            // 'surface_i-surface_j interaction'
            for (std::size_t j = i + 1; j < intersections.size(); ++j)
            {
              if (intersections[j].first && !intersections[j].second)
              {
                if (tip_surfaces.find(j) == tip_surfaces.end())
                {
                  // Check for intersection
                  const bool intersect_ij
                    = GeometryTools::surfaces_intersect(*cell, *surfaces[i], *surfaces[j]);
                  if (intersect_ij)
                  {
                    enriched_dofs[regular_dof_index].insert({{{SurfaceId(i),SurfaceId(i), SurfaceId(j)}}});
                    enriched_cells[cell_index].insert(      {{{SurfaceId(i),SurfaceId(i), SurfaceId(j)}}});
                  }
                  // 'surface_i-surface_j-surface_k interaction'
                  for (std::size_t k = j + 1; k < intersections.size(); ++k)
                  {
                    if (intersections[k].first && !intersections[k].second)
                    {
                      if (tip_surfaces.find(k) == tip_surfaces.end())
                      {
                        // Check for intersection
                        const bool intersect_jk
                          = GeometryTools::surfaces_intersect(*cell, *surfaces[j], *surfaces[k]);
                        if (intersect_jk)
                        {
                          enriched_dofs[regular_dof_index].insert({{{SurfaceId(j),SurfaceId(j), SurfaceId(k)}}});
                          enriched_cells[cell_index].insert(      {{{SurfaceId(j),SurfaceId(j), SurfaceId(k)}}});
                        }

                        // Check for intersection
                        const bool intersect_ik
                          = GeometryTools::surfaces_intersect(*cell, *surfaces[i], *surfaces[k]);
                        if (intersect_ik)
                        {
                          enriched_dofs[regular_dof_index].insert({{{SurfaceId(i),SurfaceId(i), SurfaceId(k)}}});
                          enriched_cells[cell_index].insert(      {{{SurfaceId(i),SurfaceId(i), SurfaceId(k)}}});
                        }

                        // Check for intersection
                        const bool intersect_ijk = GeometryTools::surfaces_intersect(*cell, *surfaces[i], *surfaces[j], *surfaces[k]);
                        if (intersect_ijk)
                        {
                          enriched_dofs[regular_dof_index].insert({{{SurfaceId(i),SurfaceId(j),SurfaceId(k)}}});
                          enriched_cells[cell_index].insert(      {{{SurfaceId(i),SurfaceId(j),SurfaceId(k)}}});
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
//-----------------------------------------------------------------------------
std::vector<std::set<int>>
XFEMTools::compute_tip_dofs(std::shared_ptr<const dolfin::Mesh> mesh,
                            const DomainManager& domain,
                            const dolfin::GenericDofMap& dofmap)
{
  // Create vector to hold tip info
  std::vector<std::set<int>> tip_dof_surfaces(dofmap.global_dimension());

  // Get number of surfces
  const std::size_t num_surfaces = domain.num_surfaces();

  // Loop over surfaces
  std::vector<std::size_t> facet_dofs;
  for (std::size_t n = 0; n < num_surfaces; ++n)
  {
    // Get facets on which crack tip lies
    dolfin::FacetFunction<bool> boundary_facet
      = GeometryTools::compute_boundary_facets(mesh, *domain.surface(n));

    // Build list of 'tip' regular dofs
    for (dolfin::CellIterator c(*mesh); !c.end(); ++c)
    {
      // Regular dofs
      const auto regular_dofs= dofmap.cell_dofs(c->index());
      for (dolfin::FacetIterator f(*c); !f.end(); ++f)
      {
        if (boundary_facet[*f])
        {
          // Extract local facet index
          const std::size_t local_facet = c->index(*f);

          // Tabulate cell local dofs for facet
          dofmap.tabulate_facet_dofs(facet_dofs, local_facet);

          // Mark 'tip 'dofs
          for (std::size_t dof : facet_dofs)
          {
            dolfin::la_index global_dof = regular_dofs[dof];
            tip_dof_surfaces[global_dof].insert(n);
          }
        }
      }
    }
  }

  return tip_dof_surfaces;
}
//-----------------------------------------------------------------------------
