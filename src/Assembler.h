// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_ASSEMBLER_H
#define NARWAL_ASSEMBLER_H

#include <memory>
#include <vector>
#include <dolfin/common/ArrayView.h>
#include <dolfin/fem/DirichletBC.h>

namespace dolfin
{
  class DirichletBC;
  class GenericDofMap;
  class GenericMatrix;
  class GenericVector;
}

namespace narwal
{

  class XFEMForms;
  class GenericSurface;

  /// This class assembles systems, with support for the XFEM method

  class Assembler
  {
  public:

    /// Constructor
    Assembler(std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs,
              std::shared_ptr<const narwal::XFEMForms> forms);

    /// Assemble matrix and RHS vector
    void assemble(dolfin::GenericMatrix& A, dolfin::GenericVector& b);

  private:

    void apply_bc(double* A, double* b,
                  const dolfin::DirichletBC::Map& boundary_values,
                  dolfin::ArrayView<const dolfin::la_index> global_dofs0,
                  dolfin::ArrayView<const dolfin::la_index> global_dofs1);

    std::vector<std::shared_ptr<const dolfin::DirichletBC>> _bcs;
    std::shared_ptr<const narwal::XFEMForms> _forms;

  };


}
#endif
