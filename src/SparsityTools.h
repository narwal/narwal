// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_SPARSITY_TOOLS_H
#define NARWAL_SPARSITY_TOOLS_H

#include <vector>
#include <dolfin/la/TensorLayout.h>

namespace dolfin
{
  class GenericDofMap;
  class Mesh;
}

namespace narwal
{

  /// FIXME: short description needed

  class SparsityTools
  {
  public:

    static dolfin::TensorLayout
      build_vector_layout(const dolfin::Mesh& mesh,
                          const dolfin::GenericDofMap& dofmap);

    static dolfin::TensorLayout
      build_matrix_layout(const dolfin::Mesh& mesh,
                          const dolfin::GenericDofMap& dofmap);

  };

}

#endif
