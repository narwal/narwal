// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "SolverXAMG.h"

#include <fstream>
#include <iostream>
#include <map>
#include <utility>
#include <vector>
#include <tuple>
#include <iomanip>

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/triangular.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>

#include <dolfin/function/FunctionSpace.h>
#include <dolfin/la/GenericLinearSolver.h>
#include <dolfin/la/GenericMatrix.h>
#include <dolfin/la/GenericPreconditioner.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/la/PETScKrylovSolver.h>
#include <dolfin/la/PETScPreconditioner.h>
#include <dolfin/la/PETScOptions.h>
#include <dolfin/fem/UFC.h>
#include <dolfin/la/VectorSpaceBasis.h>
#include <dolfin/parameter/GlobalParameters.h>

#include "DomainManager.h"
#include "DofKey.h"
#include "DofMap.h"
#include "XAMGTools.h"
#include "TimeMonitor.h"
#include "SparsityTools.h"
#include "XFEMForms.h"

//-----------------------------------------------------------------------------
narwal::la::SolverXAMG::SolverXAMG(
  std::shared_ptr<const dolfin::GenericMatrix> A_,
  std::shared_ptr<const dolfin::GenericVector> b_,
  std::shared_ptr<dolfin::Function> u_,
  std::shared_ptr<const dolfin::FunctionSpace> V,
  std::shared_ptr<const narwal::DomainManager> domainManager_,
  std::shared_ptr<const narwal::DofMap> dofmap_,
  std::shared_ptr<const narwal::XFEMForms> forms_,
  const bool applyTransformation_,
  const bool debugOutput_,
  const std::string &debugFilenamePrefix_) :
      domainManager(domainManager_),
      dofmap(dofmap_),
      element(V->element()->create_sub_element(0)),
      forms(forms_),
      A(A_),
      b(b_),
      u(u_),
      applyTransformation(applyTransformation_),
      enableAMG(true),
      debugOutput(debugOutput_),
      debugFilenamePrefix(debugFilenamePrefix_)
{
#ifdef HAS_PETSC
  dolfin::PETScOptions::set("mg_levels_pc_type", "sor");
#endif
  Ginv_u.reset(new dolfin::Function(V));
}
//-----------------------------------------------------------------------------
narwal::la::SolverXAMG::~SolverXAMG()
{
  // delete stuff in the correct order by having correct order of variables
  // in class
}
//-----------------------------------------------------------------------------
void narwal::la::SolverXAMG::setupDefaultPreCondAndSolver(
  const std::string& amg_lib)
{
  // This is not a very elegant implementation, but it get's the job done.
  const std::string lab = dolfin::parameters["linear_algebra_backend"];
  if (lab == "PETSc")
  {
#ifdef HAS_PETSC
    if (amg_lib == "petsc_amg")
    {
      if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
        dolfin::info("Setting up PETScKrylovSolver & PETSc AMG ...");

      // Create preconditioner
      // use "petsc_amg" or "ml_amg"
      auto petscPC = std::make_shared<dolfin::PETScPreconditioner>("petsc_amg");
      pc = petscPC;
      petscPC->parameters["report"] = false;

      // Create solver
      solver.reset(new dolfin::PETScKrylovSolver("gmres", petscPC));
      solver->parameters["relative_tolerance"] = 1.0e-8;
      solver->parameters["convergence_norm_type"] = "true";
    }
    else if (amg_lib == "ml_amg")
    {
      if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0) {
        dolfin::info("Setting up PETScKrylovSolver & ML ...");
      }
#if PETSC_HAVE_ML
#else
      dolfin::warning("narwal::la::SolverXAMG::setupDefaultPreCondAndSolver()");
      dolfin::warning("  Requesting ml_amg, but PETSc compiled without ML!");
#endif
      auto petscPC = std::make_shared<dolfin::PETScPreconditioner>("ml_amg");
      pc = petscPC;
      petscPC->parameters["report"] = false;

      // Create solver
      solver.reset(new dolfin::PETScKrylovSolver("gmres", petscPC));
      solver->parameters["relative_tolerance"] = 1.0e-8;
    }
    else if (amg_lib == "hypre_amg")
    {
      if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0) {
        dolfin::info("Setting up PETScKrylovSolver using CG & Hypre AMG (Boomeramg) ...");
      }
      auto petscPC = std::make_shared<dolfin::PETScPreconditioner>("hypre_amg");
      pc = petscPC;
      petscPC->parameters["report"] = false;

      // Create solver
      solver.reset(new dolfin::PETScKrylovSolver("cg", petscPC));
      solver->parameters["relative_tolerance"] = 1.0e-8;
    }
    else if (amg_lib == "ilu")
    {
      if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
        dolfin::info("Setting up PETScKrylovSolver & PETSc ILU ...");
      enableAMG = false;
      // Create preconditioner
      // use "petsc_amg" or "ml_amg"
      auto petscPC = std::make_shared<dolfin::PETScPreconditioner>("ilu");
      pc = petscPC;
      petscPC->parameters["report"] = false;

      // Create solver
      solver.reset(new dolfin::PETScKrylovSolver("gmres", petscPC));
      solver->parameters["relative_tolerance"] = 1.0e-8;
      solver->parameters["convergence_norm_type"] = "true";
    }
    else
    {
      if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
      {
        dolfin::warning("unknown multigrid solver for PETSc! Choose 'petsc_amg', 'ml_amg', or 'ilu'!");
      }
    }

    // parameters for both AMG backends
    solver->parameters["report"] = false;
    solver->parameters["monitor_convergence"] = true;
#endif
  }
  else
  {
    if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
      dolfin::warning("unknown linear algebra backend...");
  }
}
//-----------------------------------------------------------------------------
void narwal::la::SolverXAMG::transformSystem()
{
  narwal::Time t("narwal::la::SolverXAMG::transformSystem()");
  if (applyTransformation and enableAMG)
  {
    // create transformation matrix G
    dolfin_assert(element);
    G = buildTransformationMatrixG(*domainManager, *dofmap, *element, *forms);
    if (debugOutput)
      toMatrixMarketFile(*G, debugFilenamePrefix + "_G");

    // Transform system matrix: G'*A*G
    GtAG = narwal::la::MatPtAP(*G, *A);
    if (debugOutput)
      toMatrixMarketFile(*GtAG, debugFilenamePrefix + "_GtAG");

    // Transform rhs: G'*b
    auto Gtbtemp = b->copy();
    G->transpmult(*b, *Gtbtemp);
    Gtb = Gtbtemp;

    // Transform null space: Ginv * null space
    Ginv_null_space = narwal::la::transformGinvN(G, *null_space);
  }
  else
  {
    if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
      dolfin::info("Skipping transformation as requested...");
    GtAG = A;
    Gtb = b;
    Ginv_null_space = null_space;
  }

//  if (debugOutput) {
//    if (Ginv_null_space->is_orthogonal()) {
//      std::cout << "Vector space basis is orthogonal." << std::endl;
//    } else {
//      std::cout << "Vector space basis is *not* orthogonal." << std::endl;
//    }
//  }
}
//-----------------------------------------------------------------------------
void narwal::la::SolverXAMG::inverseTransformSystem()
{
  // inverse transform solution
  if (applyTransformation and enableAMG)
  {
    narwal::Time t("narwal::la::SolverXAMG::inverseTransformSystem()");
    G->mult(*Ginv_u->vector(), *u->vector());
  }
  else
  {
    if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
      dolfin::info("Skipping transformation as requested...");
    u = Ginv_u;
  }
}
//-----------------------------------------------------------------------------
std::size_t narwal::la::SolverXAMG::solve()
{
  narwal::Time t("narwal::la::SolverXAMG::solve()");
  transformSystem();

  dolfin::info("Solve...");

  dolfin_assert(Ginv_null_space);
  if (enableAMG)
    pc->set_nullspace(*Ginv_null_space);

  // Solve
  Ginv_u->vector()->zero();
  solver->set_operator(GtAG);
  const std::size_t num_iters = solver->solve(*Ginv_u->vector(), *Gtb);

  // inverse transform solution
  inverseTransformSystem();

  if (debugOutput)
  {
    std::cout << "Solution vector norm: " << u->vector()->norm("l2")
              << std::endl;
  }
  return num_iters;
}

//class NodeBasisTransform {
//public:
//  NodeBasisTransform(narwal::SurfaceId surfIdA, narwal::SurfaceId surfIdB,
//    double psiA, double psiB)
//   :surfId0(surfIdA), surfId1(surfIdB), psi0(psiA), psi1(psiB)
//  {
////    if (surfId0 > surfId1) {
////      std::swap(surfId0, surfId1);
////    }
//  }
//  double entry(
//    const narwal::DomLabel& domLabel,
//    const narwal::EnrLabel& enrLabel
//    ) const
//  {
//
//    static const narwal::DomLabel pp(true, true);
//    static const narwal::DomLabel pm(true, false);
//    static const narwal::DomLabel mp(false, true);
//    static const narwal::DomLabel mm(false, false);
//
//    if (std::get<0>(enrLabel) == -1) {
//      return 1.0; // standard enrichment, meaning no Heaviside involved
//    } else if (std::get<0>(enrLabel) == surfId0 && std::get<1>(enrLabel) == surfId0) {
//      if (     domLabel == pp) { return fp(psi0); }
//      else if (domLabel == pm) { return fp(psi0); }
//      else if (domLabel == mp) { return fm(psi0); }
//      else if (domLabel == mm) { return fm(psi0); }
//    } else if (std::get<0>(enrLabel) == surfId1 && std::get<1>(enrLabel) == surfId1) {
//      if (     domLabel == pp) { return fp(psi1); }
//      else if (domLabel == pm) { return fm(psi1); }
//      else if (domLabel == mp) { return fp(psi1); }
//      else if (domLabel == mm) { return fm(psi1); }
//    } else if((std::get<0>(enrLabel) == surfId0 && std::get<1>(enrLabel) == surfId1)
//            ||(std::get<0>(enrLabel) == surfId1 && std::get<1>(enrLabel) == surfId0)){
//      if (     domLabel == pp) { return fp(psi0)*fp(psi1); }
//      else if (domLabel == pm) { return fp(psi0)*fm(psi1); }
//      else if (domLabel == mp) { return fm(psi0)*fp(psi1); }
//      else if (domLabel == mm) { return fm(psi0)*fm(psi1); }
//    }
//    return 1.0;
//  }
//private:
//  NodeBasisTransform();
//  // surface ids in interaction, surfId1 shall be the smaller Id?
//  const narwal::SurfaceId surfId0;
//  const narwal::SurfaceId surfId1;
//
//  // psi value for evaluated DOF w.r.t. each surface
//  const double psi0;
//  const double psi1;
//
//  static double fp(const double a) { return sqr(std::abs((a - 1)/2)); }
//  static double fm(const double a) { return sqr(std::abs((a + 1)/2)); }
//  static double sqr(const double a) { return a*a; }
//};

class NodeBasisTransform3 {
public:
  NodeBasisTransform3(
    const narwal::DomainManager &domain_,
    const dolfin::Point p_,
    narwal::SurfaceId surfIdA,
    narwal::SurfaceId surfIdB,
    narwal::SurfaceId surfIdC
    )
   :domain(domain_), p(p_), surfId0(surfIdA), surfId1(surfIdB), surfId2(surfIdC),
    psi0(domain.surface_side(p, surfId0)),
    psi1(domain.surface_side(p, surfId1)),
    psi2(domain.surface_side(p, surfId2))
  {

  }
  double entry(
    const narwal::DomLabel& domLabel,
    const narwal::EnrLabelArray& enrLabel
    ) const
  {

    static const narwal::DomLabel ppp( true,  true,  true);
    static const narwal::DomLabel ppm( true,  true, false);
    static const narwal::DomLabel pmp( true, false,  true);
    static const narwal::DomLabel pmm( true, false, false);
    static const narwal::DomLabel mpp(false,  true,  true);
    static const narwal::DomLabel mpm(false,  true, false);
    static const narwal::DomLabel mmp(false, false,  true);
    static const narwal::DomLabel mmm(false, false, false);

    if (enrLabel[0] == -1) {
      return 1.0; // standard enrichment, meaning no Heaviside involved
    } else {
      if (       enrLabel[0] == surfId0 && enrLabel[1] == surfId0 && enrLabel[2] == surfId0) {
        if (     domLabel == ppp) { return fp(psi0); }
        else if (domLabel == ppm) { return fp(psi0); }
        else if (domLabel == pmp) { return fp(psi0); }
        else if (domLabel == pmm) { return fp(psi0); }
        else if (domLabel == mpp) { return fm(psi0); }
        else if (domLabel == mpm) { return fm(psi0); }
        else if (domLabel == mmp) { return fm(psi0); }
        else if (domLabel == mmm) { return fm(psi0); }
      } else if (enrLabel[0] == surfId1 && enrLabel[1] == surfId1 && enrLabel[2] == surfId1) {
        if (     domLabel == ppp) { return fp(psi1); }
        else if (domLabel == ppm) { return fp(psi1); }
        else if (domLabel == pmp) { return fm(psi1); }
        else if (domLabel == pmm) { return fm(psi1); }
        else if (domLabel == mpp) { return fp(psi1); }
        else if (domLabel == mpm) { return fp(psi1); }
        else if (domLabel == mmp) { return fm(psi1); }
        else if (domLabel == mmm) { return fm(psi1); }
      } else if (enrLabel[0] == surfId2 && enrLabel[1] == surfId2 && enrLabel[2] == surfId2) {
        if (     domLabel == ppp) { return fp(psi2); }
        else if (domLabel == ppm) { return fm(psi2); }
        else if (domLabel == pmp) { return fp(psi2); }
        else if (domLabel == pmm) { return fm(psi2); }
        else if (domLabel == mpp) { return fp(psi2); }
        else if (domLabel == mpm) { return fm(psi2); }
        else if (domLabel == mmp) { return fp(psi2); }
        else if (domLabel == mmm) { return fm(psi2); }
      } else if ((enrLabel[0] == surfId0 && enrLabel[1] == surfId0 && enrLabel[2] == surfId1)) {
        if (     domLabel == ppp) { return fp(psi0)*fp(psi1); }
        else if (domLabel == ppm) { return fp(psi0)*fp(psi1); }
        else if (domLabel == pmp) { return fp(psi0)*fm(psi1); }
        else if (domLabel == pmm) { return fp(psi0)*fm(psi1); }
        else if (domLabel == mpp) { return fm(psi0)*fp(psi1); }
        else if (domLabel == mpm) { return fm(psi0)*fp(psi1); }
        else if (domLabel == mmp) { return fm(psi0)*fm(psi1); }
        else if (domLabel == mmm) { return fm(psi0)*fm(psi1); }
      } else if ((enrLabel[0] == surfId0 && enrLabel[1] == surfId0 && enrLabel[2] == surfId2)) {
        if (     domLabel == ppp) { return fp(psi0)*fp(psi2); }
        else if (domLabel == ppm) { return fp(psi0)*fm(psi2); }
        else if (domLabel == pmp) { return fp(psi0)*fp(psi2); }
        else if (domLabel == pmm) { return fp(psi0)*fm(psi2); }
        else if (domLabel == mpp) { return fm(psi0)*fp(psi2); }
        else if (domLabel == mpm) { return fm(psi0)*fm(psi2); }
        else if (domLabel == mmp) { return fm(psi0)*fp(psi2); }
        else if (domLabel == mmm) { return fm(psi0)*fm(psi2); }
      } else if ((enrLabel[0] == surfId1 && enrLabel[1] == surfId1 && enrLabel[2] == surfId2)) {
        if (     domLabel == ppp) { return fp(psi1)*fp(psi2); }
        else if (domLabel == ppm) { return fp(psi1)*fm(psi2); }
        else if (domLabel == pmp) { return fm(psi1)*fp(psi2); }
        else if (domLabel == pmm) { return fm(psi1)*fm(psi2); }
        else if (domLabel == mpp) { return fp(psi1)*fp(psi2); }
        else if (domLabel == mpm) { return fp(psi1)*fm(psi2); }
        else if (domLabel == mmp) { return fm(psi1)*fp(psi2); }
        else if (domLabel == mmm) { return fm(psi1)*fm(psi2); }
      } else if( enrLabel[0] != enrLabel[1] && enrLabel[1] != enrLabel[2] ){
        if (     domLabel == ppp) { return fp(psi0)*fp(psi1)*fp(psi2); }
        else if (domLabel == ppm) { return fp(psi0)*fp(psi1)*fm(psi2); }
        else if (domLabel == pmp) { return fp(psi0)*fm(psi1)*fp(psi2); }
        else if (domLabel == pmm) { return fp(psi0)*fm(psi1)*fm(psi2); }
        else if (domLabel == mpp) { return fm(psi0)*fp(psi1)*fp(psi2); }
        else if (domLabel == mpm) { return fm(psi0)*fp(psi1)*fm(psi2); }
        else if (domLabel == mmp) { return fm(psi0)*fm(psi1)*fp(psi2); }
        else if (domLabel == mmm) { return fm(psi0)*fm(psi1)*fm(psi2); }
      }
    }
    return 1;
  }
private:
  NodeBasisTransform3();
  const narwal::DomainManager &domain;
  const dolfin::Point p;

  // surface ids in interaction, surfId1 shall be the smaller Id?
  const narwal::SurfaceId surfId0;
  const narwal::SurfaceId surfId1;
  const narwal::SurfaceId surfId2;

  // psi value for evaluated DOF w.r.t. each surface
  const double psi0;
  const double psi1;
  const double psi2;

  static double fp(const double a) { return sqr(std::abs((a - 1)/2)); }
  static double fm(const double a) { return sqr(std::abs((a + 1)/2)); }
  static double sqr(const double a) { return a*a; }
};

/*
The following code inverts the matrix input using LU-decomposition with backsubstitution of unit vectors. Reference: Numerical Recipies in C, 2nd ed., by Press, Teukolsky, Vetterling & Flannery.

you can solve Ax=b using three lines of ublas code:

permutation_matrix<> piv;
lu_factorize(A, piv);
lu_substitute(A, piv, x);

*/

namespace ublas = boost::numeric::ublas;

/* Matrix inversion routine.
    Uses lu_factorize and lu_substitute in uBLAS to invert a matrix */
template<class T>
bool InvertMatrix(const ublas::matrix<T>& input, ublas::matrix<T>& inverse) {
  using namespace boost::numeric::ublas;
  typedef permutation_matrix<std::size_t> pmatrix;
  // create a working copy of the input
  matrix<T> A(input);
  // create a permutation matrix for the LU-factorization
  pmatrix pm(A.size1());

  // perform LU-factorization
  int res = lu_factorize(A,pm);
  if( res != 0 ) return false;

  // create identity matrix of "inverse"
  inverse.assign(ublas::identity_matrix<T>(A.size1()));

  // backsubstitute to get the inverse
  lu_substitute(A, pm, inverse);

  return true;
}


//static void addEntry(
//  const std::vector<std::pair<narwal::EnrLabel, narwal::DofEleID>>& enrVec,
//  std::map<std::pair<narwal::DofGID, narwal::DofGID>, double>& e2r,
//  const narwal::DofEleID iEnr,
//  const narwal::DofEleID jEnr,
//  const double val
//  )
//{
//  const narwal::DofGID I = enrVec[iEnr].second;
//  const narwal::DofGID J = enrVec[jEnr].second;
//  if (std::abs(val) > 0.01) {
//    e2r[std::make_pair(I, J)] = val;
//  }
//}

//-----------------------------------------------------------------------------
std::unique_ptr<dolfin::GenericMatrix>
narwal::la::SolverXAMG::buildTransformationMatrixG(
  const narwal::DomainManager &domain,
  const narwal::DofMap &dofmap,
  const dolfin::FiniteElement &element,
  const narwal::XFEMForms& forms)
{

  const dolfin::Mesh &mesh = *domain.mesh();

  const std::size_t gdim = element.geometric_dimension();
  // Get number of element 'nodes'
  const std::size_t num_nodes = element.space_dimension();

  EnrLabelArray standardEnrLabel;
  for (std::size_t i = 0; i < standardEnrLabel.size(); ++i) {
    standardEnrLabel[i] = -1;
  }

  // step 0: create nodal/dof-based data enrichment information
  std::map<RegDofGID, std::set<std::pair<EnrLabelArray, DofGID>>> regDofEnrichments;
  std::map<RegDofGID, dolfin::Point> regDofPos;
  std::map<RegDofGID, std::set<DomLabel> > regDofDomains;
  ufc::cell ufc_cell;
  std::vector<double> vertex_coordinates;
  // Create UFC wrappers
  dolfin::UFC ufc_a(*forms.a());
  for (dolfin::CellIterator c(mesh); !c.end(); ++c) {

    const dolfin::ArrayView<const dolfin::la_index> cell_dofs =
        dofmap.cell_dofs(c->index());

    c->get_vertex_coordinates(vertex_coordinates);
    c->get_cell_data(ufc_cell);

    ufc_a.update(*c, vertex_coordinates, ufc_cell);
    boost::multi_array<double, 2> coordinates;
    element.tabulate_dof_coordinates(coordinates, vertex_coordinates, *c);

    //const double regdofToSpot = 9;

    if (dofmap.is_enriched(c->index())) {
      narwal::SubTriangulation sub_tri(*c, domain.surfaces());
      // Loop over cell sub-simplices
      for (dolfin::CellIterator subcell(sub_tri.mesh()); !subcell.end(); ++subcell)
      {
        const double cell_ratio = subcell->volume()/c->volume();
        if (cell_ratio < 1.0e-5)
        {
          std::cout << "WARNING: small cell volume ratio. Ratio, Cell index:"
              << cell_ratio << ", " << subcell->index() << std::endl;
          //continue;
        }

        const dolfin::Point x_q = subcell->midpoint();

        // Loop over test function 'nodes'
        for (std::size_t node_i = 0; node_i < num_nodes; ++node_i)
        {
          // Spatial point of the 'test' node
          const dolfin::Point ni_point(
            coordinates[node_i][0],
            coordinates[node_i][1],
            coordinates[node_i][2]);

          // Loop over regular dofs at node
          for (std::size_t rdof = 0; rdof < gdim; ++rdof)
          {
            // Get cell-wise index of regular dof
            const int I_reg = rdof*num_nodes + node_i;
            const std::size_t regDofGID = cell_dofs[I_reg];
            //              if (regDofGID == regdofToSpot && rdof == 0) {
            //                std::cout << "Cell: "<<c->index()<< ", " << regDofGID << ", size enriched_dofs: " << dofmap.enriched_dofs(c->index(), I_reg).size() << std::endl;
            //              }

            regDofPos[regDofGID] = ni_point;

            regDofEnrichments[regDofGID].insert(std::make_pair(standardEnrLabel, regDofGID));

            // find involved surfaces
            std::set<SurfaceId> involvedSurfaces;
            // Loop over enriched dofs associated with regular dof
            for (auto edof : dofmap.enriched_dofs(c->index(), I_reg))
            {
              const EnrLabelArray& a = edof.second;
              for (std::size_t i = 0; i < a.size(); ++i) {
                involvedSurfaces.insert(a[i]);
              }

//              const double H_q0 = domain.surface_side(x_q, a[0]);
//              const double H_q1 = domain.surface_side(x_q, a[1]);
//              const double H_q2 = domain.surface_side(x_q, a[2]);
//
//              if (H_q0 != 0.0 && H_q1 != 0.0 && H_q2 != 0.0) {
//
////                if (iSurf <=jSurf && jSurf <= kSurf) {
//
////                  if (valid) {
//                    regDofDomains[regDofGID].insert(DomLabel(H_q0>0.0, H_q1>0.0, H_q2>0.0));
////                  }
////                }
//              }

            }

            std::vector<SurfaceId> involvedSurfaceVector(involvedSurfaces.begin(), involvedSurfaces.end());

            if (involvedSurfaceVector.size() == 3) {
              const SurfaceId iSurf = involvedSurfaceVector[0];
              const SurfaceId jSurf = involvedSurfaceVector[1];
              const SurfaceId kSurf = involvedSurfaceVector[2];

              const double H_q0 = domain.surface_side(x_q, iSurf);
              const double H_q1 = domain.surface_side(x_q, jSurf);
              const double H_q2 = domain.surface_side(x_q, kSurf);

              if (H_q0 != 0.0 && H_q1 != 0.0 && H_q2 != 0.0) {
                regDofDomains[regDofGID].insert(DomLabel(H_q0>0.0, H_q1>0.0, H_q2>0.0));
              }
            } else if (involvedSurfaceVector.size() == 2) {
              const SurfaceId iSurf = involvedSurfaceVector[0];
              const SurfaceId jSurf = involvedSurfaceVector[1];

              const double H_q0 = domain.surface_side(x_q, iSurf);
              const double H_q1 = domain.surface_side(x_q, jSurf);

              if (H_q0 != 0.0 && H_q1 != 0.0) {
                regDofDomains[regDofGID].insert(DomLabel(H_q0>0.0, H_q0>0.0, H_q1>0.0));
              }
            } else if (involvedSurfaceVector.size() == 1) {
              const SurfaceId iSurf = involvedSurfaceVector[0];

              const double H_q0 = domain.surface_side(x_q, iSurf);

              if (H_q0 != 0.0) {
                regDofDomains[regDofGID].insert(DomLabel(H_q0>0.0, H_q0>0.0, H_q0>0.0));
              }
            }


//            for (auto iSurf : involvedSurfaces) {
//              for (auto jSurf : involvedSurfaces) {
//                for (auto kSurf : involvedSurfaces) {
//
//                  const bool valid = (
//                      (iSurf == jSurf && jSurf == kSurf) || // 1 surface interaction
//                      (iSurf == jSurf && jSurf != kSurf) || // 2 surface interaction
//                      (iSurf != jSurf && jSurf != kSurf && iSurf != kSurf));  // 3 surface interaction
//
//                  const double H_q0 = domain.surface_side(x_q, iSurf);
//                  const double H_q1 = domain.surface_side(x_q, jSurf);
//                  const double H_q2 = domain.surface_side(x_q, kSurf);
//
//                  if (H_q0 != 0.0 && H_q1 != 0.0 && H_q2 != 0.0) {
//
////                    if (iSurf <=jSurf && jSurf <= kSurf) {
//                    if (jSurf <= kSurf) {
////                    }
//
//                      if (valid) {
//                        regDofDomains[regDofGID].insert(DomLabel(H_q0>0.0, H_q1>0.0, H_q2>0.0));
//                      }
//                    }
//                  }
//                }
//              }
//            }


//            for (auto iSurf : involvedSurfaces) {
//              for (auto jSurf : involvedSurfaces) {
//                for (auto kSurf : involvedSurfaces) {
//
//                  const bool valid = (
//                      (iSurf == jSurf && jSurf == kSurf) || // 1 surface interaction
//                      (iSurf == jSurf && jSurf != kSurf) || // 2 surface interaction
//                      (iSurf != jSurf && jSurf != kSurf && iSurf != kSurf));  // 3 surface interaction
//
//                  const double H_q0 = domain.surface_side(x_q, iSurf);
//                  const double H_q1 = domain.surface_side(x_q, jSurf);
//                  const double H_q2 = domain.surface_side(x_q, kSurf);
//
//                  if (H_q0 != 0.0 && H_q1 != 0.0 && H_q2 != 0.0) {
//
//                    if (iSurf <=jSurf && jSurf <= kSurf) {
//
//                      if (valid) {
//                        regDofDomains[regDofGID].insert(DomLabel(H_q0>0.0, H_q1>0.0, H_q2>0.0));
//                      }
//                    }
//                  }
//                }
//              }
//            }

            // Loop over enriched dofs associated with regular dof
            for (auto edof : dofmap.enriched_dofs(c->index(), I_reg))
            {
              const DofEleID dofElemId = edof.first;
              const EnrLabelArray enrLabel(edof.second);
              //if (regDofGID == regdofToSpot && rdof == 0) {
              //  std::cout << "EnrLabel: " << edof.second[0] << "," << edof.second[1] << std::endl;
              //}
              const std::size_t dofGID = cell_dofs[dofElemId];
              regDofEnrichments[regDofGID].insert(std::make_pair(enrLabel, dofGID));
            } // loop over enrichments
          } // loop over displacement directions
        } // loop over nodes
      } // Loop over cell sub-simplices
    } else {
      // Loop over test function 'nodes'
      for (std::size_t node_i = 0; node_i < num_nodes; ++node_i)
      {
        // Spatial point of the 'test' node
        const dolfin::Point ni_point(
          coordinates[node_i][0],
          coordinates[node_i][1],
          coordinates[node_i][2]);

        // Loop over regular dofs at node
        for (std::size_t rdof = 0; rdof < gdim; ++rdof)
        {
          // Get cell-wise index of regular dof
          const std::size_t I_reg = rdof*num_nodes + node_i;
          const std::size_t regDofGID = cell_dofs[I_reg];

          regDofPos[regDofGID] = ni_point;
          //regDofDomains[regDofGID].insert(DomLabel(true, true));
          regDofEnrichments[regDofGID].insert(std::make_pair(standardEnrLabel, regDofGID));
        }
      }
    }
  }

  // collect non-zero sparse matrix entries for transformation matrix
  std::map<std::pair<DofGID, DofGID>, double> matrixEntries;
  for (auto enrDof = regDofEnrichments.begin(); enrDof != regDofEnrichments.end(); ++enrDof) {

    const DofGID regDofId = enrDof->first;
    const std::set<std::pair<EnrLabelArray, DofGID>> enrSet = enrDof->second;
    //std::cout << "Info for Global DOF ID: "<< regDofId << std::endl;

//    const std::pair<EnrLabel,DofGID> myPair = enrDof->second;

    const std::vector<std::pair<EnrLabelArray,DofGID>> enrVec(enrSet.begin(), enrSet.end());
    dolfin_assert(!enrDof->second.empty());

//    std::cout << "enrVec.size() = " << enrVec.size() << std::endl;
    boost::numeric::ublas::matrix<double> GtoHeaviSide(enrVec.size(), enrVec.size());
    if (enrVec.size() == 1) {
//      const dolfin::Point p = regDofPos.find(regDofId)->second;
//      NodeBasisTransform3 geval(domain, p, -1, -1, -1);
//      DomLabel pp(true, true, true);
      GtoHeaviSide(0, 0) = 1.0; //geval.entry(pp, standardEnrLabel);
    } else {

      const std::set<DomLabel>& myDomains = regDofDomains.find(regDofId)->second;
      const std::vector<DomLabel> domVec(myDomains.begin(), myDomains.end());

      if(enrVec.size() != domVec.size()) {

        std::cout << "enrVec: size() = " << enrVec.size() <<std::endl;
        for (std::size_t i = 0; i < enrVec.size(); ++i) {
          std::cout << enrVec[i].first[0] << ", " << enrVec[i].first[1] << ", " << enrVec[i].first[2] << std::endl;
        }
        std::cout << "domVec: size() = " << domVec.size() <<std::endl;
        for (std::size_t i = 0; i < domVec.size(); ++i) {
          std::cout << std::get<0>(domVec[i]) << ", " << std::get<1>(domVec[i]) << ", " << std::get<2>(domVec[i]) << std::endl;
        }

        dolfin::error("enrVec.size() != domVec.size() This is a bug!");
      }

      dolfin_assert(enrVec.size() == domVec.size());

      std::set<SurfaceId> ids;
      for (std::size_t iEnr = 0; iEnr < enrVec.size(); ++iEnr) {
        const EnrLabelArray enrLabel = enrVec[iEnr].first;
        for (std::size_t i = 0; i < enrLabel.size(); ++i) {
          if (enrLabel[i] >= 0) {
            ids.insert(enrLabel[i]);
          }
        }
      }

      std::vector<SurfaceId> idVec(ids.begin(), ids.end());

      if (idVec.size() == 1) {
        // 1 intersecting surface, pad id at the beginning to mimic original EnrLabel e.g. [1,1]
        idVec.insert(idVec.begin(), idVec.front());
      }
      if (idVec.size() == 2) {
        // 2 intersecting surface, pad id at the beginning to mimic original EnrLabel e.g. [1,1,1]
        idVec.insert(idVec.begin(), idVec.front());
      }

      const dolfin::Point p = regDofPos.find(regDofId)->second;

      const NodeBasisTransform3 geval(domain, p, idVec[0], idVec[1], idVec[2]);
      for (std::size_t iEnr = 0; iEnr < enrVec.size(); ++iEnr) {
        const EnrLabelArray eLabel = enrVec[iEnr].first;
//        std::cout << "eLabel = " << eLabel << std::endl;
        for (std::size_t jEnr = 0; jEnr < domVec.size(); ++jEnr) {
          const DomLabel xx = domVec[jEnr];
//          std::cout << "iEnr = " << iEnr << std::endl;
//          std::cout << "jEnr = " << jEnr << std::endl;
//          std::cout << "geval.entry(xx, eLabel) = " << geval.entry(xx, eLabel) << std::endl;
          GtoHeaviSide(iEnr, jEnr) = geval.entry(xx, eLabel);
        }
      }
    }
//    std::cout << GtoHeaviSide << std::endl;

    // invert Matrix
    boost::numeric::ublas::matrix<double> Ginv(enrVec.size(), enrVec.size(), 0.0);
    if (!InvertMatrix(GtoHeaviSide, Ginv)) {
      dolfin::error("Inverting nodal transformation matrix failed!");
    }

    // add entries to e2r map
    for (std::size_t iEnr = 0; iEnr < enrVec.size(); ++iEnr) {
      for (std::size_t jEnr = 0; jEnr < enrVec.size(); ++jEnr) {
        const double val = Ginv(iEnr, jEnr);
        if (std::abs(val) > 0.01) { // only add non-zero values
          const narwal::DofGID I = enrVec[iEnr].second;
          const narwal::DofGID J = enrVec[jEnr].second;
          // we need to build the transposed matrix, so switch I and J
          matrixEntries[std::make_pair(J, I)] = val;
        }
      }
    }
  }

  // create transformation matrix G and insert entries from e2r map
  std::unique_ptr<dolfin::GenericMatrix> G = narwal::la::initSystemMatrix();
  dolfin::TensorLayout layout(
    narwal::SparsityTools::build_matrix_layout(mesh, dofmap));
  G->init(layout);
  G->zero();

  for (auto it = matrixEntries.begin(); it != matrixEntries.end(); ++it) {
    G->setitem(it->first, it->second);
  }
  G->apply("insert");

  return G;
}
//-----------------------------------------------------------------------------
