// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "ElasticityProblem3D.h"

#include <iomanip>
#include <iterator>
#include <memory>
#include <Eigen/Dense>

#include <dolfin/common/ArrayView.h>
#include <dolfin/common/utils.h>
#include <dolfin/function/Function.h>
#include <dolfin/geometry/BoundingBoxTree.h>
#include <dolfin/geometry/SimplexQuadrature.h>
#include <dolfin/geometry/Point.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/mesh/Cell.h>
#include <dolfin/mesh/Mesh.h>
#include <dolfin/mesh/Vertex.h>

#include "DofMap.h"
#include "DomainManager.h"
#include "Elasticity3D_P1.h"
#include "Elasticity3D_P2.h"
#include "ImplicitSurface.h"
#include "SubTriangulation.h"
#include "XFEMForms.h"
#include "XFEMFunctionSpace.h"
#include "XFEMTools.h"

using namespace narwal;

//-----------------------------------------------------------------------------
ElasticityProblem3D::ElasticityProblem3D(
  std::shared_ptr<const narwal::XFEMFunctionSpace> V,
  std::shared_ptr<const dolfin::GenericFunction> lambda,
  std::shared_ptr<const dolfin::GenericFunction> mu,
  std::shared_ptr<const dolfin::GenericFunction> surface_pressure,
  const int polynomialOrder)
  : narwal::XFEMForms(V, polynomialOrder),
    _element(V->function_space()->element()->create_sub_element(0)),
    _lambda(lambda), _mu(mu), _pressure(surface_pressure)
{
  // Create regular forms
  std::shared_ptr<dolfin::Form> a, L;
  switch (polynomialOrder)
  {
  case 1:
  {
    auto al
      = std::make_shared<Elasticity3D_P1::BilinearForm>(V->function_space(),
                                                        V->function_space());
    al->mu = mu;
    al->lmbda = lambda;
    a = al;
    L = std::make_shared<Elasticity3D_P1::LinearForm>(V->function_space());
    break;
  }
  case 2:
  {
    auto al = std::make_shared<Elasticity3D_P2::BilinearForm>(
      V->function_space(), V->function_space());
    al->mu = mu;
    al->lmbda = lambda;
    a = al;
    L = std::make_shared<Elasticity3D_P2::LinearForm>(V->function_space());
    break;
  }
  default:
    std::cout << "ElasticityProblem3D::ElasticityProblem3D(...): "
              << "unknown polynomial degree " << std::endl;
    abort();
  }

  this->set_a(a);
  this->set_L(L);
}
//-----------------------------------------------------------------------------
ElasticityProblem3D::~ElasticityProblem3D()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
void ElasticityProblem3D::tabulate_tensor(
  double* A, double* b,
  const double* const * coefficients,
  const dolfin::Cell& cell,
  const std::vector<double>& vertex_coords,
  const ufc::cell_integral& integral) const
{
  // Get geometric dimension
  dolfin_assert(_element);
  const std::size_t gdim = _element->geometric_dimension();

  // Get number of element 'nodes'
  const std::size_t num_nodes = _element->space_dimension();

  // Get number if regular dofs (regular element dim)
  const std::size_t element_dim_regular = num_nodes*gdim;

  // Get dofmap
  std::shared_ptr<const narwal::DofMap> dofmap = function_space()->dofmap();
  dolfin_assert(dofmap);

  // Dispatch to appropriate tensor tabulate function
  if (dofmap->cell_dofs(cell.index()).size() == element_dim_regular)
    integral.tabulate_tensor(A, coefficients, vertex_coords.data(), 1);
  else
    tabulate_tensor_enriched(A, b, cell, vertex_coords);
}
//-----------------------------------------------------------------------------
namespace {
static double computeH(
  const DomainManager& domain,
  const EnrLabelArray& enrLabel,
  const dolfin::Point& x_q,
  const dolfin::Point& nj_point)
{
  double H = 1.0;
  for (std::size_t i = 0; i < enrLabel.size(); ++i) {
    const SurfaceId surfId1 = enrLabel[i];
    const double H_qi = domain.surface_side(x_q, surfId1);
    const double H_phi_nji
      = 0.5*std::abs(domain.surface_side(nj_point, surfId1) - H_qi);
    H *= H_phi_nji;
  }
  return H;
}
}
//-----------------------------------------------------------------------------
void ElasticityProblem3D::ElasticityProblem3D::tabulate_tensor_enriched(
  double* A, double* b,
  const dolfin::Cell& cell,
  const std::vector<double>& vertex_coords) const
{
  // Get DomainManager object
  dolfin_assert(function_space());
  dolfin_assert(function_space()->domain());
  const DomainManager& domain = *function_space()->domain();

  // Get geometric dimension
  dolfin_assert(_element);
  const std::size_t gdim = _element->geometric_dimension();

  // Get dofmap and element
  std::shared_ptr<const narwal::DofMap> dofmap = function_space()->dofmap();
  dolfin_assert(dofmap);

  auto cell_dofs = dofmap->cell_dofs(cell.index());

  // Get element dim
  const std::size_t element_dim = dofmap->cell_dofs(cell.index()).size();

  // Get number of element 'nodes'
  const std::size_t num_nodes = _element->space_dimension();

  // Get number of regular dofs (regular element dim)
  //const std::size_t element_dim_regular = num_nodes*gdim;

  // Get lambda and mu parameters
  dolfin_assert(_lambda);
  dolfin_assert(_mu);
  const dolfin::Point x_bar = cell.midpoint();
  const dolfin::Array<double>
    x_wrap(gdim, const_cast<double*>(x_bar.coordinates()));

  dolfin::Array<double> data(1);
  _lambda->eval(data, x_wrap);
  const double lambda = data[0];
  _mu->eval(data, x_wrap);
  const double mu = data[0];

  // Tabulate physical coordinates of all dofs on this cell
  dolfin_assert(function_space()->function_space()->dofmap());
  boost::multi_array<double, 2> coordinates;
  _element->tabulate_dof_coordinates(coordinates, vertex_coords, cell);

  // Wrap tensor A using Eigen
  Eigen::Map<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic,
                           Eigen::RowMajor>>
    Ae(A, element_dim, element_dim);

  // Zero tensors
  Ae.setZero();
  std::fill(b, b + element_dim, 0.0);

  /*
  // Get list of indices of surfaces that are active for this cell
  std::set<int> surface_list;
  for (std::size_t i = 0; i < element_dim_regular; ++i)
  {
    const std::vector<std::pair<int, EnrLabelArray>>& edof_data
      = dofmap->enriched_dofs(cell.index(), i);
    for (auto edof : edof_data)
      surface_list.insert(edof.second.begin(), edof.second.end());
  }

  std::vector<int> local_to_global_surface_index;
  std::vector<std::shared_ptr<const narwal::GenericSurface>> surfaces(surface.size());
  for (auto surface_index : surface_list)
  {
    local_to_global_surface_index.push_back(surface_index);
    surfaces.push_back(domains.surface(surface_index));
  }
  */

  // Sub-triangulate cell
  narwal::SubTriangulation sub_tri(cell, domain.surfaces());


  // --- Apply pressure load (surface integral)


  // FIXME: Update pressure loading for multiple surfaces

  // Loop over surface sub-simplex quadrature points
  for (std::size_t surface_index = 0; surface_index < domain.num_surfaces();
       ++surface_index)
  {
    dolfin_assert(sub_tri.num_surface_meshes() == domain.num_surfaces());
    dolfin_assert(sub_tri.surface_mesh(surface_index));

    if (sub_tri.surface_mesh(surface_index)->num_cells() == 0)
      continue;

    for (dolfin::CellIterator c(*(sub_tri.surface_mesh(surface_index)));
         !c.end(); ++c)
    {
      const double v = c->volume();
      const dolfin::Point x_q = c->midpoint();

      // Generate consistent normal vector for this surface (points in
      // the f > 0 side)
      dolfin::Point normal = c->cell_normal();
      const dolfin::Point n0_point(coordinates[0][0],
                                   coordinates[0][1],
                                   coordinates[0][2]);
      const double H_n0 = domain.surface_side(n0_point, surface_index);
      const dolfin::Point node_vector = (n0_point - x_q)*H_n0;
      if (node_vector.dot(normal) < 0.0)
        normal *= -1.0;

      // Pressure loading (supplied by user)
      dolfin_assert(_pressure);
      const double p = (*_pressure)(x_q);

      // Tabulate basis functions at quadrature point
      std::vector<double> phi(num_nodes);
      _element->evaluate_basis_all(phi.data(), x_q.coordinates(),
                                   vertex_coords.data(), 0);

      // Loop over test function 'nodes'
      for (std::size_t node_i = 0; node_i < num_nodes; ++node_i)
      {
        // Spatial point of the 'test' node
        const dolfin::Point ni_point(coordinates[node_i][0],
                                     coordinates[node_i][1],
                                     coordinates[node_i][2]);

        // FIXME: Move this outside on loop?
        // Build list of trial dofs at 'node', with associated
        // enrichment values, at node i
        std::vector<std::pair<std::array<int, 2>, double>>
          local_dof_index_i;
        for (std::size_t rdof = 0; rdof < gdim; ++rdof)
        {
          // Get cell-wise index of regular dof
          const int I_reg = rdof*num_nodes + node_i;

          // Loop over enriched dofs associated with regular dof
          for (auto edof : dofmap->enriched_dofs(cell.index(), I_reg))
          {
            const int dof_surface0 = edof.second[0];
            const int dof_surface1 = edof.second[1];

            // (regular, enriched) dof pair
            std::array<int, 2> dof_pair = {{I_reg, edof.first}};

            // Store dof pair and sign for traction force
            if (dof_surface0 == dof_surface1)
            {
              if (dof_surface0 == static_cast<int>(surface_index))
              {
                const int side = domain.surface_side(ni_point, surface_index);
                local_dof_index_i.push_back(std::make_pair(dof_pair, side));
              }
              else
                local_dof_index_i.push_back(std::make_pair(dof_pair, 0.0));
              }
            else
            {
              if (dof_surface0 == static_cast<int>(surface_index))
              {
                const double side1 = domain.surface_side(x_q, dof_surface1);
                const double sign = 0.5*(side1 + std::abs(side1));
                local_dof_index_i.push_back(std::make_pair(dof_pair, sign));
              }
              else if (dof_surface1 == static_cast<int>(surface_index))
              {
                const double side0 = domain.surface_side(x_q, dof_surface0);
                const double sign = 0.5*(side0 + std::abs(side0));
                local_dof_index_i.push_back(std::make_pair(dof_pair, sign));
              }
              else
              {
//                dolfin::error("Oops, problem with surface/dof indices when assembly surface tractions.");
              }
            }
          }
        }

        // Loop over 'test' dofs at node i
        for (std::size_t i = 0; i < local_dof_index_i.size(); ++i)
        {
          // The 'regular' dof component, e.g. '0, 1 or 2' for 'x, y
          // or z' component dof
          const std::size_t i_component
            = local_dof_index_i[i].first[0]/num_nodes;

          // Get local matrix entry
          const std::size_t I = local_dof_index_i[i].first[1];

          const double sign = local_dof_index_i[i].second;

          dolfin_assert(I < element_dim);

          b[I] += v*phi[node_i]*p*(normal[i_component]*sign);
        }

      }
    }
  }

// --- Form element volume integrals

  // Loop over cell sub-simplices
  for (dolfin::CellIterator c(sub_tri.mesh()); !c.end(); ++c)
  {
    const double cell_ratio = c->volume()/cell.volume();
    if (cell_ratio < 1.0e-5)
    {
      std::cout << "WARNING: small cell volume ratio. Ratio, Cell index:"
                << cell_ratio << ", " << cell.index() << std::endl;
    }

    // Get polynomial degree
    const std::size_t order = (num_nodes == 10) ? 2 : 1;

    // Compute quadrature rule
    const auto quadrature_rule = dolfin::SimplexQuadrature::compute_quadrature_rule(*c, order);
    const std::vector<double>& quadrature_points = quadrature_rule.first;
    const std::vector<double>& quadrature_weights = quadrature_rule.second;

    // Loop over quadrature points
    for (std::size_t iq = 0; iq < quadrature_weights.size(); ++iq)
    {
      const double w_q = quadrature_weights[iq];
      const dolfin::Point x_q(quadrature_points[3*iq],
                              quadrature_points[3*iq + 1],
                              quadrature_points[3*iq + 2]);

      // Tabulate basis function derivatives at quadrature point
      std::vector<double> dphi_dx(num_nodes*gdim);
      _element->evaluate_basis_derivatives_all(1, dphi_dx.data(),
                                               x_q.coordinates(),
                                               vertex_coords.data(), 0);

      // Loop over test function 'nodes'
      for (std::size_t node_i = 0; node_i < num_nodes; ++node_i)
      {
        // Spatial point of the 'test' node
        const dolfin::Point ni_point(coordinates[node_i][0],
                                     coordinates[node_i][1],
                                     coordinates[node_i][2]);

        // FIXME: Move this outside on loop?
        // Build list of trial dofs at 'node', with associated
        // enrichment values, at node i
        std::vector<std::pair<std::array<int, 2>, double>>
          local_dof_index_i(gdim);

        // Loop over regular dofs at node
        for (std::size_t rdof = 0; rdof < gdim; ++rdof)
        {
          // Get cell-wise index of regular dof
          const int I_reg = rdof*num_nodes + node_i;

          // Set cell-wise index of regular dof
          local_dof_index_i[rdof].first[0] = I_reg;
          local_dof_index_i[rdof].first[1] = I_reg;
          local_dof_index_i[rdof].second   = 1.0;

          // Loop over enriched dofs associated with regular dof
          for (auto edof : dofmap->enriched_dofs(cell.index(), I_reg))
          {
            // Jump function for 'test' dof
            const double H = computeH(domain, edof.second, x_q, ni_point);
            std::array<int, 2> dof_pair = {{I_reg, edof.first}};
            local_dof_index_i.push_back(std::make_pair(dof_pair, H));
          }
        }

        // Test function (phi_I) derivatives
        const double* dphi_I_dx = &dphi_dx[node_i*gdim];

        // Loop over 'test' dofs at node i
        for (std::size_t i = 0; i < local_dof_index_i.size(); ++i)
        {
          // The 'regular' dof component, e.g. '0, 1 or 2' for 'x, y or z' component dof
          const std::size_t i_component = local_dof_index_i[i].first[0]/num_nodes;

          // Get local matrix entry
          std::size_t I = local_dof_index_i[i].first[1];

          // Jump function associated with trial function
          const double Hi = local_dof_index_i[i].second;

          // Loop over trial function 'nodes'
          for (std::size_t node_j = 0; node_j < num_nodes; ++node_j)
          {
            // Spatial point for the 'trial' node
            const dolfin::Point nj_point(coordinates[node_j][0],
                                         coordinates[node_j][1],
                                         coordinates[node_j][2]);

            // FIXME: Move this outside on loop?

            // Build list of trial dofs at this node, with associated
            // enrichment value, at node j
            std::vector<std::pair<std::array<int, 2>, double>>
              local_dof_index_j(3);
            for (std::size_t rdof = 0; rdof < gdim; ++rdof)
            {
              const int J_reg = rdof*num_nodes + node_j;
              local_dof_index_j[rdof].first[0] = J_reg;
              local_dof_index_j[rdof].first[1] = J_reg;
              local_dof_index_j[rdof].second   = 1.0;
              for (auto edof : dofmap->enriched_dofs(cell.index(), J_reg))
              {
                // Jump function for 'trial' dof
                const double H = computeH(domain, edof.second, x_q, nj_point);

                std::array<int, 2> dof_pair = {{J_reg, edof.first}};
                local_dof_index_j.push_back(std::make_pair(dof_pair, H));

                //if (edof.second[0] == edof.second[1])
                //  local_dof_index_j.push_back(std::make_pair(dof_pair, H_phi_nj0*H_phi_nj1));
                //else
                //  local_dof_index_j.push_back(std::make_pair(dof_pair, 0));

                /*
                const double H_q = domain.surface_side(x_q, edof.second[0]);
                const double H_phi_nj0
                = 0.5*std::abs(domain.surface_side(nj_point, edof.second[0]) - H_q);
                const double H_phi_nj1
                  = 0.5*std::abs(domain.surface_side(nj_point, edof.second[1]) - H_q);
                std::array<int, 2> dof_pair = {{J_reg, edof.first}};
                local_dof_index_j.push_back(std::make_pair(dof_pair, H_phi_nj0*H_phi_nj1));
                */

                /*
                if (edof.second[0] == edof.second[1])
                {
                  const double H_q0 = domain.surface_side(x_q, edof.second[0]);
                  const double H_phi_nj0
                    = 0.5*std::abs(domain.surface_side(nj_point, edof.second[0]) - H_q0);

                  std::array<int, 2> dof_pair = {{J_reg, edof.first}};
                  local_dof_index_j.push_back(std::make_pair(dof_pair, H_phi_nj0));
                }
                else
                {
                  const double H_q0  = domain.surface_side(x_q, edof.second[0])*domain.surface_side(x_q, edof.second[1]);
                  const double H_phi = domain.surface_side(nj_point, edof.second[0])*domain.surface_side(nj_point, edof.second[1]);
                  const double H_phi_nj0
                    = 0.5*std::abs(H_phi - H_q0);
                  std::array<int, 2> dof_pair = {{J_reg, edof.first}};
                  local_dof_index_j.push_back(std::make_pair(dof_pair, H_phi_nj0));
                }
                */
              }
            }

            // Trial function (phi_J) derivatives
            const double* dphi_J_dx = &dphi_dx[node_j*gdim];

            // Loops over dofs at node j, and add contributions to
            // element matrix
            for (std::size_t j = 0; j < local_dof_index_j.size(); ++j)
            {
              // The 'regular' dof component, e.g. '0, 1 or 2' for 'x, y or z' component dof
              const std::size_t j_component = local_dof_index_j[j].first[0]/num_nodes;

              // Get local matrix entry
              std::size_t J = local_dof_index_j[j].first[1];

              // Jump function associated with trial function
              double Hj = local_dof_index_j[j].second;

              // Add contribution to A(I, J)
              Ae(I, J) += compute_matrix_entry(i_component, j_component, gdim,
                                               Hi, Hj,
                                               mu, lambda, w_q,
                                               dphi_I_dx, dphi_J_dx);
            }
          }
        }
      }
    }
  }
}
//-----------------------------------------------------------------------------
double ElasticityProblem3D::compute_matrix_entry(int i_regular, int j_regular,
                                                 int gdim,
                                                 double Hi, double Hj,
                                                 double mu, double lambda,
                                                 double weight,
                                                 const double* dphi_I_dx,
                                                 const double* dphi_J_dx)
{
  // Loop over spatial dim (dN/dx0, dN/dx1, dN/dx2)
  double entry = 0.0;
  for (int d0 = 0; d0 < gdim; ++d0)
  {
    if (i_regular == j_regular)
    {
      if (i_regular == d0)
      {
        // dNi/dxi - dNi/dxi (N(i, i) N(i, i))
        entry +=  weight*Hi*dphi_I_dx[d0]*(2.0*mu*Hj*dphi_J_dx[d0]
                                        + lambda*Hj*dphi_J_dx[d0]);
      }
      else
      {
        // dNi/dxj - dNi/dxj (N(i, j) N(i, j)), i \ne j
        entry += 0.5*weight*Hi*dphi_I_dx[d0]*2.0*mu*Hj*dphi_J_dx[d0];
      }
    }
    else if (i_regular == d0)
    {
      // dNi/dxi - dNj/dxj (N(i, i) N(j, j)), i \ne j
      for (int d1 = 0; d1 < gdim; ++d1)
      {
        if (j_regular == d1)
        {
          entry += weight*Hi*dphi_I_dx[d0]*lambda*Hj*dphi_J_dx[d1];
        }
      }
    }
    else if (j_regular == d0)
    {
      // dNi/dxj - dNj/dxi (N(i, j) N(j, i))
      entry += 0.5*weight*Hi*dphi_I_dx[d0]*2.0*mu*Hj*dphi_J_dx[i_regular];
    }
  }

  return entry;
}
//-----------------------------------------------------------------------------
void ElasticityProblem3D::evaluate(
  dolfin::Array<double>& values,
  const dolfin::Point& x_eval,
  const dolfin::Point& x_eval_heaviside,
  const double* const coefficients,
  const dolfin::Cell& cell,
  const std::vector<double>& vertex_coords,
  const Evaluator::Mode evalMode,
  const int surface_index
) const
{
  // Get geometric dimension
  dolfin_assert(_element);
  const std::size_t gdim = _element->geometric_dimension();

  // Get number of element 'nodes'
  const std::size_t num_nodes = _element->space_dimension();

  // Get number if regular dofs (regular element dim)
  const std::size_t element_dim_regular = num_nodes*gdim;

  // Get dofmap
  std::shared_ptr<const narwal::DofMap> dofmap = function_space()->dofmap();
  dolfin_assert(dofmap);

  // Dispatch to appropriate tensor tabulate function
  if (dofmap->cell_dofs(cell.index()).size() == element_dim_regular) {
//    integral.tabulate_tensor(A, coefficients, vertex_coords.data(), 1);
  } else {
    evaluate_enriched(values, x_eval, x_eval_heaviside, coefficients, cell, vertex_coords, evalMode, surface_index);
  }
}

void ElasticityProblem3D::fill(
  std::vector<std::pair<std::array<int, 2>, double>>& local_dof_index_i,
  const dolfin::Point x,
  const std::size_t node_i,
  const boost::multi_array<double, 2>& coordinates,
  const dolfin::Cell& cell
  ) const
{

  const DomainManager& domain = *function_space()->domain();

  // Get dofmap
  std::shared_ptr<const narwal::DofMap> dofmap = function_space()->dofmap();
  dolfin_assert(dofmap);

  // Get geometric dimension
  dolfin_assert(_element);
  const std::size_t gdim = _element->geometric_dimension();

  // Get number of element 'nodes'
  const std::size_t num_nodes = _element->space_dimension();

  // Spatial point of the 'test' node
  const dolfin::Point ni_point(coordinates[node_i][0],
                               coordinates[node_i][1],
                               coordinates[node_i][2]);

  // Loop over regular dofs at node
  for (std::size_t rdof = 0; rdof < gdim; ++rdof)
  {
    // Get cell-wise index of regular dof
    const int I_reg = rdof*num_nodes + node_i;

    // Set cell-wise index of regular dof
    local_dof_index_i[rdof].first[0] = I_reg;
    local_dof_index_i[rdof].first[1] = I_reg;
    local_dof_index_i[rdof].second   = 1.0;

    // Loop over enriched dofs associated with regular dof
    for (auto edof : dofmap->enriched_dofs(cell.index(), I_reg))
    {
      // Jump function for 'test' dof
      double H = computeH(domain, edof.second, x, ni_point);

      std::array<int, 2> dof_pair = {{I_reg, edof.first}};
      local_dof_index_i.push_back(std::make_pair(dof_pair, H));
    }
  }
}


void ElasticityProblem3D::fillWithGivenSide(
  std::vector<std::pair<std::array<int, 2>, double>>& local_dof_index_i,
  const std::size_t node_i,
  const boost::multi_array<double, 2>& coordinates,
  const dolfin::Cell& cell,
  const int surface_index,
  const double H_side
  ) const
{

  const DomainManager& domain = *function_space()->domain();

  // Get dofmap
  std::shared_ptr<const narwal::DofMap> dofmap = function_space()->dofmap();
  dolfin_assert(dofmap);

  // Get geometric dimension
  dolfin_assert(_element);
  const std::size_t gdim = _element->geometric_dimension();

  // Get number of element 'nodes'
  const std::size_t num_nodes = _element->space_dimension();

  // Spatial point of the 'test' node
  const dolfin::Point ni_point(coordinates[node_i][0],
                               coordinates[node_i][1],
                               coordinates[node_i][2]);

  // Loop over regular dofs at node
  for (std::size_t rdof = 0; rdof < gdim; ++rdof)
  {
    // Get cell-wise index of regular dof
    const int I_reg = rdof*num_nodes + node_i;

    // Set cell-wise index of regular dof
    local_dof_index_i[rdof].first[0] = I_reg;
    local_dof_index_i[rdof].first[1] = I_reg;
    local_dof_index_i[rdof].second   = 1.0;

    // Loop over enriched dofs associated with regular dof
    for (auto edof : dofmap->enriched_dofs(cell.index(), I_reg))
    {
      // Jump function for 'test' dof, the position w.r.t to the second interface does not matter
      const auto enrLabel = edof.second;
      double H = 1.0;
      for (std::size_t i = 0; i < enrLabel.size(); ++i) {
        const SurfaceId surfIdi = enrLabel[i];
        const double H_qi = (surface_index == surfIdi) ? H_side : 1.0;;
        const double H_phi_nji
          = 0.5*std::abs(domain.surface_side(ni_point, surfIdi) - H_qi);
        H *= H_phi_nji;
      }
      std::array<int, 2> dof_pair = {{I_reg, edof.first}};
      local_dof_index_i.push_back(std::make_pair(dof_pair, H));
    }
  }
}

void ElasticityProblem3D::computeValues(
  dolfin::Array<double>& values,
  const std::vector<std::pair<std::array<int, 2>, double>>& local_dof_index_i,
  const double N_node_i,
  const double* const coefficients
  ) const
{
  // Get number of element 'nodes'
  const std::size_t num_nodes = _element->space_dimension();

  // Loop over 'test' dofs at node i
  for (std::size_t iDofPerTestNode = 0; iDofPerTestNode < local_dof_index_i.size(); ++iDofPerTestNode)
  {
    // The 'regular' dof component, e.g. '0, 1 or 2' for 'x, y or z' component dof
    const std::size_t i_component = local_dof_index_i[iDofPerTestNode].first[0]/num_nodes;

    // Get local matrix entry
    std::size_t I = local_dof_index_i[iDofPerTestNode].first[1];

    // Jump function associated with trial function
    const double Hi = local_dof_index_i[iDofPerTestNode].second;

    values[i_component] += Hi * N_node_i * coefficients[I];
  }
}

//-----------------------------------------------------------------------------
void ElasticityProblem3D::evaluate_enriched(
  dolfin::Array<double>& values,
  const dolfin::Point& x_eval,
  const dolfin::Point& x_eval_heaviside,
  const double* const coefficients,
  const dolfin::Cell& cell,
  const std::vector<double>& vertex_coords,
  const Evaluator::Mode evalMode,
  const int surface_index
  ) const
{
  // Get DomainManager object
  dolfin_assert(function_space());
  dolfin_assert(function_space()->domain());
  const DomainManager& domain = *function_space()->domain();

  // Get dofmap
  std::shared_ptr<const narwal::DofMap> dofmap = function_space()->dofmap();
  dolfin_assert(dofmap);

  auto cell_dofs = dofmap->cell_dofs(cell.index());

  // Get element dim
  const std::size_t gdim = _element->geometric_dimension();
  //const std::size_t element_dim = dofmap->cell_dofs(cell.index()).size();

  // Get number of element 'nodes'
  const std::size_t num_nodes = _element->space_dimension();

  // Get number of regular dofs (regular element dim)
  const std::size_t element_dim_regular = num_nodes*gdim;

  // Tabulate physical coordinates of all dofs on this cell
  dolfin_assert(function_space()->function_space()->dofmap());
  boost::multi_array<double, 2> coordinates;
  _element->tabulate_dof_coordinates(coordinates, vertex_coords, cell);

  // Get list of indices of surfaces that are active for this cell
  std::set<int> surface_list;
  for (std::size_t i = 0; i < element_dim_regular; ++i)
  {
    const std::vector<std::pair<int, EnrLabelArray>>& edof_data
      = dofmap->enriched_dofs(cell.index(), i);
    for (auto edof : edof_data)
      surface_list.insert(edof.second.begin(), edof.second.end());
  }

  std::vector<int> local_to_global_surface_index;
  std::vector<std::shared_ptr<const narwal::GenericSurface>> surfaces(domain.surfaces().size());
  for (auto surface_index : surface_list)
  {
    local_to_global_surface_index.push_back(surface_index);
    surfaces.push_back(domain.surface(surface_index));
  }

  // Sub-triangulate cell
//  narwal::SubTriangulation sub_tri(cell, domain.surfaces());


  // --- Apply pressure load (surface integral)


  // --- Form element volume integrals



    // Get polynomial degree
//    const std::size_t order = (num_nodes == 10) ? 2 : 1;

//    // Compute quadrature rule
//    const auto quadrature_rule = dolfin::SimplexQuadrature::compute_quadrature_rule(*c, order);
//    const std::vector<double>& quadrature_points = quadrature_rule.first;
//    const std::vector<double>& quadrature_weights = quadrature_rule.second;

//    // Loop over quadrature points
//    for (std::size_t iq = 0; iq < quadrature_weights.size(); ++iq)
//    {
//      const dolfin::Point x_q(quadrature_points[3*iq],
//                              quadrature_points[3*iq + 1],
//                              quadrature_points[3*iq + 2]);

      // Tabulate basis function derivatives at quadrature point
//      std::vector<double> dphi_dx(num_nodes*gdim);
//      _element->evaluate_basis_derivatives_all(1, dphi_dx.data(),
//                                               x_q.coordinates(),
//                                               vertex_coords.data(), 0);
    std::vector<double> dphi_dx(num_nodes);
    _element->evaluate_basis_all(dphi_dx.data(),
      x_eval.coordinates(),
      vertex_coords.data(),
      0);

    if (evalMode == Evaluator::domain) {
      // Loop over test function 'nodes'
      for (std::size_t node_i = 0; node_i < num_nodes; ++node_i)
      {
        // FIXME: Move this outside on loop?
        // Build list of trial dofs at 'node', with associated
        // enrichment values, at node i
        std::vector<std::pair<std::array<int, 2>, double>>
          local_dof_index_i(3);

        fill(local_dof_index_i, x_eval_heaviside, node_i, coordinates, cell);

        // Test function (N_I)
        const double N_node_i = dphi_dx[node_i];

        computeValues(values, local_dof_index_i, N_node_i, coefficients);
      }
    } else {

      // Loop over test function 'nodes', compute left side
      for (std::size_t node_i = 0; node_i < num_nodes; ++node_i)
      {
        // FIXME: Move this outside on loop?
        // Build list of trial dofs at 'node', with associated
        // enrichment values, at node i
        std::vector<std::pair<std::array<int, 2>, double>>
          local_dof_index_i(3);

        fillWithGivenSide(local_dof_index_i, node_i, coordinates, cell, surface_index, 1);

        // Test function (N_I)
        const double N_node_i = dphi_dx[node_i];

        computeValues(values, local_dof_index_i, N_node_i, coefficients);
      }
      //std::cout << values << std::endl;

      dolfin::Array<double> gap(values.size());
      for (std::size_t i = 0; i < gap.size(); ++i) {
        gap[i] = 0.0;
      }

      // Loop over test function 'nodes', compute right side
      for (std::size_t node_i = 0; node_i < num_nodes; ++node_i)
      {
        // FIXME: Move this outside on loop?
        // Build list of trial dofs at 'node', with associated
        // enrichment values, at node i
        std::vector<std::pair<std::array<int, 2>, double>>
          local_dof_index_i(3);

        fillWithGivenSide(local_dof_index_i, node_i, coordinates, cell, surface_index, -1);

        // Test function (N_I)
        const double N_node_i = dphi_dx[node_i];

        computeValues(gap, local_dof_index_i, N_node_i, coefficients);
      }
      //std::cout << gap << std::endl;
      for (std::size_t i = 0; i < gap.size(); ++i) {
        values[i] -= gap[i];
      }

      if (evalMode == Evaluator::interface_normal) {
        //dolfin::info("compute normal displacement");
        // produce vector with normal displacement
        const std::vector<double> n =
            domain.surface(surface_index)->normal(x_eval, 1.0e-7);

        // get length of normal components
        const double length = n[0]*values[0] + n[1]*values[1] + n[2]*values[2];

        // multiply length with the normal unit vector
        values[0] = length * n[0];
        values[1] = length * n[1];
        values[2] = length * n[2];
      }
      //std::cout << " " << values << std::endl;
    }

}
//-----------------------------------------------------------------------------
