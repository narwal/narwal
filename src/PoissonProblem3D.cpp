// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "PoissonProblem3D.h"

#include <iomanip>

#include <dolfin/function/Function.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/geometry/BoundingBoxTree.h>
#include <dolfin/geometry/Point.h>
#include <dolfin/mesh/Cell.h>
#include <dolfin/mesh/Mesh.h>
#include <dolfin/mesh/Vertex.h>

#include "DomainManager.h"
#include "Poisson3D_P1.h"
#include "Poisson3D_P2.h"
#include "ImplicitSurface.h"
#include "SubTriangulation.h"
#include "XFEMForms.h"
#include "XFEMFunctionSpace.h"
#include "XFEMTools.h"

using namespace narwal;

#define PLOTME(a) std::cout << std::setw(30) << #a << "   = " << a << std::endl;

//-----------------------------------------------------------------------------
PoissonProblem3D::PoissonProblem3D(
  std::shared_ptr<const narwal::XFEMFunctionSpace> V,
  const int polynomialOrder)
  :  narwal::XFEMForms(V, polynomialOrder)
{
  // Create regular forms
  std::shared_ptr<dolfin::Form> a, L;
  switch (polynomialOrder) {
  case 1:
  {
    a = std::make_shared<Poisson3D_P1::BilinearForm>(
        V->function_space(), V->function_space());
    L = std::make_shared<Poisson3D_P1::LinearForm>(V->function_space());
    break;
  }
  case 2:
  {
    a = std::make_shared<Poisson3D_P2::BilinearForm>(
        V->function_space(), V->function_space());
    L = std::make_shared<Poisson3D_P2::LinearForm>(V->function_space());
    break;
  }
  default:
    std::cout << "PoissonProblem3D::PoissonProblem3D(...): "
      << "unknown polynomial degree " << std::endl;
    abort();
  }

  this->set_a(a);
  this->set_L(L);
}
//-----------------------------------------------------------------------------
PoissonProblem3D::~PoissonProblem3D()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
void
PoissonProblem3D::tabulate_tensor(double* A,
                                     double* b,
                                     const double* const * coefficients,
                                     const dolfin::Cell& cell,
                                     const std::vector<double>& vertex_coords,
                                     const ufc::cell_integral& integral) const
{
  dolfin_assert(function_space());
  dolfin_assert(function_space()->domain());
  const DomainManager& domain = *function_space()->domain();

  // Check if cell is intersected
  const int surface_index = domain.intersecting_surface(cell);

  // Handle intersected cells
  if (surface_index != -1)
  {
    const std::pair<bool, bool> intersected
      = domain.entity_intersection(cell, surface_index);
    dolfin_assert(intersected.first);

    const bool edge_cell = intersected.second;
    if (!edge_cell)
    {
      tabulate_tensor_enriched(A, b, coefficients, cell, vertex_coords,
                               surface_index);
    }
    else
    {
      // Tabulate tensor
      integral.tabulate_tensor(A, NULL, vertex_coords.data(), 1);
    }
  }
  else
  {
    // Tabulate regular tensor
    //std::cout << "Tabulating regular tensor" << std::endl;
    integral.tabulate_tensor(A, NULL, vertex_coords.data(), 1);
  }
}
//-----------------------------------------------------------------------------
void PoissonProblem3D::tabulate_tensor_enriched(
  double* A,
  double* b,
  const double* const * coefficients,
  const dolfin::Cell& cell,
  const std::vector<double>& vertex_coordinates,
  const int surface_index) const
{
  dolfin_assert(function_space());
  dolfin_assert(function_space()->domain());
  const DomainManager& domain = *function_space()->domain();

  // Get dofmap
  //  dolfin_assert(function_space()->function_space()->dofmap());
  //  const dolfin::GenericDofMap& dofmap
  //      = *(function_space()->function_space()->dofmap());

  // Get geometric dimension of element
  dolfin_assert(a()->function_space(0)->element());
  const std::size_t gdim = a()->function_space(0)->element()->geometric_dimension();

  // Get element dim
  //const std::size_t element_dim = dofmap.cell_dofs(cell.index()).size();
  const std::size_t nElemNodes = getNumElemNodes(polynomial_order());
  const std::size_t nMaxNumberOfEnrichments = 2; // includes regular dofs
  const std::size_t nMaxElementDofs = nElemNodes*nMaxNumberOfEnrichments;

  // Element
  dolfin_assert(function_space()->function_space()->element());
  const dolfin::FiniteElement& element
    = *function_space()->function_space()->element();

  // Tabulate coordinates of all dofs on this cell
  boost::multi_array<double, 2> coordinates;
  element.tabulate_dof_coordinates(coordinates, vertex_coordinates, cell);

  // Triangulate cell
  narwal::SubTriangulation sub_tri = domain.triangulate(cell, surface_index);

  // Zero tensor
  std::fill(A, A + nMaxElementDofs*nMaxElementDofs, 0.0);
  std::fill(b, b + nMaxElementDofs, 0.0);

  // Loop over sub-simplex quadrature points
  for (dolfin::CellIterator c(sub_tri.mesh()); !c.end(); ++c)
  {
    double v = c->volume();
    const dolfin::Point pbar = c->midpoint();

    // Compute basis function derivatives at quadrature point
    std::vector<double> dphi_dx(nElemNodes*gdim);
    element.evaluate_basis_derivatives_all(1, dphi_dx.data(),
                                           pbar.coordinates(),
                                           vertex_coordinates.data(),
                                           0);

    // Determine side of quadrature point relative to surface
    const double H_q = domain.surface_side(pbar, 0);

    // Loop over test functions (phi_i)
    for (std::size_t i = 0; i < nMaxElementDofs; ++i)
    {
      // 'Regular' index
      const std::size_t i_regular = i % nElemNodes;

      // Evaluate Heaviside for phi_i
      double H_phi_i = 1.0;
      if (i >= nElemNodes)
      {
        const dolfin::Point i_point(coordinates[i_regular][0],
                                    coordinates[i_regular][1],
                                    coordinates[i_regular][2]);
        H_phi_i = 0.5*std::abs((domain.surface_side(i_point, 0) - H_q));
      }

      // Loop over trial functions (phi_j)
      for (std::size_t j = 0; j < nMaxElementDofs; ++j)
      {
        // 'Regular' index
        const std::size_t j_regular = j % nElemNodes;

        double H_phi_j = 1.0;
        if (j >= nElemNodes)
        {
          const dolfin::Point j_point(coordinates[j_regular][0],
                                      coordinates[j_regular][1],
                                      coordinates[j_regular][2]);
          H_phi_j = 0.5*std::abs((domain.surface_side(j_point, 0) - H_q));
        }

        // Add contribution to element matrix (i, j)
        A[i*nMaxElementDofs + j]
          += v*H_phi_i*H_phi_j*(dphi_dx[i_regular*3+0]*dphi_dx[j_regular*3+0]
                              + dphi_dx[i_regular*3+1]*dphi_dx[j_regular*3+1]
                              + dphi_dx[i_regular*3+2]*dphi_dx[j_regular*3+2]);
      }
    }
  }
}
//-----------------------------------------------------------------------------
