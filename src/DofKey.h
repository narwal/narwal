// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2015-11
// Last changed:

#ifndef NARWAL_DOF_KEY_H
#define NARWAL_DOF_KEY_H

#include <dolfin/common/types.h>

namespace narwal
{

  typedef short int SurfaceId;                      // surface id type
  typedef std::array<SurfaceId, 3> EnrLabelArray;   // enriching surfaces pair
  typedef std::tuple<bool,bool,bool> DomLabel; // plus or minus side for 2 surfaces
  typedef dolfin::la_index DofEleID;  // element local dof index
  typedef dolfin::la_index DofGID;    // global dof index
  typedef dolfin::la_index RegDofGID; // regular global dof index

}

#endif
