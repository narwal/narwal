// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "SubTriangulation.h"

#include <dolfin/mesh/Cell.h>
#include <dolfin/mesh/Edge.h>
#include <dolfin/mesh/Facet.h>
#include <dolfin/mesh/MeshEditor.h>
#include <dolfin/mesh/Vertex.h>
#include <dolfin/refinement/PlazaRefinementND.h>

#include "GenericSurface.h"

using namespace narwal;

//-----------------------------------------------------------------------------
SubTriangulation::SubTriangulation(const dolfin::Mesh& mesh,
                                   const narwal::GenericSurface& surface)
  : _submesh(mesh), _surfmesh(1), _ranges(1, mesh.num_vertices())
{
  build(surface);
  extract_surface_mesh(0);
}
//-----------------------------------------------------------------------------
SubTriangulation::SubTriangulation(const dolfin::Cell& cell,
                                   const narwal::GenericSurface& surface)
  : _surfmesh(1), _ranges(1, 4)
{
  boost::multi_array<double, 2> geometry (boost::extents[4][3]);
  std::vector<std::size_t> topology = {0, 1, 2, 3};

  unsigned int i = 0;
  for (dolfin::VertexIterator v(cell); !v.end(); ++v)
  {
    std::copy(v->x(), v->x() + 3, geometry[i].begin());
    ++i;
  }

  create_mesh(_submesh, geometry, topology, 3);

  build(surface);
  extract_surface_mesh(0);
}
//-----------------------------------------------------------------------------
SubTriangulation::SubTriangulation(const dolfin::Cell& cell,
  const std::vector<std::shared_ptr<const narwal::GenericSurface>>& surfaces)
  : _surfmesh(surfaces.size()), _ranges(1, 4)
{
  dolfin_assert(surfaces.size() > 0);

  boost::multi_array<double, 2> geometry(boost::extents[4][3]);
  std::vector<std::size_t> topology = {0, 1, 2, 3};

  unsigned int i = 0;
  for (dolfin::VertexIterator v(cell); !v.end(); ++v)
  {
    std::copy(v->x(), v->x() + 3, geometry[i].begin());
    ++i;
  }

  create_mesh(_submesh, geometry, topology, 3);

  for (unsigned int j = 0; j != surfaces.size(); ++j)
    build(*surfaces[j]);

  for (unsigned int j = 0; j != surfaces.size(); ++j)
    extract_surface_mesh(j);
}
//-----------------------------------------------------------------------------
void SubTriangulation::build(const narwal::GenericSurface& surface)
{
  // Number of existing surface cuts through volume
  // giving index for this new surface cut
  const std::size_t new_surface = _ranges.size() - 1;

  // Assign new vertices where surface cuts mesh Edges, and store their
  // coordinates
  std::map<std::size_t, std::size_t> edge_to_vertex;

  std::vector<double> new_geometry(_submesh.coordinates().begin(),
                                   _submesh.coordinates().end());
  std::size_t count = _submesh.num_vertices();
  _submesh.init(1, 0);
  for (dolfin::EdgeIterator e(_submesh); !e.end(); ++e)
  {
    if (surface.entity_intersection(*e).first)
    {
      const dolfin::Point p = surface.compute_intersection(*e);
      new_geometry.insert(new_geometry.end(),
                          p.coordinates(), p.coordinates() + 3);
      edge_to_vertex[e->index()] = count;
      for (unsigned int i = 0; i != new_surface; ++i)
      {
        if (vertex_on_surface(e->entities(0)[0], i) and
            vertex_on_surface(e->entities(0)[1], i))
        {
          // Both vertices of this edge lie on a pre-existing surface,
          // so need to add this new vertex to that surface too.
          _vertex_to_surface[count] = i;
        }
      }

      ++count;
    }
  }

  boost::multi_array_ref<double, 2> geometry(new_geometry.data(),
                                         boost::extents
                                         [count][3]);

  std::vector<std::size_t> topology;
  std::vector<std::size_t> local_map(10);
  for (dolfin::CellIterator cell(_submesh); !cell.end(); ++cell)
  {
    // Create local mapping for this cell - add existing vertices
    for (unsigned int i = 0; i != 4; ++i)
      local_map[i] = cell->entities(0)[i];

    // Also add vertices where edges are cut by surface
    std::vector<bool> marked_edges(6);
    for (unsigned int i = 0; i != 6; ++i)
    {
      const std::size_t edge_idx = cell->entities(1)[i];
      auto map_it = edge_to_vertex.find(edge_idx);
      if(map_it != edge_to_vertex.end())
      {
        local_map[4 + i] = map_it->second;
        marked_edges[i] = true;
      }
    }

    // Edges on each facet in cell local indexing
    static std::size_t facet_edge[4][3]
      = { {1, 2, 0},
          {3, 4, 0},
          {3, 5, 1},
          {4, 5, 2} };

    std::vector<std::size_t> facet_key_edge;
    for (std::size_t i = 0; i != 4; ++i)
    {
      std::size_t max_idx = 0;
      double max_len = 0.0;
      bool max_mark = false;

      for (unsigned int j = 0; j != 3; ++j)
      {
        std::size_t e_idx = facet_edge[i][j];
        double e_len = dolfin::Edge(_submesh,
                                    cell->entities(1)[e_idx]).length();
        bool e_mark = marked_edges[e_idx];

        if (e_mark > max_mark)
        {
          max_mark = e_mark;
          max_len = e_len;
          max_idx = e_idx;
        }
        else if (e_mark == max_mark && e_len > max_len)
        {
          max_len = e_len;
          max_idx = e_idx;
        }
      }
      facet_key_edge.push_back(max_idx);
    }

    std::vector<std::size_t> tetrahedra;
    dolfin::PlazaRefinementND::get_simplices(tetrahedra, marked_edges,
                                             facet_key_edge, 3, false);

    // Add to topology, mapping to the new local indexing
    for (const auto &p : tetrahedra)
      topology.push_back(local_map[p]);

  }

  create_mesh(_submesh, geometry, topology, 3);

  // Mark end of new surface in _submesh vertices
  _ranges.push_back(_submesh.num_vertices());
}
//-----------------------------------------------------------------------------
bool SubTriangulation::vertex_on_surface(std::size_t vertex_index,
                                         std::size_t surface_index)
{
  dolfin_assert((surface_index + 1) < _ranges.size());
  // Ignore all original, non-surface vertices
  if (vertex_index < _ranges[0])
    return false;
  // Check for vertices naturally created by surface intersection
  if (vertex_index >= _ranges[surface_index]
      and vertex_index < _ranges[surface_index + 1])
    return true;
  // Check for vertices assigned to a surface by a second intersection
  auto it = _vertex_to_surface.find(vertex_index);
  return (it != _vertex_to_surface.end() and it->second == surface_index);
}
//-----------------------------------------------------------------------------
void SubTriangulation::extract_surface_mesh(std::size_t surface_index)
{
  std::vector<std::size_t> topology;

  // look for Facets with all vertices on required mesh
  for (dolfin::FacetIterator f(_submesh); !f.end(); ++f)
  {
    int scount = 0;
    for (dolfin::VertexIterator v(*f); !v.end(); ++v)
    {
      if (vertex_on_surface(v->index(), surface_index))
        scount++;
    }
    if (scount == 3)
    {
      topology.insert(topology.end(), f->entities(0),
                      f->entities(0) + 3);
    }
  }

  // Find set of points which are in topology and remap
  std::set<std::size_t> topo_set(topology.begin(), topology.end());
  std::map<std::size_t, std::size_t> vol_to_surf_vertex;
  boost::multi_array<double, 2> surf_geom_points(boost::extents
                                                 [topo_set.size()][3]);
  std::size_t vidx = 0;
  for (auto p : topo_set)
  {
    const auto pt = _submesh.coordinates().begin() + p*3;
    std::copy(pt, pt + 3,
              surf_geom_points[vidx].begin());
    vol_to_surf_vertex[p] = vidx;
    ++vidx;
  }

  // Remap
  for (auto &p : topology)
    p = vol_to_surf_vertex[p];

  // Add new surface mesh to SubTriangulation
  std::shared_ptr<dolfin::Mesh> new_surf(new dolfin::Mesh);
  create_mesh(*new_surf, surf_geom_points, topology, 2);
  dolfin_assert(surface_index < _surfmesh.size());
  _surfmesh[surface_index] = new_surf;
}
//-----------------------------------------------------------------------------
void SubTriangulation::create_mesh(
  dolfin::Mesh& mesh,
  const boost::multi_array<double, 2>& geometry,
  const std::vector<std::size_t>& topology,
  std::size_t tdim)
{
  dolfin::MeshEditor ed;

  ed.open(mesh, tdim, 3);

  ed.init_vertices(geometry.size());
  unsigned int i = 0;
  for (auto p : geometry)
  {
    ed.add_vertex(i, p[0], p[1], p[2]);
    ++i;
  }

  ed.init_cells(topology.size()/(tdim + 1));
  i = 0;
  for (auto t = topology.begin(); t != topology.end(); t += (tdim + 1))
  {
    if (tdim == 3)
      ed.add_cell(i, *t, *(t+1), *(t+2), *(t+3));
    else if (tdim == 2)
      ed.add_cell(i, *t, *(t+1), *(t+2));
    ++i;
  }

  ed.close();
}
//-----------------------------------------------------------------------------
