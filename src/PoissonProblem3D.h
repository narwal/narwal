// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_POISSONPROBLEM3D_ENRICHED_H
#define NARWAL_POISSONPROBLEM3D_ENRICHED_H

#include <memory>
#include <utility>
#include <vector>
#include <iostream>

#include "XFEMForms.h"
#include "Evaluator.h"

namespace dolfin
{
  class Cell;
  class FiniteElement;
  class Function;
  class Mesh;
  template<typename T> class Array;
}

namespace ufc
{
  class cell;
  class cell_integral;
}

namespace narwal
{

  /// Poisson Problem in 3D with P1 elements

  class PoissonProblem3D : public narwal::XFEMForms
  {

    //class DomainManager;
    class ImplicitSurface;

  public:

    /// Constructor
    PoissonProblem3D(std::shared_ptr<const narwal::XFEMFunctionSpace> V,
                     const int polynomialDegree);

    /// Destructor
    ~PoissonProblem3D();

    /// Tabulate element lhs matrix and rhs vector
    void tabulate_tensor(double* A, double* b,
                         const double* const * coefficients,
                         const dolfin::Cell& cell,
                         const std::vector<double>& vertex_coordinates,
                         const ufc::cell_integral& cell_integral) const;

    /// Tabulate tensor
    void evaluate(
      dolfin::Array<double>& values,
      const dolfin::Point& x_eval,
      const dolfin::Point& x_eval_heaviside,
      const double* const coefficients,
      const dolfin::Cell& cell,
      const std::vector<double>& vertex_coords,
      const Evaluator::Mode evalMode,
      const int surface_index
    ) const
    {
      std::cout << "Not implemented, yet!" << std::endl;
      abort();
    }

  private:

    // Tabulate tensor for enriched cells
    void
      tabulate_tensor_enriched(double* A,
                               double* b,
                               const double* const * coefficients,
                               const dolfin::Cell& cell,
                               const std::vector<double>& vertex_coords,
                               const int surface_index) const;

  };
}

#endif
