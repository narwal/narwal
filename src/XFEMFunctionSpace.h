// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_XFEMFUNCTIONSPACE_H
#define NARWAL_XFEMFUNCTIONSPACE_H

#include <memory>
#include <vector>

namespace ufc
{
  //class cell;
}

namespace dolfin
{
  class FunctionSpace;

}

namespace narwal
{
  class DofMap;
  class DomainManager;
  class FiniteElement;
  class FunctionSpace;
  class GenericSurface;
  class ImplicitSurface;

  /// This class implements an XFEM 'function space' interface

  class XFEMFunctionSpace
  {
  public:

    // FIXME: Remove ufc::finite_element argument. Issue is that
    //dolfin::FiniteElement does not expose ufc::finite_element
    /// Constructor
    XFEMFunctionSpace(
      std::shared_ptr<const dolfin::FunctionSpace> Vs,
      std::shared_ptr<const narwal::DomainManager> domain,
      std::size_t max_element_dim);

    /// Destructor
    ~XFEMFunctionSpace();

    /// Return (enriched) DOLFIN function space (const version)
    std::shared_ptr<const dolfin::FunctionSpace> function_space() const;

    /// Return (enriched) DOLFIN function space
    std::shared_ptr<dolfin::FunctionSpace> function_space();

    /// Return domain manager
    std::shared_ptr<const narwal::DomainManager> domain() const;

    /// Return Narwal dofmap
    std::shared_ptr<const narwal::DofMap> dofmap() const;

    /// The dimension of the function space
    std::size_t dim() const;

  private:

    // The regular function space
    std::shared_ptr<const dolfin::FunctionSpace> _Vs;

    // The enriched function space
    std::shared_ptr<dolfin::FunctionSpace> _Ve;

    // The domain manager object that holds mesh/surface topological
    // data
    std::shared_ptr<const DomainManager> _domain_manager;

  };

}

#endif
