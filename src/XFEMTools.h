// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_XFEMTOOLS_H
#define NARWAL_XFEMTOOLS_H

#include <array>
#include <set>
#include <vector>
#include <memory>

#include "DofKey.h"

namespace dolfin
{
  template <typename T> class CellFunction;
  template <typename T> class FacetFunction;
  class Cell;
  class DomainManager;
  class FunctionSpace;
  class GenericDofMap;
  class Mesh;
  class Facet;
}

namespace narwal
{
  class DomainManager;
  class GenericSurface;

  /// A variety of tools that are useful for XFEM methods

  class XFEMTools
  {
  public:

    /// For each regular dof, compute set of surfce interaction pairs
    /// for associated enrichment, e.g. enriched_dofs[reg_dof] = {
    /// (0,0), (0,1), (1,1) } for reg_dof has enriched dofs
    /// representing the interaction of surfaces 0 and 1.
    ///
    /// NOTE: only dofs of fully intersected cells are enriched, and
    /// tip dofs are 'deactivated'
    static void compute_enriched_dof_markers(
      std::vector<std::set<EnrLabelArray>>& enriched_dofs,
      std::vector<std::set<EnrLabelArray>>& enriched_cells,
      const dolfin::FunctionSpace& V,
      const DomainManager& domain);

  private:

    static std::vector<std::set<int>>
      compute_tip_dofs(std::shared_ptr<const dolfin::Mesh> mesh,
                       const DomainManager& domain,
                       const dolfin::GenericDofMap& dofmap);

  };

}

#endif
