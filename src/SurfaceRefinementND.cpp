// Copyright (C) 2016 Chris Richardson
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.
//
// First Added: 2014-05-21

#include "SurfaceRefinementND.h"

#include <vector>
#include <set>
#include <map>

#include <dolfin/common/Timer.h>
#include <dolfin/mesh/Mesh.h>
#include <dolfin/mesh/MeshEditor.h>
#include <dolfin/mesh/MeshEntityIterator.h>
#include <dolfin/mesh/Cell.h>
#include <dolfin/mesh/Edge.h>
#include <dolfin/mesh/Face.h>
#include <dolfin/mesh/Vertex.h>
#include <dolfin/refinement/ParallelRefinement.h>

#include "GenericSurface.h"
#include "SubTriangulation.h"

using namespace narwal;

//-----------------------------------------------------------------------------
void SurfaceRefinementND::refine(dolfin::Mesh& newmesh,
                                 std::shared_ptr<const dolfin::Mesh> mesh,
                                 const GenericSurface& surface)
{
  dolfin_assert(mesh);

  dolfin::EdgeFunction<bool> marker(mesh, false);

  // Get set of edges in partially intersected cells
  std::set<std::size_t> partial_edge;
  for (dolfin::CellIterator c(*mesh); !c.end(); ++c)
  {
    const std::pair<bool, bool> status = surface.entity_intersection(*c);
    // Only get partially intersected cells
    if (status.first && status.second)
    {
      for (dolfin::EdgeIterator e(*c); !e.end(); ++e)
        partial_edge.insert(e->index());
    }
  }

  // Store new coordinates
  std::vector<double> new_coordinates(mesh->coordinates().begin(),
                                      mesh->coordinates().end());

  std::map<std::size_t, std::size_t> edge_to_new_vertex;
  std::size_t count = mesh->num_vertices();
  for (dolfin::EdgeIterator e(*mesh);!e.end(); ++e)
  {
    if (surface.entity_intersection(*e).first)
    {
      edge_to_new_vertex.insert(std::make_pair(e->index(), count));
      marker[*e] = true;
      dolfin::Point p = surface.compute_intersection(*e);
      new_coordinates.insert(new_coordinates.end(),
                             p.coordinates(), p.coordinates() + 3);
      ++count;

      if (partial_edge.find(e->index()) == partial_edge.end())
      {
        // repeat point for other side of crack
        // only if edge does not appear in a partially intersected
        // cell
        new_coordinates.insert(new_coordinates.end(),
                               p.coordinates(), p.coordinates() + 3);
        ++count;
      }

    }
  }

  // Build mesh
  dolfin::MeshEditor ed;
  ed.open(newmesh, 3, 3);

  ed.init_vertices(count);
  count = 0;
  for (std::vector<double>::iterator coord = new_coordinates.begin();
       coord != new_coordinates.end(); coord += 3)
    ed.add_vertex(count++, *coord, *(coord+1), *(coord+2));

  // Topology
  std::vector<std::size_t>& parent_cell
    = newmesh.data().create_array("parent_cell", newmesh.topology().dim());
  parent_cell.reserve(mesh->num_cells());

  // If mesh already has parent cell data, copy over
  bool has_parent_data = mesh->data().exists("parent_cell",
                                             mesh->topology().dim());
  std::vector<std::size_t> parent_data;
  if (has_parent_data)
    parent_data = mesh->data().array("parent_cell", mesh->topology().dim());

  std::vector<std::size_t> topology;
  count = 0;
  for (dolfin::CellIterator cell(*mesh); !cell.end(); ++cell)
  {
    const std::size_t parent_cell_index
      = has_parent_data ? parent_data[cell->index()] : cell->index();

    // Local map maps vertices(0-3) and edges (4-9) to new indices
    std::vector<std::size_t> local_map;
    local_map.reserve(10);
    for (dolfin::VertexIterator v(*cell); !v.end(); ++v)
      local_map.push_back(v->index());

    // Fill local mapping for intersected edges using edge index
    for (dolfin::EdgeIterator e(*cell); !e.end(); ++e)
    {
      if (marker[*e])
        local_map.push_back(e->index());
    }

    if (local_map.size() == 4)
    {
      // Cell not intersected, simply copy over
      topology.insert(topology.end(),
                      local_map.begin(),
                      local_map.end());
      parent_cell.push_back(parent_cell_index);
    }
    else
    {
      // Get SubTriangulation
      SubTriangulation submesh(*cell, surface);
      for (dolfin::CellIterator scell(submesh.mesh()); !scell.end(); ++scell)
      {
        // FIXME: this test may not be good enough
        const int side = surface.side(scell->midpoint(), 0.0);
        for (dolfin::VertexIterator v(*scell); !v.end(); ++v)
        {
          const std::size_t idx = v->index();
          // SubTriangulation indices > 3 are new vertices
          // from edge bisection. Now map to actual vertices.
          if (idx > 3)
          {
            auto it = edge_to_new_vertex.find(local_map[idx]);
            assert(it != edge_to_new_vertex.end());
            auto part = partial_edge.find(local_map[idx]);
            // Split vertex if edge not part of partially intersected cell
            if (side == 1 && part == partial_edge.end())
              topology.push_back(it->second + 1);
            else
              topology.push_back(it->second);
          }
          else
            topology.push_back(local_map[idx]);
        }
        parent_cell.push_back(parent_cell_index);
      }
    }
  }

  dolfin_assert(topology.size() == 4*parent_cell.size());

  ed.init_cells(topology.size()/4);
  count = 0;
  for (auto p = topology.begin(); p != topology.end(); p += 4)
    ed.add_cell(count++, *p, *(p+1), *(p+2), *(p+3));

  ed.close();
}
//-----------------------------------------------------------------------------
std::shared_ptr<dolfin::Mesh>
SurfaceRefinementND::refine(std::shared_ptr<const dolfin::Mesh> mesh,
                            const GenericSurface& surface)
{
  dolfin::Mesh mesh_new;
  refine(mesh_new, mesh, surface);
  return std::make_shared<dolfin::Mesh>(mesh_new);
}
//-----------------------------------------------------------------------------
