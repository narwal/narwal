// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_XFEMFORMS_H
#define NARWAL_XFEMFORMS_H

#include <memory>
#include <vector>

#include "Evaluator.h"

namespace dolfin
{
  class Cell;
  class Form;
  class Point;
  template<class T>
  class Array;
}

namespace ufc
{
  class cell;
  class cell_integral;
}

namespace narwal
{
  class XFEMFunctionSpace;

  /// FIXME: brief description
  // base class for ElasticityProblem3D, PoissonProblem3D, ...
  class XFEMForms
  {
  public:

    // FIXME: Remove polynomial order argument. It is a property of
    // the element.
    XFEMForms(std::shared_ptr<const narwal::XFEMFunctionSpace> V,
              const int polynomial_order);

    /// Destructor
    virtual ~XFEMForms();

    void set_a(std::shared_ptr<const dolfin::Form> a);
    void set_L(std::shared_ptr<const dolfin::Form> L);

    std::shared_ptr<const narwal::XFEMFunctionSpace> function_space() const
    { return _V; }

    std::shared_ptr<const dolfin::Form> a() const;
    std::shared_ptr<const dolfin::Form> L() const;

    /// Tabulate tensor
    virtual void
      tabulate_tensor(double* A,
                      double* b,
                      const double* const * coefficients,
                      const dolfin::Cell& cell,
                      const std::vector<double>& vertex_coorodinates,
                      const ufc::cell_integral& cell_integral) const = 0;

    /// Tabulate tensor
    virtual void
      evaluate(
        dolfin::Array<double>& values,
        const dolfin::Point& x_eval,
        const dolfin::Point& x_eval_heaviside,
        const double* const coefficients,
        const dolfin::Cell& cell,
        const std::vector<double>& vertex_coords,
        const Evaluator::Mode evalMode,
        const int surface_index
      ) const = 0;

    // FIXME: Remove this function. It is a property of the element
    /// Return polynomial order of basis functions
    virtual int polynomial_order() const
    { return _polynomial_order; }

    static std::size_t getNumElemNodes(const int polynomialDegree);

  private:

    // The XFEM function space
    std::shared_ptr<const narwal::XFEMFunctionSpace> _V;

    // Regular bilinear and linear forms
    std::shared_ptr<const dolfin::Form> _a, _L;

    // FIXME: remove
    // Polynomial order
    const int _polynomial_order;

  };

}

#endif
