// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "XFEMForms.h"

#include <cstdlib>

#include <dolfin/fem/Form.h>
#include <dolfin/log/log.h>

//-----------------------------------------------------------------------------
narwal::XFEMForms::XFEMForms(
  std::shared_ptr<const narwal::XFEMFunctionSpace> V,
  const int polynomial_order)
  : _V(V), _polynomial_order(polynomial_order)
{
  // Do nothing
}
//-----------------------------------------------------------------------------
narwal::XFEMForms::~XFEMForms()

{
  // Do nothing
}
//-----------------------------------------------------------------------------
void narwal::XFEMForms::set_a(std::shared_ptr<const dolfin::Form> a)
{
  _a = a;
}
//-----------------------------------------------------------------------------
void narwal::XFEMForms::set_L(std::shared_ptr<const dolfin::Form> L)
{
  _L = L;
}
//-----------------------------------------------------------------------------
std::shared_ptr<const dolfin::Form> narwal::XFEMForms::a() const
{
  return _a;
}
//-----------------------------------------------------------------------------
std::shared_ptr<const dolfin::Form> narwal::XFEMForms::L() const
{
  return _L;
}
//-----------------------------------------------------------------------------
std::size_t narwal::XFEMForms::getNumElemNodes(const int polynomialDegree)
{
  std::size_t nElemNodes;
  switch (polynomialDegree)
  {
  case 1: nElemNodes = 4;
    break;
  case 2: nElemNodes = 10;
    break;
  default: nElemNodes = 0;
    abort();
  }
  return nElemNodes;
}
