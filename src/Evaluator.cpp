// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

// TODO: Some code has been taken from Narwal. Fix copyright notice.

#include "Evaluator.h"

#include <vector>
#include <Eigen/Dense>

#include <dolfin/fem/DirichletBC.h>
#include <dolfin/fem/Form.h>
#include <dolfin/function/Function.h>
#include <dolfin/fem/GenericDofMap.h>
#include <dolfin/fem/UFC.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/la/GenericMatrix.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/mesh/Cell.h>
#include <dolfin/mesh/Mesh.h>
#include <dolfin/geometry/BoundingBoxTree.h>
#include <dolfin/log/log.h>

#include "SparsityTools.h"
#include "XFEMForms.h"
#include "XFEMFunctionSpace.h"
#include "DomainManager.h"
#include "DofMap.h"

#include "Gap3D_P1.h"

#include "ufc.h"

using namespace narwal;

//-----------------------------------------------------------------------------
Evaluator::Evaluator(
  const std::shared_ptr<const dolfin::Function> function,
  const std::shared_ptr<const narwal::DofMap> dofmap,
  const std::shared_ptr<const narwal::DomainManager> domain,
  const std::shared_ptr<const narwal::XFEMForms> forms
  )
  : _function(function),
    _dofmap(forms->function_space()->dofmap()),
    _domain(domain),
    _forms(forms)
{
  // Do nothing
}
//-----------------------------------------------------------------------------
static double sqr(const double x) { return x*x; }
//-----------------------------------------------------------------------------
std::unique_ptr<dolfin::Function> Evaluator::evaluate_discontinuity(
  std::shared_ptr<const dolfin::Mesh> surface_mesh,
    const int surface_index
    ) const
{
  auto V_s = std::make_shared<Gap3D_P1::FunctionSpace>(surface_mesh);
  std::unique_ptr<dolfin::Function> gap_function(new dolfin::Function(V_s));

  // Get geometric dimension
  dolfin_assert(V_s->element());

  const std::size_t gdim = V_s->element()->geometric_dimension();

  // Get dofmap
  std::shared_ptr<const dolfin::GenericDofMap> dofmap = V_s->dofmap();
  dolfin_assert(dofmap);

  // Get number of element 'nodes'
  const std::size_t num_nodes = V_s->element()->space_dimension();

  // loop over cells
  for (dolfin::CellIterator c(*surface_mesh); !c.end(); ++c)
  {
    auto cell_dofs = dofmap->cell_dofs(c->index());

    // loop cell nodes
    // Cell vertices (re-allocated inside function for thread safety)
    std::vector<double> vertex_coordinates;
    c->get_vertex_coordinates(vertex_coordinates);

    for (std::size_t iNode = 0; iNode < num_nodes; ++iNode) {

      // compute normal displacement
      dolfin::Array<double> x(3);
      x[0] = vertex_coordinates[iNode*gdim + 0];
      x[1] = vertex_coordinates[iNode*gdim + 1];
      x[2] = vertex_coordinates[iNode*gdim + 2];

      // compute normal displacement at x
      dolfin::Array<double> nv(3);
      evaluate_discontinuity(nv, x, narwal::Evaluator::interface_normal,
                             surface_index);

      // compute gap (vector length)
      const double gap_value = std::sqrt(sqr(nv[0]) + sqr(nv[1]) + sqr(nv[2]));

      // add to gap function
      const int rowGID = cell_dofs[iNode];

      const dolfin::la_index num_rows = 1;
      const std::vector<dolfin::la_index> rows(1, rowGID);
      const dolfin::la_index * const  prows = &rows[0];
      gap_function->vector()->set(
          &gap_value,
          &num_rows,
          &prows
          );
    }
    gap_function->vector()->apply("apply");
  }

  return gap_function;
}
//-----------------------------------------------------------------------------
void Evaluator::evaluate_discontinuity(
    dolfin::Array<double>& values,
    const dolfin::Array<double>& x,
    const Mode evalMode,
    const int surface_index
    ) const
{
  eval(values, x, x, evalMode, surface_index);
}

/// Evaluate basis function i at given point in cell
/// this could be in a narwal::FiniteElement
void Evaluator::evaluate_basis(
  const dolfin::FiniteElement& element,
  const dolfin::Cell &c,
  std::size_t i,
  double* basis,
  const double* x,
  const double* vertex_coordinates,
  int cell_orientation) const
{



  const std::size_t gdim = element.geometric_dimension();
  // Get number of element 'nodes'
  const std::size_t num_nodes = element.space_dimension();
  // std::cout << "gdim = " << gdim << std::endl;
  // std::cout << "num_nodes = " << num_nodes << std::endl;
  const std::size_t numRegularDofs = gdim*num_nodes;

  std::shared_ptr<const ufc::finite_element> _ufc_element =
      element.ufc_element();

  dolfin_assert(_ufc_element);
  if (i < numRegularDofs) {// standard DOF
//    _ufc_element->evaluate_basis(i, values, x, vertex_coordinates,
//                                 cell_orientation);
  } else {
//    const std::vector<std::pair<int, EnrLabelArray>> &a =
//        _dofmap->enriched_dofs(c->index(), iRegDof);
//    _ufc_element->evaluate_basis(i, values, x, vertex_coordinates,
//                                 cell_orientation);
  }
}

//-----------------------------------------------------------------------------
// copied straight from Function.cpp
//-----------------------------------------------------------------------------
void Evaluator::eval(
    dolfin::Array<double>& values,
    const dolfin::Array<double>& x_eval
    ) const
{
  eval(values, x_eval, x_eval, domain, -1);
}

void Evaluator::eval(
    dolfin::Array<double>& values,
    const dolfin::Array<double>& x_eval,
    const dolfin::Array<double>& x_eval_heaviside
    ) const
{
  eval(values, x_eval, x_eval_heaviside, domain, -1);
}

void Evaluator::eval(
  dolfin::Array<double>& values,
  const dolfin::Array<double>& x_eval,
  const dolfin::Array<double>& x_eval_heaviside,
  const Mode evalMode,
  const int surface_index) const
{
  dolfin_assert(_domain);
  dolfin_assert(_domain->mesh());
  const dolfin::Mesh& mesh = *_domain->mesh();

  // Find the cell that contains x
  const double* _x = x_eval.data();
  const dolfin::Point point(mesh.geometry().dim(), _x);

  // Get index of first cell containing point
  unsigned int id
    = mesh.bounding_box_tree()->compute_first_entity_collision(point);

  // If not found, use the closest cell
  if (id == std::numeric_limits<unsigned int>::max())
  {
    const bool _allow_extrapolation = false;
    if (_allow_extrapolation)
      id = mesh.bounding_box_tree()->compute_closest_entity(point).first;
    else
    {
      dolfin::dolfin_error("Function.cpp",
                   "evaluate function at point",
                   "The point is not inside the domain. Consider calling \"Function::set_allow_extrapolation(true)\" on this Function to allow extrapolation");
    }
  }

  // Create cell that contains point
  const dolfin::Cell cell(mesh, id);
  ufc::cell ufc_cell;
  cell.get_cell_data(ufc_cell);

  // Call evaluate function
  eval(values, x_eval, x_eval_heaviside, cell, ufc_cell, evalMode, surface_index);
}

//-----------------------------------------------------------------------------
// copied and adapted from dolfin::Function::eval(...)
void Evaluator::eval(
  dolfin::Array<double>& values,
  const dolfin::Array<double>& x_eval,
  const dolfin::Array<double>& x_eval_heaviside,
  const dolfin::Cell& cell,
  const ufc::cell& ufc_cell,
  const Mode evalMode,
  const int surface_index) const
{
  // Developer note: work arrays/vectors are re-created each time this
  //                 function is called for thread-safety
  std::shared_ptr<const dolfin::FunctionSpace> _function_space =
      _function->function_space();
  dolfin_assert(_function_space->element());
  const dolfin::FiniteElement& element = *_function_space->element();

  // Compute in tensor (one for scalar function, . . .)
  // number of physical fields (elasticity: dispX, dispZ, dispZ) -> 3
  const std::size_t value_size_loc = _function->value_size();
  dolfin_assert(value_size_loc == 3);

  dolfin_assert(values.size() == value_size_loc);

  // Create work vector for expansion coefficients
  std::vector<double> coefficients(element.space_dimension());

  // Cell vertices (re-allocated inside function for thread safety)
  std::vector<double> vertex_coordinates;
  cell.get_vertex_coordinates(vertex_coordinates);

  // Restrict function to cell
  _function->restrict(coefficients.data(), element, cell,
           vertex_coordinates.data(), ufc_cell);

  for (std::size_t i = 0; i < value_size_loc; ++i) {
    values[i] = 0.0;
  }

  if (_dofmap->is_enriched(cell.index())) {

    std::vector<double> PHIe;
    // Compute A_e and b_e
    _forms->evaluate(
        values,
        dolfin::Point(x_eval[0], x_eval[1], x_eval[2]),
        dolfin::Point(x_eval_heaviside[0], x_eval_heaviside[1], x_eval_heaviside[2]),
        coefficients.data(),
        cell,
        vertex_coordinates,
        evalMode,
        surface_index
    );

  } else {
    // Compute linear combination space_dimension is number of unknowns
    const std::size_t numEleDofs = element.space_dimension();

    for (std::size_t iEleDof = 0; iEleDof < numEleDofs; ++iEleDof) // elem dofs per PhysField
    {
      // Create work vector for basis
      std::vector<double> basis(value_size_loc, 0.0); // 3

      // call standard evaluation function
      element.evaluate_basis(iEleDof, basis.data(), x_eval.data(),
          vertex_coordinates.data(),
          ufc_cell.orientation);

      // assumes equal interpolation for dispx, dispy, dispz
      for (std::size_t iField = 0; iField < value_size_loc; ++iField) { // 3
        values[iField] += coefficients[iEleDof]*basis[iField];
      }
    }
  }
}
//-----------------------------------------------------------------------------
void Evaluator::eval(dolfin::Array<double>& values,
                    const dolfin::Array<double>& x_eval,
                    const dolfin::Array<double>& x_eval_heaviside,
                    const ufc::cell& ufc_cell,
                    const Mode evalMode,
                    const int surface_index) const
{
  dolfin_assert(_domain);
  dolfin_assert(_domain->mesh());
  const dolfin::Mesh& mesh = *_domain->mesh();

  // Check if UFC cell comes from mesh, otherwise
  // find the cell which contains the point
  dolfin_assert(ufc_cell.mesh_identifier >= 0);
  if (ufc_cell.mesh_identifier == static_cast<int>(mesh.id()))
  {
    const dolfin::Cell cell(mesh, ufc_cell.index);
    eval(values, x_eval, x_eval_heaviside, cell, ufc_cell, evalMode, surface_index);
  }
  else
    eval(values, x_eval, x_eval_heaviside, evalMode, surface_index);
}
//-----------------------------------------------------------------------------
