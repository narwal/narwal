// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "SparsityTools.h"

#include <memory>
#include <utility>
#include <vector>
#include <dolfin/fem/GenericDofMap.h>
#include <dolfin/fem/SparsityPatternBuilder.h>
#include <dolfin/la/SparsityPattern.h>
#include <dolfin/log/log.h>

using namespace narwal;

//-----------------------------------------------------------------------------
dolfin::TensorLayout
SparsityTools::build_vector_layout(const dolfin::Mesh& mesh,
                                   const dolfin::GenericDofMap& dofmap)
{
  dolfin::TensorLayout tensor_layout(0, dolfin::TensorLayout::Sparsity::DENSE);

  std::vector<std::shared_ptr<const dolfin::IndexMap>> index_maps
    = { dofmap.index_map() };

  //tensor_layout.init(MPI_COMM_WORLD, global_dimensions, block_size,
  //                   local_range);
  tensor_layout.init(MPI_COMM_WORLD, index_maps,
                     dolfin::TensorLayout::Ghosts::UNGHOSTED);

  return tensor_layout;
}
//-----------------------------------------------------------------------------
dolfin::TensorLayout
SparsityTools::build_matrix_layout(const dolfin::Mesh& mesh,
                                   const dolfin::GenericDofMap& dofmap)
{
  // Create TensorLayout object
  dolfin::TensorLayout tensor_layout(0, dolfin::TensorLayout::Sparsity::SPARSE);

  // Access sparsity pattern
  auto sparsity_pattern = tensor_layout.sparsity_pattern();
  dolfin_assert(sparsity_pattern);

  // Collect dofmap
  std::vector<const dolfin::GenericDofMap*> dofmaps;
  dofmaps.push_back(&dofmap);
  dofmaps.push_back(&dofmap);

  // Build sparsity pattern
  dolfin::SparsityPatternBuilder::build(*sparsity_pattern, mesh, dofmaps,
                                        true, false, false, false, false);

  // Get dimensions
  //std::vector<std::size_t> global_dimensions;
  //std::vector<std::pair<std::size_t, std::size_t> > local_range;
  //std::vector<std::size_t> block_sizes;
  std::vector<std::shared_ptr<const dolfin::IndexMap>> index_maps;
  for (std::size_t i = 0; i < 2; i++)
  {
    dolfin_assert(dofmaps[i]);
    index_maps.push_back(dofmaps[i]->index_map());
  }

  // Initialise tensor layout
  tensor_layout.init(MPI_COMM_WORLD, index_maps,
                     dolfin::TensorLayout::Ghosts::UNGHOSTED);

  return tensor_layout;
}
//-----------------------------------------------------------------------------
