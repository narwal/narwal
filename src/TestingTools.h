// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_TESTING_TOOLS_H
#define NARWAL_TESTING_TOOLS_H

namespace dolfin
{
  class Function;
  class GenericDofMap;
  class GenericMatrix;
  class GenericVector;
}

namespace narwal
{

  class GenericSurface;

  /// Provides a variety of tools that are useful for code/solver testing

  class TestingTools
  {
  public:

    /// Print enriched dof value, Heaviside function and coordinate
    static void test_enriched_dofs(const dolfin::Function& u,
                                   const GenericSurface& surface);

    /// Extract the diagonal of a matrix into a vector
    static void extract_matrix_diagonal(dolfin::GenericVector& x,
                                        const dolfin::GenericMatrix& A);

    /// Check that max dof index is equal to (N - 1), where N is the number of dofs
    static void check_dof_count(const dolfin::GenericDofMap& dofmap);

  };

}

#endif
