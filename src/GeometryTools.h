// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_GEOMETRY_TOOLS_H
#define NARWAL_GEOMETRY_TOOLS_H

#include <memory>

namespace dolfin
{
  template <typename T> class CellFunction;
  template <typename T> class FacetFunction;
  class Cell;
  class Mesh;
  class Facet;
}

namespace narwal
{
  class DomainManager;
  class GenericSurface;

  /// A variety of geometry tools that are useful for XFEM methods

  class GeometryTools
  {
  public:

    /// Compute a CellFunction which is true for fully intersected
    /// cells. Cells that contain surface edge are not included.
    static dolfin::CellFunction<bool>
    compute_intersected_cells(std::shared_ptr<const dolfin::Mesh> mesh,
                              const GenericSurface& surface);

    /// Compute which facets contain the crack boundary.
    /// Marks facets which are shared between cells, one of which is
    /// fully intersected, and the other partially intersected.
    /// Useful for debugging / visualisation
    static dolfin::FacetFunction<bool>
      compute_boundary_facets(std::shared_ptr<const dolfin::Mesh> mesh,
                              const GenericSurface& surface);

    // Detect if a facet is a boundary facet, i.e. one of the
    // neighbouring cells is completely cut by the surface, and the
    // other neighbouring cell is only partially cut.
    static bool is_boundary_facet(const dolfin::Facet& facet,
                                  const GenericSurface& surface);

    /// Compute ratio of sub-cell to cell volume ratio (returns
    /// minimum ratio)
    /// Debug cells which may cause degenerate cases
    static double volume_ratio(const GenericSurface& surface,
                               const dolfin::Cell& cell,
                               const dolfin::Mesh& sub_mesh);


    /// Check if two surfaces intersect each other inside given
    /// cell. Function assumes that cell is intersected by surface0
    /// and surface1.
    static bool surfaces_intersect(const dolfin::Cell& c,
                                   const GenericSurface& surface0,
                                   const GenericSurface& surface1);

    /// Check if three surfaces intersect each other inside given
    /// cell. Function assumes that cell is intersected by surface0, surface1,
    /// and surface2
    static bool surfaces_intersect(const dolfin::Cell& c,
                                   const GenericSurface& surface0,
                                   const GenericSurface& surface1,
                                   const GenericSurface& surface2);

  };

}

#endif
