// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_XAMG_TOOLS_H
#define NARWAL_XAMG_TOOLS_H

#include <memory>

#ifdef HAVE_MPI
#include <mpi.h>
#endif

namespace dolfin
{
  class GenericMatrix;
  class GenericVector;
  class VectorSpaceBasis;
}

namespace narwal
{

  /// <b>Linear Algebra</b>

  namespace la
  {
    /// create system matrix depending on the backend
    std::unique_ptr<dolfin::GenericMatrix> initSystemMatrix();

    /// create system vector depending on the backend
    std::unique_ptr<dolfin::GenericVector> initSystemVector();

    /// transform system matrix \b P<sup>T</sup>\b A \b P
    std::unique_ptr<dolfin::GenericMatrix>
    MatPtAP(const dolfin::GenericMatrix &P,
            const dolfin::GenericMatrix &A);

    /// convert NullSpace: \b G<sup>-1</sup> \b N
    std::unique_ptr<dolfin::VectorSpaceBasis>
    transformGinvN(const std::shared_ptr<const dolfin::GenericMatrix> G,
                   const dolfin::VectorSpaceBasis& null_space,
                   const bool approx = false); // FIXME: default should be false

    /// pretty print columns of nullspace to stream
    void outputNullSpace(const dolfin::VectorSpaceBasis &nullspace, std::ostream &os);

    /// output matrix to file
    void toMatrixMarketFile(const dolfin::GenericMatrix& A,
                            const std::string& filename);
  }

}

#endif
