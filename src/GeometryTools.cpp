// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "GeometryTools.h"

#include <utility>

#include <dolfin/log/log.h>
#include <dolfin/mesh/Cell.h>
#include <dolfin/mesh/Edge.h>
#include <dolfin/mesh/Facet.h>
#include <dolfin/mesh/Mesh.h>
#include <dolfin/mesh/Vertex.h>

#include "DomainManager.h"
#include "GenericSurface.h"
#include "ImplicitSurface.h"

using namespace narwal;

//-----------------------------------------------------------------------------
dolfin::CellFunction<bool> GeometryTools::compute_intersected_cells(
  std::shared_ptr<const dolfin::Mesh> mesh,
  const GenericSurface& surface)
{
  dolfin_assert(mesh);

  // Create CellFunction
  dolfin::CellFunction<bool> cf(mesh, false);

  // Loop over cells
  for (dolfin::CellIterator cell(*mesh); !cell.end(); ++cell)
  {
    const std::pair<bool, bool> intersect
      = surface.entity_intersection(*cell);
    cf[*cell] = intersect.first && !intersect.second;
  }

  return cf;
}
//-----------------------------------------------------------------------------
dolfin::FacetFunction<bool> GeometryTools::compute_boundary_facets(
  std::shared_ptr<const dolfin::Mesh> mesh,
  const GenericSurface& surface)
{
  // Create MeshFunction
  dolfin_assert(mesh);
  dolfin::FacetFunction<bool> bf(mesh, false);

  // Mesh initialisation
  const std::size_t tdim = mesh->topology().dim();
  mesh->init(tdim - 1, tdim);

  dolfin::CellFunction<std::size_t> cf(mesh, 0);
  for (dolfin::CellIterator cell(*mesh); !cell.end(); ++cell)
  {
    const std::pair<bool, bool> status = surface.entity_intersection(*cell);
    const std::size_t key =  2*(status.first) + status.second;
    dolfin_assert(key <= 3);
    cf[*cell] = key;
  }

  for (dolfin::FacetIterator f(*mesh); !f.end(); ++f)
  {
    if (f->num_entities(tdim) == 2)
    {
      // Status (cf) may only be 0, 1, 2 or 3
      // Check for cell values of 2 + 3 = 5
      // which only occur when a partially intersected cell neighbours
      // a fully-intersected cell
      const std::size_t c0_index = f->entities(tdim)[0];
      const std::size_t c1_index = f->entities(tdim)[1];
      bf[*f] = ((cf[c0_index] + cf[c1_index]) == 5);
    }
  }

  return bf;
}
//-----------------------------------------------------------------------------
bool GeometryTools::is_boundary_facet(const dolfin::Facet& f,
                                      const GenericSurface& surface)
{
  std::size_t facet_status = 0;
  for(dolfin::CellIterator cell(f); !cell.end(); ++cell)
  {
    const std::pair<bool, bool> status = surface.entity_intersection(*cell);
    facet_status += 2*(status.first) + status.second;
  }
  // Status may only be 0, 1, 2 or 3
  // Check for cell values of 2 + 3 = 5
  // which only occur when a partially intersected cell neighbours
  // a fully-intersected cell
  return (facet_status == 5);
}
//-----------------------------------------------------------------------------
double GeometryTools::volume_ratio(const GenericSurface& surface,
                                   const dolfin::Cell& cell,
                                   const dolfin::Mesh& cell_mesh)
{
  double v0(0.0), v1(0.0);
  for (dolfin::CellIterator sub_cell(cell_mesh); !sub_cell.end(); ++sub_cell)
  {
    if (surface.side(sub_cell->midpoint(), 0.0) == -1)
      v0 += sub_cell->volume();
    else
      v1 += sub_cell->volume();
  }
  return std::min(v0, v1)/cell.volume();
}
//-----------------------------------------------------------------------------
bool GeometryTools::surfaces_intersect(const dolfin::Cell& c,
                                       const GenericSurface& surface0,
                                       const GenericSurface& surface1)
{
  bool plus_side = false;
  bool minus_side = false;
  const double eps = 1.0e-9;

  for (dolfin::EdgeIterator e(c); !e.end(); ++e)
  {
    // Find Edge intersections of surface0 with cell
    if (surface0.entity_intersection(*e).first)
    {
      // Check which side of surface 1 the point lies
      const dolfin::Point p = surface0.compute_intersection(*e);
      if (surface1.side(p, eps) > 0.0)
        plus_side = true;
      else
        minus_side = true;
    }
  }

  // Two surfaces intersect in cell
  if (plus_side and minus_side)
    return true;
  else
    return false;
}
//-----------------------------------------------------------------------------
bool GeometryTools::surfaces_intersect(const dolfin::Cell& c,
                                       const GenericSurface& s0,
                                       const GenericSurface& s1,
                                       const GenericSurface& s2)
{
  bool plus_side_01 = false;
  bool minus_side_01 = false;
  bool plus_side_02 = false;
  bool minus_side_02 = false;
  bool plus_side_12 = false;
  bool minus_side_12 = false;
  const double eps = 1.0e-9;

  for (dolfin::EdgeIterator e(c); !e.end(); ++e)
  {
    // Find Edge intersections of surface0 with cell
    if (s0.entity_intersection(*e).first) {
      const dolfin::Point p = s0.compute_intersection(*e);
      // Check which side of surface 1 the point lies
      if (s1.side(p, eps) > 0.0) {
        plus_side_01 = true;
      } else {
        minus_side_01 = true;
      }
      // Check which side of surface 2 the point lies
      if (s2.side(p, eps) > 0.0) {
        plus_side_02 = true;
      } else {
        minus_side_02 = true;
      }
    }
    // Find Edge intersections of surface0 with cell
    if (s1.entity_intersection(*e).first) {
      const dolfin::Point p = s1.compute_intersection(*e);
      // Check which side of surface 2 the point lies
      if (s2.side(p, eps) > 0.0) {
        plus_side_12 = true;
      } else {
        minus_side_12 = true;
      }
    }
  }

  // Three surfaces intersect in cell
  if (plus_side_01 and minus_side_01 and
      plus_side_02 and minus_side_02 and
      plus_side_12 and minus_side_12)
    return true;
  else
    return false;
}
//-----------------------------------------------------------------------------
