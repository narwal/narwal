// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

// TODO: Some code has been taken from Narwal. Fix copyright notice.

#include "Assembler.h"

#include <vector>
#include <Eigen/Dense>

#include <dolfin/fem/DirichletBC.h>
#include <dolfin/fem/Form.h>
#include <dolfin/fem/GenericDofMap.h>
#include <dolfin/fem/UFC.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/la/GenericMatrix.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/mesh/Cell.h>
#include <dolfin/mesh/Mesh.h>

#include "SparsityTools.h"
#include "XFEMForms.h"

using namespace narwal;

//-----------------------------------------------------------------------------
Assembler::Assembler(
  std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs,
  std::shared_ptr<const narwal::XFEMForms> forms)
  : _bcs(bcs), _forms(forms)
{
  // Do nothing
}
//-----------------------------------------------------------------------------
void Assembler::assemble(dolfin::GenericMatrix& A, dolfin::GenericVector& b)
{
  dolfin_assert(_forms);

  // Check that we have a bilinear and a linear form
  dolfin_assert(_forms->a());
  dolfin_assert(_forms->a()->rank() == 2);
  dolfin_assert(_forms->L());
  dolfin_assert(_forms->L()->rank() == 1);

  // Extract mesh
  dolfin_assert(_forms->a()->mesh());
  const dolfin::Mesh& mesh = *(_forms->a()->mesh());

  const std::size_t gdim = mesh.geometry().dim();
  mesh.init(gdim);

  // Dof map
  const dolfin::GenericDofMap& dofmap
  = *(_forms->a()->function_space(0)->dofmap().get());

  // Initialise tensors
  if (A.empty())
  {
    dolfin::TensorLayout layout
      = narwal::SparsityTools::build_matrix_layout(mesh, dofmap);
    A.init(layout);
  }
  // Initialise tensors
  if (b.empty())
  {
    dolfin::TensorLayout layout
      = narwal::SparsityTools::build_vector_layout(mesh, dofmap);
    b.init(layout);
  }

  // Collect dofmap vector for cells
  std::vector<dolfin::ArrayView<const dolfin::la_index>> cell_dofs(2);
  std::vector<dolfin::ArrayView<const dolfin::la_index>> cell_dofs_L(1);

  // Create UFC wrappers
  dolfin::UFC ufc_a(*_forms->a());

  // Cell integral
  ufc::cell_integral* integral = ufc_a.default_cell_integral.get();

  // Allocate objects to hold element tensor
  //std::cout << "Max cell dim: " << dofmap.max_cell_dimension() << std::endl;
  std::vector<double>
    A_e(dofmap.max_cell_dimension()*dofmap.max_cell_dimension(), 0.0);
  std::vector<double> b_e(dofmap.max_cell_dimension(), 0.0);

  // Get Dirichlet dofs and values for local mesh
  dolfin::DirichletBC::Map boundary_values;
  for (std::size_t i = 0; i < _bcs.size(); ++i)
    _bcs[i]->get_boundary_values(boundary_values);

  ufc::cell ufc_cell;
  std::vector<double> vertex_coordinates;
  for (dolfin::CellIterator c(mesh); !c.end(); ++c)
  {
    c->get_vertex_coordinates(vertex_coordinates);
    c->get_cell_data(ufc_cell);
    ufc_a.update(*c, vertex_coordinates, ufc_cell);
    //ufc_L.update(*c, vertex_coordinates, ufc_cell);

    std::fill(b_e.begin(), b_e.end(), 0.0);

    // Get dof map(s)
    cell_dofs[0]   = dofmap.cell_dofs(c->index());
    cell_dofs[1]   = dofmap.cell_dofs(c->index());
    cell_dofs_L[0] = dofmap.cell_dofs(c->index());

    // Compute A_e and b_e
    _forms->tabulate_tensor(A_e.data(), b_e.data(), ufc_a.w(), *c,
                            vertex_coordinates, *integral);

    // Apply Dirichlet BC to element tensors
    apply_bc(A_e.data(), b_e.data(), boundary_values,
             cell_dofs[0], cell_dofs[1]);

    // Add to global tensors
    A.add(A_e.data(), cell_dofs);
    b.add(b_e.data(), cell_dofs_L);
  }

  A.apply("add");
  b.apply("add");
}
//-----------------------------------------------------------------------------
void Assembler::apply_bc(
  double* A, double* b,
  const dolfin::DirichletBC::Map& boundary_values,
  dolfin::ArrayView<const dolfin::la_index> global_dofs0,
  dolfin::ArrayView<const dolfin::la_index> global_dofs1)
{
  dolfin_assert(A);
  dolfin_assert(b);
  dolfin_assert(global_dofs0.size() == global_dofs1.size());

  // Wrap matrix and vector using Eigen
  Eigen::Map<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic,
                           Eigen::RowMajor> >
    _A(A, global_dofs0.size(), global_dofs1.size());
  Eigen::Map<Eigen::VectorXd>  _b(b, global_dofs1.size());

  // Loop over rows
  for (int i = 0; i < _A.cols(); ++i)
  {
    const std::size_t ii = global_dofs1[i];
    dolfin::DirichletBC::Map::const_iterator bc_value
      = boundary_values.find(ii);
    if (bc_value != boundary_values.end())
    {
      // Zero row
      _A.row(i).setZero();

      // Modify RHS (subtract (bc_column(A))*bc_val from b)
      _b -= _A.col(i)*bc_value->second;

      // Zero column
      _A.col(i).setZero();

      // Place 1 on diagonal and bc on RHS (i th row ).
      _b(i)    = bc_value->second;
      _A(i, i) = 1.0;
    }
  }
}
//-----------------------------------------------------------------------------
