// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_IMPLICITSURFACE_H
#define NARWAL_IMPLICITSURFACE_H

#include "GenericSurface.h"

namespace dolfin
{
  class Point;
}

namespace narwal
{

  class SubTriangulation;

  /// This class implements the narwal::GenericSurface interface

  // FIXME: Should this class inherit from dolfin::ImplcitSurface, or
  // just store a reference/pointer to the implicit surface?

  class ImplicitSurface : public GenericSurface
  {
  public:

    /// Constructor
    ImplicitSurface(const dolfin::Point& center, const double radius);

    /// Destructor
    ~ImplicitSurface();

    /// Signed distance function surface. If f0(p) = 0, the point p is
    /// possibly on the surface, which case ImplicitSurface::f1 can be
    /// called to check.
    virtual double f0(const dolfin::Point& point) const = 0;

    /// For a point for which f0 &asymp; 0, return <0 if point is on
    /// the surface.
    virtual double f1(const dolfin::Point& point) const
    { return -1.0; }

    /// Determine if entity is intersected by surface f0 by testing
    /// each vertex. If any vertices evaluate with opposite signs then
    /// return true. Note that it is technically possible to miss an
    /// intersection if, for example, the surface curves through a
    /// facet, but does not pass either side of two vertices.

    // FIXME: delete
    bool entity_intersects_f0(const dolfin::MeshEntity& cell) const;

    // --- narwal::GenericSurface interface

    /// Returns a pair denoting the type of entity
    /// intersection with the surface
    /// <inside, outside>
    /// <true, false> - intersects fully inside surface
    /// <true, true> - intersects at boundary of surface
    /// <false, true> - intersects f0, but not f1.
    /// <false, false> - no intersection
    std::pair<bool, bool>
      entity_intersection(const dolfin::MeshEntity& cell) const;

    /// Determine if an intersected cell contains the surface edge
    bool contains_boundary(const dolfin::Cell& cell) const;

    /// Determine side of surface (+1/-1) above/below, zero on surface.
    int side(const dolfin::Point p, double eps=0.0) const;

    /// Determine normal vector of surface pointing in plus direction
    virtual std::vector<double> normal(const dolfin::Point p,
                                       double eps) const;

    /// Compute intersection point of an edge with a surface
    /// iterating to a tolerance eps.
    dolfin::Point compute_intersection(const dolfin::Edge& edge,
                                       const double eps=1.0e-10) const;

    /// Triangulate a cell that is intersected by surface.
    /// Return list of points, and associated topology.
    SubTriangulation triangulate(const dolfin::Cell& cell) const;

  };

}


#endif
