// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_ELASTICITYPROBLEM3D_ENRICHED_H
#define NARWAL_ELASTICITYPROBLEM3D_ENRICHED_H

#include <memory>
#include <utility>
#include <vector>

#include <boost/multi_array.hpp>

#include "XFEMForms.h"
#include "Evaluator.h"

namespace dolfin
{
  class Point;
  class Cell;
  class FiniteElement;
  class GenericFunction;
}

namespace ufc
{
  class cell_integral;
}

namespace narwal
{

  /// Elasticity Problem in 3D
  class ElasticityProblem3D : public narwal::XFEMForms
  {

  public:

    /// Constructor
    ElasticityProblem3D(
      std::shared_ptr<const narwal::XFEMFunctionSpace> V,
      std::shared_ptr<const dolfin::GenericFunction> lambda,
      std::shared_ptr<const dolfin::GenericFunction> mu,
      std::shared_ptr<const dolfin::GenericFunction> surface_pressure,
      const int polynomialDegree);

    /// Destructor
    ~ElasticityProblem3D();

    /// Tabulate tensor
    void tabulate_tensor(double* A,
                         double* b,
                         const double* const * coefficients,
                         const dolfin::Cell& cell,
                         const std::vector<double>& vertex_coords,
                         const ufc::cell_integral& cell_integral) const;

    /// Tabulate tensor
    void evaluate(
      dolfin::Array<double>& values,
      const dolfin::Point& x_eval,
      const dolfin::Point& x_eval_heaviside,
      const double* const coefficients,
      const dolfin::Cell& cell,
      const std::vector<double>& vertex_coords,
      const Evaluator::Mode evalMode,
      const int surface_index
    ) const;

  private:

    // Tabulate tensor for enriched cells
    void
      tabulate_tensor_enriched(double* A, double* b,
                               const dolfin::Cell& cell,
                               const std::vector<double>& vertex_coords) const;

    void evaluate_enriched(
      dolfin::Array<double>& values,
      const dolfin::Point& x_eval,
      const dolfin::Point& x_eval_heaviside,
      const double* const coefficients,
      const dolfin::Cell& cell,
      const std::vector<double>& vertex_coords,
      const Evaluator::Mode evalMode,
      const int surface_index
      ) const;

    static double compute_matrix_entry(int i_regular, int j_regular,
                                       int gdim,
                                       double Hi, double Hj,
                                       double mu, double lambda,
                                       double weight,
                                       const double* dphi_I_dx,
                                       const double* dphi_J_dx);

    void fill(
      std::vector<std::pair<std::array<int, 2>, double>>& local_dof_index_i,
      const dolfin::Point x_q,
      const std::size_t node_i,
      const boost::multi_array<double, 2>& coordinates,
      const dolfin::Cell& cell
      ) const;

    void fillWithGivenSide(
      std::vector<std::pair<std::array<int, 2>, double>>& local_dof_index_i,
      const std::size_t node_i,
      const boost::multi_array<double, 2>& coordinates,
      const dolfin::Cell& cell,
      const int surface_index,
      const double H_side
      ) const;

    void computeValues(
      dolfin::Array<double>& values,
      const std::vector<std::pair<std::array<int, 2>, double>>& local_dof_index_i,
      const double N_node_i,
      const double* const coefficients
      ) const;

    // Underlying finite element type (scalar)
    std::shared_ptr<const dolfin::FiniteElement> _element;

    // Elasticity parameters
    std::shared_ptr<const dolfin::GenericFunction> _lambda;
    std::shared_ptr<const dolfin::GenericFunction> _mu;

    // Pressure on crack surface
    std::shared_ptr<const dolfin::GenericFunction> _pressure;

  };

}
#endif
