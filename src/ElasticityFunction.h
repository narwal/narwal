// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_ELASTICITY_FUNCTION_H
#define NARWAL_ELASTICITY_FUNCTION_H

#include <memory>
#include <utility>
#include <vector>

#include <dolfin/function/Function.h>
#include <dolfin/function/GenericFunction.h>
#include <dolfin/io/XDMFFile.h>

#include "ConformingMeshGenerator.h"
#include "TimeMonitor.h"
#include "XFEMForms.h"
#include "XFEMFunctionSpace.h"


namespace dolfin
{
  class Cell;
  class FiniteElement;
  class Function;
  class Mesh;
  template<typename T> class Array;
}

namespace ufc
{
  class cell;
}

namespace narwal
{

  class DofMap;
  class DomainManager;
  class XFEMForms;

  /// This class adds a function to evaluate an XFEM function on a
  /// regular mesh. The mesh typically conforms to cracks surfaces.

  class ElasticFunction : public dolfin::GenericFunction
  {
  public:

    /// Constructor
    ElasticFunction(const dolfin::Mesh& mesh, const dolfin::Function& u,
                    const DomainManager& domain,
                    const int polynomialOrder);

    /// alternative constructor for multicracks
    ElasticFunction(
      const std::shared_ptr<const dolfin::Mesh> conforming_mesh,
      const std::shared_ptr<const dolfin::Function> u,
      const std::shared_ptr<const narwal::DomainManager> domain_manager,
      const std::shared_ptr<const narwal::XFEMForms> forms);

    /// Destructor
    virtual ~ElasticFunction();

    /// Return value rank
    std::size_t value_rank() const;

    /// Return value dimension for given axis
    std::size_t value_dimension(std::size_t i) const;

    /// Evaluate at given point in given cell
    void eval(dolfin::Array<double>& values,
              const dolfin::Array<double>& x,
              const ufc::cell& ufc_cell) const;

    /// Evaluate at given point
    void eval(dolfin::Array<double>& values,
              const dolfin::Array<double>& x) const;

    /// Restrict function to local cell (compute expansion
    /// coefficients w)
    void restrict(double* w,
                  const dolfin::FiniteElement& element,
                  const dolfin::Cell& dolfin_cell,
                  const double* vertex_coordinates,
                  const ufc::cell& ufc_cell) const;

    /// Compute values at all mesh vertices
    void compute_vertex_values(std::vector<double>& vertex_values,
                               const dolfin::Mesh& mesh) const;

    int polynomial_order() const
    { return _polynomialOrder; }

    template<class FuncSpace>
      static void write_crack_conforming_solution(
        const narwal::DomainManager& domainManager,
        const narwal::XFEMForms& xfem_forms,
        const dolfin::Function& u,
        const std::string &testname)
    {
      narwal::Time t("narwal::ElasticFunction::write_crack_conforming_solution()");
      std::cout << "narwal::ElasticFunction::write_crack_conforming_solution()"
                << std::endl;

      // Create mesh that conforms to crack surface and write to file
      auto conforming_mesh = narwal::ConformingMeshGenerator::build(domainManager);
      const std::string join = testname==std::string() ? "" : "-";
      dolfin::XDMFFile conforming_mesh_file(MPI_COMM_WORLD,
                                            "output/" + testname+join
                                            + "conforming-mesh.xdmf");
      conforming_mesh_file.write(*conforming_mesh);

      // Interpolate XFEM solution onto regular space with mesh
      // conforming to crack surface
      auto V_interp = std::make_shared<FuncSpace>(conforming_mesh);
      dolfin::Function u_interp(V_interp);

      u_interp.interpolate(u);
      dolfin::XDMFFile file_interp_standard(MPI_COMM_WORLD,
                                            "output/" + testname+join
                                            + "interpolated-standard.xdmf");
      file_interp_standard.write(u_interp);

      narwal::ElasticFunction u_test(*conforming_mesh, u, domainManager,
                                     xfem_forms.polynomial_order());
      u_interp.interpolate(u_test);
      dolfin::XDMFFile file_interp(MPI_COMM_WORLD, "output/" + testname
                                   + join + "interpolated-xfem.xdmf");
      file_interp.write(u_interp);
    }

    // use alternative implementation of ElasticFunction to evaluate multiple
    // intersecting cracks
    template<class FuncSpace>
      static void write_crack_conforming_solution(
        const std::shared_ptr<const narwal::DomainManager> domainManager,
        const std::shared_ptr<const narwal::XFEMForms> xfem_forms,
        const std::shared_ptr<const dolfin::Function> u,
        const std::string &testname)
    {
      narwal::Time t("narwal::ElasticFunction::write_crack_conforming_solution() MultiCrack");
      std::cout << "narwal::ElasticFunction::write_crack_conforming_solution() MultiCrack" << std::endl;

      // Create mesh that conforms to crack surface and write to file
      auto conforming_mesh = narwal::ConformingMeshGenerator::build(*domainManager);
      const std::string join = testname==std::string() ? "" : "-";
      dolfin::XDMFFile conforming_mesh_file(MPI_COMM_WORLD,
                                            "output/" + testname+join
                                            + "conforming-mesh.xdmf");
      conforming_mesh_file.write(*conforming_mesh);

      // Interpolate XFEM solution onto regular space with mesh
      // conforming to crack surface
      auto V_interp = std::make_shared<FuncSpace>(conforming_mesh);
      dolfin::Function u_interp(V_interp);

      u_interp.interpolate(*u);
      dolfin::XDMFFile file_interp_standard(MPI_COMM_WORLD,
                                            "output/" + testname+join
                                            + "interpolated-to-nonconforming-mesh.xdmf");
      file_interp_standard.write(u_interp);

      narwal::ElasticFunction u_test(conforming_mesh, u, domainManager, xfem_forms);
      u_interp.interpolate(u_test);
      dolfin::XDMFFile file_interp(MPI_COMM_WORLD, "output/" + testname + join
                                   + "interpolated-to-conforming-mesh.xdmf");
      file_interp.write(u_interp);
    }

    std::shared_ptr<const dolfin::FunctionSpace> function_space() const
    {
      std::cout << "ElasticityFunction::function_space() not implemented"
                << std::endl;
      return std::shared_ptr<const dolfin::FunctionSpace>();
    }

    private:

      const dolfin::Mesh& _conforming_mesh;
      const dolfin::Function& _u_enriched;
      const DomainManager& _domain;
      const int _polynomialOrder;
      std::shared_ptr<Evaluator> _evaluator;
      bool _useEvaluator;

  };

}
#endif
