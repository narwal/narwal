// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "ConformingMeshGenerator.h"

#include <map>
#include <vector>

#include <dolfin/log/log.h>
#include <dolfin/mesh/Cell.h>
#include <dolfin/mesh/Mesh.h>
#include <dolfin/mesh/MeshEditor.h>
#include <dolfin/mesh/Vertex.h>
#include "DomainManager.h"
#include "GenericSurface.h"
#include "SubTriangulation.h"
#include "SurfaceRefinementND.h"

using namespace narwal;

//-----------------------------------------------------------------------------
std::shared_ptr<dolfin::Mesh>
ConformingMeshGenerator::build(std::shared_ptr<const dolfin::Mesh> mesh,
                               const GenericSurface& surface)
{
  dolfin::Mesh conforming_mesh;
  SurfaceRefinementND::refine(conforming_mesh, mesh, surface);
  return std::make_shared<dolfin::Mesh>(conforming_mesh);
}
//-----------------------------------------------------------------------------
std::shared_ptr<dolfin::Mesh>
ConformingMeshGenerator::build(const DomainManager& domain)
{
  // Get number of surfaces
  const std::size_t num_surfaces = domain.num_surfaces();

  // Iterator over surfaces
  std::shared_ptr<const dolfin::Mesh> mesh0 = domain.mesh();
  dolfin_assert(mesh0);
  std::shared_ptr<dolfin::Mesh> mesh;
  for (std::size_t i = 0; i < num_surfaces; ++i)
  {
    // Get surface
    std::shared_ptr<const narwal::GenericSurface> surface
      = domain.surface(i);
    dolfin_assert(surface);

    mesh = SurfaceRefinementND::refine(mesh0, *surface);
    mesh0 = mesh;
  }

  return mesh;
}
//-----------------------------------------------------------------------------
std::shared_ptr<dolfin::Mesh>
ConformingMeshGenerator::build_surface(const dolfin::Mesh& mesh,
                                       const GenericSurface& surface)
{
  std::vector<dolfin::Point> point_set;
  std::vector<std::size_t> topology;

  std::size_t count = 0;
  std::size_t cell_count = 0;
  for (dolfin::CellIterator cell(mesh); !cell.end(); ++cell)
  {
    // Only get fully intersected cells
    const std::pair<bool, bool> status = surface.entity_intersection(*cell);
    if (status.first && !status.second)
    {
      SubTriangulation s(*cell, surface);
      std::shared_ptr<const dolfin::Mesh> smesh = s.surface_mesh();
      for (dolfin::VertexIterator v(*smesh); !v.end(); ++v)
        point_set.push_back(v->point());
      for (auto p = smesh->cells().begin(); p != smesh->cells().end(); ++p)
        topology.push_back(count + *p);
      count += smesh->num_vertices();
      cell_count += smesh->num_cells();
    }
  }

  // Build mesh
  dolfin::Mesh surface_mesh;
  dolfin::MeshEditor ed;
  ed.open(surface_mesh, 2, 3);

  unsigned int v = 0;
  ed.init_vertices(point_set.size());
  for (auto p = point_set.begin(); p != point_set.end(); ++p)
    ed.add_vertex(v++, *p);

  unsigned int c = 0;
  ed.init_cells(topology.size()/3);
  for (auto p = topology.begin(); p != topology.end(); p += 3)
  {
    ed.add_cell(c, std::vector<std::size_t>(p, p + 3));
    ++c;
  }
  ed.close();

  return std::make_shared<dolfin::Mesh>(surface_mesh);
}
//-----------------------------------------------------------------------------
