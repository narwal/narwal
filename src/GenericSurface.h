// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_GENERICSURFACE_H
#define NARWAL_GENERICSURFACE_H

#include <utility>
#include <vector>

namespace dolfin
{
  class Cell;
  class Edge;
  class MeshEntity;
  class Point;
}

namespace narwal
{

  class SubTriangulation;

  /// Base class for surface implementations in Narwal.

  class GenericSurface
  {
  public:

    /// Destructor
    virtual ~GenericSurface() {}

    /// Returns a pair denoting the type of intersection of
    /// MeshEntity with the surface <inside, outside>, i.e.:
    ///
    ///    <false, *>    - no intersection
    ///    <true, false> - full intersected
    ///    <true, true>  - partially intersected, ie. conatins
    ///                    boundary of surface
    virtual std::pair<bool, bool>
      entity_intersection(const dolfin::MeshEntity& cell) const = 0;

    /// Determine if an intersected cell contains the surface edge
    virtual bool contains_boundary(const dolfin::Cell& cell) const = 0;

    /// Determine side of surface (+1/-1) above/below (zero on
    /// surface)
    virtual int side(const dolfin::Point p, double eps) const = 0;

    /// Determine normal vector of surface pointing in plus direction
    virtual std::vector<double> normal(const dolfin::Point p,
                                       double eps) const = 0;

    /// Compute intersection point of an edge with a surface
    /// iterating to a tolerance eps.
    virtual dolfin::Point
      compute_intersection(const dolfin::Edge& edge,
                           const double eps=1.0e-10) const = 0;

    /// Triangulate a cell that is intersected by surface.
    /// Returns list of points and associated topology.
    virtual SubTriangulation triangulate(const dolfin::Cell& cell) const = 0;

  };

}

#endif
