// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include <dolfin.h>
#include <ElasticityProblem3D.h>
#include <Elasticity3D_P1.h>
#include <Narwal.h>

// Pressure on crack surface
class CrackPressure : public dolfin::Expression
{
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    values[0] = -10.0;
  }
};

// Dirichlet boundary condition function
class BoundaryValue : public dolfin::Expression
{
public:

  BoundaryValue() : Expression(3) {}

  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    if (x[1] < 0.0)
    {
      values[0] = 0.0;
      values[1] = 0.0;
      values[2] = 0.0;
    }
    else
    {
      values[0] = 0.0;
      values[1] = 1.0;
      values[2] = 0.0;
    }
  }
};

class DirichletBoundary : public dolfin::SubDomain
{  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
  {
    //return std::abs(x[1] + 0.2) < 1.0e-4
    //                              || std::abs(x[1] - 0.2) < 1.0e-4;
    return std::abs(x[1] + 0.2) < 1.0e-4;
  }
};

// Surface representation class
class EllipticalSurface : public narwal::ImplicitSurface
{
public:
  EllipticalSurface(double h, double a, double b, double theta)
    : narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1), _h(h), a(a),
      b(b), beta(0.0), _theta(theta) {}

  // Normal distance from the crack surface
  double f0(const dolfin::Point& p) const
  {
    const double x = std::cos(_theta)*p.x() - std::sin(_theta)*p.y();
    const double y = std::sin(_theta)*p.x() + std::cos(_theta)*p.y();
    const double z = p.z();
    const double d = (x/a)*(x/a) + (z/b)*(z/b) - 1.0;
    return (y - _h - beta*d);
  }

  // If f0 = 0, then if f1 < 1 -> on surface and f1 >= = -> not on surface
  double f1(const dolfin::Point& p) const
  {
    const double x = std::cos(_theta)*p.x() - std::sin(_theta)*p.y();
    const double z = p.z();
    const double d = (x/a)*(x/a) + (z/b)*(z/b);
    if (d < 1.0)
      return -1.0;
    else
      return 1.0;
  }

  // Surface height position (average y)
  const double _h;

  // Ellipse major and minor radii
  const double a, b;

  // Ellipse curvature
  const double beta;

 // Rotate axis
  const double _theta;

};

int main()
{
 // Create mesh
  std::shared_ptr<dolfin::Mesh>
    mesh(new dolfin::BoxMesh(dolfin::Point(-0.4, -0.2, -0.4),
                             dolfin::Point(0.4, 0.2, 0.4),
                             15, 15, 15));

  mesh->init(2);
  dolfin::FacetFunction<std::size_t> ff(mesh, 0);
  dolfin::File ff_file("facets.pvd");
  ff_file << ff;

  // Create fracture surfaces
  std::shared_ptr<EllipticalSurface>
    surface0(new EllipticalSurface(0.07, 0.3, 0.3, 0.0));
  std::shared_ptr<EllipticalSurface>
    surface1(new EllipticalSurface(0.07, 0.3, 0.3, DOLFIN_PI/2.0));

  // Create DomainManager
  auto domain_manager =
    std::make_shared<narwal::DomainManager>(mesh);
  //domain_manager->register_surface(surface0, "surface0");
  domain_manager->register_surface(surface1, "surface1");

  // Create standard DOLFIN function space
  std::shared_ptr<dolfin::FunctionSpace>
    V_s(new Elasticity3D_P1::FunctionSpace(mesh));

  // Create XFEM space (this computes the enriched dofmap)
  std::shared_ptr<narwal::XFEMFunctionSpace>
    V(new narwal::XFEMFunctionSpace(V_s, domain_manager, 48));

  // Test dofmap
  /*
  std::shared_ptr<const narwal::DofMap> dofmap = V->dofmap();
  for (std::size_t c = 0; c < mesh->num_cells(); ++c)
  {
    if (dofmap->cell_dofs(c).size() > 12)
    {
      std::cout << "Cell: " << c << std::endl;
      for (std::size_t reg_dof = 0; reg_dof < 12; ++reg_dof)
      {
        const std::vector<std::pair<int, std::array<short int, 2>>>&
          enriched_dof_indices = dofmap->enriched_dofs(c, reg_dof);

        if (!enriched_dof_indices.empty())
          std::cout << "  regular dof index (local): " << reg_dof << std::endl;
        for (auto edofs : enriched_dof_indices)
        {
          std::cout << "    enriched dof index (local): "
                    << edofs.first << std::endl;
          std::cout << "    enriched dof index (global): "
                    << dofmap->cell_dofs(c)[edofs.first] << std::endl;
          std::cout << "    enriched dof surfaces: "
                    << edofs.second[0] << ", " << edofs.second[1] << std::endl
                    << "    --" << std::endl;
        }
      }
    }
  }
  std::cout << "Dofmap dim: " << dofmap->global_dimension() << std::endl;
  */

  // Elasticity parameters (as coefficients to allow spatial variation)
  const double E = 1.0;
  const double nu = 0.2;
  std::shared_ptr<dolfin::GenericFunction>
    mu(new dolfin::Constant(E/(2.0*(1.0 + nu))));
  std::shared_ptr<dolfin::GenericFunction>
    lambda(new dolfin::Constant(E*nu/((1.0 + nu)*(1.0 - 2.0*nu))));

  // Crack pressure function
  std::shared_ptr<dolfin::GenericFunction> crack_pressure(new CrackPressure());

  // Create XFEM elasticity forms
  std::shared_ptr<narwal::XFEMForms>
    xfem_forms(new narwal::ElasticityProblem3D(V, lambda, mu,
                                               crack_pressure, 1));
  // Create Dirichlet boundary conditions
  auto boundary
    = std::make_shared<DirichletBoundary>(DirichletBoundary());
  auto boundary_value
    = std::make_shared<BoundaryValue>(BoundaryValue());
  std::shared_ptr<dolfin::DirichletBC>
    bc(new dolfin::DirichletBC(V_s, boundary_value, boundary));
  std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs;
  bcs.push_back(bc);

  // Create XFEM problem
  auto elasticity_problem
    = std::make_shared<narwal::XFEMProblem>(xfem_forms, bcs);

  // Create that conforms to crack surface and write to file
  std::shared_ptr<dolfin::Mesh> conforming_mesh
    = narwal::ConformingMeshGenerator::build(*domain_manager);
  dolfin::XDMFFile conforming_mesh_file(mesh->mpi_comm(),
                                        "output/conforming_mesh.xdmf");
  conforming_mesh_file.write(*conforming_mesh);

  dolfin::XDMFFile mesh_file(mesh->mpi_comm(), "output/mesh.xdmf");
  mesh_file.write(*mesh);

  // Build surface meshes for visualisation
  std::shared_ptr<dolfin::Mesh> surface_mesh0
    = narwal::ConformingMeshGenerator::build_surface(*mesh, *surface0);
  dolfin::XDMFFile surface0_file(mesh->mpi_comm(), "output/surfaceA.xdmf");
  surface0_file.write(*surface_mesh0);

  std::shared_ptr<dolfin::Mesh> surface_mesh1
    = narwal::ConformingMeshGenerator::build_surface(*mesh, *surface1);
  dolfin::File surface1_file(mesh->mpi_comm(), "output/surfaceB.xdmf");
  surface1_file.write(*surface_mesh1);

  // Assemble system
  dolfin::PETScMatrix A;
  dolfin::PETScVector b;
  elasticity_problem->assemble(A, b);

  /*
  dolfin::SLEPcEigenSolver esolver(A);
  esolver.parameters["solver"] = "lapack";
  esolver.solve();
  for (std::size_t i = 0; i < A.size(0); ++i)
  {
    double er, ec;
    esolver.get_eigenvalue(er, ec, i);
    if (std::abs(er) < 1.0e-8)
    {
      std::cout << "** eigen val: " << i << ", " << er << std::endl;
      dolfin::PETScVector rx, cx;
      esolver.get_eigenpair(er, ec, rx, cx, i);
      std::vector<double> evector;
      rx.get_local(evector);
      for (std::size_t i = 0; i < evector.size(); ++i)
      {
        if (std::abs(evector[i]) > 1.0e-4)
        {
          std::cout << "Non-zero evec component: " << evector[i] << ", " << i << std::endl;
        }
      }
    }
  }
  */

  // Create solution Function
  dolfin::Function u(V->function_space());

  // Solve
  #if PETSC_HAVE_SUPERLU
  dolfin::LUSolver solver("superlu");
  #else
  dolfin::LUSolver solver;
  #endif
  std::cout << "Size: " << u.vector()->size() << std::endl;
  std::cout << "Solver Ax=b. This can take some time for large problems . . ."
    << std::endl;
  solver.solve(A, *u.vector(), b);
  std::cout << "Solution vector norm: " << u.vector()->norm("l2")
            << std::endl;
  std::cout << "Max vector entry: " << u.vector()->max() << std::endl;
  std::cout << "Min vector entry: " << u.vector()->min() << std::endl;


  // Write solution to PVD file to visualise with ParaView
  dolfin::File file_u("output/u.pvd");
  file_u << u;

  return 0;

  // Create that conforms to crack surface and write to file
  //std::shared_ptr<dolfin::Mesh> conforming_mesh
  //  = narwal::ConformingMeshGenerator::build(*domain_manager);
  //dolfin::File conforming_mesh_file("output/conforming_mesh.xdmf");
  //conforming_mesh_file << *conforming_mesh;

  // TODO: Interpolate XFEM solution onto regular space with mesh
  // conforming to crack surface
  //Elasticity3D_P1::FunctionSpace V_interp(conforming_mesh);
  //dolfin::Function u_interp(V_interp);

  //u_interp.interpolate(u);
  //dolfin::File file_interp_standard("output/interpolated-standard.pvd");
  //file_interp_standard << u_interp;

  //narwal::ElasticFunction u_test(*conforming_mesh, u, *domain_manager,
  //                               xfem_forms->polynomial_order());
  //u_interp.interpolate(u_test);
  //dolfin::File file_interp("output/interpolated-xfem.pvd");
  //file_interp << u_interp;

  return 0;
}
