// Copyright (C) 2016 Melior Innovations
// Author: Chris Richardson
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include <dolfin.h>
#include <Narwal.h>

#include "P1.h"

// Surface representation class
class FlatSurface : public narwal::ImplicitSurface
{
public:
  FlatSurface(double theta) :
    narwal::ImplicitSurface(dolfin::Sphere(dolfin::Point(0.0, 0.0, 0.0), 3.1),
                              "manifold"), beta(0.0), theta(theta)
  {
    // Add polylines
    std::vector<dolfin::Point> polyline;
    a = 0.4;
    b = 0.3;
    for (unsigned int i = 0; i < 100; i++)
    {
      const double th = 2.0*M_PI*(double)i/100.0;
      polyline.push_back(dolfin::Point(a*cos(th), 0.0,  b*sin(th)));
    }

    polyline.push_back(polyline.front());
    polylines.push_back(polyline);
  }

  double f0(const dolfin::Point& p) const
  {
    const dolfin::Point nhat(cos(theta), sin(theta), 0.0);
    const double dist = nhat.dot(p);
    return dist;
  }

  double f1(const dolfin::Point& p) const
  {
    const double x = p.x();
    const double y = p.y();
    const double z = p.z();
    const double d = (x/a)*(x/a) + (z/b)*(z/b);
    if (d < 1.0)
      return -1.0;
    else
      return 1.0;
  }

  double a, b, beta, theta;
};

class CurvedSurface : public narwal::ImplicitSurface
{
public:
  CurvedSurface() :
    narwal::ImplicitSurface(dolfin::Sphere(dolfin::Point(0.0, 0.0, 0.0), 3.1),
                              "manifold"), a(50), b(50), beta(5.0) {}

  // Normal distance from the crack surface
  double f0(const dolfin::Point& p) const
  {
    const double x = p.x();
    const double y = p.y();
    double d = sin(y / 10.0);
    return x - beta*d;
  }

  // If f0 = 0, then if f1<1 -> on surface and f1 >= = -> not on surface
  double f1(const dolfin::Point& p) const
  {
    //const double x = p.x();
    const double y = p.y();
    const double z = p.z();
    const double d = (y/a)*(y/a) + (z/b)*(z/b) - 1.0;
    if (d < 0.0)
      return -1.0;
    else
      return 1.0;
  }

  // Ellipse major and minor radii
  const double a, b;

  // Ellipse curvature
  const double beta;
};


int main()
{
  std::shared_ptr<dolfin::Mesh> mesh;
  std::shared_ptr<narwal::ImplicitSurface> surface;

  // Case A
  {
    // Create box domain mesh
    mesh.reset(new dolfin::BoxMesh(dolfin::Point(-0.5, -0.1, -0.5),
                                   dolfin::Point(0.5, 0.1, 0.5),
                                   28, 6, 28));

    // Create fracture surface
    surface.reset(new FlatSurface(9.0*M_PI/16.0));
  }

  // Case B (see README.md on how to get mesh file)
  {
    // Create box domain mesh
    mesh.reset(new dolfin::Mesh("mesh_box.xml"));

    // Refine mesh
    mesh = std::make_shared<dolfin::Mesh>(dolfin::refine(*mesh));
    mesh = std::make_shared<dolfin::Mesh>(dolfin::refine(*mesh));

    // Create fracture surface
    surface.reset(new CurvedSurface());
  }

  // Compute CellFunction of intersected cells
  const dolfin::CellFunction<bool> intersected_cells
    = narwal::XFEMTools::compute_intersected_cells(*mesh, *surface);
  dolfin::File file_markers("output/cell_markers.pvd");
  file_markers << intersected_cells;

  // Create mesh that conforms to crack surface and write to file
  const dolfin::Mesh conforming_mesh
    = narwal::XFEMTools::compute_combined_mesh(*mesh, *surface);
  dolfin::XDMFFile mesh_with_crack(mesh->mpi_comm(),
                                   "output/conforming_mesh.xdmf");
  mesh_with_crack.write(conforming_mesh);

  // Check quality of conforming mesh
  std::pair<double, double> radii
    = dolfin::MeshQuality::radius_ratio_min_max(conforming_mesh);
  std::cout << "Max, min cell radii ratios: " << radii.second << ", "
            << radii.first << std::endl;

  // Create mesh of surface and write to file
  const dolfin::Mesh surf_mesh
    = narwal::XFEMTools::compute_intersection_surface(*mesh, *surface);
  dolfin::XDMFFile surf_file(mesh->mpi_comm(),
                             "output/crack_surface.xdmf");
  surf_file.write(surf_mesh);

  // Marks cells either side of conforming mesh
  dolfin::CellFunction<int> cell_marker0(conforming_mesh, 0);
  for (dolfin::CellIterator c(conforming_mesh); !c.end(); ++c)
  {
    const dolfin::Point midpoint = c->midpoint();
    cell_marker0[*c] = surface->side(c->midpoint());
  }
  dolfin::XDMFFile cell_marker0_file(mesh->mpi_comm(),
                                     "output/conforming_cell_markers.xdmf");
  cell_marker0_file.write(cell_marker0);

  // Warp conforming mesh to test for correctness
  P1::FunctionSpace P1(conforming_mesh);
  dolfin::Function warped(P1);
  std::vector<double> zvec(conforming_mesh.num_vertices()*3, 0.0);
  const std::vector<int> q = dolfin::vertex_to_dof_map(P1);
  for (dolfin::CellIterator c(conforming_mesh); !c.end(); ++c)
  {
    // Determine crack side
    double side = 0.0;
    const double f1 = surface->f1(c->midpoint());
    if (f1 < 1.0)
    {
      double f0_sum = 0.0;
      for(dolfin::VertexIterator v(*c); !v.end(); ++v)
        f0_sum += surface->f0(v->point());
      side = f0_sum > 0.0 ? 1.0 : -1.0;
    }

    // Assign value to 'warp' mesh
    for(dolfin::VertexIterator v(*c); !v.end(); ++v)
    {
      const std::size_t vertex_index = v->index();
      if (f1 < 1.0)
      {
        zvec[q[vertex_index*3]] = side;
        //zvec[q[vertex_index*3 + 1]] = 0.0;
        //zvec[q[vertex_index*3 + 2]] = 0.0;
      }
    }
  }
  warped.vector()->set_local(zvec);
  dolfin::XDMFFile mesh_warped_file(mesh->mpi_comm(),
                                    "output/conforming_mesh_warp.xdmf");
  mesh_warped_file.write(warped);

  return 0;
}
