// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include <dolfin.h>
#include <Narwal.h>
#include <Elasticity3D_P1.h>
#include <ElasticityFunction.h>
#include <ElasticityProblem3D.h>



// Pressure on crack surface
class CrackPressure : public dolfin::Expression
{
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    values[0] = -0.0;
  }
};

// Dirichlet boundary condition function
class BoundaryValue : public dolfin::Expression
{
public:

  BoundaryValue() : Expression(3) {}

  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    if (x[1] < 0.0)
    {
      values[0] = 0.0;
      values[1] = 0.0;
      values[2] = 0.0;
    }
    else
    {
      values[0] = 0.0;
      values[1] = 1.0;
      values[2] = 0.0;
    }
  }
};

class DirichletBoundary : public dolfin::SubDomain
{  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
   {
     return std::abs(x[1] + 0.2) < 1.0e-4
                                   || std::abs(x[1] - 0.2) < 1.0e-4;
   }
};

// Surface representation class
class EllipticalSurface : public narwal::ImplicitSurface
{
public:
  EllipticalSurface(double h, double theta)
    : narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1), _h(h), a(0.3),
      b(0.3), beta(0.05), _theta(theta) {}

  // Normal distance from the crack surface
  double f0(const dolfin::Point& p) const
  {
    const double x = std::cos(_theta)*p.x() - std::sin(_theta)*p.y();
    const double y = std::sin(_theta)*p.x() + std::cos(_theta)*p.y();
    const double z = p.z();
    const double d = (x/a)*(x/a) + (z/b)*(z/b) - 1.0;
    return (y - _h - beta*d);
  }

  // If f0 = 0, then if f1 < 1 -> on surface and f1 >= = -> not on surface
  double f1(const dolfin::Point& p) const
  {
    const double x = std::cos(_theta)*p.x() - std::sin(_theta)*p.y();
    const double z = p.z();
    const double d = (x/a)*(x/a) + (z/b)*(z/b);
    if (d < 1.0)
      return -1.0;
    else
      return 1.0;
  }

  // Surface height position (average y)
  const double _h;

  // Ellipse major and minor radii
  const double a, b;

  // Ellipse curvature
  const double beta;

 // Rotate axis
  const double _theta;

};

int main()
{
 // Create mesh
  std::shared_ptr<dolfin::Mesh>
    mesh(new dolfin::BoxMesh(dolfin::Point(-0.4, -0.2, -0.4),
                             dolfin::Point(0.4, 0.2, 0.4),
                             15, 15, 15));

  // Create fracture surfaces
  std::shared_ptr<EllipticalSurface> surface0(new EllipticalSurface(-0.101,
                                                                    0.0));
  std::shared_ptr<EllipticalSurface> surface1(new EllipticalSurface(0.103,
                                                                    0.0));
  std::shared_ptr<EllipticalSurface> surface2(new EllipticalSurface(0.002,
                                                                    0.0));

  // Create DomainManager
  auto domain_manager =
    std::make_shared<narwal::DomainManager>(mesh);
  domain_manager->register_surface(surface0, "surface0");
  domain_manager->register_surface(surface1, "surface1");
  domain_manager->register_surface(surface2, "surface2");

  // Create standard DOLFIN function space
  std::shared_ptr<dolfin::FunctionSpace>
    V_s(new Elasticity3D_P1::FunctionSpace(mesh));

  // Create XFEM space holder (this computes the enriched dofmap)
  std::shared_ptr<narwal::XFEMFunctionSpace>
    V(new narwal::XFEMFunctionSpace(V_s, domain_manager, 24));

  // Elasticity parameters (as coefficients to allow spatial variation)
  const double E = 1.0;
  const double nu = 0.2;
  std::shared_ptr<dolfin::GenericFunction>
    mu(new dolfin::Constant(E/(2.0*(1.0 + nu))));
  std::shared_ptr<dolfin::GenericFunction>
    lambda(new dolfin::Constant(E*nu/((1.0 + nu)*(1.0 - 2.0*nu))));

  // Crack pressure function
  std::shared_ptr<dolfin::GenericFunction> crack_pressure(new CrackPressure());

  // Create XFEM elasticity forms
  std::shared_ptr<narwal::XFEMForms>
    xfem_forms(new narwal::ElasticityProblem3D(V, lambda, mu,
                                               crack_pressure, 1));

  // Create Dirichlet boundary conditions
  auto boundary
    = std::make_shared<DirichletBoundary>(DirichletBoundary());
  auto boundary_value
    = std::make_shared<BoundaryValue>(BoundaryValue());
  std::shared_ptr<dolfin::DirichletBC>
    bc(new dolfin::DirichletBC(V_s, boundary_value, boundary));
  std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs;
  bcs.push_back(bc);

  // Create XFEM problem
  auto elasticity_problem
    = std::make_shared<narwal::XFEMProblem>(xfem_forms, bcs);

  // Assemble system
  dolfin::PETScMatrix A;
  dolfin::PETScVector b;
  elasticity_problem->assemble(A, b);

  // Create solution Function
  dolfin::Function u(V->function_space());

  // Solve
  #if PETSC_HAVE_SUPERLU
  dolfin::LUSolver solver("superlu");
  #else
  dolfin::LUSolver solver;
  #endif
  std::cout << "Size: " << u.vector()->size() << std::endl;
  std::cout << "Solver Ax=b. This can take some time for large problems . . ."
    << std::endl;
  solver.solve(A, *u.vector(), b);
  std::cout << "Solution vector norm: " << u.vector()->norm("l2")
            << std::endl;
  std::cout << "Max vector entry: " << u.vector()->max() << std::endl;
  std::cout << "Min vector entry: " << u.vector()->min() << std::endl;

  // Write solution to PVD file to visualise with ParaView
  dolfin::File file_u("output/u.pvd");
  file_u << u;

  narwal::ElasticFunction::write_crack_conforming_solution<Elasticity3D_P1::FunctionSpace>(
    *domain_manager, *xfem_forms, u, "elasticity-multi");

  return 0;
}
