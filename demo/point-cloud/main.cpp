// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include <dolfin.h>
#include <Narwal.h>

int main()
{
  // Create a mesh
  auto mesh = std::make_shared<dolfin::BoxMesh>(dolfin::Point(-0.5, -0.1, -0.5),
                                                dolfin::Point(0.5, 0.1, 0.5),
                                                8, 1, 8);

  std::vector<dolfin::Point> points;
  std::vector<double> values;
  int i = 0;
  for (dolfin::CellIterator c(*mesh); !c.end(); ++c)
  {
    points.push_back(c->midpoint());
    values.push_back(i%3);
    i++;
  }

  dolfin::XDMFFile file(mesh->mpi_comm(), "point-cloud.xdmf");
  file.write(points, values);

  dolfin::XDMFFile file1(mesh->mpi_comm(), "mesh.xdmf");
  file1 << *mesh;

  dolfin::CellFunction<std::size_t> cf(mesh, 3);
  dolfin::File file_cf("mf_cell.pvd");
  file_cf << cf;

  dolfin::VertexFunction<std::size_t> vf(mesh, 3);
  dolfin::File file_vf("mf_vertex.pvd");
  file_vf << vf;

  return 0;
}
