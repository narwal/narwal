// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include <dolfin.h>
#include <Narwal.h>
#include <Elasticity3D_P1.h>
#include <ElasticityFunction.h>
#include <ElasticityProblem3D.h>

// Pressure on crack surface
class CrackPressure : public dolfin::Expression
{
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    values[0] = -2.0*(x[0]*x[0] + x[2]*x[2] + 1.0);
  }
};

// Dirichlet boundary condition function
class BoundaryValue : public dolfin::Expression
{
public:

  BoundaryValue() : Expression(3) {}

  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    if (x[1] < 0.0)
    {
      values[0] = 0.0;
      values[1] = 0.0;
      values[2] = 0.0;
    }
    else
    {
      values[0] = 0.0;
      values[1] = 1.0;
      values[2] = 0.0;
    }
  }
};

// Sub domain for Dirichlet boundary condition
class DirichletBoundary : public dolfin::SubDomain
{  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
   {
     //return std::abs(x[1] + 0.1) < 1.0e-4
     //                              || std::abs(x[1] - 0.1) < 1.0e-4;
     return std::abs(x[1] + 0.1) < 1.0e-4;
   }
};

// Surface representation class
class EllipticalSurface : public narwal::ImplicitSurface
{
public:
  EllipticalSurface()
    : narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1), a(0.2),
      b(0.5), beta(0.0) {}

  // Normal distance from the crack surface
  double f0(const dolfin::Point& p) const
  {
    const double x = p.x();
    const double y = p.y();
    const double z = p.z();
    const double d = (x/a)*(x/a) + (z/b)*(z/b) - 1.0;
    return (y - beta*d);
  }

  // If f0 = 0, then if f1<1 -> on surface and f1 >= = -> not on surface
  double f1(const dolfin::Point& p) const
  {
    const double x = p.x();
    //const double y = p.y();
    const double z = p.z();
    const double d = (x/a)*(x/a) + (z/b)*(z/b);
    if (d < 1.0)
      return -1.0;
    else
      return 1.0;
  }

  // Ellipse major and minor radii
  const double a, b;

  // Ellipse curvature
  const double beta;
};


int main()
{
  // NOTE: Make sure that the number of cells in the y-direction is
  //       odd, otherwise surface lies precisely on cell boundary.
  //       Create box domain mesh that contains surface
  std::shared_ptr<dolfin::Mesh>
    mesh(new dolfin::BoxMesh(dolfin::Point(-0.4, -0.1, -0.4),
                             dolfin::Point(0.4, 0.1, 0.4),
                             15, 15, 15));

  // Create fracture surface
  std::shared_ptr<EllipticalSurface> surface(new EllipticalSurface());

  // Create DomainManager
  auto domain_manager = std::make_shared<narwal::DomainManager>(mesh);
  domain_manager->register_surface(surface, "surface0");

  // Create standard DOLFIN function space
  std::shared_ptr<dolfin::FunctionSpace>
    V_s(new Elasticity3D_P1::FunctionSpace(mesh));

  // Create XFEM space holder (this computes the enriched dofmap)
  std::shared_ptr<narwal::XFEMFunctionSpace>
    V(new narwal::XFEMFunctionSpace(V_s, domain_manager, 24));

  std::cout << "Dimension of regular function space: "
            << V_s->dim() << std::endl;

  std::cout << "Dimension of enriched function space: "
            << V->dim() << std::endl;

  // Elasticity parameters (as coefficients to allow spatial variation)
  const double E = 1.0;
  const double nu = 0.2;
  std::shared_ptr<dolfin::GenericFunction>
    mu(new dolfin::Constant(E/(2.0*(1.0 + nu))));
  std::shared_ptr<dolfin::GenericFunction>
    lambda(new dolfin::Constant(E*nu/((1.0 + nu)*(1.0 - 2.0*nu))));

  // Crack pressure function
  std::shared_ptr<dolfin::GenericFunction> crack_pressure(new CrackPressure());

  // Create XFEM elasticity forms
  std::shared_ptr<narwal::XFEMForms>
    xfem_forms(new narwal::ElasticityProblem3D(V, lambda, mu,
                                               crack_pressure, 1));

  // Create Dirichlet boundary conditions
  auto boundary = std::make_shared<DirichletBoundary>();
  auto boundary_value = std::make_shared<BoundaryValue>();
  auto bc = std::make_shared<dolfin::DirichletBC>(V_s, boundary_value, boundary);
  std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs;
  //bcs.push_back(bc);

  // Create XFEM problem
  auto elasticity_problem = std::make_shared<narwal::XFEMProblem>(xfem_forms,
                                                                  bcs);

  // Assemble system
  dolfin::PETScMatrix A;
  dolfin::PETScVector b;
  elasticity_problem->assemble(A, b);
  std::cout << "RHS norm: " << b.norm("l2") << std::endl;

  return 0;

  // Create solution Function
  dolfin::Function u(V->function_space());

  // Solve
  #if PETSC_HAVE_SUPERLU
  dolfin::LUSolver solver;
  //dolfin::LUSolver solver("superlu_dist");
  #else
  dolfin::LUSolver solver;
  #endif
  std::cout << "Size: " << u.vector()->size() << std::endl;
  std::cout << "Solver Ax=b. This can take some time for large problems . . ."
    << std::endl;
  solver.solve(A, *u.vector(), b);
  std::cout << "Solution vector norm: " << u.vector()->norm("l2")
            << std::endl;
  std::cout << "Max vector entry: " << u.vector()->max() << std::endl;
  std::cout << "Min vector entry: " << u.vector()->min() << std::endl;

  // Write (nodal) solution to PVD file to visualise with ParaView
  //dolfin::File file_u("output/u.pvd");
  //file_u << u;

  // Create mesh that conforms to crack surface and write to file
  narwal::ElasticFunction::write_crack_conforming_solution<Elasticity3D_P1::FunctionSpace>(
    *domain_manager, *xfem_forms, u, "elasticity");

  // Output intersected cells
  //const dolfin::CellFunction<bool> intersected_cells
  //  = narwal::XFEMTools::compute_intersected_cells(*domain_manager);
  //dolfin::File file_cells("output/cells.pvd");
  //file_cells << intersected_cells;

  // Output tip facets
  //const dolfin::FacetFunction<bool> tip_facets
  //  = narwal::XFEMTools::compute_boundary_facets(*mesh, *domain_manager);
  //dolfin::File file_facets("output/tip_facets.pvd");
  //file_facets << tip_facets;

  std::cout << "All done" << std::endl;
  return 0;
}
