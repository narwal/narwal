// Copyright (C) 2016 Melior Innovations
// Author: Axel Gerstenberger
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include <set>
#include <dolfin.h>
#include <Narwal.h>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/tokenizer.hpp>
#include <boost/token_functions.hpp>

#ifdef HAS_TRILINOS
#include "Teuchos_TimeMonitor.hpp"
#include "Teuchos_Version.hpp"
#endif

#ifdef HAVE_MPI
#include <mpi.h>
#endif

#include <ElasticityProblem3D.h>
#include <Elasticity3D_P1.h>

// Pressure on crack surface
class CrackPressure : public dolfin::Expression
{
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  { values[0] = -2.0e9; }
};

// Dirichlet boundary condition function
class BoundaryValue : public dolfin::Expression
{
public:

  BoundaryValue() : Expression(3) {}

  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
      values[0] = 0.0;
      values[1] = 0.0;
      values[2] = 0.0;
  }
};

// Sub domain for Dirichlet boundary condition
class DirichletBoundary : public dolfin::SubDomain
{  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
   {
     return std::abs(x[0] + 100.0) < 1.0e-4
                                     || std::abs(x[0] - 100.0) < 1.0e-4;
   }
};

// Surface representation class
class EllipticalSurface : public narwal::ImplicitSurface
{
public:
  EllipticalSurface() :
    narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1), a(90), b(90),
    beta(0.0) {}

  // Normal distance from the crack surface
  double f0(const dolfin::Point& p) const
  {
    const double x = p.x();
    return x;
  }

  // If f0 = 0, then if f1<1 -> on surface and f1 >= = -> not on surface
  double f1(const dolfin::Point& p) const
  {
    //const double x = p.x();
    const double y = p.y();
    const double z = p.z();
    const double d = (y/a)*(y/a) + (z/b)*(z/b);
    if (d < 1.0)
      return -1.0;
    else
      return 1.0;
  }

  // Ellipse major and minor radii
  const double a, b;

  // Ellipse curvature
  const double beta;
};

enum SolverType
{
  EpetraML = 0,
  PETScAMG = 1,
  PETScML = 2,
  PETScLU = 3
};

enum Example
{
  BoxMesh = 0,
  Gmsh = 1
};

#ifdef HAS_TRILINOS
// Create timers
Teuchos::RCP<Teuchos::Time> timeAll
= Teuchos::TimeMonitor::getNewCounter("Main: Overall Time");
#endif

int main(int argc, const char* argv[])
{

#ifdef HAS_TRILINOS
  // Construct a local time monitor, this starts the timer and will
  // stop when leaving scope.
  Teuchos::TimeMonitor LocalTimer(*timeAll);
#endif

  namespace po = boost::program_options;

  bool enableTransformation;
  bool enableDebugOutput;
  int numRefinements = 0;
  int solverTypeI;
  int meshI;

  // Declare the supported options.
  po::options_description desc("Allowed options");
  desc.add_options()
      ("help,h", "produce help message")
      ("solvertype,s", po::value<int>(&solverTypeI)->default_value(1), "select linalg backend and AMG solver\n 0 - Epetra + ML\n*1 - PETSc + PETSc AMG\n 2 - PETSc + ML\n 3 - PETSc + LU")
      ("mesh,m", po::value<int>(&meshI)->default_value(0), "select mesh\n*0 - Regular BoxMesh 10x10x10\n 1 - Irregular Mesh (Gmsh)")
      ("transform,t", po::value<bool>(&enableTransformation)->default_value(true), "enable transformation")
      ("refinement levels,r", po::value<int>(&numRefinements)->default_value(0), "refinement levels")
      ("debug output,d", po::value<bool>(&enableDebugOutput)->default_value(true), "write debug output to screen and files")
      ;
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  const SolverType solverType = (SolverType)(solverTypeI);
  const Example example = (Example)(meshI);

  if (vm.count("help"))
  {
      std::cout << desc << "\n";
      return 1;
  }

  switch (solverType)
  {
  case EpetraML:
    dolfin::parameters["linear_algebra_backend"] = "Epetra";
    break;
  case PETScAMG:
    dolfin::parameters["linear_algebra_backend"] = "PETSc";
    break;
  case PETScML:
    dolfin::parameters["linear_algebra_backend"] = "PETSc";
    break;
  case PETScLU:
    dolfin::parameters["linear_algebra_backend"] = "PETSc";
    break;
  }
  dolfin::parameters["reorder_dofs_serial"] = false;

  // Mesh that contains surface
  std::shared_ptr<dolfin::Mesh> mesh;
  switch (example)
  {
  case BoxMesh:
  {
    const int numNodes = numRefinements;
    const int numCells = numNodes - 1;
    mesh.reset(new dolfin::BoxMesh(dolfin::Point(-100.0, -100.0, -100.0),
                                   dolfin::Point(100.0, 100.0, 100.0),
                                   numCells, numCells, numCells));
    break;
  }
  case Gmsh:
  {
    mesh.reset(new dolfin::Mesh("../LinearElasticityPerformanceMesh.xml"));
    // something funny happens when numRefinements is changed from 2 to 3
    for (short i = 0; i < numRefinements; ++i) {
      dolfin::CellFunction<bool> f(mesh, true);
      *mesh = refine(*mesh, f);
    }
    break;
  }
  }

  dolfin::cout << "Num. mesh vertices: " << mesh->num_vertices()
               << dolfin::endl;

  // Create fracture surface
  auto surface = std::make_shared<EllipticalSurface>();

  // Create DomainManager
  auto domain_manager = std::make_shared<narwal::DomainManager>(mesh);
  domain_manager->register_surface(surface, "surface0");

  // Create standard DOLFIN function space
  auto V_s = std::make_shared<Elasticity3D_P1::FunctionSpace>(mesh);

  // Create XFEM space holder (this computes the enriched dofmap)
  auto V = std::make_shared<narwal::XFEMFunctionSpace>(V_s, domain_manager, 24);

  // Elasticity parameters (as coefficients to allow spatial variation)
  const double E = 20.0e9;
  const double nu = 0.2;
  auto mu = std::make_shared<dolfin::Constant>(E/(2.0*(1.0 + nu)));
  auto lambda = std::make_shared<dolfin::Constant>(E*nu/((1.0 + nu)*(1.0 - 2.0*nu)));

  // Crack pressure function
  auto crack_pressure = std::make_shared<CrackPressure>();

  // Create XFEM elasticity forms
  auto xfem_forms
    = std::make_shared<narwal::ElasticityProblem3D>(V, lambda, mu,
                                                    crack_pressure, 1);

  // Create Dirichlet boundary conditions
  auto boundary = std::make_shared<DirichletBoundary>();
  auto boundary_value = std::make_shared<BoundaryValue>();
  auto bc = std::make_shared<dolfin::DirichletBC>(V_s, boundary_value,
                                                  boundary);
  std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs;
  bcs.push_back(bc);

  // Create XFEM problem
  auto problem = std::make_shared<narwal::XFEMProblem>(xfem_forms, bcs);

  // Assemble system
  std::shared_ptr<dolfin::GenericMatrix> A = narwal::la::initSystemMatrix();
  std::shared_ptr<dolfin::GenericVector> b = narwal::la::initSystemVector();
  problem->assemble(*A, *b);
  //narwal::la::toMatrixMarketFile(A, "output/" + gtest_nameroot() + "_A");

  // Create solution Function
  auto u = std::make_shared<dolfin::Function>(V->function_space());

  auto dofmap = V->function_space()->dofmap();
  auto tdofmap = std::dynamic_pointer_cast<const narwal::DofMap>(dofmap);

  // Create system matrix and rhs vector
  narwal::la::SolverXAMG xSolver(A, b, u, V->function_space(), domain_manager,
                                 tdofmap, xfem_forms,
                                 enableTransformation, enableDebugOutput);

  // Create null space
  xSolver.null_space
    = narwal::xamg::NullSpace3DDisplacement2(V->function_space(),
      tdofmap, b, mesh).build();

  // Create preconditioner & solver
  switch (solverType)
  {
  case EpetraML:
    xSolver.setupDefaultPreCondAndSolver();
    break;
  case PETScAMG:
    xSolver.setupDefaultPreCondAndSolver("petsc_amg");
    break;
  case PETScML:
    xSolver.setupDefaultPreCondAndSolver("ml_amg");
    break;
  case PETScLU:
    xSolver.setupDefaultPreCondAndSolver("ilu");
    break;
  }

  // Solve
  const std::size_t num_iters = xSolver.solve();
  std::cout << "Number of iterations: " << num_iters << std::endl;

#ifdef HAS_TRILINOS
  // Get a summary from the time monitor.
  Teuchos::TimeMonitor::summarize();
#endif

  return 0;
}
