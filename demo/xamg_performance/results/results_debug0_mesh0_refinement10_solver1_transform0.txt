Num. mesh vertices: 1000
Number of dofs in enriched dofmap: 3372
Create PETSc system matrix...
Create PETSc system vector...
!!!!!!!! Create Assemble: 
!!!!!!!! Assemble: 
!!!!!!!! Num cells (a): 
!!!!!!!! Num cells (b): 4374
Max cell dim: 24
*** Warning: Experimental implementation of Dofmap::set(..)
*** Warning: Experimental implementation of Dofmap::set(..)
*** Warning: Experimental implementation of Dofmap::set(..)
Setting up PETScKrylovSolver & PETSc AMG ...
Skipping transformation as requested...
Solve...
  0 KSP unpreconditioned resid norm 4.428220655453e+12 true resid norm 4.428220655453e+12 ||r(i)||/||b|| 1.000000000000e+00
  1 KSP unpreconditioned resid norm 2.102031096116e+12 true resid norm 2.102031096116e+12 ||r(i)||/||b|| 4.746897816684e-01
  2 KSP unpreconditioned resid norm 1.960783889934e+11 true resid norm 1.960783889934e+11 ||r(i)||/||b|| 4.427927247752e-02
  3 KSP unpreconditioned resid norm 3.544425456754e+10 true resid norm 3.544425456754e+10 ||r(i)||/||b|| 8.004175339342e-03
  4 KSP unpreconditioned resid norm 1.408694680225e+10 true resid norm 1.408694680225e+10 ||r(i)||/||b|| 3.181175442309e-03
  5 KSP unpreconditioned resid norm 1.450320688930e+09 true resid norm 1.450320688931e+09 ||r(i)||/||b|| 3.275177101088e-04
  6 KSP unpreconditioned resid norm 1.518163426436e+08 true resid norm 1.518163426445e+08 ||r(i)||/||b|| 3.428382514262e-05
  7 KSP unpreconditioned resid norm 2.195210466165e+07 true resid norm 2.195210466285e+07 ||r(i)||/||b|| 4.957319512933e-06
  8 KSP unpreconditioned resid norm 4.434575432832e+06 true resid norm 4.434575432447e+06 ||r(i)||/||b|| 1.001435063310e-06
  9 KSP unpreconditioned resid norm 1.402707833984e+06 true resid norm 1.402707835098e+06 ||r(i)||/||b|| 3.167655688907e-07
 10 KSP unpreconditioned resid norm 2.710901090700e+05 true resid norm 2.710901076358e+05 ||r(i)||/||b|| 6.121874421545e-08
 11 KSP unpreconditioned resid norm 4.053628006174e+04 true resid norm 4.053628163679e+04 ||r(i)||/||b|| 9.154078983592e-09
Skipping transformation as requested...
Number of iterations: 11
================================================================================

                      TimeMonitor results over 1 processor

Timer Name                                Global time (num calls)    
--------------------------------------------------------------------------------
Main: Overall Time                        0 (1)                      
SolverXAMG: Assemble XFEM system          0.2147 (1)                 
SolverXAMG: Solve                         1.377 (1)                  
SolverXAMG: Transform system              1.407e-05 (1)              
SolverXAMG: Transform system - inverse    0.0002682 (1)              
XAMG: Build transformation matrix         0 (0)                      
XAMG: Invert nullspace                    0 (0)                      
XAMG: MatPtAP                             0 (0)                      
================================================================================
