Num. mesh vertices: 27000
Number of dofs in enriched dofmap: 84378
Create Epetra system matrix...
Create Epetra system vector...
!!!!!!!! Create Assemble: 
!!!!!!!! Assemble: 
!!!!!!!! Num cells (a): 
!!!!!!!! Num cells (b): 146334
Max cell dim: 24
*** Warning: Experimental implementation of Dofmap::set(..)
*** Warning: Experimental implementation of Dofmap::set(..)
*** Warning: Experimental implementation of Dofmap::set(..)
Setting up EpetraKrylovSolver & ML ...
*** Warning: Filling Transformation matrix G. Assumes shifted enrichment XFEM and 1 enriched DOF per regular DOF!
Create Epetra system matrix...
MatPtAP
Inverse-transform Nullspace...

*******************************************************
***** Belos Iterative Solver:  Pseudo Block Gmres 
***** Maximum Iterations: 10000
***** Block Size: 1
***** Residual Test: 
*****   Test 1 : Belos::StatusTestImpResNorm<>: (2-Norm Res Vec) / (2-Norm Prec Res0), tol = 1e-09
*******************************************************
Iter     0, [ 1] :    1.000000e+00
Iter     1, [ 1] :    0.000000e+00

*******************************************************
***** Belos Iterative Solver:  Pseudo Block Gmres 
***** Maximum Iterations: 10000
***** Block Size: 1
***** Residual Test: 
*****   Test 1 : Belos::StatusTestImpResNorm<>: (2-Norm Res Vec) / (2-Norm Prec Res0), tol = 1e-09
*******************************************************
Iter     0, [ 1] :    1.000000e+00
Iter     1, [ 1] :    0.000000e+00

*******************************************************
***** Belos Iterative Solver:  Pseudo Block Gmres 
***** Maximum Iterations: 10000
***** Block Size: 1
***** Residual Test: 
*****   Test 1 : Belos::StatusTestImpResNorm<>: (2-Norm Res Vec) / (2-Norm Prec Res0), tol = 1e-09
*******************************************************
Iter     0, [ 1] :    1.000000e+00
Iter     1, [ 1] :    0.000000e+00

*******************************************************
***** Belos Iterative Solver:  Pseudo Block Gmres 
***** Maximum Iterations: 10000
***** Block Size: 1
***** Residual Test: 
*****   Test 1 : Belos::StatusTestImpResNorm<>: (2-Norm Res Vec) / (2-Norm Prec Res0), tol = 1e-09
*******************************************************
Iter     0, [ 1] :    1.000000e+00
Iter     1, [ 1] :    0.000000e+00

*******************************************************
***** Belos Iterative Solver:  Pseudo Block Gmres 
***** Maximum Iterations: 10000
***** Block Size: 1
***** Residual Test: 
*****   Test 1 : Belos::StatusTestImpResNorm<>: (2-Norm Res Vec) / (2-Norm Prec Res0), tol = 1e-09
*******************************************************
Iter     0, [ 1] :    1.000000e+00
Iter     1, [ 1] :    0.000000e+00

*******************************************************
***** Belos Iterative Solver:  Pseudo Block Gmres 
***** Maximum Iterations: 10000
***** Block Size: 1
***** Residual Test: 
*****   Test 1 : Belos::StatusTestImpResNorm<>: (2-Norm Res Vec) / (2-Norm Prec Res0), tol = 1e-09
*******************************************************
Iter     0, [ 1] :    1.000000e+00
Iter     1, [ 1] :    0.000000e+00
Solve...
Solving linear system of size 84378 x 84378 (Epetra Krylov solver).

*******************************************************
***** Belos Iterative Solver:  Pseudo Block Gmres 
***** Maximum Iterations: 40
***** Block Size: 1
***** Residual Test: 
*****   Test 1 : Belos::StatusTestImpResNorm<>: (2-Norm Res Vec) / (2-Norm Prec Res0), tol = 1e-08
*******************************************************
Iter  0, [ 1] :    1.000000e+00
Iter  1, [ 1] :    2.809936e-01
Iter  2, [ 1] :    5.812648e-02
Iter  3, [ 1] :    9.706263e-03
Iter  4, [ 1] :    1.985684e-03
Iter  5, [ 1] :    5.852214e-04
Iter  6, [ 1] :    1.305086e-04
Iter  7, [ 1] :    2.697021e-05
Iter  8, [ 1] :    6.470027e-06
Iter  9, [ 1] :    1.717502e-06
Iter 10, [ 1] :    4.362669e-07
Iter 11, [ 1] :    9.756050e-08
Iter 12, [ 1] :    2.464023e-08
Iter 13, [ 1] :    5.751397e-09
================================================================================

                      TimeMonitor results over 1 processor

Timer Name                                        Global time (num calls)    
--------------------------------------------------------------------------------
Belos: Operation Op*x                             0.07572 (33)               
Belos: Operation Prec*x                           1.5 (26)                   
Belos: Orthogonalization                          0.09603 (26)               
Belos: PseudoBlockGmresSolMgr total solve time    1.686 (7)                  
Ifpack_ILU::ApplyInverse                          0.007076 (12)              
Ifpack_ILU::ApplyInverse - Solve                  0.007016 (12)              
Ifpack_ILU::Compute                               0.06273 (6)                
Ifpack_ILU::ComputeSetup                          0.02473 (6)                
Ifpack_ILU::Initialize                            0.03438 (6)                
Main: Overall Time                                0 (1)                      
--------------------------------------------------------------------------------
SolverXAMG: Assemble XFEM system                  2.663 (1)                  
SolverXAMG: Solve                                 0 (1)                      
SolverXAMG: Transform system                      0.824 (1)                  
SolverXAMG: Transform system - inverse            0 (0)                      
XAMG: Build transformation matrix                 0.3465 (1)                 
XAMG: Invert nullspace                            0.168 (1)                  
XAMG: MatPtAP                                     0.309 (1)                  
================================================================================
Epetra (Belos) Krylov solver (gmres, ml_amg) converged in 13 iterations.
Number of iterations: 13
================================================================================

                      TimeMonitor results over 1 processor

Timer Name                                        Global time (num calls)    
--------------------------------------------------------------------------------
Belos: Operation Op*x                             0.07572 (33)               
Belos: Operation Prec*x                           1.5 (26)                   
Belos: Orthogonalization                          0.09603 (26)               
Belos: PseudoBlockGmresSolMgr total solve time    1.686 (7)                  
Ifpack_ILU::ApplyInverse                          0.007076 (12)              
Ifpack_ILU::ApplyInverse - Solve                  0.007016 (12)              
Ifpack_ILU::Compute                               0.06273 (6)                
Ifpack_ILU::ComputeSetup                          0.02473 (6)                
Ifpack_ILU::Initialize                            0.03438 (6)                
Main: Overall Time                                0 (1)                      
--------------------------------------------------------------------------------
SolverXAMG: Assemble XFEM system                  2.663 (1)                  
SolverXAMG: Solve                                 4.129 (1)                  
SolverXAMG: Transform system                      0.824 (1)                  
SolverXAMG: Transform system - inverse            0.0005369 (1)              
XAMG: Build transformation matrix                 0.3465 (1)                 
XAMG: Invert nullspace                            0.168 (1)                  
XAMG: MatPtAP                                     0.309 (1)                  
================================================================================
