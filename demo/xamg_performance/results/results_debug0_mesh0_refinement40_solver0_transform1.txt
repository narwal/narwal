Num. mesh vertices: 64000
Number of dofs in enriched dofmap: 198018
Create Epetra system matrix...
Create Epetra system vector...
!!!!!!!! Create Assemble: 
!!!!!!!! Assemble: 
!!!!!!!! Num cells (a): 
!!!!!!!! Num cells (b): 355914
Max cell dim: 24
*** Warning: Experimental implementation of Dofmap::set(..)
*** Warning: Experimental implementation of Dofmap::set(..)
*** Warning: Experimental implementation of Dofmap::set(..)
Setting up EpetraKrylovSolver & ML ...
*** Warning: Filling Transformation matrix G. Assumes shifted enrichment XFEM and 1 enriched DOF per regular DOF!
Create Epetra system matrix...
MatPtAP
Inverse-transform Nullspace...

*******************************************************
***** Belos Iterative Solver:  Pseudo Block Gmres 
***** Maximum Iterations: 10000
***** Block Size: 1
***** Residual Test: 
*****   Test 1 : Belos::StatusTestImpResNorm<>: (2-Norm Res Vec) / (2-Norm Prec Res0), tol = 1e-09
*******************************************************
Iter     0, [ 1] :    1.000000e+00
Iter     1, [ 1] :    0.000000e+00

*******************************************************
***** Belos Iterative Solver:  Pseudo Block Gmres 
***** Maximum Iterations: 10000
***** Block Size: 1
***** Residual Test: 
*****   Test 1 : Belos::StatusTestImpResNorm<>: (2-Norm Res Vec) / (2-Norm Prec Res0), tol = 1e-09
*******************************************************
Iter     0, [ 1] :    1.000000e+00
Iter     1, [ 1] :    0.000000e+00

*******************************************************
***** Belos Iterative Solver:  Pseudo Block Gmres 
***** Maximum Iterations: 10000
***** Block Size: 1
***** Residual Test: 
*****   Test 1 : Belos::StatusTestImpResNorm<>: (2-Norm Res Vec) / (2-Norm Prec Res0), tol = 1e-09
*******************************************************
Iter     0, [ 1] :    1.000000e+00
Iter     1, [ 1] :    0.000000e+00

*******************************************************
***** Belos Iterative Solver:  Pseudo Block Gmres 
***** Maximum Iterations: 10000
***** Block Size: 1
***** Residual Test: 
*****   Test 1 : Belos::StatusTestImpResNorm<>: (2-Norm Res Vec) / (2-Norm Prec Res0), tol = 1e-09
*******************************************************
Iter     0, [ 1] :    1.000000e+00
Iter     1, [ 1] :    0.000000e+00

*******************************************************
***** Belos Iterative Solver:  Pseudo Block Gmres 
***** Maximum Iterations: 10000
***** Block Size: 1
***** Residual Test: 
*****   Test 1 : Belos::StatusTestImpResNorm<>: (2-Norm Res Vec) / (2-Norm Prec Res0), tol = 1e-09
*******************************************************
Iter     0, [ 1] :    1.000000e+00
Iter     1, [ 1] :    0.000000e+00

*******************************************************
***** Belos Iterative Solver:  Pseudo Block Gmres 
***** Maximum Iterations: 10000
***** Block Size: 1
***** Residual Test: 
*****   Test 1 : Belos::StatusTestImpResNorm<>: (2-Norm Res Vec) / (2-Norm Prec Res0), tol = 1e-09
*******************************************************
Iter     0, [ 1] :    1.000000e+00
Iter     1, [ 1] :    0.000000e+00
Solve...
Solving linear system of size 198018 x 198018 (Epetra Krylov solver).

*******************************************************
***** Belos Iterative Solver:  Pseudo Block Gmres 
***** Maximum Iterations: 40
***** Block Size: 1
***** Residual Test: 
*****   Test 1 : Belos::StatusTestImpResNorm<>: (2-Norm Res Vec) / (2-Norm Prec Res0), tol = 1e-08
*******************************************************
Iter  0, [ 1] :    1.000000e+00
Iter  1, [ 1] :    3.277668e-01
Iter  2, [ 1] :    9.540115e-02
Iter  3, [ 1] :    2.271791e-02
Iter  4, [ 1] :    5.854251e-03
Iter  5, [ 1] :    2.061538e-03
Iter  6, [ 1] :    6.592690e-04
Iter  7, [ 1] :    1.931229e-04
Iter  8, [ 1] :    5.619208e-05
Iter  9, [ 1] :    1.732804e-05
Iter 10, [ 1] :    5.475616e-06
Iter 11, [ 1] :    1.842873e-06
Iter 12, [ 1] :    5.500269e-07
Iter 13, [ 1] :    1.809549e-07
Iter 14, [ 1] :    5.833702e-08
Iter 15, [ 1] :    1.613649e-08
Iter 16, [ 1] :    5.057219e-09
================================================================================

                      TimeMonitor results over 1 processor

Timer Name                                        Global time (num calls)    
--------------------------------------------------------------------------------
Belos: Operation Op*x                             0.2316 (36)                
Belos: Operation Prec*x                           3.567 (29)                 
Belos: Orthogonalization                          0.2907 (29)                
Belos: PseudoBlockGmresSolMgr total solve time    4.118 (7)                  
Ifpack_ILU::ApplyInverse                          0.01713 (12)               
Ifpack_ILU::ApplyInverse - Solve                  0.01705 (12)               
Ifpack_ILU::Compute                               0.1383 (6)                 
Ifpack_ILU::ComputeSetup                          0.05865 (6)                
Ifpack_ILU::Initialize                            0.0779 (6)                 
Main: Overall Time                                0 (1)                      
--------------------------------------------------------------------------------
SolverXAMG: Assemble XFEM system                  6.068 (1)                  
SolverXAMG: Solve                                 0 (1)                      
SolverXAMG: Transform system                      1.941 (1)                  
SolverXAMG: Transform system - inverse            0 (0)                      
XAMG: Build transformation matrix                 0.8094 (1)                 
XAMG: Invert nullspace                            0.3935 (1)                 
XAMG: MatPtAP                                     0.7366 (1)                 
================================================================================
Epetra (Belos) Krylov solver (gmres, ml_amg) converged in 16 iterations.
Number of iterations: 16
================================================================================

                      TimeMonitor results over 1 processor

Timer Name                                        Global time (num calls)    
--------------------------------------------------------------------------------
Belos: Operation Op*x                             0.2316 (36)                
Belos: Operation Prec*x                           3.567 (29)                 
Belos: Orthogonalization                          0.2907 (29)                
Belos: PseudoBlockGmresSolMgr total solve time    4.118 (7)                  
Ifpack_ILU::ApplyInverse                          0.01713 (12)               
Ifpack_ILU::ApplyInverse - Solve                  0.01705 (12)               
Ifpack_ILU::Compute                               0.1383 (6)                 
Ifpack_ILU::ComputeSetup                          0.05865 (6)                
Ifpack_ILU::Initialize                            0.0779 (6)                 
Main: Overall Time                                0 (1)                      
--------------------------------------------------------------------------------
SolverXAMG: Assemble XFEM system                  6.068 (1)                  
SolverXAMG: Solve                                 9.11 (1)                   
SolverXAMG: Transform system                      1.941 (1)                  
SolverXAMG: Transform system - inverse            0.00124 (1)                
XAMG: Build transformation matrix                 0.8094 (1)                 
XAMG: Invert nullspace                            0.3935 (1)                 
XAMG: MatPtAP                                     0.7366 (1)                 
================================================================================
