Num. mesh vertices: 343000
Number of dofs in enriched dofmap: 1047540
Create PETSc system matrix...
Create PETSc system vector...
!!!!!!!! Create Assemble: 
!!!!!!!! Assemble: 
!!!!!!!! Num cells (a): 
!!!!!!!! Num cells (b): 1971054
Max cell dim: 24
*** Warning: Experimental implementation of Dofmap::set(..)
*** Warning: Experimental implementation of Dofmap::set(..)
*** Warning: Experimental implementation of Dofmap::set(..)
Setting up PETScKrylovSolver & PETSc AMG ...
*** Warning: Filling Transformation matrix G. Assumes shifted enrichment XFEM and 1 enriched DOF per regular DOF!
Create PETSc system matrix...
MatPtAP
Inverse-transform Nullspace...
  0 KSP preconditioned resid norm 5.856620185739e+02 true resid norm 5.856620185739e+02 ||r(i)||/||b|| 1.000000000000e+00
  1 KSP preconditioned resid norm 7.791420025600e+01 true resid norm 7.791420025595e+01 ||r(i)||/||b|| 1.330361160276e-01
  2 KSP preconditioned resid norm 4.011112194641e-10 true resid norm 4.011829389009e-10 ||r(i)||/||b|| 6.850076087874e-13
  0 KSP preconditioned resid norm 5.856620185739e+02 true resid norm 5.856620185739e+02 ||r(i)||/||b|| 1.000000000000e+00
  1 KSP preconditioned resid norm 7.791420025600e+01 true resid norm 7.791420025595e+01 ||r(i)||/||b|| 1.330361160276e-01
  2 KSP preconditioned resid norm 4.011112194641e-10 true resid norm 4.011829389009e-10 ||r(i)||/||b|| 6.850076087874e-13
  0 KSP preconditioned resid norm 5.856620185739e+02 true resid norm 5.856620185739e+02 ||r(i)||/||b|| 1.000000000000e+00
  1 KSP preconditioned resid norm 7.791420025600e+01 true resid norm 7.791420025595e+01 ||r(i)||/||b|| 1.330361160276e-01
  2 KSP preconditioned resid norm 4.011112194641e-10 true resid norm 4.011829389009e-10 ||r(i)||/||b|| 6.850076087874e-13
  0 KSP preconditioned resid norm 4.863877509956e+04 true resid norm 4.863877509956e+04 ||r(i)||/||b|| 1.000000000000e+00
  1 KSP preconditioned resid norm 3.575413721631e+03 true resid norm 3.575413721630e+03 ||r(i)||/||b|| 7.350953461125e-02
  2 KSP preconditioned resid norm 3.192273819558e-09 true resid norm 3.192531328467e-09 ||r(i)||/||b|| 6.563757664399e-14
  0 KSP preconditioned resid norm 4.863877509955e+04 true resid norm 4.863877509955e+04 ||r(i)||/||b|| 1.000000000000e+00
  1 KSP preconditioned resid norm 3.575413721630e+03 true resid norm 3.575413721630e+03 ||r(i)||/||b|| 7.350953461126e-02
  2 KSP preconditioned resid norm 1.492815645502e-08 true resid norm 1.492725850212e-08 ||r(i)||/||b|| 3.069003787937e-13
  0 KSP preconditioned resid norm 4.876974539131e+04 true resid norm 4.876974539131e+04 ||r(i)||/||b|| 1.000000000000e+00
  1 KSP preconditioned resid norm 5.053830787540e+03 true resid norm 5.053830787538e+03 ||r(i)||/||b|| 1.036263516856e-01
  2 KSP preconditioned resid norm 1.894937718235e-08 true resid norm 1.895011446029e-08 ||r(i)||/||b|| 3.885629155584e-13
Solve...
  0 KSP unpreconditioned resid norm 9.097177325623e+11 true resid norm 9.097177325623e+11 ||r(i)||/||b|| 1.000000000000e+00
  1 KSP unpreconditioned resid norm 4.505645931652e+11 true resid norm 4.505645931651e+11 ||r(i)||/||b|| 4.952795543471e-01
  2 KSP unpreconditioned resid norm 1.924393585327e+11 true resid norm 1.924393585327e+11 ||r(i)||/||b|| 2.115374380915e-01
  3 KSP unpreconditioned resid norm 6.546060698522e+10 true resid norm 6.546060698521e+10 ||r(i)||/||b|| 7.195705287709e-02
  4 KSP unpreconditioned resid norm 2.327395425700e+10 true resid norm 2.327395425699e+10 ||r(i)||/||b|| 2.558370956609e-02
  5 KSP unpreconditioned resid norm 8.950061215512e+09 true resid norm 8.950061215511e+09 ||r(i)||/||b|| 9.838283783149e-03
  6 KSP unpreconditioned resid norm 3.968677047570e+09 true resid norm 3.968677047570e+09 ||r(i)||/||b|| 4.362536757848e-03
  7 KSP unpreconditioned resid norm 1.627671107659e+09 true resid norm 1.627671107659e+09 ||r(i)||/||b|| 1.789204551476e-03
  8 KSP unpreconditioned resid norm 5.690718611930e+08 true resid norm 5.690718611931e+08 ||r(i)||/||b|| 6.255477285138e-04
  9 KSP unpreconditioned resid norm 1.945884216396e+08 true resid norm 1.945884216394e+08 ||r(i)||/||b|| 2.138997786614e-04
 10 KSP unpreconditioned resid norm 6.883379119533e+07 true resid norm 6.883379119565e+07 ||r(i)||/||b|| 7.566499885824e-05
 11 KSP unpreconditioned resid norm 2.410684963478e+07 true resid norm 2.410684963480e+07 ||r(i)||/||b|| 2.649926320211e-05
 12 KSP unpreconditioned resid norm 8.942276599577e+06 true resid norm 8.942276599551e+06 ||r(i)||/||b|| 9.829726605816e-06
 13 KSP unpreconditioned resid norm 3.299575606823e+06 true resid norm 3.299575606482e+06 ||r(i)||/||b|| 3.627032307250e-06
 14 KSP unpreconditioned resid norm 1.462846967482e+06 true resid norm 1.462846967467e+06 ||r(i)||/||b|| 1.608022923052e-06
 15 KSP unpreconditioned resid norm 5.747659771616e+05 true resid norm 5.747659771939e+05 ||r(i)||/||b|| 6.318069403517e-07
 16 KSP unpreconditioned resid norm 2.077373069742e+05 true resid norm 2.077373071930e+05 ||r(i)||/||b|| 2.283535867855e-07
 17 KSP unpreconditioned resid norm 8.429623896153e+04 true resid norm 8.429623884676e+04 ||r(i)||/||b|| 9.266197176275e-08
 18 KSP unpreconditioned resid norm 3.496353458136e+04 true resid norm 3.496353479673e+04 ||r(i)||/||b|| 3.843338823159e-08
 19 KSP unpreconditioned resid norm 1.243828134066e+04 true resid norm 1.243828121669e+04 ||r(i)||/||b|| 1.367268194460e-08
 20 KSP unpreconditioned resid norm 4.430397745930e+03 true resid norm 4.430397518606e+03 ||r(i)||/||b|| 4.870079322437e-09
Number of iterations: 20
================================================================================

                      TimeMonitor results over 1 processor

Timer Name                                Global time (num calls)    
--------------------------------------------------------------------------------
Main: Overall Time                        0 (1)                      
SolverXAMG: Assemble XFEM system          68.38 (1)                  
SolverXAMG: Solve                         924.9 (1)                  
SolverXAMG: Transform system              38.32 (1)                  
SolverXAMG: Transform system - inverse    0.07552 (1)                
XAMG: Build transformation matrix         17.83 (1)                  
XAMG: Invert nullspace                    3.719 (1)                  
XAMG: MatPtAP                             16.72 (1)                  
================================================================================
