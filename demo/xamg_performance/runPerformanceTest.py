#!/usr/bin/env python
import numpy as np
import argparse
import scipy.io
import itertools as it
import subprocess
import sys
import os
import numpy as np
import matplotlib.pyplot as plt


'''

              3 solver variants

        w/o transform, w/ transform


'''

colors = {}
colors["gray"] = "\[\033[1;30m\]"
colors["light_gray"] = "\[\033[0;37m\]"
colors["cyan"] = "\[\033[0;36m\]"
colors["light_cyan"] = "\[\033[1;36m\]"
colors["red"] = "\[\033[0;31m\]"
colors["no_color"] = "\[\033[0m\]"

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

methods_labels =  ['Epetra + ML', 'PETSc + PETSc AMG', 'PETSc + ML', 'PETSc + ILU']
methods_colors =  ['r', 'b', 'g', 'm']

def colorize(str, color):
    return color + str + bcolors.ENDC


def runExamples(key, options, root, resultFileName):
    # run
    bashCommand = '/usr/bin/time -p ./xamg-performance ' + options
    run = True
    exetime = os.path.getmtime(os.path.join(root, "xamg-performance"))
    if os.path.isfile(resultFileName):
        txttime = os.path.getmtime(resultFileName)
        if (txttime > exetime):
            run = False
        if readNumIter(resultFileName) == -1:
            run = True
    if run:
        print colorize("Running " + key, bcolors.OKBLUE)
        process = subprocess.Popen(bashCommand, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=False, shell=True)
        output = ''
        # Poll process for new output until finished
        while True:
            nextline = process.stdout.readline()
            if nextline == '' and process.poll() != None:
                 break
            sys.stdout.write(nextline)
            sys.stdout.flush()
            output = output + nextline

        sys.stdout.write(nextline)
        sys.stdout.flush()
        output = output + nextline

        output = output + process.communicate()[0]
        text_file = open(resultFileName, "w")
        text_file.write(output)
        text_file.close()
    else:
        print colorize("Result for '" + key + "' exists. Skipping...", bcolors.OKGREEN)


def createKey(method, ref, transform):
    if transform:
        t = 1
    else:
        t = 0
    key = "debug0_mesh0_refinement" + str(ref) + "_solver" + str(method) + "_transform" + str(t)
    return key


def generateResultFileName(key, root):
    outputfile = os.path.join(root, "results_" + key + ".txt")
    return outputfile


def readNumIter(outputfile):
    numIter = -1
    text_file = open(outputfile, "r")
    for l in text_file.readlines():
        if l[0:21] == "Number of iterations:":
            numIter = int(l[21:])
    text_file.close()
    return numIter

def readTime(outputfile):
    userTime = -1
    text_file = open(outputfile, "r")
    for l in text_file.readlines():
        if "user " == l[0:5]:
            userTime = float(l.split()[1])
    text_file.close()
    return userTime

def plotConvergence(args, methods, x, yt0, yt1):
    fig, ax = plt.subplots()

    # set ticks and tick labels
    ax.set_xlim((0, x[-1]+20))
#     ax.set_ylim((0, 45))

    for method in methods:
        if not yt0[method] == []:
            ax.plot(x, yt0[method], '--o'+methods_colors[method], linewidth=3)
        if not yt1[method] == []:
            ax.plot(x, yt1[method], '-o'+methods_colors[method], linewidth=3, label=methods_labels[method])

    plt.xlabel('# nodes per space dimension')
    plt.ylabel('# linear iterations')
    plt.title('Convergence behavior w/ and w/o transformation')

    ax.annotate('original', xy=(71, 35), xycoords='data',
        xytext=(75, 38), textcoords='data',
        arrowprops=dict(arrowstyle="->",
        connectionstyle="angle3,angleA=-90,angleB=-180"),
        )
    ax.annotate('original', xy=(71, 40), xycoords='data',
        xytext=(75, 38), textcoords='data',
        arrowprops=dict(arrowstyle="->",
        connectionstyle="angle3,angleA=-90,angleB=-180"),
        )
    ax.annotate('original', xy=(71, 41),  xycoords='data',
        xytext=(75, 38), textcoords='data',
        arrowprops=dict(arrowstyle="->",
        connectionstyle="angle3,angleA=-90,angleB=-180"),
        )

    ax.annotate('transformed', xy=(71, 20),  xycoords='data',
        xytext=(75, 17), textcoords='data',
        arrowprops=dict(arrowstyle="->",
        connectionstyle="angle3,angleA=-90,angleB=-180"),
        )
    ax.annotate('transformed', xy=(71, 19),  xycoords='data',
        xytext=(75, 17), textcoords='data',
        arrowprops=dict(arrowstyle="->",
        connectionstyle="angle3,angleA=-90,angleB=-180"),
        )
    ax.annotate('transformed', xy=(71, 15),  xycoords='data',
        xytext=(75, 17), textcoords='data',
        arrowprops=dict(arrowstyle="->",
        connectionstyle="angle3,angleA=-90,angleB=-180"),
        )

    plt.legend(loc=2)
    plt.tight_layout()
    plt.savefig("convergence.png")
    if args.show:
        plt.show()

def plotTimes(args, methods, x, yt0, yt1):
    fig, ax = plt.subplots()

    # set ticks and tick labels
    ax.set_xlim((0, x[-1]+20))
#     ax.set_ylim((0, 45))

    for method in methods:
        if not yt0[method] == []:
            ax.plot(x, yt0[method], '--o'+methods_colors[method], linewidth=3)
        if not yt1[method] == []:
            ax.plot(x, yt1[method], '-o'+methods_colors[method], linewidth=3, label=methods_labels[method])

    plt.xlabel('# nodes per space dimension')
    plt.ylabel('run time (user) [seconds]')
    plt.title('Runtime behavior w/ and w/o transformation')

    plt.legend(loc=2)
    plt.tight_layout()
    plt.savefig("times.png")
    if args.show:
        plt.show()

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--show", action='store_true', help="show graph")
    parser.add_argument("-x", "--xkcd", action='store_true', help="graphs in xkcd style")
    args = parser.parse_args()


    methods = [0, 1, 2, 3]
    trafos = [1]

    refs = [10, 20, 30, 40, 50, 60, 70]
    cases = {}
    for method in methods:
        for ref in refs:
            for t in trafos:
                key = createKey(method, ref, t)
                cases[key] = "-m 0 -r "+str(ref)+" -d 0 -s "+str(method)+" -t "+str(t)
                print key

    results = {}
    root = "./"
    for key, options in cases.iteritems():
        resultFileName = generateResultFileName(key, root)
#         runExamples(key, options, root, resultFileName)


    x = []
    yt0 = {}
    yt1 = {}
    timet0 = {}
    timet1 = {}
    for method in methods:
        yt0[method] = []
        yt1[method] = []
        timet0[method] = []
        timet1[method] = []
    for ref in refs:
        x.append(ref)
        for method in methods:
            for t in trafos:
                key = createKey(method, ref, t)
                resultFileName = generateResultFileName(key, root)
                numIter = readNumIter(resultFileName)
                userTime = readTime(resultFileName)
                if t == 0:
                    yt0[method].append(numIter)
                    timet0[method].append(userTime)
                if t == 1:
                    yt1[method].append(numIter)
                    timet1[method].append(userTime)
    if args.xkcd:
        plt.xkcd()

    plotConvergence(args, methods, x, yt0, yt1)
    plotTimes(args, methods, x, timet0, timet1)


if __name__ == "__main__":
    main()
