// Copyright (C) 2016 Melior Innovations
// Author: David Bernstein
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include <set>
#include <dolfin.h>
#include <Narwal.h>

#include "../../models/ElasticityProblem3D_P1.h"

// Pressure on crack surface
class CrackPressure : public dolfin::Expression
{
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    values[0] = -2.0e6;
  }
};

// Dirichlet boundary condition function
class BoundaryValue : public dolfin::Expression
{
public:

  BoundaryValue() : Expression(3) {}

  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    values[0] = 0.0;
    values[1] = 0.0;
    values[2] = 0.0;
  }
};

// Sub domain for Dirichlet boundary condition
class DirichletBoundary : public dolfin::SubDomain
{  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
  {
     return std::abs(x[0] + 50.0) < 1.0e-4 || std::abs(x[0] - 50.0) < 1.0e-4;
   }
};

// Surface representation class
class EllipticalSurface : public narwal::ImplicitSurface
{
public:
  EllipticalSurface() :
    narwal::ImplicitSurface(dolfin::Sphere(dolfin::Point(0.0, 0.0, 0.0), 3.1),
                              "manifold"), a(50), b(50), beta(5.0) {}

  // Normal distance from the crack surface
  double f0(const dolfin::Point& p) const
  {
    const double x = p.x();
    const double y = p.y();
    //const double z = p.z();

    //double d = pow(y, 2.0) + pow(z, 2.0);
    double d = sin(y/10.0);

    return x - beta*d;
  }

  // If f0 = 0, then if f1<1 -> on surface and f1 >= = -> not on surface
  double f1(const dolfin::Point& p) const
  {
    //const double x = p.x();
    const double y = p.y();
    const double z = p.z();
    const double d = (y/a)*(y/a) + (z/b)*(z/b) - 1.0;
    if (d < 0.0)
      return -1.0;
    else
      return 1.0;
  }

  // Ellipse major and minor radii
  const double a, b;

  // Ellipse curvature
  const double beta;
};

int main()
{
  std::shared_ptr<dolfin::Mesh>
    mesh(new dolfin::Mesh("LEFMSimulationMesh.xml"));

  short numRefinements = 1;
  for (short i = 0; i < numRefinements; ++i)
    mesh = std::make_shared<dolfin::Mesh>(dolfin::refine(*mesh));

  // Create fracture surface
  std::shared_ptr<EllipticalSurface> surface(new EllipticalSurface());
  std::vector<std::shared_ptr<narwal::ImplicitSurface> > surfaces(1, surface);

  // Create standard function space
  Elasticity3D_P1::FunctionSpace V_s(mesh);
  std::cout << "Number of dofs in standard dofmap: "
            << V_s.dofmap()->dofs().size() << std::endl;

  // Create Narwal dofmap
  std::shared_ptr<narwal::DofMap>
    dofmap(new narwal::DofMap(V_s, *surface));

  // Test
  dolfin_assert(dofmap);
  narwal::TestingTools::check_dof_count(*dofmap);

  // Create layout (sparsity patern in case of matrix) for
  // initialisation of la objects
  dolfin::TensorLayout matrix_layout
    = narwal::SparsityTools::build_matrix_layout(*mesh, *dofmap);
  dolfin::TensorLayout vector_layout
    = narwal::SparsityTools::build_vector_layout(*mesh, *dofmap);

  // Create UFC forms
  std::shared_ptr<ufc::form> ufc_a(new elasticity3d_p1_form_0());
  std::shared_ptr<ufc::form> ufc_L(new elasticity3d_p1_form_1());

  // Create UFC finite elements
  std::shared_ptr<ufc::finite_element>
    ufc_element(ufc_a->create_finite_element(0));
  std::shared_ptr<dolfin::FiniteElement>
    element(new narwal::FiniteElement(ufc_element, 24));

  // Create function space
  std::shared_ptr<dolfin::FunctionSpace>
    Ve(new dolfin::FunctionSpace(mesh, element, dofmap));

  // Elasticity parameters
  const double E = 20e9;
  const double nu = 0.2;
  std::shared_ptr<dolfin::GenericFunction>
    mu(new dolfin::Constant(E/(2.0*(1.0 + nu))));
  std::shared_ptr<dolfin::GenericFunction>
    lambda(new dolfin::Constant(E*nu/((1.0 + nu)*(1.0 - 2.0*nu))));

  // Create DOLFIN forms
  std::vector<std::shared_ptr<const dolfin::FunctionSpace> > spaces(2, Ve);
  std::vector<std::shared_ptr<const dolfin::GenericFunction> > coefficients;
  coefficients.push_back(mu);
  coefficients.push_back(lambda);

  std::shared_ptr<dolfin::Form>
    a(new dolfin::Form(ufc_a, spaces, coefficients));

  std::vector<std::shared_ptr<const dolfin::FunctionSpace> > space(1, Ve);
  std::shared_ptr<dolfin::Form>
    L(new dolfin::Form(ufc_L, space, coefficients));

  // Crack pressure function
  CrackPressure crack_pressure;

  // Create enriched element
  narwal::ElasticityProblem3D_P1 enriched_element(surfaces, *dofmap, *element,
                                                    lambda, mu, crack_pressure);

  DirichletBoundary boundary;
  BoundaryValue boundary_value;
  std::shared_ptr<dolfin::DirichletBC> pbc(
      new dolfin::DirichletBC(V_s, boundary_value, boundary));
  std::vector<std::shared_ptr<const dolfin::DirichletBC> > bcps;
  bcps.push_back(pbc);

  // Initialise matrix and vector layouts
  dolfin::PETScMatrix A_new;
  A_new.init(matrix_layout);
  dolfin::PETScVector b_new;
  b_new.init(vector_layout);

  // Assemble system
  narwal::Assembler assembler_new(a, L, bcps, enriched_element);
  assembler_new.assemble(A_new, b_new);

  // Test diagonal (debugging)
  std::vector<std::shared_ptr<const dolfin::DirichletBC> > bcs_test;
  narwal::Assembler assembler_test(a, L, bcs_test, enriched_element);
  dolfin::PETScMatrix A_test;
  A_test.init(matrix_layout);
  dolfin::PETScVector b_test;
  b_test.init(vector_layout);
  assembler_test.assemble(A_test, b_test);
  dolfin::PETScVector diagonal;
  narwal::TestingTools::extract_matrix_diagonal(diagonal, A_test);

  //dolfin::info(diagonal, true);
  std::vector<double> diagonal_entries;
  diagonal.get_local(diagonal_entries);
  for (std::size_t i = 0; i < diagonal_entries.size(); ++i)
  {
    if (diagonal_entries[i] < DOLFIN_EPS)
    {
      std::cout << "(Near)-zero entry: " << i << ", " << diagonal_entries[i]
                << std::endl;
    }
  }

  // Create solution Function
  dolfin::Function u(spaces[0]);

  //#if PETSC_HAVE_SUPERLU
  //dolfin::LUSolver solver("superlu");
  //#else
  dolfin::LUSolver solver("mumps");
  //#endif
  std::cout << "Size: " << u.vector()->size() << std::endl;
  std::cout << "Solver Ax=b. This can take some time for large problems . . ."
    << std::endl;
  solver.solve(A_new, *u.vector(), b_new);
  std::cout << "Solution vector norm: " << u.vector()->norm("l2")
            << std::endl;

  std::cout << "Max vector entry: " << u.vector()->max() << std::endl;
  std::cout << "Min vector entry: " << u.vector()->min() << std::endl;

  narwal::TestingTools::test_enriched_dofs(u, *surface);


  const dolfin::Mesh surf_mesh
    = narwal::XFEMTools::compute_intersection_surface(*mesh, *surface);
  dolfin::File file_surface("output/surface.pvd");
  file_surface << surf_mesh;

  // Write solution to PVD file to visualise with ParaView
  dolfin::File file_u("output/u.pvd");
  file_u << u;

  // Create that conforms to crack surface
  const dolfin::Mesh cut_mesh
    = narwal::XFEMTools::compute_combined_mesh(*mesh, *surface);

  // Interpolate XFEM solution onto regular space with mesh conforming
  // to crack surface
  Elasticity3D_P1::FunctionSpace V_interp(cut_mesh);
  dolfin::Function u_interp(V_interp);

  u_interp.interpolate(u);
  dolfin::File file_interp_standard("output/interpolated-standard.pvd");
  file_interp_standard << u_interp;

  narwal::ElasticFunction u_test(cut_mesh, u, *surface);

  u_interp.interpolate(u_test);

  dolfin::File file_interp("output/interpolated-conforming.pvd");
  file_interp << u_interp;

  return 0;
}
