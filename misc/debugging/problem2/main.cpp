// Copyright (C) 2016 Melior Innovations
// Author: David Bernstein
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include <set>
#include <dolfin.h>
#include <Narwal.h>

#include "../../models/ElasticityProblem3D_P1.h"

// Pressure on crack surface
class CrackPressure : public dolfin::Expression
{
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
      values[0] = -2.0;
  }
};

// Dirichlet boundary condition function
class BoundaryValue : public dolfin::Expression
{
public:

  BoundaryValue() : Expression(3) {}

  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    if (x[1] < 0.0)
    {
      values[0] = 0.0;
      values[1] = 0.0;
      values[2] = 0.0;
    }
    else
    {
      values[0] = 0.0;
      values[1] = 0.0;
      values[2] = 0.0;
    }
  }
};

// Sub domain for Dirichlet boundary condition
class DirichletBoundary : public dolfin::SubDomain
{  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
   {
     return std::abs(x[1] + 0.1) < 1.0e-4
                                   || std::abs(x[1] - 0.1) < 1.0e-4;
   }
};


// Surface representation class
class EllipticalSurface : public narwal::ImplicitSurface
{
public:
  EllipticalSurface() :
    narwal::ImplicitSurface(dolfin::Sphere(dolfin::Point(0.0, 0.0, 0.0), 3.1),
                              "manifold"), a(0.32), b(0.32), beta(0.0) {}

  // Normal distance from the crack surface
  double f0(const dolfin::Point& p) const
  {
    const double x = p.x();
    const double y = p.y();
    const double z = p.z();
    const double d = (x/a)*(x/a) + (z/b)*(z/b) - 1.0;
    return (y - beta*d);
  }

  // If f0 = 0, then if f1<1 -> on surface and f1 >= = -> not on surface
  double f1(const dolfin::Point& p) const
  {
    const double x = p.x();
    //const double y = p.y();
    const double z = p.z();
    const double d = (x/a)*(x/a) + (z/b)*(z/b);
    if (d < 1.0)
      return -1.0;
    else
      return 1.0;
  }

  // Ellipse major and minor radii
  const double a, b;

  // Ellipse curvature
  const double beta;
};

// Surface class that is suitable for CGAL meshing. It is only used in
// creating a surface mesh for visualisation. It is not used in the
// computation.
class EllipticalSurfaceCGAL : public narwal::ImplicitSurface
{
public:
  EllipticalSurfaceCGAL(const dolfin::ImplicitSurface& surface, double a,
                        double b)
    : narwal::ImplicitSurface(dolfin::Sphere(dolfin::Point(0.0, 0.0, 0.0),
                                               3.1),
                                "manifold"), _surface(surface)
  {
    // Add polylines (used only to have CGAL generate meshes with a
    // sharp edge
    std::vector<dolfin::Point> polyline;
    for (unsigned int i = 0; i < 100; i++)
    {
      const double th = 2.0*DOLFIN_PI*(double)i/100.0;
      polyline.push_back(dolfin::Point(a*cos(th), 0.0,  b*sin(th)));
    }
    polyline.push_back(polyline.front());
    polylines.push_back(polyline);
  }

  // Normal distance from the crack surface
  double f0(const dolfin::Point& p) const
  {
    const double thickness = 0.15;

    // Evaluate f0
    const double d = _surface.f0(p);

    // Handle f1 case
    if(_surface.f1(p) > 0.0)
      return std::abs(d);
    else if (d > -thickness)
      return d;
    else
      return -d;
 }

  bool on_surface(const dolfin::Point& point, double tol) const
  { return std::abs(f0(point)) < tol && f1(point) < 0.0; }

private:

  const dolfin::ImplicitSurface& _surface;

};

int main()
{
  // Note that radius has been set to 0.32 in surface above
    
  // Create box domain mesh that contains surface
    
    // works when numPointsX = 10 but not when it is 11
    int numPointsX = 11;
    int numPointsZ = 10;
  std::shared_ptr<dolfin::Mesh>
    mesh(new dolfin::BoxMesh(-0.4, -0.1, -0.4, 0.4, 0.1, 0.4, numPointsX, numPointsX, numPointsZ));
  dolfin::cout << "Num. mesh vertices: " << mesh->num_vertices()
               << dolfin::endl;

  // Create fracture surface
  std::shared_ptr<EllipticalSurface> surface(new EllipticalSurface());
  std::vector<std::shared_ptr<narwal::ImplicitSurface> > surfaces(1, surface);

  // Generate surface mesh (used for visualisation only)
  EllipticalSurfaceCGAL surface_cgal(*surface, surface->a, surface->b);
  dolfin::Mesh surface_mesh;
  dolfin::ImplicitDomainMeshGenerator::generate_surface(surface_mesh,
                                                        surface_cgal, 0.05);
  // Write surface to file
  dolfin::File file_surface("output/surface.pvd");
  file_surface << surface_mesh;

  // Create standard function space
  Elasticity3D_P1::FunctionSpace V_s(mesh);

  // Create Narwal dofmap
  std::shared_ptr<narwal::DofMap>
    dofmap(new narwal::DofMap(V_s, *surface));
  std::cout << "Number of dofs in enriched dofmap: "
            << dofmap->dofs().size() << std::endl;

  // Create layout (sparsity patern in case of matrix) for
  // initialisation of la objects
  dolfin::TensorLayout matrix_layout
    = narwal::SparsityTools::build_matrix_layout(*mesh, *dofmap);
  dolfin::TensorLayout vector_layout
    = narwal::SparsityTools::build_vector_layout(*mesh, *dofmap);

  // Create UFC forms
  std::shared_ptr<ufc::form> ufc_a(new elasticity3d_p1_form_0());
  std::shared_ptr<ufc::form> ufc_L(new elasticity3d_p1_form_1());

  // Create UFC finite elements
  std::shared_ptr<ufc::finite_element>
    ufc_element(ufc_a->create_finite_element(0));
  std::shared_ptr<dolfin::FiniteElement>
    element(new narwal::FiniteElement(ufc_element, 24));

  // Create function space
  std::shared_ptr<dolfin::FunctionSpace>
    Ve(new dolfin::FunctionSpace(mesh, element, dofmap));

  // Elasticity parameters
  const double E = 1.0;
  const double nu = 0.2;
  std::shared_ptr<dolfin::GenericFunction>
    mu(new dolfin::Constant(E/(2.0*(1.0 + nu))));
  std::shared_ptr<dolfin::GenericFunction>
    lambda(new dolfin::Constant(E*nu/((1.0 + nu)*(1.0 - 2.0*nu))));

  // Create DOLFIN forms
  std::vector<std::shared_ptr<const dolfin::FunctionSpace> > spaces(2, Ve);
  std::vector<std::shared_ptr<const dolfin::GenericFunction> > coefficients;
  coefficients.push_back(mu);
  coefficients.push_back(lambda);

  std::shared_ptr<dolfin::Form>
    a(new dolfin::Form(ufc_a, spaces, coefficients));

  std::vector<std::shared_ptr<const dolfin::FunctionSpace> > space(1, Ve);
  std::shared_ptr<dolfin::Form>
    L(new dolfin::Form(ufc_L, space, coefficients));

  // Crack pressure function
  CrackPressure crack_pressure;

  // Create enriched element
  narwal::ElasticityProblem3D_P1 enriched_element(surfaces, *dofmap, *element,
                                                    lambda, mu, crack_pressure);

  DirichletBoundary boundary;
  BoundaryValue boundary_value;
  std::shared_ptr<dolfin::DirichletBC> bc(
      new dolfin::DirichletBC(V_s, boundary_value, boundary));

  // Collect boundary conditions
  std::vector<std::shared_ptr<const dolfin::DirichletBC> > bcs;
  bcs.push_back(bc);

  // Initialise matrix and vector layouts
  dolfin::PETScMatrix A_new;
  A_new.init(matrix_layout);
  dolfin::PETScVector b_new;
  b_new.init(vector_layout);

  // Assemble system
  narwal::Assembler assembler_new(a, L, bcs, enriched_element);
  assembler_new.assemble(A_new, b_new);

  /*
  // Compute eigenvalues (for debugging - small problems only)
  dolfin::SLEPcEigenSolver esolver(A_new);
  esolver.parameters["solver"] = "lapack";
  esolver.solve();
  for (std::size_t i = 0; i < A_new.size(0); ++i)
  {
    double lr = 0.0;
    double lc = 0.0;
    esolver.get_eigenvalue(lr, lc, i);
    if (std::abs(lr) < 1.0e-6 || std::abs(lc) > 1.0e-12)
    {
      std::cout << "zero eigenvalue " << i << ": " << lr << ", " << lc
                << std::endl;
    }
  }
  */

  // Create solution Function
  dolfin::Function u(spaces[0]);

  #if PETSC_HAVE_SUPERLU
  dolfin::LUSolver solver("superlu");
  #else
  dolfin::LUSolver solver;
   #endif
  std::cout << "Size: " << u.vector()->size() << std::endl;
  std::cout << "Solver Ax=b. This can take some time for large problems . . ."
    << std::endl;
  solver.solve(A_new, *u.vector(), b_new);
  std::cout << "Solution vector norm: " << u.vector()->norm("l2")
            << std::endl;

  std::cout << "Max vector entry: " << u.vector()->max() << std::endl;
  std::cout << "Min vector entry: " << u.vector()->min() << std::endl;

  // Write solution to PVD file to visualise with ParaView
  dolfin::File file_u("output/u.pvd");
  file_u << u;

  return 0;
}
