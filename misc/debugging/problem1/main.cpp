// Copyright (C) 2016 Melior Innovations
// Author: David Bernstein
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include <set>
#include <dolfin.h>
#include <Narwal.h>

#include "../../models/ElasticityProblem3D_P1.h"

// Pressure on crack surface
class CrackPressure : public dolfin::Expression
{
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    values[0] = -2.0e6;
  }
};

// Dirichlet boundary condition function
class BoundaryValue : public dolfin::Expression
{
public:

  BoundaryValue() : Expression(3) {}

  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
      values[0] = 0.0;
      values[1] = 0.0;
      values[2] = 0.0;
  }
};

// Sub domain for Dirichlet boundary condition
class DirichletBoundary : public dolfin::SubDomain
{  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
   {
        return std::abs(x[0] + 50.0) < 1.0e-4
                                   || std::abs(x[0] - 50.0) < 1.0e-4;;
   }
};

// Surface representation class
class EllipticalSurface : public narwal::ImplicitSurface
{
public:
  EllipticalSurface() :
    narwal::ImplicitSurface(dolfin::Sphere(dolfin::Point(0.0, 0.0, 0.0), 3.1),
                              "manifold"), a(50), b(50), beta(0.0) {}

  // Normal distance from the crack surface
  double f0(const dolfin::Point& p) const
  {
    const double x = p.x();
    //const double y = p.y();
    //const double z = p.z();
    return x;
  }

  // If f0 = 0, then if f1<1 -> on surface and f1 >= = -> not on surface
  double f1(const dolfin::Point& p) const
  {
    //const double x = p.x();
    const double y = p.y();
    const double z = p.z();
    const double d = (y/a)*(y/a) + (z/b)*(z/b);
    if (d < 1.0)
      return -1.0;
    else
      return 1.0;
  }

  // Ellipse major and minor radii
  const double a, b;

  // Ellipse curvature
  const double beta;
};

int main()
{
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  std::shared_ptr<dolfin::Mesh> pMesh(new dolfin::Mesh("LEFMSimulationMesh.xml"));

  // something funny happens when numRefinements is changed from 2 to 3
  short numRefinements = 3;
  for (short i = 0; i < numRefinements; ++i) {
        dolfin::CellFunction<bool> f(pMesh, true);
        *pMesh = refine(*pMesh, f);
  }

  dolfin::cout << "Num. mesh vertices: " << pMesh->num_vertices() << dolfin::endl;

  // Create fracture surface
  std::shared_ptr<EllipticalSurface> surface(new EllipticalSurface());
  std::vector<std::shared_ptr<narwal::ImplicitSurface> > surfaces(1, surface);

  // Create standard function space
  Elasticity3D_P1::FunctionSpace V_s(*pMesh);

  // Create Narwal dofmap
  std::shared_ptr<narwal::DofMap>
    dofmap(new narwal::DofMap(V_s, *surface));
  std::cout << "Number of dofs in enriched dofmap: "
            << dofmap->dofs().size() << std::endl;

  // Set up Dirichlet boundary conditions
  DirichletBoundary boundary;
  BoundaryValue boundary_value;
  std::shared_ptr<dolfin::DirichletBC> bc(
      new dolfin::DirichletBC(V_s, boundary_value, boundary));

  // Collect boundary conditions
  std::vector<std::shared_ptr<const dolfin::DirichletBC> > bcs;
  bcs.push_back(bc);

  // Elasticity parameters
  const double E = 20.0e9;
  const double nu = 0.2;
  std::shared_ptr<dolfin::GenericFunction>
    mu(new dolfin::Constant(E/(2.0*(1.0 + nu))));
  std::shared_ptr<dolfin::GenericFunction>
    lambda(new dolfin::Constant(E*nu/((1.0 + nu)*(1.0 - 2.0*nu))));

  std::vector<std::shared_ptr<const dolfin::GenericFunction> > coefficients;
  coefficients.push_back(mu);
  coefficients.push_back(lambda);

  // Create UFC forms
  std::shared_ptr<ufc::form> ufc_a(new elasticity3d_p1_form_0());
  std::shared_ptr<ufc::form> ufc_L(new elasticity3d_p1_form_1());

  // Create UFC finite elements
  std::shared_ptr<ufc::finite_element>
    ufc_element(ufc_a->create_finite_element(0));
  std::shared_ptr<dolfin::FiniteElement>
    element(new narwal::FiniteElement(ufc_element, 24));

  // Create function space
  std::shared_ptr<dolfin::FunctionSpace>
    Ve(new dolfin::FunctionSpace(pMesh, element, dofmap));

  // Create DOLFIN forms
  std::vector<std::shared_ptr<const dolfin::FunctionSpace> > spaces(2, Ve);
  std::shared_ptr<dolfin::Form>
    a(new dolfin::Form(ufc_a, spaces, coefficients));
  std::vector<std::shared_ptr<const dolfin::FunctionSpace> > space(1, Ve);
  std::shared_ptr<dolfin::Form>
    L(new dolfin::Form(ufc_L, space, coefficients));

  // Crack pressure function
  CrackPressure crack_pressure;

  // Create enriched element
  narwal::ElasticityProblem3D_P1 enriched_element(surfaces, *dofmap, *element,
                                                    lambda, mu, crack_pressure);

  // Create layout (sparsity pattern in case of matrix) for
  // initialization of la objects
  dolfin::TensorLayout matrix_layout
    = narwal::SparsityTools::build_matrix_layout(*pMesh, *dofmap);
  dolfin::TensorLayout vector_layout
    = narwal::SparsityTools::build_vector_layout(*pMesh, *dofmap);

  // Create system matrix and rhs vector
  narwal::la::SolverXAMG xSolver(Ve, dofmap, matrix_layout, vector_layout);

  // Start assembly
  narwal::Assembler assembler(a, L, bcs, enriched_element);
  xSolver.assemble(assembler);

  // Create null space
  xSolver.null_space =
      narwal::xamg::NullSpace3DDisplacement2(*dofmap, *xSolver.b, *pMesh).build();

  // Create preconditioner & solver
  xSolver.setupDefaultPreCondAndSolver("petsc_amg");

  // Solve
  const std::size_t num_iters = xSolver.solve();

  std::cout << num_iters << std::endl;

  dolfin::File file_u("u.pvd");
  file_u << *xSolver.u;

//  // Assemble system
//  narwal::Assembler assembler_new(a, L, bcs, enriched_element);
//  assembler_new.assemble(A_new, b_new);
//
//  // Create solution Function
//  dolfin::Function u(spaces[0]);
//
//  #if PETSC_HAVE_SUPERLU
//  dolfin::LUSolver solver("superlu");
//  #else
//  dolfin::LUSolver solver;
//   #endif
//  std::cout << "Size: " << u.vector()->size() << std::endl;
//  std::cout << "Solver Ax=b. This can take some time for large problems . . ."
//    << std::endl;
//  solver.solve(A_new, *u.vector(), b_new);
//  std::cout << "Solution vector norm: " << u.vector()->norm("l2")
//            << std::endl;
//
//  std::cout << "Max vector entry: " << u.vector()->max() << std::endl;
//  std::cout << "Min vector entry: " << u.vector()->min() << std::endl;
//
//  // Write solution to PVD file to visualise with ParaView
//  dolfin::File file_u("output/u.pvd");
//  file_u << u;


  return 0;
}
