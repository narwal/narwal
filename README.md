# Narwal

The is the development version of Narwal, and FEniCS-based fracture,
code for large-scale simulations. It builds on the FEnics Project
libraries (<http://www.fenicsproject.org>).

Narwal is under heavy development and is not yet ready for use.


## License

Narwal is free software: you can redistribute it and/or modify it
under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FEniCS Fracture is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with this program.  If not, see
<http://www.gnu.org/licenses/>.


## Installation and dependencies

Narwal depends on the FEniCS Project libraries, and in particular
DOLFIN. Information on the FEniCS Project can be found at
<http://www.fenicsproject.org>.

Narwal currently tracks the FEniCS development version. The FEniCS
development code can be found at
<http://www.bitbucket.org/fenics-project>.

Narwal depends heavily in DOLFIN from the FEniCS Project, and
Narwal-specifc DOLFIN features are developed in a DOLFIN fork at
<https://bitbucket.org/narwal/dolfin>. The code is this repository
should be build against the 'narwal' branch
(<https://bitbucket.org/narwal/dolfin/commits/branch/narwal>).  To get
the 'narwal' branch, use:

```bash
git clone -b narwal git@bitbucket.org:narwal/dolfin.git
```

or

```bash
git clone -b narwal https://bitbucket.org/narwal/garth/dolfin.git
```

As Narwal features in DOLFIN mature, they are merged in the DOLFIN
master code base.
