// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

// Unit tests for function in narwal::XFEMTools

#include "gtest_tools.h"

#include <vector>
#include <dolfin.h>
#include <Narwal.h>
#include "Elasticity3D_P1.h"
#include "SquareSurface.h"

// Test XFEMFunctionSpace data access
TEST(XFEMFunctionSpace, data_access)
{
  auto mesh = std::make_shared<dolfin::UnitCubeMesh>(8, 8, 8);
  auto surface = std::make_shared<test_surface::SquareSurface>(10.0);
  auto dm = std::make_shared<narwal::DomainManager>(mesh);
  dm->register_surface(surface, "surface0");

  auto V_s = std::make_shared<Elasticity3D_P1::FunctionSpace>(mesh);
  auto V = std::make_shared<narwal::XFEMFunctionSpace>(V_s, dm, 24);
  EXPECT_TRUE(V_s != V->function_space());
  EXPECT_TRUE(dm == V->domain());
  EXPECT_TRUE(V->dofmap() != NULL);
}


// Test XFEMFunctionSpace dimension (fully intersected mesh)
TEST(XFEMFunctionSpace, dimension_full_intersect)
{
  for (std::size_t n = 1; n < 10; ++n)
  {
    auto mesh = std::make_shared<dolfin::UnitCubeMesh>(n, n, n);
    auto surface = std::make_shared<test_surface::SquareSurface>(10.0);
    auto dm = std::make_shared<narwal::DomainManager>(mesh);
    dm->register_surface(surface, "surface0");
    auto V_s = std::make_shared<Elasticity3D_P1::FunctionSpace>(mesh);
    auto V = std::make_shared<narwal::XFEMFunctionSpace>(V_s, dm, 24);

    const std::size_t nv = n + 1;
    EXPECT_EQ(3*(nv*nv*nv + 2*nv*nv), V->dim());
  }
}

// Test XFEMFunctionSpace dimension (partially intersected mesh)
TEST(XFEMFunctionSpace, dimension_part_intersect)
{
  const std::vector<std::pair<std::size_t, double>> test_data
    = {{1, 0.201}, {10, 0.201}, {15, 0.235}, {20, 0.201}};
  for (auto data : test_data)
  {
    // Get number of mesh edges in each dir (n), and surface side
    // half-length (dx)
    const int n = data.first;
    const double dx = data.second;

    // Compute number of fully intersected 'cell' edges in each
    // direction (num_edges)
    const double delta = 1.0/n;
    const int num_edges = std::min(n, static_cast<int>(2.0*dx/delta));

    // Create mesh, function space, etc
    auto mesh = std::make_shared<dolfin::UnitCubeMesh>(n, n, n);
    auto surface = std::make_shared<test_surface::SquareSurface>(dx);
    auto dm = std::make_shared<narwal::DomainManager>(mesh);
    dm->register_surface(surface, "surface0");
    auto V_s = std::make_shared<Elasticity3D_P1::FunctionSpace>(mesh);
    auto V = std::make_shared<narwal::XFEMFunctionSpace>(V_s, dm, 24);

    // Get XFEM space dimension
    const int dim = V->dim();

    // Test enriched space dimension
    const int nv = std::max(0, num_edges - 1);
    EXPECT_EQ(3*((n+1)*(n+1)*(n+1) + 2*nv*nv), dim);
  }
}
