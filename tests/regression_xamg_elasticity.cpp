// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//         Axel Gerstenberger
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.


#include <ElasticityFunction.h>
#include <Elasticity3D_P1.h>
#include "xamg_elasticity.h"

#include "gtest_tools.h"

namespace
{
  static const int polynomialDegree = 1;
  static const int max_element_dim = 24;
  typedef Elasticity3D_P1::FunctionSpace FuncSpace;
}

TEST(xamg, ElasticityPetscGeneralAMG)
{
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  test::xamg_elasticity::ElasticityTestSetup<FuncSpace> s(
      "output/" + gtest_nameroot(), polynomialDegree, max_element_dim);

  auto dofmap = s.V->function_space()->dofmap();
  auto tdofmap = std::dynamic_pointer_cast<const narwal::DofMap>(dofmap);

  // Create null space
  auto null_space =
      narwal::xamg::NullSpace3DDisplacement2(
        s.V->function_space(), tdofmap, s.b, s.domainManager->mesh()).build();

  // Create preconditioner and attach null space
  auto pc = std::make_shared<dolfin::PETScPreconditioner>("petsc_amg"); // use "petsc_amg" or "ml_amg"
  pc->set_nullspace(*null_space);
  pc->parameters["report"] = false;

  // Create solver
  dolfin::PETScKrylovSolver solver("gmres", pc);
  solver.set_operator(s.A);
  solver.parameters["report"] = false;
  solver.parameters["monitor_convergence"] = true;
  solver.parameters["relative_tolerance"] = 1.0e-8;
  solver.parameters["convergence_norm_type"] = "true";

  solver.solve(*s.A, *s.u->vector(), *s.b);
  EXPECT_NEAR(s.u->vector()->norm("l2"), 12.054506451575499, 1.0e-2);

  dolfin::File file_u("output/"+gtest_nameroot()+"_u.pvd");
  file_u << *s.u;
}

TEST(xamg, ElasticityPetscGeneralLUSlow)
{
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  test::xamg_elasticity::ElasticityTestSetup<FuncSpace> s(
      "output/" + gtest_nameroot(), polynomialDegree, max_element_dim);

  // Solve
  #ifdef PETSC_HAVE_SUPERLU
  dolfin::LUSolver solver("superlu");
  #else
  dolfin::LUSolver solver;
  #endif
  solver.solve(*s.A, *s.u->vector(), *s.b);
  EXPECT_NEAR(s.u->vector()->norm("l2"), 12.054506451575499, 1.0e-2);
}

TEST(xamg, ElasticityPetscTransformPetscAMG)
{
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  test::xamg_elasticity::ElasticityTestSetup<FuncSpace> s(
      "output/" + gtest_nameroot(), polynomialDegree, max_element_dim);

  auto dofmap = s.V->function_space()->dofmap();
  auto tdofmap = std::dynamic_pointer_cast<const narwal::DofMap>(dofmap);

  // Create system matrix and rhs vector
  narwal::la::SolverXAMG xSolver(s.A, s.b, s.u, s.V->function_space(),
                                 s.domainManager, tdofmap, s.xfem_forms,
                                 true, true, "output/" + gtest_nameroot());

  // Create null space
  xSolver.null_space
    = narwal::xamg::NullSpace3DDisplacement2(s.V->function_space(),
                                             tdofmap, s.b, s.domainManager->mesh()).build();

  // Create preconditioner & solver
  xSolver.setupDefaultPreCondAndSolver("petsc_amg");

  // Solve
  EXPECT_LT(xSolver.solve(), std::size_t(50));
  EXPECT_NEAR(s.u->vector()->norm("l2"), 12.054506451575499, 1.0e-2);
}

#if PETSC_HAVE_ML
TEST(xamg, DISABLED_ElasticityPetscTransformPetscML)
{
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  test::xamg_elasticity::ElasticityTestSetup<FuncSpace> s(
      "output/" + gtest_nameroot(), polynomialDegree, max_element_dim);

  auto dofmap = s.V->function_space()->dofmap();
  auto tdofmap = std::dynamic_pointer_cast<const narwal::DofMap>(dofmap);

  // Create system matrix and rhs vector
  narwal::la::SolverXAMG xSolver(s.A, s.b, s.u, s.V->function_space(),
                                 s.domainManager, tdofmap, s.xfem_forms, true,
                                 true, "output/" + gtest_nameroot());

  // Create null space
  xSolver.null_space =
      narwal::xamg::NullSpace3DDisplacement2(
        s.V->function_space(), tdofmap, s.b, s.domainManager->mesh()).build();

  // Create preconditioner & solver
  xSolver.setupDefaultPreCondAndSolver("ml_amg");

  // Solve
  EXPECT_LT(xSolver.solve(), std::size_t(50));
  EXPECT_NEAR(s.u->vector()->norm("l2"), 12.054506451575499, 1.0e-2);

  // Write (nodal) solution to PVD file to visualise with ParaView
  dolfin::File file_u("output/"+gtest_nameroot()+"-u.pvd");
  file_u << *s.u;

  narwal::ElasticFunction::write_crack_conforming_solution<Elasticity3D_P1::FunctionSpace>(
    *s.domainManager, *s.xfem_forms, *s.u, gtest_nameroot());
}
#endif

#if PETSC_HAVE_HYPRE
TEST(xamg, ElasticityPetscTransformPetscHypreAMG)
{
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  const char* params[] = {"dummy_first_arg",
//    "--petsc.pc_hypre_boomeramg_relax_type_up", "symmetric-SOR/Jacobi",
//    "--petsc.pc_hypre_boomeramg_relax_type_down", "symmetric-SOR/Jacobi",
//    "--petsc.pc_hypre_boomeramg_max_iter", "1",
//    "--petsc.pc_hypre_boomeramg_rtol", "0.0",
//    "--petsc.pc_hypre_boomeramg_strong_threshold", "0.00001",
//    "--petsc.pc_hypre_boomeramg_coarsen_type", "PMIS",
//    "--petsc.pc_hypre_boomeramg_max_levels", "10",
//    "--petsc.pc_hypre_boomeramg_agg_nl", "5",
    "--petsc.pc_hypre_boomeramg_P_max", "4",
//    "--petsc.pc_hypre_boomeramg_relax_type_all", "symmetric-SOR/Jacobi",
    "--petsc.pc_hypre_boomeramg_print_statistics", "1",
//    "--petsc.pc_hypre_boomeramg_truncfactor", "0.01",
    // http://lists.mcs.anl.gov/pipermail/petsc-users/2007-April/001488.html
//    "--petsc.pc_hypre_boomeramg_set_num_functions", "3",
//    "--petsc.pc_hypre_boomeramg_set_nodal", "1",
//    "--petsc.pc_hypre_boomeramg_set_num_paths", "2",
//    "--petsc.pc_hypre_boomeramg_interp_type", "ext+i",
//    "--petsc.pc_hypre_ams_amg_alpha_theta", "0.0001"
  };

  dolfin::GlobalParameters parameters;
  parameters.parse(sizeof(params)/sizeof(char *), const_cast<char**>(params));

  test::xamg_elasticity::ElasticityTestSetup<FuncSpace> s("output/" + gtest_nameroot(),
    polynomialDegree, max_element_dim);

  auto dofmap = s.V->function_space()->dofmap();
  auto tdofmap = std::dynamic_pointer_cast<const narwal::DofMap>(dofmap);

  // Create system matrix and rhs vector
  narwal::la::SolverXAMG xSolver(s.A, s.b, s.u, s.V->function_space(),
                                 s.domainManager, tdofmap, s.xfem_forms, true,
                                 true, "output/" + gtest_nameroot());

  // Create null space
  xSolver.null_space
    = narwal::xamg::NullSpace3DDisplacement2(s.V->function_space(), tdofmap,
                                             s.b, s.domainManager->mesh()).build();

  // Create preconditioner & solver
  xSolver.setupDefaultPreCondAndSolver("hypre_amg");

  // Solve
  // TODO: this is not optimal, yet.
  //       Needs proper dropping of small entries in aggregation phase
  EXPECT_LT(xSolver.solve(), std::size_t(100)); // TODO: this is not optimal, yet
  EXPECT_NEAR(s.u->vector()->norm("l2"), 12.054506451575499, 1.0e-2);

  // Write (nodal) solution to PVD file to visualise with ParaView
  dolfin::File file_u("output/"+gtest_nameroot()+"-u.pvd");
  file_u << *s.u;

  //narwal::ElasticFunction::write_crack_conforming_solution<Elasticity3D_P1::FunctionSpace>(
  //  *s.domainManager, *s.xfem_forms, *s.u, gtest_nameroot());
}
#endif
