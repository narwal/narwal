// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "gtest_tools.h"

#include <dolfin.h>
#include <Narwal.h>
#include <Poisson3D_P1.h>
#include <PoissonProblem3D.h>

namespace
{
  // Dirichlet boundary condition function
  class BoundaryValue : public dolfin::Expression
  {
    void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
    {
      if (x[1] < 0.0)
        values[0] = 0.0;
      else
        values[0] = 1.0;
    }
  };

  // Sub domain for Dirichlet boundary condition
  class DirichletBoundary : public dolfin::SubDomain
  {  bool inside(const dolfin::Array<double>& x, bool /*on_boundary*/) const
    { return std::abs(x[1] + 0.1) < DOLFIN_EPS
          || std::abs(x[1] - 0.1) < DOLFIN_EPS ; }
  };

  // Surface representation class
  class EllipticalSurface : public narwal::ImplicitSurface
  {
  public:
    EllipticalSurface()
      : narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1),
        a(0.3), b(0.3), beta(0.05) {}

    // Normal distance from the crack surface
    double f0(const dolfin::Point& p) const
    {
      const double x = p.x();
      const double y = p.y();
      const double z = p.z();
      const double d = (x/a)*(x/a) + (z/b)*(z/b) - 1.0;
      return (y - beta*d);
    }

    // If f0 = 0, then if f1<1 -> on surface and f1 >= = -> not on surface
    double f1(const dolfin::Point& p) const
    {
      const double x = p.x();
      //const double y = p.y();
      const double z = p.z();
      const double d = (x/a)*(x/a) + (z/b)*(z/b);
      if (d < 1.0)
        return -1.0;
      else
      return 1.0;
    }

    // Ellipse major and minor radii
    const double a, b;

    // Ellipse curvature
    const double beta;
  };
}

TEST(Demo, Poisson)
{
  //Mesh that contains surface
  auto mesh = std::make_shared<dolfin::BoxMesh>(dolfin::Point(-0.4, -0.1, -0.4),
                                                dolfin::Point(0.4,  0.1,  0.4),
                                                 20, 20, 20);

  // Mesh fully intersected by surface
  dolfin::cout << "Num. mesh vertices: " << mesh->num_vertices()
               << dolfin::endl;
  dolfin::File mesh_file("output/"+gtest_nameroot()+"_mesh.pvd");
  mesh_file << *mesh;

  // Create fracture surface
  auto surface = std::make_shared<EllipticalSurface>();

  // Create DomainManager
  auto domain_manager = std::make_shared<narwal::DomainManager>(mesh);
  domain_manager->register_surface(surface, "surface0");

  // Create standard function space
  auto V_s = std::make_shared<Poisson3D_P1::FunctionSpace>(mesh);

  // Create XFEM space holder (this computes the enriched dofmap)
  auto V = std::make_shared<narwal::XFEMFunctionSpace>(V_s, domain_manager, 8);

  // Create XFEM elasticity forms
  auto xfem_forms = std::make_shared<narwal::PoissonProblem3D>(V, 1);

  // Create Dirichlet boundary conditions
  auto boundary = std::make_shared<DirichletBoundary>();
  auto boundary_value = std::make_shared<BoundaryValue>();
  auto bc = std::make_shared<dolfin::DirichletBC>(V_s, boundary_value, boundary);
  std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs;
  bcs.push_back(bc);

  // Create XFEM problem
  auto elasticity_problem = std::make_shared<narwal::XFEMProblem>(xfem_forms, bcs);

  // Assemble system
  dolfin::PETScMatrix A;
  dolfin::PETScVector b;
  elasticity_problem->assemble(A, b);

  // Create solution Function
  dolfin::Function u(V->function_space());

  dolfin::LUSolver solver;
  std::cout << "Size: " << u.vector()->size() << std::endl;
  solver.solve(A, *u.vector(), b);
  std::cout << "Solution vector norm: " << u.vector()->norm("l2") << std::endl;

  dolfin::File file_u("output/"+gtest_nameroot()+"_u.pvd");
  file_u << u;
}
