// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include <dolfin.h>
#include <Narwal.h>

#include "gtest_tools.h"

namespace
{
  // Surface representation class
  class StraightSurface : public narwal::ImplicitSurface
  {
  public:
    StraightSurface()
      : narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1), a(10.3),
        b(10.3), beta(0.05) {}

    // Normal distance from the crack surface
    double f0(const dolfin::Point& p) const
    {
      const double y = p.y();
      return y;
    }

    // If f0 = 0, then if f1<1 -> on surface and f1 >= = -> not on surface
    double f1(const dolfin::Point& p) const
    {
      const double x = p.x();
      //const double y = p.y();
      const double z = p.z();
      const double d = (x/a)*(x/a) + (z/b)*(z/b);
      if (d < 1.0)
        return -1.0;
      else
        return 1.0;
    }

    // Ellipse major and minor radii
    const double a, b;

    // Ellipse curvature
    const double beta;
  };

  // Surface representation class
  class EllipticalSurface : public narwal::ImplicitSurface
  {
  public:
    EllipticalSurface() :
      narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1), a(0.3),
      b(0.3), beta(0.05) {}
    explicit EllipticalSurface(const dolfin::Point& pt)
      : narwal::ImplicitSurface(pt, 3.1), a(0.3), b(0.3), beta(0.05) {}

    double f0(const dolfin::Point& p) const
    {
      const double y = p.y();
      return y;
    }

    // If f0 = 0, then if f1<1 -> on surface and f1 >= = -> not on surface
    double f1(const dolfin::Point& p) const
    {
      const double x = p.x();
      //const double y = p.y();
      const double z = p.z();
      const double d = (x/a)*(x/a) + (z/b)*(z/b);
      if (d < 1.0)
        return -1.0;
      else
        return 1.0;
    }

    // Ellipse major and minor radii
    const double a, b;

    // Ellipse curvature
    const double beta;
  };
}

/*
TEST(ConformingMeshGenerator, OneStraightCutUnitTet)
{
  // Create mesh
  auto mesh = std::make_shared<dolfin::UnitTetrahedronMesh>();
  dolfin::MeshTransformation::translate(*mesh, dolfin::Point(0.0,-0.5, 0.0));

  // Create fracture surfaces
  // Create fracture surface
  auto surface0 = std::make_shared<test_conforming_mes_gen::StraightSurface>();

  // Create DomainManager and register surfaces
  auto domain_manager =
    std::make_shared<narwal::DomainManager>(narwal::DomainManager(mesh));
  dolfin_assert(domain_manager);
  domain_manager->register_surface(surface0, "surface0");

  // Create mesh that conforms to crack surfaces
  auto conforming_mesh
    = narwal::ConformingMeshGenerator::build(*domain_manager);

  // Write mesh to file
  dolfin::File conforming_mesh_file("output/"+gtest_nameroot()
                                    + "_conforming_mesh.xdmf");
  conforming_mesh_file << *conforming_mesh;

  EXPECT_EQ((unsigned int) 4, conforming_mesh->num_cells());
  EXPECT_EQ((unsigned int) 10, conforming_mesh->num_vertices());

  {
    // Create mesh that conforms to crack surfaces
    auto conforming_surface
      = narwal::ConformingMeshGenerator::build_surface(*mesh, *surface0);

    dolfin::File conforming_mesh_file("output/"+gtest_nameroot()
                                      + "_conforming_mesh_surface.xdmf");
    conforming_mesh_file << *conforming_surface;

    EXPECT_EQ((unsigned int) 1, conforming_surface->num_cells());
    EXPECT_EQ((unsigned int) 3, conforming_surface->num_vertices());
  }
}
*/


/*
TEST(ConformingMeshGenerator, OneStraightCutSixElements)
{
  // Create mesh
  auto mesh = std::make_shared<dolfin::BoxMesh>(dolfin::Point(-0.1, -0.3, -0.1), dolfin::Point(0.1, 0.3, 0.1), 1, 1, 1);

  // Create fracture surfaces
  // Create fracture surface
  auto surface0 = std::make_shared<test_conforming_mes_gen::StraightSurface>();

  // Create DomainManager and register surfaces
  auto domain_manager =
    std::make_shared<narwal::DomainManager>(narwal::DomainManager(mesh));
  dolfin_assert(domain_manager);
  domain_manager->register_surface(surface0, "surface0");

  // Create mesh that conforms to crack surfaces
  auto conforming_mesh
    = narwal::ConformingMeshGenerator::build(*domain_manager);

  // Write mesh to file
  dolfin::File conforming_mesh_file("output/"+gtest_nameroot()
                                    + "_conforming_mesh.xdmf");
  conforming_mesh_file << *conforming_mesh;

  EXPECT_EQ((unsigned int) 28, conforming_mesh->num_cells());
  EXPECT_EQ((unsigned int) 26, conforming_mesh->num_vertices());

  {
    // Create mesh that conforms to crack surfaces
    auto conforming_surface
      = narwal::ConformingMeshGenerator::build_surface(*mesh, *surface0);

    dolfin::File conforming_mesh_file("output/"+gtest_nameroot() +
                                      "_conforming_mesh_surface.xdmf");
    conforming_mesh_file << *conforming_surface;

    EXPECT_EQ((unsigned int) 8, conforming_surface->num_cells());
    EXPECT_EQ((unsigned int) 20, conforming_surface->num_vertices());
  }
}
*/


TEST(ConformingMeshGenerator, OneStraightCutLargeMesh)
{
  // Create mesh
  auto mesh = std::make_shared<dolfin::BoxMesh>(dolfin::Point(0.0, -0.3, -0.1),
                                                dolfin::Point(0.1, 0.3, 0.1),
                                                21, 21, 21);

  // Create fracture surfaces
  // Create fracture surface
  auto surface0 = std::make_shared<::StraightSurface>();

  // Create DomainManager and register surfaces
  auto domain_manager =
    std::make_shared<narwal::DomainManager>(narwal::DomainManager(mesh));
  dolfin_assert(domain_manager);
  domain_manager->register_surface(surface0, "surface0");

  // Create mesh that conforms to crack surfaces
  auto conforming_mesh
    = narwal::ConformingMeshGenerator::build(*domain_manager);

  // Write mesh to file
  dolfin::XDMFFile conforming_mesh_file(mesh->mpi_comm(),
                                        "output/"+gtest_nameroot()
                                    + "_conforming_mesh.xdmf");
  conforming_mesh_file.write(*conforming_mesh);

  EXPECT_EQ(std::size_t(65268), conforming_mesh->num_cells());
  EXPECT_EQ(std::size_t(14346), conforming_mesh->num_vertices());
}


/*
TEST(ConformingMeshGenerator, OneCutUnitTet)
{
  // Create mesh
  auto mesh = std::make_shared<dolfin::UnitTetrahedronMesh>();

  // Create fracture surfaces
  // Create fracture surface
  auto surface0 = std::make_shared<EllipticalSurface>();

  // Create DomainManager and register surfaces
  auto domain_manager =
    std::make_shared<narwal::DomainManager>(narwal::DomainManager(mesh));
  dolfin_assert(domain_manager);
  domain_manager->register_surface(surface0, "surface0");

  // Create mesh that conforms to crack surfaces
  auto conforming_mesh
    = narwal::ConformingMeshGenerator::build(*domain_manager);

  // Write mesh to file
  dolfin::File conforming_mesh_file("output/" + gtest_nameroot()
                                    + "_conforming_mesh.xdmf");
  conforming_mesh_file << *conforming_mesh;

  EXPECT_EQ((unsigned int) 3, conforming_mesh->num_cells());
  EXPECT_EQ((unsigned int) 6, conforming_mesh->num_vertices());
}
*/

/*
TEST(ConformingMeshGenerator, OneCutSixElements)
{
  // Create mesh
  auto mesh = std::make_shared<dolfin::BoxMesh>(-0.1, -0.3, -0.1, 0.1, 0.3, 0.1,
                                                1, 1, 1);

  // Create fracture surfaces
  // Create fracture surface
  auto surface0 = std::make_shared<EllipticalSurface>();

  // Create DomainManager and register surfaces
  auto domain_manager =
    std::make_shared<narwal::DomainManager>(narwal::DomainManager(mesh));
  dolfin_assert(domain_manager);
  domain_manager->register_surface(surface0, "surface0");

  // Create mesh that conforms to crack surfaces
  auto conforming_mesh
    = narwal::ConformingMeshGenerator::build(*domain_manager);

  // Write mesh to file
  dolfin::File conforming_mesh_file("output/"+gtest_nameroot()
                                    + "_conforming_mesh.xdmf");
  conforming_mesh_file << *conforming_mesh;

  EXPECT_EQ((unsigned int) 28, conforming_mesh->num_cells());
  EXPECT_EQ((unsigned int) 26, conforming_mesh->num_vertices());
}
*/

/*
TEST(ConformingMeshGenerator, OneCutLargeMesh)
{
  // Create mesh
  auto mesh = std::make_shared<dolfin::BoxMesh>(0.0, -0.3, -0.1, 0.1, 0.3, 0.1,
                                                21, 21, 21);

  // Create fracture surfaces
  // Create fracture surface
  auto surface0 = std::make_shared<test_conforming_mesh_gen::EllipticalSurface>();

  // Create DomainManager and register surfaces
  auto domain_manager =
    std::make_shared<narwal::DomainManager>(narwal::DomainManager(mesh));
  dolfin_assert(domain_manager);
  domain_manager->register_surface(surface0, "surface0");

  // Create mesh that conforms to crack surfaces
  auto conforming_mesh
    = narwal::ConformingMeshGenerator::build(*domain_manager);

  // Write mesh to file
  dolfin::File conforming_mesh_file("output/" + gtest_nameroot()
                                    + "_conforming_mesh.xdmf");
  conforming_mesh_file << *conforming_mesh;

  EXPECT_EQ((unsigned int) 65432, conforming_mesh->num_cells());
  EXPECT_EQ((unsigned int) 14406, conforming_mesh->num_vertices());

  {
    // Create mesh that conforms to crack surfaces
    auto conforming_surface
      = narwal::ConformingMeshGenerator::build_surface(*mesh, *surface0);

    dolfin::File conforming_mesh_file("output/" + gtest_nameroot()
                                      + "_conforming_mesh_surface.xdmf");
    conforming_mesh_file << *conforming_surface;

    EXPECT_EQ((unsigned int) 3586, conforming_surface->num_cells());
    EXPECT_EQ((unsigned int) 8974, conforming_surface->num_vertices());
  }
}
*/
