// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

// Unit tests for function in narwal::XFEMTools

#include "gtest_tools.h"

#include <dolfin.h>
#include <Narwal.h>
#include "Elasticity3D_P1.h"
#include <ElasticityProblem3D.h>


namespace
{
  class FlatSurface : public narwal::ImplicitSurface
  {
  public:
    FlatSurface(double theta, double shift)
      : narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1),
        _theta(theta), _shift(shift) {}

    // Normal distance from the crack surface
    double f0(const dolfin::Point& p) const
    {
      const double y = std::sin(_theta)*(p.x() - _shift) + std::cos(_theta)*(p.y() - _shift);
      return y;
    }

    // If f0 = 0, then if f1 < 1 -> on surface and f1 >= = -> not on surface
    double f1(const dolfin::Point& p) const
    { return -1; }

    // Rotate axis
    const double _theta, _shift;

  };

  std::shared_ptr<narwal::XFEMFunctionSpace>
  assemble_elastic_problem(
    dolfin::PETScMatrix& A, dolfin::PETScVector& b,
    std::shared_ptr<const dolfin::Mesh> mesh,
    const double E, const double nu,
    std::shared_ptr<const dolfin::GenericFunction> pressure,
    std::shared_ptr<const dolfin::SubDomain> dirichlet_boundary,
    std::vector<std::shared_ptr<const narwal::GenericSurface>> surfaces,
    int degree)
  {
    // Create DomainManager
    auto domain_manager = std::make_shared<narwal::DomainManager>(mesh);

    // Add surfaces
    for (std::size_t i = 0; i < surfaces.size(); ++i)
    {
      domain_manager->register_surface(surfaces[i],
                                       "surface" + std::to_string(i));
    }

    // Create standard DOLFIN function space
    std::shared_ptr<dolfin::FunctionSpace>
      V_s(new Elasticity3D_P1::FunctionSpace(mesh));

    // Create XFEM space (this computes the enriched dofmap)
    std::shared_ptr<narwal::XFEMFunctionSpace>
      V(new narwal::XFEMFunctionSpace(V_s, domain_manager, 24));

    // Elasticity parameters (as coefficients to allow spatial variation)
    std::shared_ptr<dolfin::GenericFunction>
      mu(new dolfin::Constant(E/(2.0*(1.0 + nu))));
    std::shared_ptr<dolfin::GenericFunction>
      lambda(new dolfin::Constant(E*nu/((1.0 + nu)*(1.0 - 2.0*nu))));

    // Create XFEM elasticity forms
    dolfin_assert(pressure);
    std::shared_ptr<narwal::XFEMForms>
      xfem_forms(new narwal::ElasticityProblem3D(V, lambda, mu, pressure,
                                                 degree));

    // Dirichlet boundary conditions
    std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs;
    if (dirichlet_boundary)
    {
      auto boundary_value = std::make_shared<dolfin::Constant>(0.0, 0.0, 0.0);
      auto bc = std::make_shared<dolfin::DirichletBC>(V_s, boundary_value,
                                                      dirichlet_boundary);
      bcs.push_back(bc);
    }

    // Create XFEM problem
    auto elasticity_problem
      = std::make_shared<narwal::XFEMProblem>(xfem_forms, bcs);

    // Assemble matrix and vector
    elasticity_problem->assemble(A, b);

    // Return function space
    return V;
  }

}


TEST(AssembleSolve, surface_pressure)
{
  class DirichletBoundary : public dolfin::SubDomain
  {  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
    {
      return std::abs(x[1] + 0.5) < 1.0e-4
                                    || std::abs(x[1] - 0.5) < 1.0e-4;
    }
  };

  // Create mesh
  auto mesh = std::make_shared<dolfin::BoxMesh>(dolfin::Point(-0.5, -0.5, -0.5),
                                                dolfin::Point(0.5, 0.5, 0.5),
                                                1, 11, 1);

  // Create Dirichlet boundary marker
  auto dirichlet_boundary = std::make_shared<DirichletBoundary>();

  // Create fracture surface
  auto surface = std::make_shared<::FlatSurface>(0.0, 0.0);

  // Create pressure loading
  auto pressure = std::make_shared<const dolfin::Constant>(-1.0);

  // Assemble system
  dolfin::PETScMatrix A; dolfin::PETScVector b;
  auto V = ::assemble_elastic_problem(A, b, mesh, 1.0, 0.0,
                                      pressure,
                                      dirichlet_boundary,
                                      {{surface}}, 1);

  // Create solution Function and solve
  dolfin::Function u(V->function_space());
  dolfin::LUSolver solver;
  solver.solve(A, *u.vector(), b);

  // Evalute function at points and check
  dolfin::Array<double> values(3);

  u(values, dolfin::Point(0.0, -0.4, 0.0));
  ASSERT_NEAR(0.0, values[0], 1.0e-12);
  ASSERT_NEAR(-0.1, values[1], 1.0e-12);
  ASSERT_NEAR(0.0, values[2], 1.0e-12);

  u(values, dolfin::Point(-0.48, -0.3, 0.0));
  ASSERT_NEAR(0.0, values[0], 1.0e-12);
  ASSERT_NEAR(-0.2, values[1], 1.0e-12);
  ASSERT_NEAR(0.0, values[2], 1.0e-12);

  u(values, dolfin::Point(0.0, -0.2, 0.48));
  ASSERT_NEAR(0.0, values[0], 1.0e-12);
  ASSERT_NEAR(-0.3, values[1], 1.0e-12);
  ASSERT_NEAR(0.0, values[2], 1.0e-12);

  u(values, dolfin::Point(0.0, 0.2, 0.0));
  ASSERT_NEAR(0.0, values[0], 1.0e-12);
  ASSERT_NEAR(0.3, values[1], 1.0e-12);
  ASSERT_NEAR(0.0, values[2], 1.0e-12);

  u(values, dolfin::Point(0.0, 0.3, 0.0));
  ASSERT_NEAR(0.0, values[0], 1.0e-12);
  ASSERT_NEAR(0.2, values[1], 1.0e-12);
  ASSERT_NEAR(0.0, values[2], 1.0e-12);
}
//-----------------------------------------------------------------------------
TEST(AssembleSolve, intersecting_surface_dirichlet)
{
  class TopLeft : public dolfin::SubDomain
  {
  public:
    bool inside(const dolfin::Array<double>& x, bool on_boundary) const
    {
      if (std::abs(x[0] + 1.0) > 1.0e-4)
        return false;
      else if (std::abs(x[1] - 1.0) > 0.3)
        return false;
      else
        return true;
    }
  };

  class BottomLeft : public dolfin::SubDomain
  {
  public:
    bool inside(const dolfin::Array<double>& x, bool on_boundary) const
    {
      if (std::abs(x[0] + 1.0) > 1.0e-4)
        return false;
      else if (std::abs(x[1] + 1.0) > 0.3)
        return false;
      else
        return true;
    }
  };

  class TopRight : public dolfin::SubDomain
  {
  public:
    bool inside(const dolfin::Array<double>& x, bool on_boundary) const
    {
      if (std::abs(x[0] - 1.0) > 1.0e-4)
        return false;
      else if (std::abs(x[1] - 1.0) > 0.3)
        return false;
      else
        return true;
    }
  };

  class BottomRight : public dolfin::SubDomain
  {
  public:
    bool inside(const dolfin::Array<double>& x, bool on_boundary) const
    {
      if (std::abs(x[0] - 1.0) > 1.0e-4)
        return false;
      else if (std::abs(x[1] + 1.0) > 0.3)
        return false;
      else
        return true;
    }
  };

  // Create mesh
  auto mesh = std::make_shared<dolfin::BoxMesh>(dolfin::Point(-1.0, -1.0, -0.5),
                                                dolfin::Point(1.0, 1.0, 0.5),
                                                9, 9, 5);

  // Create fracture surfaces
  auto surface0 = std::make_shared<::FlatSurface>(0.0, 0.0);
  auto surface1 = std::make_shared<::FlatSurface>(DOLFIN_PI/2.0, 0.05);

  // Create DomainManager
  auto domain_manager = std::make_shared<narwal::DomainManager>(mesh);
  domain_manager->register_surface(surface0, "surface0");
  domain_manager->register_surface(surface1, "surface1");

  // Create standard DOLFIN function space and subspaces
  auto V_s = std::make_shared<Elasticity3D_P1::FunctionSpace>(mesh);

  // Create XFEM space (this computes the enriched dofmap)
  auto V = std::make_shared<narwal::XFEMFunctionSpace>(V_s, domain_manager, 128);

  // Elasticity parameters (as coefficients to allow spatial variation)
  const double E = 100.0;
  const double nu = 0.0;
  std::shared_ptr<dolfin::GenericFunction>
    mu(new dolfin::Constant(E/(2.0*(1.0 + nu))));
  std::shared_ptr<dolfin::GenericFunction>
    lambda(new dolfin::Constant(E*nu/((1.0 + nu)*(1.0 - 2.0*nu))));

  // Create pressure loading
  auto pressure = std::make_shared<const dolfin::Constant>(0.0);

  // Create XFEM elasticity forms
  dolfin_assert(pressure);
  auto xfem_forms = std::make_shared<narwal::ElasticityProblem3D>(V, lambda,
                                                                  mu, pressure, 1);

  // Create Dirichlet boundary marker
  auto boundary_top_left = std::make_shared<TopLeft>();
  auto boundary_bottom_left = std::make_shared<BottomLeft>();
  auto boundary_top_right = std::make_shared<TopRight>();
  auto boundary_bottom_right = std::make_shared<BottomRight>();

  auto top_left_vector = std::make_shared<dolfin::Constant>(-1.0, 1.0, -1.0);
  auto bottom_left_vector = std::make_shared<dolfin::Constant>(-1.0, -1.0, 1.0);
  auto top_right_vector = std::make_shared<dolfin::Constant>(1.0, 1.0, 1.0);
  auto bottom_right_vector = std::make_shared<dolfin::Constant>(1.0, -1.0, -1.0);

  auto bc_top_left = std::make_shared<dolfin::DirichletBC>(V_s, top_left_vector,
                                                           boundary_top_left);
  auto bc_bottom_left = std::make_shared<dolfin::DirichletBC>(V_s, bottom_left_vector,
                                                              boundary_bottom_left);
  auto bc_top_right = std::make_shared<dolfin::DirichletBC>(V_s, top_right_vector,
                                                            boundary_top_right);
  auto bc_bottom_right = std::make_shared<dolfin::DirichletBC>(V_s, bottom_right_vector,
                                                               boundary_bottom_right);

  std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs
    = {bc_top_left, bc_bottom_left, bc_top_right, bc_bottom_right};

  // Create XFEM problem
  auto elasticity_problem
    = std::make_shared<narwal::XFEMProblem>(xfem_forms, bcs);

  // Assemble matrix and vector
  dolfin::PETScMatrix A;
  dolfin::PETScVector b;
  elasticity_problem->assemble(A, b);

  // Create solution Function and solve
  dolfin::Function u(V->function_space());
  dolfin::LUSolver solver;
  solver.solve(A, *u.vector(), b);

  // Evalute function at points and check
  dolfin::Array<double> values(3);

  // Top right block
  u(values, dolfin::Point(0.8, 0.8, 0.45));
  ASSERT_NEAR(1.0, values[0], 1.0e-12);
  ASSERT_NEAR(1.0, values[1], 1.0e-12);
  ASSERT_NEAR(1.0, values[2], 1.0e-12);

  // Bottom right block
  u(values, dolfin::Point(0.8, -0.8, -0.33));
  ASSERT_NEAR(1.0, values[0], 1.0e-12);
  ASSERT_NEAR(-1.0, values[1], 1.0e-12);
  ASSERT_NEAR(-1.0, values[2], 1.0e-12);

  // Top left block
  u(values, dolfin::Point(-0.83, 0.79, 0.02));
  ASSERT_NEAR(-1.0, values[0], 1.0e-12);
  ASSERT_NEAR(1.0, values[1], 1.0e-12);
  ASSERT_NEAR(-1.0, values[2], 1.0e-12);

  // Bottom left block
  u(values, dolfin::Point(-0.93, -0.79, 0.49));
  ASSERT_NEAR(-1.0, values[0], 1.0e-12);
  ASSERT_NEAR(-1.0, values[1], 1.0e-12);
  ASSERT_NEAR(1.0, values[2], 1.0e-12);
}
//-----------------------------------------------------------------------------
