// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

// Unit tests for function in narwal::XFEMTools

#include "gtest_tools.h"

#include <dolfin.h>
#include <Narwal.h>
#include "Elasticity3D_P1.h"
#include <ElasticityProblem3D.h>


namespace
{
  class FlatSurface : public narwal::ImplicitSurface
  {
  public:
    FlatSurface(double theta, double shift)
      : narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1),
        _theta(theta), _shift(shift) {}

    // Normal distance from the crack surface
    double f0(const dolfin::Point& p) const
    {
      const double y = std::sin(_theta)*(p.x() - _shift) + std::cos(_theta)*(p.y() - _shift);
      return y;
    }

    // If f0 = 0, then if f1 < 1 -> on surface and f1 >= = -> not on surface
    double f1(const dolfin::Point& p) const
    { return -1; }

    // Rotate axis
    const double _theta, _shift;

  };

  // Compute eigenvalues and check number of zero eigenvalues
  int compute_num_zero_evals(const dolfin::PETScMatrix& A)
  {
    dolfin::SLEPcEigenSolver esolver(dolfin::reference_to_no_delete_pointer(A));
    esolver.parameters["solver"] = "krylov-schur";
    esolver.solve();
    int num_zero_evals = 0;
    for (std::size_t i = 0; i < A.size(0); ++i)
    {
      double er, ec;
      esolver.get_eigenvalue(er, ec, i);
      if (std::abs(er) < 1.0e-12)
        ++num_zero_evals;
    }

    return num_zero_evals;
  }

  int assemble_elastic_problem(dolfin::PETScMatrix& A, dolfin::PETScVector& b,
                               std::shared_ptr<const dolfin::Mesh> mesh,
                               std::shared_ptr<const dolfin::GenericFunction> pressure,
                               std::vector<std::shared_ptr<const narwal::GenericSurface>> surfaces,
                               int degree)
  {
    // Create DomainManager
    auto domain_manager = std::make_shared<narwal::DomainManager>(mesh);

    // Add surfaces
    for (std::size_t i = 0; i < surfaces.size(); ++i)
      domain_manager->register_surface(surfaces[i], "surface" + std::to_string(i));

    // Create standard DOLFIN function space
    std::shared_ptr<dolfin::FunctionSpace>
      V_s(new Elasticity3D_P1::FunctionSpace(mesh));

    // Create XFEM space (this computes the enriched dofmap)
    std::shared_ptr<narwal::XFEMFunctionSpace>
      V(new narwal::XFEMFunctionSpace(V_s, domain_manager, 24));

    // Elasticity parameters (as coefficients to allow spatial variation)
    const double E(1.0), nu(0.2);
    std::shared_ptr<dolfin::GenericFunction>
      mu(new dolfin::Constant(E/(2.0*(1.0 + nu))));
    std::shared_ptr<dolfin::GenericFunction>
      lambda(new dolfin::Constant(E*nu/((1.0 + nu)*(1.0 - 2.0*nu))));

    std::shared_ptr<dolfin::GenericFunction> crack_pressure(new dolfin::Constant(0.0));

    // Create XFEM elasticity forms
    dolfin_assert(pressure);
    std::shared_ptr<narwal::XFEMForms>
      xfem_forms(new narwal::ElasticityProblem3D(V, lambda, mu, pressure, degree));

    // Empty Dirichlet boundary conditions
    std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs;

    // Create XFEM problem
    auto elasticity_problem
      = std::make_shared<narwal::XFEMProblem>(xfem_forms, bcs);

    // Assemble matrix and vector
    elasticity_problem->assemble(A, b);

    // Return number of zeor eigenvalues
    return compute_num_zero_evals(A);
  }

  /*
  int assemble_elastic_problem(dolfin::PETScMatrix& A, dolfin::PETScVector& b,
                               std::shared_ptr<const dolfin::Mesh> mesh,
                               std::shared_ptr<const dolfin::GenericFunction> pressure,
                               std::shared_ptr<const narwal::GenericSurface> surface,
                               int degree)
  {
    std::vector<std::shared_ptr<const narwal::GenericSurface>>
      surfaces = {{surface}};
    return assemble_elastic_problem(A, b, mesh, pressure, surfaces, degree);
  }
  */

  int assemble_elastic_problem(dolfin::PETScMatrix& A, dolfin::PETScVector& b,
                               std::shared_ptr<const dolfin::Mesh> mesh,
                               std::vector<std::shared_ptr<const narwal::GenericSurface>> surfaces,
                               int degree)
  {
    auto pressure = std::make_shared<const dolfin::Constant>(0.0);
    return assemble_elastic_problem(A, b, mesh, pressure, surfaces, degree);
  }

  int assemble_elastic_problem(dolfin::PETScMatrix& A, dolfin::PETScVector& b,
                               std::shared_ptr<const dolfin::Mesh> mesh,
                               std::shared_ptr<const narwal::GenericSurface> surface,
                               int degree)
  {
    std::vector<std::shared_ptr<const narwal::GenericSurface>>
      surfaces = {{surface}};
    return assemble_elastic_problem(A, b, mesh, surfaces, degree);
  }
}

//-----------------------------------------------------------------------------
TEST(Assembler, cell_fully_intersected)
{
  // Test assembly of cell intersected by flat, (interesecting)
  // surface(s).

  // Create single cell mesh
  std::shared_ptr<dolfin::Mesh> mesh(new dolfin::UnitTetrahedronMesh());

  // Create fracture surfaces
  auto surface0 = std::make_shared<::FlatSurface>(0.0, 0.2);
  auto surface1 = std::make_shared<::FlatSurface>(DOLFIN_PI/2.0, 0.15);
  auto surface2 = std::make_shared<::FlatSurface>(0.0, 0.15);

  // Test surface 0
  {
    // Assemble system
    dolfin::PETScMatrix A; dolfin::PETScVector b;
    const int num_zero_evals = ::assemble_elastic_problem(A, b, mesh, surface0, 1);

    // Check sizes and number of zero eigenvalues
    ASSERT_EQ((std::size_t) 24, b.size());
    ASSERT_EQ((std::size_t) 24, A.size(0));
    ASSERT_EQ((std::size_t) 24, A.size(1));
    ASSERT_EQ(2*6, num_zero_evals);

    // Check matrix norm
    const double A_norm = A.norm("frobenius");
    ASSERT_NEAR(1.261989849937114, A_norm, 1.0e-8);
  }

  // Test surface 1
  {
    // Assemble system
    dolfin::PETScMatrix A; dolfin::PETScVector b;
    const int num_zero_evals = ::assemble_elastic_problem(A, b, mesh, surface1, 1);

    // Check sizes and number of zero eigenvalues
    ASSERT_EQ((std::size_t) 24, b.size());
    ASSERT_EQ((std::size_t) 24, A.size(0));
    ASSERT_EQ((std::size_t) 24, A.size(1));
    ASSERT_EQ(2*6, num_zero_evals);

    // Check matrix norm
    //const double A_norm = A.norm("frobenius");
    //ASSERT_NEAR(0.65893044714141213, A_norm, 1.0e-9);
  }

  // Test two non-intersecting surfaces
  {
    // Assemble system and compute number of zero eigenvalues
    dolfin::PETScMatrix A; dolfin::PETScVector b;
    const int num_zero_evals
      = ::assemble_elastic_problem(A, b, mesh,
                                                 {{surface0, surface2}}, 1);

    // Check sizes and number of zero eigenvalues
    ASSERT_EQ((std::size_t) 36, b.size());
    ASSERT_EQ((std::size_t) 36, A.size(0));
    ASSERT_EQ((std::size_t) 36, A.size(1));
    ASSERT_EQ(3*6, num_zero_evals);

    // Check matrix norm
    //const double A_norm = A.norm("frobenius");
    //ASSERT_NEAR(1.920920297078526, A_norm, 1.0e-9);
  }

  // Test two intersecting surfaces
  {
    // Assemble system and compute number of zero eigenvalues
    dolfin::PETScMatrix A; dolfin::PETScVector b;
    const int num_zero_evals
      = ::assemble_elastic_problem(A, b, mesh,
                                   {{surface0, surface1}}, 1);

    // Check sizes and number of zero eigenvalues
    ASSERT_EQ((std::size_t) 48, b.size());
    ASSERT_EQ((std::size_t) 48, A.size(0));
    ASSERT_EQ((std::size_t) 48, A.size(1));
    ASSERT_EQ(4*6, num_zero_evals);

    // Check matrix norm
    //const double A_norm = A.norm("frobenius");
    //ASSERT_NEAR(1.920920297078526, A_norm, 1.0e-8);
  }

  // Test two intersecting surfaces (P2)
  {
    // Assemble system and compute number of zero eigenvalues
    dolfin::PETScMatrix A; dolfin::PETScVector b;
    const int num_zero_evals
      = ::assemble_elastic_problem(A, b, mesh, {{surface0, surface1}}, 2);

    // Check sizes and number of zero eigenvalues
    ASSERT_EQ((std::size_t) 48, b.size());
    ASSERT_EQ((std::size_t) 48, A.size(0));
    ASSERT_EQ((std::size_t) 48, A.size(1));
    ASSERT_EQ(4*6, num_zero_evals);

    // Check matrix norm
    //const double A_norm = A.norm("frobenius");
    //ASSERT_NEAR(1.920920297078526, A_norm, 1.0e-8);
  }
}
//-----------------------------------------------------------------------------
TEST(Assembler, multi_cell_fully_intersected)
{
  // Test assembly of elastic cube fully intersected by two flat,
  // interesecting surfaces. Resultign matrix should have 24 zero
  // eigenvalues (rigid body modes)

  // Create mesh
  auto mesh = std::make_shared<dolfin::BoxMesh>(dolfin::Point(-0.5, -0.5, -0.5),
                                                dolfin::Point(0.5, 0.5, 0.5),
                                                1, 1, 1);

  // Create fracture surfaces
  auto surface0 = std::make_shared<::FlatSurface>(0.0, 0.2);
  auto surface1 = std::make_shared<::FlatSurface>(DOLFIN_PI/2.0,
                                                                0.15);
  auto surface2 = std::make_shared<::FlatSurface>(0.0, 0.15);

  // Test surface 0 with cube
  {
    // Assemble system
    dolfin::PETScMatrix A; dolfin::PETScVector b;
    const int num_zero_evals = ::assemble_elastic_problem(A, b, mesh, surface0, 1);

    // Check sizes and number of zero eigenvalues
    ASSERT_EQ((std::size_t) 48, b.size());
    ASSERT_EQ((std::size_t) 48, A.size(0));
    ASSERT_EQ((std::size_t) 48, A.size(1));
    ASSERT_EQ(2*6, num_zero_evals);

    // Check matrix norm
    const double A_norm = A.norm("frobenius");
    ASSERT_NEAR(3.95365528962464, A_norm, 1.0e-8);
  }

  // Test surface 1 with cube
  {
    // Assemble system
    dolfin::PETScMatrix A; dolfin::PETScVector b;
    const int num_zero_evals = ::assemble_elastic_problem(A, b, mesh, surface1, 1);

    // Check sizes and number of zero eigenvalues
    ASSERT_EQ((std::size_t) 48, b.size());
    ASSERT_EQ((std::size_t) 48, A.size(0));
    ASSERT_EQ((std::size_t) 48, A.size(1));
    ASSERT_EQ(2*6, num_zero_evals);

    // Check matrix norm
    const double A_norm = A.norm("frobenius");
    ASSERT_NEAR(3.874257923663929, A_norm, 1.0e-9);
  }

  // Test two non-intersecting surfaces
  {
    // Assemble system and compute number of zero eigenvalues
    dolfin::PETScMatrix A; dolfin::PETScVector b;
    const int num_zero_evals
      = ::assemble_elastic_problem(A, b, mesh, {{surface0, surface2}}, 1);

    // Check sizes and number of zero eigenvalues
    ASSERT_EQ((std::size_t) 72, b.size());
    ASSERT_EQ((std::size_t) 72, A.size(0));
    ASSERT_EQ((std::size_t) 72, A.size(1));
    ASSERT_EQ(3*6, num_zero_evals);

    // Check matrix norm
    //const double A_norm = A.norm("frobenius");
    //std::cout << std::setprecision(16) << "Test norm (0/1): " << A_norm
    //          << std::endl;
    //ASSERT_NEAR(1.920920297078526, A_norm, 1.0e-9);
  }

  // Test two intersecting surfaces
  {
    // Assemble system and compute number of zero eigenvalues
    dolfin::PETScMatrix A; dolfin::PETScVector b;
    const int num_zero_evals
      = ::assemble_elastic_problem(A, b, mesh, {{surface0, surface1}}, 1);

    // Check sizes and number of zero eigenvalues
    ASSERT_EQ((std::size_t) 90, b.size());
    ASSERT_EQ((std::size_t) 90, A.size(0));
    ASSERT_EQ((std::size_t) 90, A.size(1));
    ASSERT_EQ(4*6, num_zero_evals);

    // Check matrix norm
    //const double A_norm = A.norm("frobenius");
    //std::cout << std::setprecision(16) << "Test norm (0 + 1): " << A_norm
    //          << std::endl;
    //ASSERT_NEAR(1.920920297078526, A_norm, 1.0e-9);
  }

  // Test two intersecting surfaces (P2)
  {
    // Assemble system and compute number of zero eigenvalues
    dolfin::PETScMatrix A; dolfin::PETScVector b;
    const int num_zero_evals
      = ::assemble_elastic_problem(A, b, mesh, {{surface0, surface1}}, 2);

    // Check sizes and number of zero eigenvalues
    ASSERT_EQ((std::size_t) 90, b.size());
    ASSERT_EQ((std::size_t) 90, A.size(0));
    ASSERT_EQ((std::size_t) 90, A.size(1));
    ASSERT_EQ(4*6, num_zero_evals);

    // Check matrix norm
    //const double A_norm = A.norm("frobenius");
    //std::cout << std::setprecision(16) << "Test norm (0 + 1): " << A_norm
    //          << std::endl;
    //ASSERT_NEAR(1.920920297078526, A_norm, 1.0e-9);
  }

  // Two cells, not intersecting
  {
    dolfin::CellFunction<std::size_t> domains(mesh, 0);
    domains[0] = 1;
    domains[1] = 1;
    auto submesh = std::make_shared<dolfin::SubMesh>(*mesh, domains, 1);

    auto surface3 = std::make_shared<::FlatSurface>(DOLFIN_PI/2.0, 0.1);

    // Assemble system and compute number of zero eigenvalues
    dolfin::PETScMatrix A; dolfin::PETScVector b;
    const int num_zero_evals
      = ::assemble_elastic_problem(A, b, submesh,
                                   {{surface0, surface3}}, 1);

    ASSERT_EQ(3*6, num_zero_evals);
  }

  // Two cells, fully intersecting
  {
    dolfin::CellFunction<std::size_t> domains(mesh, 0);
    domains[0] = 1;
    domains[1] = 1;
    auto submesh = std::make_shared<dolfin::SubMesh>(*mesh, domains, 1);

    auto surface3 = std::make_shared<::FlatSurface>(DOLFIN_PI/2.0, 0.25);

    // Assemble system and compute number of zero eigenvalues
    dolfin::PETScMatrix A; dolfin::PETScVector b;
    const int num_zero_evals
      = ::assemble_elastic_problem(A, b, submesh,
                                   {{surface0, surface3}}, 1);
    ASSERT_EQ(4*6, num_zero_evals);
  }

  // Two cells, fully intersecting
  {
    dolfin::CellFunction<std::size_t> domains(mesh, 0);
    domains[2] = 1;
    domains[4] = 1;
    auto submesh = std::make_shared<dolfin::SubMesh>(*mesh, domains, 1);

    auto surface3 = std::make_shared<::FlatSurface>(DOLFIN_PI/2.0, 0.25);
    auto surface4 = std::make_shared<::FlatSurface>(0.0, 0.1);

    // Assemble system and compute number of zero eigenvalues
    dolfin::PETScMatrix A; dolfin::PETScVector b;
    const int num_zero_evals
      = ::assemble_elastic_problem(A, b, submesh,
                                   {{surface4, surface3}}, 1);
    ASSERT_EQ(4*6, num_zero_evals);
  }
}
//-----------------------------------------------------------------------------
TEST(Assembler, cube_fully_intersected)
{
  // Create mesh
  auto mesh = std::make_shared<dolfin::BoxMesh>(dolfin::Point(-0.5, -0.5, -0.5),
                                                dolfin::Point(0.5, 0.5, 0.5),
                                                4, 4, 4);

  // Create fracture surfaces
  auto surface0 = std::make_shared<::FlatSurface>(0.0, 0.2);
  auto surface1 = std::make_shared<::FlatSurface>(DOLFIN_PI/2.0,
                                                                0.15);
  auto surface2 = std::make_shared<::FlatSurface>(0.0, 0.15);

  // Parallel surfaces
  {
    // Assemble system
    dolfin::PETScMatrix A; dolfin::PETScVector b;
    const int num_zero_evals
      = ::assemble_elastic_problem(A, b, mesh,
                                   {{surface0, surface2}}, 1);

    // Check sizes and number of zero eigenvalues
    ASSERT_EQ(3*6, num_zero_evals);

    // Check matrix norm
    //const double A_norm = A.norm("frobenius");
    //ASSERT_NEAR(3.95365528962464, A_norm, 1.0e-8);
  }

  // Intersecting surfaces
  {
    // Assemble system
    dolfin::PETScMatrix A; dolfin::PETScVector b;
    const int num_zero_evals
      = ::assemble_elastic_problem(A, b, mesh,
                                   {{surface0, surface1}}, 1);

    // Check sizes and number of zero eigenvalues
    ASSERT_EQ(4*6, num_zero_evals);

    // Check matrix norm
    //const double A_norm = A.norm("frobenius");
    //ASSERT_NEAR(3.95365528962464, A_norm, 1.0e-8);
  }

}
//-----------------------------------------------------------------------------
TEST(Assembler, surface_pressure)
{
  // Create mesh
  auto mesh = std::make_shared<dolfin::BoxMesh>(dolfin::Point(-0.5, -0.5, -0.5),
                                                dolfin::Point(0.5, 0.5, 0.5),
                                                4, 4, 4);

  // Create fracture surface
  auto surface = std::make_shared<::FlatSurface>(0.0, 0.2);

  // Create pressure loading
  auto pressure = std::make_shared<const dolfin::Constant>(1.0);

  // Assemble system
  dolfin::PETScMatrix A; dolfin::PETScVector b;
  ::assemble_elastic_problem(A, b, mesh, pressure, {{surface}}, 1);

  std::cout << "Sum:: " << b.sum() << std::endl;
  std::cout << "RHS norm: " << b.norm("l1") << std::endl;
  //std::cout << "Area:   : " << 0.8*0.8 << std::endl;

  //ASSERT_DOUBLE_EQ(0.0, b.sum());
  const double expected_pressure = (*pressure);
  std::cout << "Test: " << *pressure << std::endl;
  ASSERT_DOUBLE_EQ(expected_pressure, b.norm("l1"));
}
//-----------------------------------------------------------------------------
