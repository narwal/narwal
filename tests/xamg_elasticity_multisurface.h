// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//         Axel Gerstenberger
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_TEST_XAMG_ELASTICITY_MULTISURFACE_H
#define NARWAL_TEST_XAMG_ELASTICITY_MULTISURFACE_H

#include <set>
#include <dolfin.h>
#include <ElasticityProblem3D.h>
#include <Narwal.h>

namespace test
{
  namespace xamg_elasticity_ms
  {
/*
    static std::shared_ptr<dolfin::Mesh> buildOneCellMesh()
    {
      auto mesh = std::make_shared<dolfin::Mesh>();

      dolfin::MeshEditor me;
      me.open(*mesh, 3, 3);
      me.init_vertices(4);

      dolfin::Point(-1.0, -1.0, -0.5);
      dolfin::Point(1.0, 1.0, 0.5);
      int index = 0;
      me.add_vertex(index++, -1.0, -1.0,  0.5);
      me.add_vertex(index++,  1.0, -1.0,  0.5);
      me.add_vertex(index++, -1.0,  1.0,  0.5);
      me.add_vertex(index++, -1.0, -1.0, -0.5);
      me.init_cells(1);

      int eleIndex = 0;
      me.add_cell(eleIndex++, 0, 1, 2, 3);
      me.close();

      dolfin::File file("oneCellMesh.pvd");
      file << *mesh;

      return mesh;
    }
*/
    // Pressure on crack surface
    class CrackPressure : public dolfin::Expression
    {
      void eval(dolfin::Array<double>& values, const dolfin::Array<double>& /*x*/) const
      { values[0] = 0.0; }
    };

    class TopLeft : public dolfin::SubDomain
    {
    public:
      bool inside(const dolfin::Array<double>& x, bool /*on_boundary*/) const
      {
        if (std::abs(x[0] + 1.0) > 1.0e-4)
          return false;
        else if (std::abs(x[1] - 1.0) > 0.3)
          return false;
        else
          return true;
      }
    };

    class BottomLeft : public dolfin::SubDomain
    {
    public:
      bool inside(const dolfin::Array<double>& x, bool /*on_boundary*/) const
      {
        if (std::abs(x[0] + 1.0) > 1.0e-4)
          return false;
        else if (std::abs(x[1] + 1.0) > 0.3)
          return false;
        else
          return true;
      }
    };

    class TopRight : public dolfin::SubDomain
    {
    public:
      bool inside(const dolfin::Array<double>& x, bool /*on_boundary*/) const
      {
        if (std::abs(x[0] - 1.0) > 1.0e-4)
          return false;
        else if (std::abs(x[1] - 1.0) > 0.3)
          return false;
        else
          return true;
      }
    };

    class BottomRight : public dolfin::SubDomain
    {
    public:
      bool inside(const dolfin::Array<double>& x, bool /*on_boundary*/) const
      {
        if (std::abs(x[0] - 1.0) > 1.0e-4)
          return false;
        else if (std::abs(x[1] + 1.0) > 0.3)
          return false;
        else
          return true;
      }
    };

    // Dirichlet boundary condition function
    class BoundaryValue : public dolfin::Expression
    {
    public:
      BoundaryValue() : Expression(3) {}
      void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
      {
        const double disp = 0.1;
        const bool isRight = (x[0] > 0.0);
        const bool isUp = (x[1] > 0.0);

        if (isRight && isUp)
        {
          values[0] = disp;
          values[1] = disp;
          values[2] = 0.0;
        }
        else if (!isRight && isUp)
        {
          values[0] = -disp;
          values[1] =  disp;
          values[2] = 0.0;
        }
        else if (!isRight && !isUp)
        {
          values[0] = -disp;
          values[1] = -disp;
          values[2] = 0.0;
        }
        else
        {
          values[0] =  disp;
          values[1] = -disp;
          values[2] = 0.0;
        }
      }
    };

    // Sub domain for Dirichlet boundary condition
    class DirichletBoundary : public dolfin::SubDomain
    {
      bool inside(const dolfin::Array<double>& x, bool /*on_boundary*/) const
      {
        const bool isLeft  = (x[0] < -0.7);
        const bool isRight = (x[0] >  0.7);
        const bool isDown  = (x[1] < -0.7);
        const bool isUp    = (x[1] >  0.7);

        const bool inCorner =
            (isLeft && isUp) ||
            (isLeft && isDown) ||
            (isRight && isUp) ||
            (isRight && isDown);


        return inCorner;
      }
    };

    // Surface representation class
    class FlatSurface : public narwal::ImplicitSurface
    {
    public:
      FlatSurface(double theta_, double shift_) :
        narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1),
        pi(DOLFIN_PI),
        theta(theta_*pi/180.0), shift(shift_),
        sin_theta(std::sin(theta)), cos_theta(std::cos(theta)) {}

      double f0(const dolfin::Point& p) const
      {
        const double y = (p.x()-shift)*sin_theta + (p.y()-shift)*cos_theta;
        return y;
      }

      double f1(const dolfin::Point& /*p*/) const
      {
        return -1.0;
      }

      /// Determine normal vector of surface pointing in plus direction
      std::vector<double> normal(const dolfin::Point /*p*/,
                                 double /*eps*/) const
      {
        std::vector<double> n(3);
        n[0] = 1.0*sin_theta;
        n[1] = 1.0*cos_theta;
        n[2] = 0.0;
        return n;
      }

    private:
      const double pi;
      const double theta, shift;
      const double sin_theta, cos_theta;
    };

    template<class FuncSpace>
    class ElasticityTestSetup
    {
    public:

      std::shared_ptr<narwal::DomainManager> domain_manager;
      std::shared_ptr<dolfin::FunctionSpace> V_s;
      std::shared_ptr<narwal::XFEMFunctionSpace> V;
      std::shared_ptr<narwal::XFEMForms> xfem_forms;
      std::shared_ptr<narwal::XFEMProblem> elasticity_problem;
      std::shared_ptr<dolfin::GenericMatrix> A;
      std::shared_ptr<dolfin::GenericVector> b;
      std::shared_ptr<dolfin::Function> u;

      ElasticityTestSetup(
        const std::string& testname,
        const int polynomialDegree,
        const int max_element_dim,
        const bool applyDBC = true)
      {
        // Create mesh
        auto mesh //= buildOneCellMesh();
          = std::make_shared<dolfin::BoxMesh>(dolfin::Point(-1.0, -1.0, -0.5),
                                              dolfin::Point(1.0, 1.0, 0.5),
                                              9, 9, 1);

        // Create fracture surfaces
        auto surface0 = std::make_shared<test::xamg_elasticity_ms::FlatSurface>(0.0, 0.02);
        auto surface1 = std::make_shared<test::xamg_elasticity_ms::FlatSurface>(90.0, 0.05);

        // Create DomainManager
        domain_manager = std::make_shared<narwal::DomainManager>(mesh);
        domain_manager->register_surface(surface0, "surface0");
        domain_manager->register_surface(surface1, "surface1");

        // Create standard DOLFIN function space
        V_s = std::make_shared<FuncSpace>(mesh);

        // Create XFEM space (this computes the enriched dofmap)
        V = std::make_shared<narwal::XFEMFunctionSpace>(V_s, domain_manager,
                                                        max_element_dim);

        // Elasticity parameters (as coefficients to allow spatial variation)
        const double E = 100.0;
        const double nu = 0.0;
        auto mu = std::make_shared<dolfin::Constant>(E/(2.0*(1.0 + nu)));
        auto lambda = std::make_shared<dolfin::Constant>(E*nu/((1.0 + nu)*(1.0 - 2.0*nu)));

        // Create pressure loading
        auto pressure = std::make_shared<const dolfin::Constant>(0.0);

        // Create XFEM elasticity forms
        xfem_forms = std::make_shared<narwal::ElasticityProblem3D>(V, lambda,
                                                                   mu, pressure,
                                                                   polynomialDegree);

        // Create Dirichlet boundary marker
        auto boundary_top_left = std::make_shared<TopLeft>();
        auto boundary_bottom_left = std::make_shared<BottomLeft>();
        auto boundary_top_right = std::make_shared<TopRight>();
        auto boundary_bottom_right = std::make_shared<BottomRight>();

        auto top_left_vector = std::make_shared<dolfin::Constant>(-1.0, 1.0, -1.0);
        auto bottom_left_vector = std::make_shared<dolfin::Constant>(-1.0, -1.0, 1.0);
        auto top_right_vector = std::make_shared<dolfin::Constant>(1.0, 1.0, 1.0);
        auto bottom_right_vector = std::make_shared<dolfin::Constant>(1.0, -1.0, -1.0);

        auto bc_top_left = std::make_shared<dolfin::DirichletBC>(V_s, top_left_vector,
                                                                 boundary_top_left);
        auto bc_bottom_left = std::make_shared<dolfin::DirichletBC>(V_s, bottom_left_vector,
                                                                    boundary_bottom_left);
        auto bc_top_right = std::make_shared<dolfin::DirichletBC>(V_s, top_right_vector,
                                                                  boundary_top_right);
        auto bc_bottom_right = std::make_shared<dolfin::DirichletBC>(V_s, bottom_right_vector,
                                                                     boundary_bottom_right);

        std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs;
//            = {bc_top_left, bc_bottom_left, bc_top_right, bc_bottom_right};

        // Create Dirichlet boundary conditions
//        auto db = std::make_shared<DirichletBoundary>();
//        auto dbv = std::make_shared<BoundaryValue>();
//        auto dbc = std::make_shared<dolfin::DirichletBC>(V_s, dbv, db);
//        bcs.push_back(dbc);
        bcs.push_back(bc_top_left);
        bcs.push_back(bc_bottom_left);
        bcs.push_back(bc_top_right);
        bcs.push_back(bc_bottom_right);
        if (!applyDBC) bcs.clear();

        // Create XFEM problem
        elasticity_problem = std::make_shared<narwal::XFEMProblem>(xfem_forms, bcs);

        // Assemble system
        A = narwal::la::initSystemMatrix();
        b = narwal::la::initSystemVector();
        elasticity_problem->assemble(*A, *b);
        narwal::la::toMatrixMarketFile(*A, testname + "_A");

        // Create solution Function
        u = std::make_shared<dolfin::Function>(V->function_space());
      }
    };
  }
}

#endif
