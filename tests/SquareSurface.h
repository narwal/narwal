#include <ImplicitSurface.h>
#include <dolfin/geometry/Point.h>


namespace test_surface
{
  // Flat surface with normal (0, 0, 1) and passing through (0.5, 0.5,
  // 0.5)
  class SquareSurface : public narwal::ImplicitSurface
  {
  public:
  SquareSurface(double r)
    : narwal::ImplicitSurface(dolfin::Point(0.5, 0.5, 0.51), 5.0*5), _r0(r) {}

    // f0 is normal distance from the crack surface. If f0 = 0, then
    // if f1<1 -> on surface and f1 >= = -> not on surface
    double f0(const dolfin::Point& p) const
    { return p.z() - 0.513; }

    double f1(const dolfin::Point& p) const
    {
      if (std::abs(p.x()-0.5) > _r0 or std::abs(p.y()-0.5) > _r0)
        return 1.0;
      else
        return -1.0;
    }

  private:

    const double _r0;
  };
}
