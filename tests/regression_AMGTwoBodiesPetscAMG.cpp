// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

// This program solves the equations of linearized elasticity on a
// 2 body problem using AMG.
// this mimics a completely cracked domain

#include "gtest_tools.h"

#include <iostream>
#include <dolfin.h>
#include "Narwal.h"
#include "ElasticityP1Tri.h"


namespace
{
  class LeftBoundary : public dolfin::SubDomain
  {
    bool inside(const dolfin::Array<double>& x, bool on_boundary) const
  { return x[0] < DOLFIN_EPS && on_boundary; }
  };

  static std::shared_ptr<dolfin::Mesh> buildTwoBodyMesh()
  {
    auto twoBodyMesh = std::make_shared<dolfin::Mesh>();

    dolfin::MeshEditor me;
    me.open(*twoBodyMesh, 2, 2);
    int n = 10;
    int numBody = 2;
    me.init_vertices(numBody*n*n);

    int index = 0;
    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
        me.add_vertex(index++, i, j);

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
        me.add_vertex(index++, i, j+2*n);

    me.init_cells(numBody*(n-1)*(n-1)*2);

    int eleIndex = 0;
    for (int i = 0; i < n-1; ++i)
    {
      for (int j = 0; j < n-1; ++j)
      {
        me.add_cell(eleIndex++, j + n*i, j+1+n*i  , j+n*(i+1));
        me.add_cell(eleIndex++, j+1+n*i, j+n*(i+1), j +1 + n*(i+1));
        me.add_cell(eleIndex++, j + n*i+n*n, j+1+n*i  +n*n, j+n*(i+1)     +n*n);
        me.add_cell(eleIndex++, j+1+n*i+n*n, j+n*(i+1)+n*n, j +1 + n*(i+1)+n*n);
      }
    }
    me.close();

    dolfin::File file("twoBodyMesh.pvd");
    file << *twoBodyMesh;

    return twoBodyMesh;
  }
}

TEST(amg, twoBodies_petsc_amg)
{
  // Create mesh
  auto mesh(buildTwoBodyMesh());

  // Function space
  auto V = std::make_shared<ElasticityP1Tri::FunctionSpace>(mesh);

  if (dolfin::MPI::rank(mesh->mpi_comm()) == 0)
    dolfin::info("Number of dofs: %d", V->dim());

  // Set up Dirichlet boundary conditions
  auto zero = std::make_shared<dolfin::Constant>(0.0, 0.0);
  auto left_boundary = std::make_shared<LeftBoundary>();
  auto bc = std::make_shared<dolfin::DirichletBC>(V, zero, left_boundary);

  // Set elasticity parameters
  const double E  = 2.2e+07;
  const double nu = 0.25;
  auto mu = std::make_shared<dolfin::Constant>(E/(2.0*(1.0 + nu)));
  auto lambda = std::make_shared<dolfin::Constant>(E*nu/((1.0 + nu)*(1.0 - 2.0*nu)));

  // Define variational problem
  ElasticityP1Tri::BilinearForm a(V, V);
  ElasticityP1Tri::LinearForm L(V);

  // Attach parameters and force functions
  a.mu = mu; a.lmbda = lambda;
  // Create right-hand side
  auto f = std::make_shared<dolfin::Constant>(0.0, -10.0);
  L.f = f;

  // Create system matrix and rhs vector
  dolfin::parameters["linear_algebra_backend"] = "PETSc";
  std::shared_ptr<dolfin::GenericMatrix> A = narwal::la::initSystemMatrix();
  std::shared_ptr<dolfin::GenericVector> b = narwal::la::initSystemVector();

  // Start assembly
  dolfin::assemble_system(*A, *b, a, L, {bc});

  // Create null space
  auto null_space = narwal::xamg::NullSpace2DDisplacement(V, b, mesh).build();

  // Create preconditioner and attach null space
  // use "petsc_amg" or "ml_amg"
  auto pc = std::make_shared<dolfin::PETScPreconditioner>("petsc_amg");
  pc->set_nullspace(*null_space);
  pc->parameters["report"] = false;

  // Create solver
  dolfin::PETScKrylovSolver solver("gmres", pc);
  solver.set_operator(A);
  solver.parameters["report"] = false;
  solver.parameters["monitor_convergence"] = true;
  solver.parameters["relative_tolerance"] = 1.0e-6;
  solver.parameters["convergence_norm_type"] = "true";

  // Solution function
  auto u = std::make_shared<dolfin::Function>(V);

  // Solve
  u->vector()->zero();
  const std::size_t num_iters = solver.solve(*u->vector(), *b);

  // Check iteration count
  EXPECT_LT(num_iters, std::size_t(12));
}
