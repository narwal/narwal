// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

// Unit tests for function in narwal::XFEMTools

#include "gtest_tools.h"

#include <vector>
#include <dolfin.h>
#include <Narwal.h>
#include "Elasticity3D_P1.h"
#include "SquareSurface.h"

namespace
{
  // Surface representation class
  class EllipticalSurface : public narwal::ImplicitSurface
  {
  public:
    EllipticalSurface(double h_, double a_, double b_, double theta_)
      : narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1), h(h_), a(a_),
        b(b_), theta(theta_) {}

    // Normal distance from the crack surface
    double f0(const dolfin::Point& p) const
    {
      const double y = std::sin(theta)*p.x() + std::cos(theta)*p.y();
      return (y - h);
    }

    // If f0 = 0, then if f1 < 1 -> on surface and f1 >= = -> not on surface
    double f1(const dolfin::Point& p) const
    {
      const double x = std::cos(theta)*p.x() - std::sin(theta)*p.y();
      const double z = p.z();
      const double d = (x/a)*(x/a) + (z/b)*(z/b);
      if (d < 1.0)
        return -1.0;
      else
        return 1.0;
    }

    // Surface height position (average y)
    const double h;

    // Ellipse major and minor radii
    const double a, b;

    // Rotate axis
    const double theta;
  };

  // Return a domain manager object
  std::shared_ptr<narwal::DomainManager>
  create_domain_manager(std::size_t n, double r)
  {
    std::shared_ptr<dolfin::Mesh> mesh(new dolfin::UnitCubeMesh(n, n, n));
    std::shared_ptr<test_surface::SquareSurface> surface(new test_surface::SquareSurface(r));
    auto domain_manager = std::make_shared<narwal::DomainManager>(mesh);
    domain_manager->register_surface(surface, "surface0");
    return domain_manager;
  }

  // Mesh/surface data: (mesh edges each dir, surface square half-edge
  // length). This list includes full intersections and partial
  // intersections.
  static std::vector<std::pair<std::size_t, double>> test_data
    = {{1, 10.0}, {2, 10.0}, {3, 10.0}, {4, 10.0}, {9, 10.0}, {12, 10.0},
       {1, 0.201}, {10, 0.201}, {15, 0.235}, {20, 0.201}};
}


// Test XFEMTools::compute_enriched_dof_markers
TEST(XFEMTools, compute_enriched_dof_markers_simple_cube)
{
  // Run tests for range of mesh sizes and intersection types
  for (auto data : ::test_data)
  {
    // Test data
    const int n = data.first;
    const double dx = data.second;

    // Compute number of fully intersected 'edges' in each direction
    const double delta = 1.0/n;
    const int num_edges = std::min(n, static_cast<int>(2.0*dx/delta));

    auto dm = ::create_domain_manager(n, dx);
    dolfin_assert(dm);

    // Create standard DOLFIN function space
    std::shared_ptr<dolfin::FunctionSpace>
      V(new Elasticity3D_P1::FunctionSpace(dm->mesh()));

    // Compute 'dof' markers
    std::vector<std::set<narwal::EnrLabelArray>> markers;
    std::vector<std::set<narwal::EnrLabelArray>> enriched_cells;
    narwal::XFEMTools::compute_enriched_dof_markers(markers, enriched_cells,
                                                    *V, *dm);

    // Count enriched cells
    int cell_counter = 0;
    for (auto cell : enriched_cells)
    {
      if (!cell.empty())
        ++cell_counter;
    }

    // Count enriched dofs
    int counter = 0;
    for (auto marker : markers)
      counter += marker.size();

    // Test dof marker counts
    if (n == num_edges)
    {
      EXPECT_EQ(6*n*n, static_cast<int>(cell_counter));
      EXPECT_EQ(2*3*(n+1)*(n+1), static_cast<int>(counter));
    }
    else
    {
      const int nv = std::max(0, num_edges - 1);
      EXPECT_EQ(std::max(6*num_edges*num_edges - 6, 0), static_cast<int>(cell_counter));
      EXPECT_EQ(2*3*nv*nv, static_cast<int>(counter));
    }
  }
}


// Test XFEMTools::compute_enriched_dof_markers
TEST(XFEMTools, compute_enriched_dof_markers_intersecting)
{
   // Create mesh
  std::shared_ptr<dolfin::Mesh>
    mesh(new dolfin::BoxMesh(dolfin::Point(-0.4, -0.2, -0.4),
                             dolfin::Point(0.4, 0.2, 0.4),
                             15, 15, 15));

  // Create fracture surfaces
  std::shared_ptr<::EllipticalSurface>
    surface0(new ::EllipticalSurface(0.03, 0.3, 0.3, 0.0));
  std::shared_ptr<::EllipticalSurface>
    surface1(new ::EllipticalSurface(0.03, 0.18, 0.18,
                                                   DOLFIN_PI/2.0));

  // Create DomainManager
  auto domain_manager = std::make_shared<narwal::DomainManager>(mesh);
  domain_manager->register_surface(surface0, "surface0");
  domain_manager->register_surface(surface1, "surface1");

  // Create standard DOLFIN function space
  std::shared_ptr<dolfin::FunctionSpace>
    V_s(new Elasticity3D_P1::FunctionSpace(mesh));

  std::vector<std::set<narwal::EnrLabelArray>> markers;
  std::vector<std::set<narwal::EnrLabelArray>> enriched_cells;
  narwal::XFEMTools::compute_enriched_dof_markers(markers, enriched_cells,
                                                  *V_s, *domain_manager);

  int counter0 = 0.0;
  int counter1 = 0.0;
  int counter2 = 0.0;
  int counter3 = 0.0;
  for (auto m : markers)
  {
    ASSERT_TRUE(m.size() < 4);
    if (m.empty())
      ++counter0;
    else if (m.size() == 1)
      ++counter1;
    else if (m.size() == 2)
      ++counter2;
    else if (m.size() == 3)
      ++counter3;
  }

  // FIXME: check these by hand
  EXPECT_EQ(11658, counter0);
  EXPECT_EQ(576,   counter1);
  EXPECT_EQ(15,    counter2);
  EXPECT_EQ(39,    counter3);

  std::size_t count = counter0 + counter1 + counter2 + counter3;
  EXPECT_EQ(V_s->dofmap()->global_dimension(), count);
}
