// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include<iostream>
#include <set>
#include "gtest_tools.h"

#include <dolfin.h>
#include <Narwal.h>

namespace
{
  // Surface representation class
  class EllipticalSurface : public narwal::ImplicitSurface
  {
  public:
    EllipticalSurface() :
      narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1),
      a(0.4), b(0.3), beta(0.05) {}

    double f0(const dolfin::Point& p) const
    {
      const double x = p.x();
      const double y = p.y();
      const double z = p.z();
      const double d = (x/a)*(x/a) + (z/b)*(z/b) - 1.0;
      return (y - beta*d);
    }

    double f1(const dolfin::Point& p) const
    {
      const double x = p.x();
      //const double y = p.y();
      const double z = p.z();
      const double d = (x/a)*(x/a) + (z/b)*(z/b);
      if (d < 1.0) // &&  std::abs(y - beta*(d - 1.0)) < 0.002 )
        return -1.0;
      else
        return 1.0;
    }
    const double a, b, beta;
  };
}

// FIXME: restructure tests such that each function tests few (ideally
//        just one) member function
// FIXME: Move tests to file to holds tests for relevant class


TEST(SubCellVolume, volume_test)
{
  // Create box domain mesh
  std::shared_ptr<dolfin::Mesh>
    mesh(new dolfin::BoxMesh(dolfin::Point(-0.5, -0.1, -0.5),
                             dolfin::Point(0.5, 0.1, 0.5), 64, 16, 64));

  // Create fracture surface
  std::shared_ptr<::EllipticalSurface>
    surface(new ::EllipticalSurface());

  // Create domain manager
  auto domain_manager = std::make_shared<narwal::DomainManager>(mesh);
  domain_manager->register_surface(surface, "surface0");

  // Compute CellFunction of intersected cells
  const dolfin::CellFunction<bool> intersected_cells
    = narwal::GeometryTools::compute_intersected_cells(mesh, *surface);

  // Test number of intersected cells
  int tot_cells = 0;
  for(dolfin::CellIterator c(*mesh); !c.end(); ++c)
    if(intersected_cells[*c])
      tot_cells++;
  EXPECT_EQ(10472, tot_cells);

  // FIXME: comment what the below test is
  unsigned int count = 0;
  dolfin::CellFunction<std::size_t> boundary_cells(mesh);
  for(dolfin::CellIterator c(*mesh); !c.end(); ++c)
  {
    const std::pair<bool, bool> status
      = surface->entity_intersection(*c);
    boundary_cells[*c] = 2*(status.first) + status.second;
    if(status.first)
    {
      count++;
      if(count % 10 == 0)
      {
        const narwal::SubTriangulation tri(*c, *surface);
        const dolfin::Mesh& tri_mesh = tri.mesh();

        double vol_tot = 0.0;
        for(dolfin::CellIterator cc(tri_mesh); !cc.end(); ++cc)
          vol_tot += cc->volume();

        const double volume_diff = std::abs(c->volume() - vol_tot);
        ASSERT_LT(volume_diff, DOLFIN_EPS);
      }
    }
  }

  std::vector<int> cell_count(4);
  for(dolfin::CellIterator c(*mesh); !c.end(); ++c)
    cell_count[boundary_cells[*c]]++;

  std::cout << "cells = ";
  for(unsigned int i = 0; i < 4; ++i)
    std::cout << cell_count[i] << " ";
  std::cout << std::endl;

  EXPECT_EQ(365104, cell_count[0]);
  EXPECT_EQ(17154, cell_count[1]);
  EXPECT_EQ(10472, cell_count[2]);
  EXPECT_EQ(486, cell_count[3]);
}


// FIXME: Move to XFEMTools and rename
TEST(SubCellVolume, VolumeTestTiny1)
{
  // Create box domain mesh
  std::shared_ptr<dolfin::Mesh>
    mesh(new dolfin::BoxMesh(dolfin::Point(-0.1, -0.1, -0.1),
                             dolfin::Point(0.1, 0.1, 0.1), 1, 1, 1));

  // Create fracture surface and DomainManager
  std::shared_ptr<::EllipticalSurface>
    surface(new ::EllipticalSurface());
  auto domain_manager = std::make_shared<narwal::DomainManager>(mesh);
  domain_manager->register_surface(surface, "surface0");

  // Compute CellFunction of intersected cells
  const dolfin::CellFunction<bool> intersected_cells
    = narwal::GeometryTools::compute_intersected_cells(mesh, *surface);

  int tot_cells = 0;
  for(dolfin::CellIterator c(*mesh); !c.end(); ++c)
    if(intersected_cells[*c])
      tot_cells++;

  // Test number of full intersected cells
  EXPECT_EQ(6, tot_cells);
}
