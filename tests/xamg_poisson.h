// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//         Axel Gerstenberger
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_TEST_XAMG_POISSON_H
#define NARWAL_TEST_XAMG_POISSON_H

#include <set>
#include <dolfin.h>
#include <Narwal.h>
#include <PoissonProblem3D.h>

namespace test
{
  namespace xamg_poisson
  {

    inline std::shared_ptr<dolfin::Mesh> createTestBoxMesh()
    {
      auto mesh = std::make_shared<dolfin::BoxMesh>(
        dolfin::Point(0.0, -0.3, -0.1), dolfin::Point(0.1, 0.3, 0.1),
        21, 21, 21);

      dolfin::cout << "Num. mesh vertices: " << mesh->num_vertices()
                   << dolfin::endl;

      return mesh;
    }

    // Dirichlet boundary condition function
    class BoundaryValue : public dolfin::Expression
    {
      void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
      {
        if (x[1] < 0.0)
          values[0] = 0.0;
        else
          values[0] = 1.0;
      }
    };

    // Sub domain for Dirichlet boundary condition
    class DirichletBoundary : public dolfin::SubDomain
    {
      bool inside(const dolfin::Array<double>& x, bool /*on_boundary*/) const
      {
        return std::abs(x[1] + 0.3) < DOLFIN_EPS
          || std::abs(x[1] - 0.3) < DOLFIN_EPS;
      }
    };

    // Surface representation class
    class EllipticalSurface : public narwal::ImplicitSurface
    {
    public:
    EllipticalSurface() :
      narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1), a(0.3),
        b(0.3), beta(0.05) {}
      explicit EllipticalSurface(const dolfin::Point& pt)
        : narwal::ImplicitSurface(pt, 3.1), a(0.3), b(0.3), beta(0.05) {}

      double f0(const dolfin::Point& p) const
      {
        const double y = p.y();
        return y;
      }

      // If f0 = 0, then if f1<1 -> on surface and f1 >= = -> not on surface
      double f1(const dolfin::Point& p) const
      {
        const double x = p.x();
        //const double y = p.y();
        const double z = p.z();
        const double d = (x/a)*(x/a) + (z/b)*(z/b);
        if (d < 1.0)
          return -1.0;
        else
          return 1.0;
      }

      // Ellipse major and minor radii
      const double a, b;

      // Ellipse curvature
      const double beta;
    };

    template<class FuncSpace>
    class PoissonTestSetup
    {
    public:
      std::shared_ptr<narwal::DomainManager> domainManager;
      std::shared_ptr<dolfin::FunctionSpace> V_s;
      std::shared_ptr<narwal::XFEMFunctionSpace> V;
      std::shared_ptr<narwal::XFEMForms> xfem_forms;
      std::shared_ptr<narwal::XFEMProblem> problem;
      std::shared_ptr<dolfin::GenericMatrix> A;
      std::shared_ptr<dolfin::GenericVector> b;
      std::shared_ptr<dolfin::Function> u;

      PoissonTestSetup(
        const std::string& testname,
        const int polynomialDegree,
        const int max_element_dim
        )
      {
        // Mesh that contains surface
        auto mesh = createTestBoxMesh();

        // Create fracture surface
        auto surface = std::make_shared<EllipticalSurface>();

        // Create DomainManager
        domainManager = std::make_shared<narwal::DomainManager>(mesh);
        domainManager->register_surface(surface, "surface0");

        // Create standard DOLFIN function space
        V_s = std::make_shared<FuncSpace>(mesh);

        // Create XFEM space holder (this computes the enriched dofmap)
        V = std::make_shared<narwal::XFEMFunctionSpace>(V_s, domainManager,
                                                        max_element_dim);

        // Create XFEM elasticity forms
        xfem_forms = std::make_shared<narwal::PoissonProblem3D>(V, polynomialDegree);

        // Create Dirichlet boundary conditions
        auto db = std::make_shared<test::xamg_poisson::DirichletBoundary>();
        auto dbv = std::make_shared<test::xamg_poisson::BoundaryValue>();
        auto dbc = std::make_shared<dolfin::DirichletBC>(V_s, dbv, db);
        std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs;
        bcs.push_back(dbc);

        // Create XFEM problem
        problem = std::make_shared<narwal::XFEMProblem>(xfem_forms, bcs);

        // Assemble system
        A = narwal::la::initSystemMatrix();
        b = narwal::la::initSystemVector();
        problem->assemble(*A, *b);
        narwal::la::toMatrixMarketFile(*A, testname + "_A");

        // Create solution Function
        u = std::make_shared<dolfin::Function>(V->function_space());
      }
    };

  }
}

#endif
