// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

// This program solves the equations of linearized elasticity on a
// unit cube using AMG.

#include "gtest_tools.h"
#include <iostream>
#include <dolfin.h>
#include "Narwal.h"
#include "Elasticity3D_P1.h"

namespace
{
  class LeftBoundary : public dolfin::SubDomain
  {
    bool inside(const dolfin::Array<double>& x, bool on_boundary) const
    { return x[0] < DOLFIN_EPS && on_boundary; }
  };
}

TEST(amg, DISABLED_demo_petsc_ml)
{
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  // Create mesh
  auto mesh = std::make_shared<dolfin::UnitCubeMesh>(16, 16, 16);

  // Function space
  auto V = std::make_shared<Elasticity3D_P1::FunctionSpace>(mesh);

  if (dolfin::MPI::rank(mesh->mpi_comm()) == 0)
    dolfin::info("Number of dofs: %d", V->dim());

  // Set up Dirichlet boundary conditions
  auto zero = std::make_shared<dolfin::Constant>(0.0, 0.0, 0.0);
  auto left_boundary = std::make_shared<LeftBoundary>();
  auto bc = std::make_shared<dolfin::DirichletBC>(V, zero, left_boundary);

  // Set elasticity parameters
  const double E  = 2.2e+07;
  const double nu = 0.25;
  auto mu = std::make_shared<dolfin::Constant>(E/(2.0*(1.0 + nu)));
  auto lambda = std::make_shared<dolfin::Constant>(E*nu/((1.0 + nu)*(1.0 - 2.0*nu)));

  // Define variational problem
  Elasticity3D_P1::BilinearForm a(V, V);
  Elasticity3D_P1::LinearForm L(V);

  // Attach parameters and force functions
  a.mu = mu; a.lmbda = lambda;
  // Create right-hand side
  auto f = std::make_shared<dolfin::Constant>(0.0, -10.0, 0.0);
  L.f = f;

  // Create system matrix and rhs vector
  std::shared_ptr<dolfin::GenericMatrix> A = narwal::la::initSystemMatrix();
  std::shared_ptr<dolfin::GenericVector> b = narwal::la::initSystemVector();

  // Start assembly
  std::cout << "assemble" << std::endl;
  dolfin::assemble_system(*A, *b, a, L, {bc});

  // Create null space
  std::cout << "nullspace" << std::endl;
  auto null_space = narwal::xamg::NullSpace3DDisplacement(V, b, mesh).build();

  // Create preconditioner and attach null space
  std::cout << "preconditioner" << std::endl;
#if PETSC_HAVE_ML
#else
  dolfin::warning("narwal::la::SolverXAMG::setupDefaultPreCondAndSolver()");
  dolfin::warning("  Requesting ml_amg, but PETSc compiled without ML!");
#endif
  auto pc = std::make_shared<dolfin::PETScPreconditioner>("ml_amg"); // use "petsc_amg" or "ml_amg"
  pc->set_nullspace(*null_space);
  pc->parameters["report"] = false;
  //pc.parameters("ml")["print_level"] = 0;
  //pc.parameters("ml")["max_coarse_size"] = 1024;
  //pc.parameters("ml")["max_num_levels"] = 3;
  //pc.parameters("ml")["aggregation_damping_factor"] = 0.0;
  //pc.parameters("ml")["threshold"] = 0.1;
  //pc.parameters("ml")["energy_minimization"] = 3;
  //pc.parameters("ml")["repartition"] = true;
  //pc.parameters("ml")["repartition_type"] = "ParMETIS";
  //pc.parameters("ml")["zoltan_repartition_scheme"] = "fast_hypergraph";

  // Create solver
  std::cout << "solver" << std::endl;
  dolfin::PETScKrylovSolver solver("gmres", pc);
  solver.set_operator(A);
  solver.parameters["report"] = false;
  solver.parameters["monitor_convergence"] = true;
  solver.parameters["relative_tolerance"] = 1.0e-6;
  solver.parameters["convergence_norm_type"] = "true";

  // Create solution function
  auto u = std::make_shared<dolfin::Function>(V);

  // Solve
  u->vector()->zero();
  std::cout << "solve" << std::endl;
  const std::size_t num_iters = solver.solve(*u->vector(), *b);

  // Check iteration count
  EXPECT_LT(num_iters, std::size_t(30));
}
