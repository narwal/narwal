// Copyright (C) 2016 Melior Innovations
// Author: Axel Gerstenberger
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "gtest_tools.h"

#include <dolfin.h>
#include <Elasticity3D_P2.h>
#include <ElasticityFunction.h>
#include <ElasticityProblem3D.h>
#include <Narwal.h>
#include <Evaluator.h>
#include "xamg_elasticity_multisurface.h"


namespace
{
  const double solution_l2 = 79.202272694666291;

  typedef Elasticity3D_P2::FunctionSpace FuncSpace;
  static const int polynomialDegree = 2;
  static const int numNode = 10;
  static const int numField = 3;
  static const int numEnr = 4;
  static const int max_element_dim = numField*numNode*numEnr;

}

TEST(Evaluator, evaluateMultipleSurfaceP2)
{
  dolfin::parameters["allow_extrapolation"] = true;
  dolfin::parameters["refinement_algorithm"] = "plaza_with_parent_facets";
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  test::xamg_elasticity_ms::ElasticityTestSetup<FuncSpace>
    s("output/" + gtest_nameroot(), polynomialDegree, max_element_dim);

  dolfin::LUSolver solver;
  solver.solve(*s.A, *s.u->vector(), *s.b);

  ASSERT_NEAR(solution_l2, s.u->vector()->norm("l2"), 1.0e-8);

  // Write (nodal) solution to PVD file to visualise with ParaView
  dolfin::XDMFFile file_u(MPI_COMM_WORLD, "output/"+gtest_nameroot()+"-u.xdmf");
  file_u.write(*s.u);

//  narwal::ElasticFunction::write_crack_conforming_solution<FuncSpace>(
//    *s.domain_manager, *s.xfem_forms, *s.u, gtest_nameroot());

//  dolfin::XMLMesh surface_mesh("/home/axel/Downloads/PlanarCircleY_gmshisosurface (2).xml");
//  dolfin::Mesh surface_mesh("data/PlanarCircleY_gmshisosurface.xml");
//  dolfin::XDMFFile file1(surface_mesh.mpi_comm(), "output/"+gtest_nameroot()+"-surface_mesh.xdmf");
//  file1 << surface_mesh;


  // test Function/Evaluator::eval(...) interface
  { // test for being outside of domain
    narwal::Evaluator evaluator(s.u, s.V->dofmap(), s.domain_manager, s.xfem_forms);
    dolfin::Array<double> x(3);
    x[0] = 100.0; x[1] = -100.0; x[2] = 100.0;
    dolfin::Array<double> values(3);
    ASSERT_ANY_THROW(evaluator.eval(values, x));
  }

  { // check away from enriched elements, upper left part
    narwal::Evaluator evaluator(s.u, s.V->dofmap(), s.domain_manager, s.xfem_forms);
    dolfin::Array<double> x(3);
    x[0] = -0.9; x[1] = 0.9; x[2] = 0.0; // near the top
    dolfin::Array<double> values(3);
    evaluator.eval(values, x);
    EXPECT_NEAR(-1.0, values[0], 1.0e-8);
    EXPECT_NEAR( 1.0, values[1], 1.0e-8);
    EXPECT_NEAR(-1.0, values[2], 1.0e-8);
  }

  { // check away from enriched elements, upper right part
    narwal::Evaluator evaluator(s.u, s.V->dofmap(), s.domain_manager, s.xfem_forms);
    dolfin::Array<double> x(3);
    x[0] =  0.9; x[1] = 0.9; x[2] = 0.0; // near the top
    dolfin::Array<double> values(3);
    evaluator.eval(values, x);
    EXPECT_NEAR(1.0, values[0], 1.0e-8);
    EXPECT_NEAR(1.0, values[1], 1.0e-8);
    EXPECT_NEAR(1.0, values[2], 1.0e-8);
  }

  { // check in enriched element, upper left part, away from crack intersection
    narwal::Evaluator evaluator(s.u, s.V->dofmap(), s.domain_manager, s.xfem_forms);
    dolfin::Array<double> x(3);
    x[0] = 0.01; x[1] = 0.9; x[2] = 0.0; // right above the crack
    dolfin::Array<double> values(3);
    evaluator.eval(values, x);
    EXPECT_NEAR(-1.0, values[0], 1.0e-8) << values;
    EXPECT_NEAR( 1.0, values[1], 1.0e-8) << values;
    EXPECT_NEAR(-1.0, values[2], 1.0e-8) << values;
  }

  const double xcrackshiftSurface2 = 0.05;
  { // check in enriched element, lower right part, close to intersection
    narwal::Evaluator evaluator(s.u, s.V->dofmap(), s.domain_manager, s.xfem_forms);
    dolfin::Array<double> x(3);
    x[0] = xcrackshiftSurface2+0.1; x[1] = -0.03; x[2] = 0.0; // right below the crack
    dolfin::Array<double> values(3);
    evaluator.eval(values, x);
    EXPECT_NEAR( 1.0, values[0], 1.0e-8);
    EXPECT_NEAR(-1.0, values[1], 1.0e-8);
    EXPECT_NEAR(-1.0, values[2], 1.0e-8);
  }

  // test evaluation interface gaps

  { // evaluate gap on surface 2 (x-z plane)
    narwal::Evaluator evaluator(s.u, s.V->dofmap(), s.domain_manager, s.xfem_forms);
    dolfin::Array<double> x(3);
    x[0] = xcrackshiftSurface2; x[1] = 0.9; x[2] = 0.1; // right on the crack
    dolfin::Array<double> values(3);
    evaluator.evaluate_discontinuity(values, x, narwal::Evaluator::interface_full, 1);
    EXPECT_NEAR( 2.0, values[0], 1.0e-8);
    EXPECT_NEAR( 0.0, values[1], 1.0e-8);
    EXPECT_NEAR( 2.0, values[2], 1.0e-8);
  }

  { // evaluate gap on surface 2 (x-z plane)
    narwal::Evaluator evaluator(s.u, s.V->dofmap(), s.domain_manager, s.xfem_forms);
    dolfin::Array<double> x(3);
    x[0] = xcrackshiftSurface2; x[1] = 0.9; x[2] = 0.1; // right on the crack
    dolfin::Array<double> values(3);
    evaluator.evaluate_discontinuity(values, x, narwal::Evaluator::interface_normal, 1);
    EXPECT_NEAR( 2.0, values[0], 1.0e-8);
    EXPECT_NEAR( 0.0, values[1], 1.0e-8);
    EXPECT_NEAR( 0.0, values[2], 1.0e-8);
  }

  { // evaluate gap with point 0n intersection
    narwal::Evaluator evaluator(s.u, s.V->dofmap(), s.domain_manager, s.xfem_forms);
    dolfin::Array<double> x(3);
    x[0] = xcrackshiftSurface2; x[1] = 0.0; x[2] = 0.1; // right on the crack
    dolfin::Array<double> values(3);
    evaluator.evaluate_discontinuity(values, x, narwal::Evaluator::interface_normal, 0); // (x-z plane)
    EXPECT_NEAR( 0.0, values[0], 1.0e-8);
    EXPECT_NEAR( 2.0, values[1], 1.0e-8);
    EXPECT_NEAR( 0.0, values[2], 1.0e-8);
  }

  { // evaluate gap with point 0n intersection
    narwal::Evaluator evaluator(s.u, s.V->dofmap(), s.domain_manager, s.xfem_forms);
    dolfin::Array<double> x(3);
    x[0] = xcrackshiftSurface2; x[1] = 0.0; x[2] = 0.1; // right on the crack
    dolfin::Array<double> values(3);
    evaluator.evaluate_discontinuity(values, x, narwal::Evaluator::interface_normal, 1); // (y-z plane)
    EXPECT_NEAR( 2.0, values[0], 1.0e-8);
    EXPECT_NEAR( 0.0, values[1], 1.0e-8);
    EXPECT_NEAR( 0.0, values[2], 1.0e-8);
  }


  // TODO: test for point not near surface that was indicated
  // --> should fail gracefully


  { // evaluate gap with mesh TODO: add appropriate mesh
//    narwal::Evaluator evaluator(s.u, s.V->dofmap(), s.domain_manager, s.xfem_forms);
//    const std::unique_ptr<dolfin::Function> gap_function =
//        evaluator.evaluate_discontinuity(surface_mesh, narwal::Evaluator::interface_full, 0);
//  ASSERT_NEAR(1.0, gap_function->vector()->max(), 1.0e-8);
//  ASSERT_NEAR(1.0, gap_function->vector()->min(), 1.0e-8);
  }


}
