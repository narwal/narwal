// Copyright (C) 2016 Melior Innovations
// Author: David Bernstein
//
// This file is part of Narwal.
//
// NARWAL is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// NARWAL is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with NARWAL. If not, see <http://www.gnu.org/licenses/>.

// Notes:
// All units SI in this test

#include "gtest_tools.h"

#include <dolfin.h>
#include <Elasticity3D_P1.h>
#include <ElasticityFunction.h>
#include <ElasticityProblem3D.h>
#include <Narwal.h>


// Sub domain for Dirichlet boundary condition top and bottom faces
// are clamped
namespace
{
  class DirichletBoundary244 : public dolfin::SubDomain
  {
    bool inside(const dolfin::Array<double>& x, bool /*on_boundary*/) const
    {
      double cubeX = 122.0;
      return (std::abs(x[2] + cubeX) < DOLFIN_EPS)  // bottom
        || (std::abs(x[2] - cubeX) < DOLFIN_EPS);  // top
    }
  };

  class DirichletBoundaryThin : public dolfin::SubDomain
  {
    bool inside(const dolfin::Array<double>& x, bool /*on_boundary*/) const
    {
      bool in_z_range = x[2] < -60;
      bool in_y_range = x[1] < -60;

      return in_z_range && in_y_range ;
    }
  };

  class PlanarCircleNormalX : public narwal::ImplicitSurface
  {
  public:
    PlanarCircleNormalX(const double circleXShift_)
      : narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1),
        r(50.0), circleXShift(circleXShift_)
    {}

    // normal distance from the crack surface
    double f0(const dolfin::Point& p) const
    { return p.x() - circleXShift; }

    // if f0 = 0, then if f1<1 -> on surface and f1 >= = -> not on surface
    double f1(const dolfin::Point& p) const
    {
      const double y = p.y();
      const double z = p.z();
      const double d = (y/r)*(y/r) + (z/r)*(z/r);
      if (d < 1.0)
        return -1.0;
      else
        return 1.0;
    }

    bool IntersectsCell(const dolfin::Cell &cell) const
    {
      std::vector<double> coord;
      cell.get_vertex_coordinates(coord);
      dolfin::Point p0(coord[0], coord[1], coord[2]);
      dolfin::Point p1(coord[3], coord[4], coord[5]);
      dolfin::Point p2(coord[6], coord[7], coord[8]);
      dolfin::Point p3(coord[9], coord[10], coord[11]);

      double sum = copysign(1.0, f0(p0));
      sum += copysign(1.0, f0(p1));
      sum += copysign(1.0, f0(p2));
      sum += copysign(1.0, f0(p3));

      // if cell lies entirely on one side of the surface the sum will
      // be +/-4
      bool intersectsSurface = std::abs(sum) < 3.5;
      if (intersectsSurface)
      {
        // return true if any node lies inside of tip
        sum = copysign(1.0, f1(p0));
        sum += copysign(1.0, f1(p1));
        sum += copysign(1.0, f1(p2));
        sum += copysign(1.0, f1(p3));

        // return true if any node lies inside of tip
        return sum < 3.5;
      }
      else
        return false;
    }

    // radius
    const double r;

    const double circleXShift;
  };


  void RefineMeshAroundSurface(std::shared_ptr<dolfin::Mesh> &pMesh,
                               const std::shared_ptr<const PlanarCircleNormalX> pSurface)
  {
    dolfin::CellFunction<bool> f(pMesh, false);

    // mark cells for refinement if they intersect surface
    for (dolfin::CellIterator c(*pMesh); !c.end(); ++c)
      f[c->index()] = pSurface->IntersectsCell(*c);

    pMesh = std::make_shared<dolfin::Mesh>(dolfin::refine(*pMesh, f));
  }

  void RefineMeshInVolume(std::shared_ptr<dolfin::Mesh> &pMesh,
                          const dolfin::Point pMin,
                          const dolfin::Point pMax)
  {
    dolfin::CellFunction<bool> f(pMesh, false);

    // mark cells for refinement if they intersect surface
    for (dolfin::CellIterator c(*pMesh); !c.end(); ++c)
    {
      std::vector<double> coords;
      c->get_vertex_coordinates(coords);
      for (std::size_t i = 0; i < c->num_vertices(); ++i)
      {
        const bool inVolume =
          (pMin.x() < coords[i*3  ])    && (coords[i*3  ] < pMax.x())
          && (pMin.y() < coords[i*3+1]) && (coords[i*3+1] < pMax.y())
          && (pMin.z() < coords[i*3+2]) && (coords[i*3+2] < pMax.z());

        if (inVolume)
        {
          f[c->index()] = true;
          break;
        }
      }
    }

    pMesh = std::make_shared<dolfin::Mesh>(dolfin::refine(*pMesh, f));
  }
}

//----------------------------------------------------------------------------//
TEST(Elasticity, CircularCrackGeneralRefinement)
{
  dolfin::parameters["allow_extrapolation"] = true;
  dolfin::parameters["refinement_algorithm"] = "plaza_with_parent_facets";

  // assumes executable is in tests/build and mesh is in tests/data
  // this is an axis aligned cube centered at the origin with a side
  // length of 244 meters
  auto pMesh = std::make_shared<dolfin::Mesh>("data/small_mesh_thin_X.xml");
  pMesh = std::make_shared<dolfin::Mesh>(dolfin::refine(*pMesh));

  // surface
  auto pSurface = std::make_shared<PlanarCircleNormalX>(0.0);

  // refine mesh in a corner to apply Dirichlet conditions
  RefineMeshInVolume(pMesh,
    dolfin::Point(-1000, -1000, -1000),
    dolfin::Point( 1000,   -70,   -70));

  // domain manager
  auto pDomainManager = std::make_shared<narwal::DomainManager>(pMesh);
  pDomainManager->register_surface(pSurface, "circle");

  // Create standard DOLFIN function space
  auto pV = std::make_shared<Elasticity3D_P1::FunctionSpace>(pMesh);

  // Create XFEM space holder (this computes the enriched dofmap)
  auto V = std::make_shared<narwal::XFEMFunctionSpace>(pV, pDomainManager, 24);

  // Elasticity parameters (as coefficients to allow spatial variation)
  const double E = 20.0e9;
  const double nu = 0.1;
  auto pMu = std::make_shared<dolfin::Constant>(E/(2.0*(1.0 + nu)));
  auto pLambda
    = std::make_shared<dolfin::Constant>(E*nu/((1.0 + nu)*(1.0 - 2.0*nu)));

  // Crack pressure function
  double pressure = 2.0e6;
  auto pCrackPressure = std::make_shared<dolfin::Constant>(-pressure);

  // P1 XFEM forms
  auto xfem_forms = std::make_shared<narwal::ElasticityProblem3D>
    (V, pLambda, pMu, pCrackPressure, 1);

  // boundary conditions
  auto clamp = std::make_shared<dolfin::Constant>(0, 0, 0);
  auto bnd = std::make_shared<DirichletBoundaryThin>();
  auto pBC = std::make_shared<const dolfin::DirichletBC>(pV, clamp, bnd);
  std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs;
  bcs.push_back(pBC);

  // Create XFEM problem
  auto problem = std::make_shared<narwal::XFEMProblem>(xfem_forms, bcs);

  // Assemble system
  std::shared_ptr<dolfin::GenericMatrix> A = narwal::la::initSystemMatrix();
  std::shared_ptr<dolfin::GenericVector> b = narwal::la::initSystemVector();
  problem->assemble(*A, *b);

  auto dofmap = V->function_space()->dofmap();
  auto tdofmap = std::dynamic_pointer_cast<const narwal::DofMap>(dofmap);

  auto u = std::make_shared<dolfin::Function>(V->function_space());

  // Create system matrix and rhs vector
  narwal::la::SolverXAMG xSolver(A, b, u, V->function_space(), pDomainManager,
    tdofmap, xfem_forms, true, false);

  // Create null space  --> create function object for this
  xSolver.null_space
    = narwal::xamg::NullSpace3DDisplacement2(V->function_space(), tdofmap, b,
                                             pMesh).build();

  // Create preconditioner & solver
  xSolver.setupDefaultPreCondAndSolver("petsc_amg");

  // solve
  std::cout << "system size = " << u->vector()->size() << std::endl;
  const std::size_t num_iters = xSolver.solve();
  std::cout << "Max vector entry = " << u->vector()->max() << std::endl;

  EXPECT_LT(num_iters, std::size_t(20));
}

TEST(Elasticity, CircularCrackIrregularMesh244RefineAroundSurface)
{
  narwal::TimeMonitor::instance().start();

  dolfin::parameters["allow_extrapolation"] = true;
  dolfin::parameters["refinement_algorithm"] = "plaza_with_parent_facets";

  dolfin::MeshQuality quality;

  // assumes executable is in tests/build and mesh is in tests/data
  // this is an axis aligned cube centered at the origin with a side
  // length of 244 meters
  auto pMesh = std::make_shared<dolfin::Mesh>("data/cube_mesh_side_244.xml");

  // report quality of mesh
  std::pair<double, double> stats = quality.radius_ratio_min_max(*pMesh);
  std::cout << "mesh min/max cell quality: " << stats.first << ", "
            << stats.second << std::endl;

  // surface
  auto pSurface = std::make_shared<PlanarCircleNormalX>(0.1);

  // refine mesh near surface
  RefineMeshInVolume(pMesh,
    dolfin::Point(-1000, -1000, -1000),
    dolfin::Point( 1000, -1000,  -120));

  RefineMeshAroundSurface(pMesh, pSurface);
  RefineMeshAroundSurface(pMesh, pSurface);
  RefineMeshAroundSurface(pMesh, pSurface);
  stats = quality.radius_ratio_min_max(*pMesh);
  std::cout << "mesh min/max cell quality: " << stats.first << ", "
            << stats.second << std::endl;

  // domain manager
  auto pDomainManager = std::make_shared<narwal::DomainManager>(pMesh);
  pDomainManager->register_surface(pSurface, "circle");

  // Create standard DOLFIN function space
  auto pV = std::make_shared<Elasticity3D_P1::FunctionSpace>(pMesh);

  // Create XFEM space holder (this computes the enriched dofmap)
  auto V = std::make_shared<narwal::XFEMFunctionSpace>(pV, pDomainManager, 24);

  // Elasticity parameters (as coefficients to allow spatial variation)
  const double E = 20.0e9;
  const double nu = 0.1;
  auto pMu = std::make_shared<dolfin::Constant>(E/(2.0*(1.0 + nu)));
  auto pLambda = std::make_shared<dolfin::Constant>(E*nu/((1.0 + nu)*(1.0 - 2.0*nu)));

  // Crack pressure function
  double pressure = 2.0e6;
  auto pCrackPressure = std::make_shared<dolfin::Constant>(-pressure);

  // P1 XFEM forms
  auto xfem_forms = std::make_shared<narwal::ElasticityProblem3D>
    (V, pLambda, pMu, pCrackPressure, 1);

  // boundary conditions
  auto clamp = std::make_shared<dolfin::Constant>(0, 0, 0);
  auto bnd = std::make_shared<DirichletBoundary244>();
  auto pBC = std::make_shared<const dolfin::DirichletBC>(pV, clamp, bnd);
  std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs;
  bcs.push_back(pBC);

  // Create XFEM problem
  auto problem = std::make_shared<narwal::XFEMProblem>(xfem_forms, bcs);

  // Assemble system
  std::shared_ptr<dolfin::GenericMatrix> A = narwal::la::initSystemMatrix();
  std::shared_ptr<dolfin::GenericVector> b = narwal::la::initSystemVector();
  problem->assemble(*A, *b);

  auto dofmap = V->function_space()->dofmap();
  auto tdofmap = std::dynamic_pointer_cast<const narwal::DofMap>(dofmap);

  auto u = std::make_shared<dolfin::Function>(V->function_space());
  std::cout << "system size = " << u->vector()->size() << std::endl;

  // now try XAMG
  narwal::la::SolverXAMG xSolver(A, b, u, V->function_space(), pDomainManager,
    tdofmap, xfem_forms, true, false);

  // Create null space
  xSolver.null_space
    = narwal::xamg::NullSpace3DDisplacement2(V->function_space(), tdofmap, b,
                                             pMesh).build();

  // Create preconditioner & solver
  xSolver.setupDefaultPreCondAndSolver("hypre_amg");

  // solve
  const std::size_t num_iters = xSolver.solve();
  std::cout << "Max vector entry = " << u->vector()->max() << std::endl;

  EXPECT_LT(num_iters, std::size_t(25));

  // Write solution to PVD file to visualise with ParaView
  dolfin::File file_u("output/"+gtest_nameroot()+"-u.pvd");
  file_u << *u;

  narwal::ElasticFunction::write_crack_conforming_solution<Elasticity3D_P1::FunctionSpace>(
    *pDomainManager, *xfem_forms, *u, gtest_nameroot());

  narwal::TimeMonitor::instance().summarize(gtest_nameroot()+"-timemonitor.log");
}

TEST(Elasticity, CircularCrackIrregularMeshThinRefineAroundSurface)
{
  narwal::TimeMonitor::instance().start();

  dolfin::parameters["allow_extrapolation"] = true;
  dolfin::parameters["refinement_algorithm"] = "plaza_with_parent_facets";

  dolfin::MeshQuality quality;

  // assumes executable is in tests/build and mesh is in tests/data
  // this is an axis aligned cube centered at the origin with a side
  // length of 160 meters
  auto pMesh = std::make_shared<dolfin::Mesh>("data/small_mesh_thin_X.xml");

  // report quality of mesh
  std::pair<double, double> stats = quality.radius_ratio_min_max(*pMesh);
  std::cout << "mesh min/max cell quality: " << stats.first << ", " << stats.second << std::endl;

  // surface
  auto pSurface = std::make_shared<PlanarCircleNormalX>(0.1);

  // refine mesh in a corner to apply Dirichlet conditions
  RefineMeshInVolume(pMesh,
    dolfin::Point(-1000, -1000, -1000),
    dolfin::Point( 1000,   -70,   -70));
  RefineMeshInVolume(pMesh,
    dolfin::Point(-1000, -1000, -1000),
    dolfin::Point( 1000,   -70,   -70));

  RefineMeshAroundSurface(pMesh, pSurface);
  RefineMeshAroundSurface(pMesh, pSurface);
  RefineMeshAroundSurface(pMesh, pSurface);
  stats = quality.radius_ratio_min_max(*pMesh);
  std::cout << "mesh min/max cell quality: " << stats.first << ", " << stats.second << std::endl;

  // domain manager
  auto pDomainManager = std::make_shared<narwal::DomainManager>(pMesh);
  pDomainManager->register_surface(pSurface, "circle");

  // Create standard DOLFIN function space
  auto pV = std::make_shared<Elasticity3D_P1::FunctionSpace>(pMesh);

  // Create XFEM space holder (this computes the enriched dofmap)
  auto V = std::make_shared<narwal::XFEMFunctionSpace>(pV, pDomainManager, 24);

  // Elasticity parameters (as coefficients to allow spatial variation)
  const double E = 20.0e9;
  const double nu = 0.1;
  auto pMu = std::make_shared<dolfin::Constant>(E/(2.0*(1.0 + nu)));
  auto pLambda = std::make_shared<dolfin::Constant>(E*nu/((1.0 + nu)*(1.0 - 2.0*nu)));

  // Crack pressure function
  double pressure = 2.0e6;
  auto pCrackPressure = std::make_shared<dolfin::Constant>(-pressure);

  // P1 XFEM forms
  auto xfem_forms = std::make_shared<narwal::ElasticityProblem3D>
    (V, pLambda, pMu, pCrackPressure, 1);

  // boundary conditions
  auto clamp = std::make_shared< dolfin::Constant>(0, 0, 0);
  auto bnd = std::make_shared<DirichletBoundaryThin>();
  auto pBC = std::make_shared<const dolfin::DirichletBC>(pV, clamp, bnd);
  std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs;
  bcs.push_back(pBC);

  // Create XFEM problem
  auto problem = std::make_shared<narwal::XFEMProblem>(xfem_forms, bcs);

  // Assemble system
  std::shared_ptr<dolfin::GenericMatrix> A = narwal::la::initSystemMatrix();
  std::shared_ptr<dolfin::GenericVector> b = narwal::la::initSystemVector();
  problem->assemble(*A, *b);

  auto dofmap = V->function_space()->dofmap();
  auto tdofmap = std::dynamic_pointer_cast<const narwal::DofMap>(dofmap);

  auto u = std::make_shared<dolfin::Function>(V->function_space());
  std::cout << "system size = " << u->vector()->size() << std::endl;

  // setup XAMG
  narwal::la::SolverXAMG xSolver(A, b, u, V->function_space(), pDomainManager,
    tdofmap, xfem_forms, true, false);
  xSolver.null_space
    = narwal::xamg::NullSpace3DDisplacement2(V->function_space(), tdofmap, b,
                                             pMesh).build();
  xSolver.setupDefaultPreCondAndSolver("petsc_amg");

  // solve
  const std::size_t num_iters = xSolver.solve();
  std::cout << "Max vector entry = " << u->vector()->max() << std::endl;

  EXPECT_LT(num_iters, std::size_t(30));

  // Write solution to PVD file to visualise with ParaView
  dolfin::File file_u("output/"+gtest_nameroot()+"-u.pvd");
  file_u << *u;

  narwal::ElasticFunction::write_crack_conforming_solution<Elasticity3D_P1::FunctionSpace>(
    *pDomainManager, *xfem_forms, *u, gtest_nameroot());

  narwal::TimeMonitor::instance().summarize(gtest_nameroot()
                                            + "-timemonitor.log");
}

TEST(Elasticity, CircularCrackSpeedSlow)
{
  narwal::TimeMonitor::instance().start();
  // assumes executable is in tests/build and mesh is in tests/data
  // this is an axis aligned cube centered at the origin with a side
  // length of 244 meters
  auto pMesh = std::make_shared<dolfin::Mesh>("data/cube_mesh_side_244.xml");

  // globally refine mesh
  pMesh = std::make_shared<dolfin::Mesh>(dolfin::refine(*pMesh));

  // surface
  auto pSurface = std::make_shared<PlanarCircleNormalX>(0.0);

  // domain manager
  auto pDomainManager = std::make_shared<narwal::DomainManager>(pMesh);
  pDomainManager->register_surface(pSurface, "circle");

  // Create standard DOLFIN function space
  auto pV = std::make_shared<Elasticity3D_P1::FunctionSpace>(pMesh);

  // Create XFEM space holder (this computes the enriched dofmap)
  auto V = std::make_shared<narwal::XFEMFunctionSpace>(pV, pDomainManager, 24);

  // Elasticity parameters (as coefficients to allow spatial variation)
  const double E = 20.0e9;
  const double nu = 0.1;
  auto pMu = std::make_shared<dolfin::Constant>(E/(2.0*(1.0 + nu)));
  auto pLambda = std::make_shared<dolfin::Constant>(E*nu/((1.0 + nu)*(1.0 - 2.0*nu)));

  // Crack pressure function
  double pressure = 2.0e6;
  auto pCrackPressure = std::make_shared<dolfin::Constant>(-pressure);

  // P1 XFEM forms
  auto xfem_forms = std::make_shared<narwal::ElasticityProblem3D>
    (V, pLambda, pMu, pCrackPressure, 1);

  // boundary conditions
  auto clamp = std::make_shared<dolfin::Constant>(0, 0, 0);
  auto bnd = std::make_shared<DirichletBoundary244>();
  auto pBC = std::make_shared<const dolfin::DirichletBC>(pV, clamp, bnd);
  std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs;
  bcs.push_back(pBC);

  // Create XFEM problem
  auto problem = std::make_shared<narwal::XFEMProblem>(xfem_forms, bcs);

  // Assemble system
  std::shared_ptr<dolfin::GenericMatrix> A = narwal::la::initSystemMatrix();
  std::shared_ptr<dolfin::GenericVector> b = narwal::la::initSystemVector();
  problem->assemble(*A, *b);

  auto dofmap = V->function_space()->dofmap();
  auto tdofmap = std::dynamic_pointer_cast<const narwal::DofMap>(dofmap);

  auto uLU = std::make_shared<dolfin::Function>(V->function_space());
  std::cout << "system size = " << uLU->vector()->size() << std::endl;

  // LU solver
  dolfin::LUSolver lu_solver;
  std::cout << "running LU solver" << std::endl;
  dolfin::Timer timer("solver timer");
  timer.start();
  lu_solver.solve(*A, *(uLU->vector()), *b);
  double lu_time = timer.stop();
  double vec_norm_lu = uLU->vector()->norm("l2");
  std::cout << "LU solution max vector entry = " << std::scientific
            << vec_norm_lu << std::endl;
  uLU.reset();

  // XAMG solver
  auto uX = std::make_shared<dolfin::Function>(V->function_space());
  // Create system matrix and rhs vector
  narwal::la::SolverXAMG xSolver(A, b, uX, V->function_space(), pDomainManager,
                                 tdofmap, xfem_forms, true, false);

  // Create null space  --> create function object for this
  xSolver.null_space
    = narwal::xamg::NullSpace3DDisplacement2(V->function_space(),
                                             tdofmap, b, pMesh).build();

  // Create preconditioner & solver
  xSolver.setupDefaultPreCondAndSolver("petsc_amg");

  // solve
  std::cout << "running XAMG solver" << std::endl;
  timer.start();
  const std::size_t num_iters = xSolver.solve();
  double xamg_time = timer.stop();

  double vec_norm_xamg = uX->vector()->norm("l2");
  std::cout << "XAMG solution max vector entry = " << std::scientific
            << vec_norm_xamg << std::endl;

  std::cout << "LU solver time = " << lu_time;
  std::cout << ", XAMG solver time = " << xamg_time << std::endl;

  EXPECT_LT(xamg_time, lu_time);
  EXPECT_LT(num_iters, std::size_t(30));

  // default tolerance for SolverXAMG is 1.0e-8, so let's test for that
  EXPECT_NEAR(vec_norm_lu, vec_norm_xamg, 1.0e-8);

  narwal::TimeMonitor::instance().summarize(gtest_nameroot()
                                            + "-timemonitor.log");
}
