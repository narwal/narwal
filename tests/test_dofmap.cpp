// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

// Unit tests for function in narwal::XFEMTools

#include "gtest_tools.h"

#include <vector>
#include <dolfin.h>
#include <Narwal.h>
#include "Elasticity3D_P1.h"
#include "SquareSurface.h"


namespace
{
  // Surface representation class
  class EllipticalSurface : public narwal::ImplicitSurface
  {
  public:
    EllipticalSurface(double h_, double a_, double b_, double theta_)
      : narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1), h(h_), a(a_),
        b(b_), beta(0.0), theta(theta_) {}

    // Normal distance from the crack surface
    double f0(const dolfin::Point& p) const
    {
      const double x = std::cos(theta)*p.x() - std::sin(theta)*p.y();
      const double y = std::sin(theta)*p.x() + std::cos(theta)*p.y();
      const double z = p.z();
      const double d = (x/a)*(x/a) + (z/b)*(z/b) - 1.0;
      return (y - h - beta*d);
    }

    // If f0 = 0, then if f1 < 1 -> on surface and f1 >= = -> not on surface
    double f1(const dolfin::Point& p) const
    {
      const double x = std::cos(theta)*p.x() - std::sin(theta)*p.y();
      const double z = p.z();
      const double d = (x/a)*(x/a) + (z/b)*(z/b);
      if (d < 1.0)
        return -1.0;
      else
        return 1.0;
    }

    // Surface height position (average y)
    const double h;

    // Ellipse major and minor radii
    const double a, b;

    // Ellipse curvature
    const double beta;

    // Rotate axis
    const double theta;

  };


/*
// Surface representation class
  class EllipticalSurface : public narwal::ImplicitSurface
  {
  public:
    EllipticalSurface(double theta) :
      narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1),
      _a(0.4), _b(0.3), _beta(0.05), _theta(theta) {}

    double f0(const dolfin::Point& p) const
    {
      const double x =  p.x()*cos(_theta) + p.y()*sin(_theta);
      const double y = -p.x()*sin(_theta) + p.y()*cos(_theta);
      const double z = p.z();
      const double d = (x/_a)*(x/_a) + (z/_b)*(z/_b) - 1.0;
      return (y - _beta*d);
    }

    double f1(const dolfin::Point& p) const
    {
      const double x = p.x()*cos(_theta) + p.y()*sin(_theta);
      const double z = p.z();
      const double d = (x/_a)*(x/_a) + (z/_b)*(z/_b);
      if (d < 1.0)
        return -1.0;
      else
        return 1.0;
    }

  private:
    const double _a, _b, _beta, _theta;
  };
*/
}


// Test dimension-related member functions
TEST(DofMap, dimension)
{
  const std::vector<std::pair<std::size_t, double>> test_data
    = {{1, 10.0}, {2, 10.0}, {3, 10.0}, {5, 10.0}, {8, 10.0},
       {1, 0.201}, {10, 0.201}, {15, 0.235}, {20, 0.201}};
  for (auto data : test_data)
  {
    // Get number of mesh edges in each dir (n), and surface side
    // half-length (dx)
    const int n = data.first;
    const double dx = data.second;

    // Compute number of fully intersected 'cell' edges in each
    // direction (num_edges)
    const double delta = 1.0/n;
    const int num_edges = std::min(n, static_cast<int>(2.0*dx/delta));

    auto mesh = std::make_shared<dolfin::UnitCubeMesh>(n, n, n);
    auto surface = std::make_shared<test_surface::SquareSurface>(dx);
    auto dm = std::make_shared<narwal::DomainManager>(mesh);
    dm->register_surface(surface, "surface0");
    auto V = std::make_shared<Elasticity3D_P1::FunctionSpace>(mesh);
    narwal::DofMap dofmap(*V, *dm);

    // Test DofMap::global_dimension()
    if (n == num_edges)
    {
      const std::size_t nv = n + 1;
      EXPECT_EQ(3*(nv*nv*nv + 2*nv*nv), dofmap.global_dimension());
    }
    else
    {
      const std::size_t nv = std::max(0, num_edges - 1);
      EXPECT_EQ(3*((n+1)*(n+1)*(n+1) + 2*nv*nv), dofmap.global_dimension());
    }

    // Test DofMap::max_cell_dimension()
    if (num_edges == 0)
      EXPECT_EQ(static_cast<std::size_t>(12), dofmap.max_cell_dimension());
    else
      EXPECT_EQ(static_cast<std::size_t>(24), dofmap.max_cell_dimension());

    // TODO: test DofMap::cell_dimension(...)
    //dolfin::CellFunction<bool> intersected
    //  = XFEMTools::compute_intersected_cells(*dm);
    mesh->init(0, mesh->topology().dim());
    mesh->init(mesh->topology().dim(), 0);

    std::vector<std::pair<bool, bool>> intersections(mesh->num_cells());
    for (dolfin::CellIterator c(*mesh); !c.end(); ++c)
      intersections[c->index()] = surface->entity_intersection(*c);

    for (dolfin::CellIterator c(*mesh); !c.end(); ++c)
    {
      const int cell_dim = dofmap.cell_dimension(c->index());
      const std::pair<bool, bool> section = surface->entity_intersection(*c);
      if (intersections[c->index()].first and !intersections[c->index()].second)
      {
        // Count vertices that should be 'enriched' by checking if any
        // neighboring (by vertex) cells contain crack tip
        int num_tip_vertices = 0;
        for (dolfin::VertexIterator v(*c); !v.end(); ++v)
        {
          bool enriched_vertex = true;
          for (dolfin::CellIterator c0(*v); !c0.end(); ++c0)
          {
            if (intersections[c0->index()].first
                and intersections[c0->index()].second)
            {
              enriched_vertex = false;
              break;
            }
          }

          // Increment number of enriched vertices
          if (enriched_vertex)
            ++num_tip_vertices;
        }

        EXPECT_EQ(12 + 3*num_tip_vertices, cell_dim);
      }
      else if (section.first and section.second) // Contain tip
      {
        //std::cout << "d: " << d << std::endl;
        //EXPECT_TRUE(d == 15);
      }
      //else
      //  EXPECT_TRUE(d == 12 or d == 15 or d == 18);
    }
  }
}



TEST(DofMap, tabulate_coordinate)
{
  // TODO
}


TEST(DofMap, dofs)
{
  // TODO
}


TEST(DofMap, set)
{
  // TODO
}


TEST(DofMap, set_x)
{
  // TODO
}


TEST(DofMap, tabulate_local_to_global)
{
  // TODO
}


TEST(DofMap, enriched_dofs)
{
  // TODO
}


TEST(DofMap, enriched_to_regular)
{
  // TODO
}


// Test dimension-related member functions
TEST(DofMap, multi_surface)
{
  // Create mesh
  //const int n = 15;
  //std::shared_ptr<dolfin::Mesh> mesh(new dolfin::UnitCubeMesh(n, n, n));
  std::shared_ptr<dolfin::Mesh>
    mesh(new dolfin::BoxMesh(dolfin::Point(-0.4, -0.2, -0.4),
                             dolfin::Point(0.4, 0.2, 0.4), 3, 3, 3));

  // Create fracture surfaces
  //const double th0 =  M_PI/6.0;
  //const double th1 = -M_PI/6.0;
  //std::shared_ptr<test_dofmap::EllipticalSurface>
  //  surface0(new test_dofmap::EllipticalSurface(th0));
  //std::shared_ptr<test_dofmap::EllipticalSurface>
  //  surface1(new test_dofmap::EllipticalSurface(th1));

  // Create fracture surfaces
  std::shared_ptr<narwal::ImplicitSurface>
    surface0(new ::EllipticalSurface(0.03, 0.3, 0.3, 0.0));
  std::shared_ptr<narwal::ImplicitSurface>
    surface1(new ::EllipticalSurface(0.03, 0.18, 0.18, DOLFIN_PI/2.0));


  // Create DomainManager
  auto domain_manager = std::make_shared<narwal::DomainManager>(mesh);
  domain_manager->register_surface(surface0, "surface0");
  domain_manager->register_surface(surface1, "surface1");

  // Create standard DOLFIN function space
  std::shared_ptr<dolfin::FunctionSpace>
    V_s(new Elasticity3D_P1::FunctionSpace(mesh));

  // Create XFEM space (this computes the enriched dofmap)
  std::shared_ptr<narwal::XFEMFunctionSpace>
    V(new narwal::XFEMFunctionSpace(V_s, domain_manager, 24));

  // Get dofmap
  std::shared_ptr<const narwal::DofMap> dofmap = V->dofmap();

  // Check that sub-triangulation has 'cells' on both sides surfaces
  for (dolfin::CellIterator c(*mesh); !c.end(); ++c)
  {
    //std::cout << "Cell index: " << c->index() << std::endl;

    // Compute intersections
    const std::pair<bool, bool> i0 = surface0->entity_intersection(*c);
    const std::pair<bool, bool> i1 = surface1->entity_intersection(*c);

    // Sub-trianguate cell
    //narwal::SubTriangulation t0(*c, *surface0);
    //narwal::SubTriangulation t1(t0.mesh(), *surface1);

    // Test case with enrichment
    if (dofmap->cell_dofs(c->index()).size() > 12)
    {
      // Get indices of surfaces that are active for this cell
      std::set<int> surfaces;
      for (std::size_t i = 0; i < 12; ++i)
      {
        const std::vector<std::pair<int, narwal::EnrLabelArray>>& edof_data
          = dofmap->enriched_dofs(c->index(), i);
        for (auto edof : edof_data)
        {
          //if (c->index() == 63)
          //{
          //  std::cout << "Interactions: " << edof.second[0] << ", " << edof.second[1] << std::endl;
          //}
          surfaces.insert(edof.second.begin(), edof.second.end());
        }
      }

      // Test
      for (auto surface : surfaces)
      {
        //std::cout << "Surface: " << surface << std::endl;
        if (surface == 0)
        {
          EXPECT_TRUE(i0.first);
          EXPECT_FALSE(i0.second);
        }

        if (surface == 1)
        {
          EXPECT_TRUE(i1.first);
          EXPECT_FALSE(i1.second);
        }
      }
    }
  }
}
