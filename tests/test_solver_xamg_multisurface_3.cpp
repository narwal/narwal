// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//         Axel Gerstenberger
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

// Unit tests for function in narwal::XFEMTools

#include "gtest_tools.h"

#include <dolfin.h>
#include <Narwal.h>
#include "xamg_elasticity_multisurface_3.h"
#include <Elasticity3D_P1.h>
#include <Elasticity3D_P2.h>

namespace
{
  // define parameters for P1 and P2 setup
  class P1 {
  public:
    typedef Elasticity3D_P1::FunctionSpace FuncSpace;
    static const int polynomialDegree = 1;
    static const int numNode = 4;
    static const int numField = 3;
    static const int numEnr = 8;
    static const int max_element_dim = numField*numNode*numEnr;

    static constexpr double solution_l2 = 34.409301068170635;
  };

  class P2 {
  public:
    typedef Elasticity3D_P2::FunctionSpace FuncSpace;
    static const int polynomialDegree = 2;
    static const int numNode = 10;
    static const int numField = 3;
    static const int numEnr = 8;
    static const int max_element_dim = numField*numNode*numEnr;

    static constexpr double solution_l2 = 82.389319696176202;
  };

  // template class to get parameters from
  template <typename T>
  class SolverXAMGTestMultiSurface3 : public ::testing::Test { };

  // register template parameters to be used
  typedef ::testing::Types<P1,P2> MyTypes ;
  TYPED_TEST_CASE(SolverXAMGTestMultiSurface3, MyTypes);
}



// Test that elasticity nullspace vectors are in nullspace (check
// Ax=0)
TYPED_TEST(SolverXAMGTestMultiSurface3, InNullspaceElastic)
{
  dolfin::parameters["allow_extrapolation"] = true;
  dolfin::parameters["refinement_algorithm"] = "plaza_with_parent_facets";
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  test::xamg_elasticity_ms_3::ElasticityTestSetup<typename TypeParam::FuncSpace>
  s("output/" + gtest_nameroot(),
      TypeParam::polynomialDegree, TypeParam::max_element_dim, false);

  auto dofmap = s.V->function_space()->dofmap();
  auto tdofmap = std::dynamic_pointer_cast<const narwal::DofMap>(dofmap);

  // Create null space
  auto V = s.V->function_space();
  auto null_space =
      narwal::xamg::NullSpace3DDisplacement2(
        V, tdofmap, s.b, s.domain_manager->mesh()).build();

  // Create system matrix and rhs vector
  narwal::la::SolverXAMG xSolver(s.A, s.b, s.u, s.V->function_space(),
                                 s.domain_manager,
                                 tdofmap, s.xfem_forms,
                                 true, true, "output/" + gtest_nameroot());

  std::shared_ptr<dolfin::GenericMatrix>
    G = narwal::la::SolverXAMG::buildTransformationMatrixG(
      *s.domain_manager,
      *tdofmap,
      *s.V->function_space()->element()->create_sub_element(0),
      *s.xfem_forms);

  narwal::la::toMatrixMarketFile(*G, "output/" + gtest_nameroot() + "_G");

  const std::size_t numRow = G->size(0);
  for (std::size_t iRow = 0; iRow < numRow; ++iRow)
  {
    std::vector<std::size_t> columns;
    std::vector<double> values;
    G->getrow(iRow, columns, values);
    ASSERT_EQ(columns.size(), values.size());
//    switch (columns.size())
//    {
//    case 1:
//      // check diagonal value, it should be the only value present
//      ASSERT_EQ(iRow, columns[0]);
//      ASSERT_DOUBLE_EQ(1.0, values[0]);
//      break;
//    case 2:
//      // check diagonal entry to be 1
//      ASSERT_EQ(columns[1], iRow);
//      ASSERT_DOUBLE_EQ( 1.0, values[1]);
//      // check off diagonal entry to be -1.0 and in the lower left triangle
//      ASSERT_LT(columns[0], iRow);
//      ASSERT_DOUBLE_EQ(-1.0, values[0]);
//      break;
//    default:
//      EXPECT_EQ(std::size_t(1), columns.size());
//      for (std::size_t i = 0; i < values.size(); ++i)
//        std::cout << "  " << values[i];
//      std::cout << std::endl;
//      ASSERT_TRUE(false) << "Illegal entry detected!" << std::endl;
//    }
  }

  // Transform system matrix: G'*A*G
  ASSERT_TRUE(s.A->is_symmetric(1.0e-12));
  auto GtAG = narwal::la::MatPtAP(*G, *s.A);
  ASSERT_TRUE(GtAG->is_symmetric(1.0e-12));
  narwal::la::toMatrixMarketFile(*GtAG, "output/" + gtest_nameroot() + "_GtAG");

  // Transform rhs: G'*b
  auto Gtbtemp = s.b->copy();
  G->transpmult(*s.b, *Gtbtemp);
  auto Gtb = Gtbtemp;

  // Transform null space: Ginv * null space
  auto Ginv_null_space = narwal::la::transformGinvN(G, *null_space);

}

TYPED_TEST(SolverXAMGTestMultiSurface3, ElasticityPetscDirectSolve)
{
  dolfin::parameters["allow_extrapolation"] = true;
  dolfin::parameters["refinement_algorithm"] = "plaza_with_parent_facets";
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  test::xamg_elasticity_ms_3::ElasticityTestSetup<typename TypeParam::FuncSpace>
  s("output/" + gtest_nameroot(), TypeParam::polynomialDegree,
      TypeParam::max_element_dim);

  dolfin::LUSolver solver;
  solver.solve(*s.A, *s.u->vector(), *s.b);

  EXPECT_NEAR(TypeParam::solution_l2, s.u->vector()->norm("l2"), 1.0e-8);

  narwal::ElasticFunction::write_crack_conforming_solution<typename TypeParam::FuncSpace>(
      s.domain_manager, s.xfem_forms, s.u, gtest_nameroot());
}

TYPED_TEST(SolverXAMGTestMultiSurface3, ElasticityPetscGeneralAMG)
{
  dolfin::parameters["allow_extrapolation"] = true;
  dolfin::parameters["refinement_algorithm"] = "plaza_with_parent_facets";
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  test::xamg_elasticity_ms_3::ElasticityTestSetup<typename TypeParam::FuncSpace>
  s("output/" + gtest_nameroot(), TypeParam::polynomialDegree,
      TypeParam::max_element_dim);

  auto dofmap = s.V->function_space()->dofmap();
  auto tdofmap = std::dynamic_pointer_cast<const narwal::DofMap>(dofmap);

  // Create null space
  auto null_space =
    narwal::xamg::NullSpace3DDisplacement2(s.V->function_space(),
                                           tdofmap, s.b, s.domain_manager->mesh()).build();

  // Create preconditioner and attach null space
  auto pc = std::make_shared<dolfin::PETScPreconditioner>("petsc_amg"); // use "ilu", "petsc_amg", or "ml_amg"
  pc->set_nullspace(*null_space);
  pc->parameters["report"] = true;

  // Create solver
  dolfin::PETScKrylovSolver solver("gmres", pc);
  solver.parameters["report"] = true;
  solver.parameters["monitor_convergence"] = true;
  solver.parameters["relative_tolerance"] = 1.0e-8;
  solver.parameters["convergence_norm_type"] = "true";

  solver.solve(*s.A, *s.u->vector(), *s.b);

  EXPECT_NEAR(TypeParam::solution_l2, s.u->vector()->norm("l2"), 5.0e-2);

  narwal::ElasticFunction::write_crack_conforming_solution<typename TypeParam::FuncSpace>(
      s.domain_manager, s.xfem_forms, s.u, gtest_nameroot());
}

TYPED_TEST(SolverXAMGTestMultiSurface3, ElasticityPetscTransformPetscAMG)
{
  dolfin::parameters["allow_extrapolation"] = true;
  dolfin::parameters["refinement_algorithm"] = "plaza_with_parent_facets";
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

//  const char* params[] = {"dummy_first_arg",
////    "--petsc.pc_hypre_boomeramg_relax_type_up", "symmetric-SOR/Jacobi",
////    "--petsc.pc_hypre_boomeramg_relax_type_down", "symmetric-SOR/Jacobi",
////    "--petsc.pc_hypre_boomeramg_max_iter", "1",
////    "--petsc.pc_hypre_boomeramg_rtol", "0.0",
////    "--petsc.pc_hypre_boomeramg_strong_threshold", "0.00001",
////    "--petsc.pc_hypre_boomeramg_coarsen_type", "PMIS",
////    "--petsc.pc_hypre_boomeramg_max_levels", "10",
////    "--petsc.pc_hypre_boomeramg_agg_nl", "5",
//    "--petsc.pc_hypre_boomeramg_P_max", "4",
////    "--petsc.pc_hypre_boomeramg_relax_type_all", "symmetric-SOR/Jacobi",
//    "--petsc.pc_hypre_boomeramg_print_statistics", "1",
////    "--petsc.pc_hypre_boomeramg_truncfactor", "0.01",
//    // http://lists.mcs.anl.gov/pipermail/petsc-users/2007-April/001488.html
////    "--petsc.pc_hypre_boomeramg_set_num_functions", "3",
////    "--petsc.pc_hypre_boomeramg_set_nodal", "1",
////    "--petsc.pc_hypre_boomeramg_set_num_paths", "2",
////    "--petsc.pc_hypre_boomeramg_interp_type", "ext+i",
////    "--petsc.pc_hypre_ams_amg_alpha_theta", "0.0001"
//  };
//
//  dolfin::GlobalParameters parameters;
//  parameters.parse(sizeof(params)/sizeof(char *), const_cast<char**>(params));

  test::xamg_elasticity_ms_3::ElasticityTestSetup<typename TypeParam::FuncSpace>
  s("output/" + gtest_nameroot(), TypeParam::polynomialDegree,
      TypeParam::max_element_dim);

  auto dofmap = s.V->function_space()->dofmap();
  auto tdofmap = std::dynamic_pointer_cast<const narwal::DofMap>(dofmap);

  // Create system matrix and rhs vector
  narwal::la::SolverXAMG xSolver(s.A, s.b, s.u, s.V->function_space(),
                                 s.domain_manager, tdofmap, s.xfem_forms,
                                 true, true, "output/" + gtest_nameroot());

  // Create null space
  xSolver.null_space =
      narwal::xamg::NullSpace3DDisplacement2(
        s.V->function_space(), tdofmap, s.b, s.domain_manager->mesh()).build();

  // Create preconditioner & solver
  xSolver.setupDefaultPreCondAndSolver("petsc_amg"); // use "ilu", "petsc_amg", or "ml_amg"

  // Solve
  EXPECT_LT(xSolver.solve(), std::size_t(100));

  EXPECT_NEAR(TypeParam::solution_l2, s.u->vector()->norm("l2"), 1.0e-2);

  narwal::ElasticFunction::write_crack_conforming_solution<typename TypeParam::FuncSpace>(
      s.domain_manager, s.xfem_forms, s.u, gtest_nameroot());
}
