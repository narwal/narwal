// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//         Axel Gerstenberger
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "gtest_tools.h"

#include <dolfin.h>
#include "Narwal.h"
#include "Poisson3D_P1.h"

namespace
{
  // Source term (right-hand side)
  class Source : public dolfin::Expression
  {
    void eval(dolfin::Array<double>& values, const dolfin::Array<double>& /*x*/) const
    { values[0] = 0.0; }
  };

  // Sub domain for Dirichlet boundary condition
  class DirichletBoundary : public dolfin::SubDomain
  {
    bool inside(const dolfin::Array<double>& x, bool /*on_boundary*/) const
    { return DOLFIN_EPS < std::abs(x[0]); }
  };

  // Surface representation class
  class EllipticalSurface : public narwal::ImplicitSurface
  {
  public:
    EllipticalSurface()
      : narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1), a(0.3),
        b(0.3), beta(0.0) {}

    // Normal distance from the crack surface
    double f0(const dolfin::Point& p) const
    {
      const double x = p.x();
      const double y = p.y();
      const double z = p.z();
      const double d = (x/a)*(x/a) + (z/b)*(z/b) - 1.0;
      return (y - beta*d);
    }

    // If f0 = 0, then if f1<1 -> on surface and f1 >= = -> not on surface
    double f1(const dolfin::Point& p) const
    {
      const double x = p.x();
      //const double y = p.y();
      const double z = p.z();
      const double d = (x/a)*(x/a) + (z/b)*(z/b);
      if (d < 1.0)
        return -1.0;
      else
        return 1.0;
    }

    // Ellipse major and minor radii
    const double a, b;

    // Ellipse curvature
    const double beta;
  };
}


TEST(DomainManager, constructor)
{
  dolfin::parameters["linear_algebra_backend"] = "PETSc";
  dolfin::parameters["reorder_dofs_serial"] = false;
  auto mesh = std::make_shared<dolfin::UnitCubeMesh>(2, 2, 2);
  Poisson3D_P1::FunctionSpace V(mesh);
  narwal::DomainManager dm(mesh);
  ASSERT_EQ(0, (int) dm.num_surfaces());
}


TEST(DomainManager, register_surface)
{
  dolfin::parameters["linear_algebra_backend"] = "PETSc";
  dolfin::parameters["reorder_dofs_serial"] = false;
  auto mesh = std::make_shared<dolfin::UnitCubeMesh>(2, 2, 2);
  Poisson3D_P1::FunctionSpace V(mesh);
  narwal::DomainManager dm(mesh);
  ASSERT_EQ(0, (int) dm.num_surfaces());
  auto surface = std::make_shared<::EllipticalSurface>();
  dm.register_surface(surface, "The earth is a disk!");
  ASSERT_EQ(1, (int) dm.num_surfaces());
}


// FIXME: test DomainManager::mesh

// FIXME: test DomainManager::intersecting_surface

// FIXME: test DomainManager::triangulate

// FIXME: test DomainManager::surface_side

// FIXME: test DomainManager::surface
