// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

// Unit tests for function in narwal::GeometryTools

#include "gtest_tools.h"

#include <vector>
#include <dolfin.h>
#include <Narwal.h>
#include "Elasticity3D_P1.h"
#include "SquareSurface.h"

namespace
{
  // Return a domain manager object
  std::shared_ptr<narwal::DomainManager>
  create_domain_manager(std::size_t n, double r)
  {
    std::shared_ptr<dolfin::Mesh> mesh(new dolfin::UnitCubeMesh(n, n, n));
    std::shared_ptr<test_surface::SquareSurface> surface(new test_surface::SquareSurface(r));
    auto domain_manager = std::make_shared<narwal::DomainManager>(mesh);
    domain_manager->register_surface(surface, "surface0");
    return domain_manager;
  }

  // Mesh/surface data: (mesh edges each dir, surface square half-edge
  // length). This list includes full intersections and partial
  // intersections.
  static std::vector<std::pair<std::size_t, double>> test_data
    = {{1, 10.0}, {2, 10.0}, {3, 10.0}, {4, 10.0}, {9, 10.0}, {12, 10.0},
       {1, 0.201}, {10, 0.201}, {15, 0.235}, {20, 0.201}};
}

// Check number of intersected cells for a unit cube intersected by a
// square surface. Tests fully and partially cases.
TEST(GeometryTools, compute_intersected_cells_simple_cube)
{
  // Loop over different mesh tests
  for (auto data : ::test_data)
  {
    // Test data
    const std::size_t n = data.first;
    const double dx = data.second;

    // Compute number of fully intersected 'edges' in each direction
    const double delta = 1.0/n;
    const std::size_t num_edges
      = std::min(n, static_cast<std::size_t>(2.0*dx/delta));

    // Create domain manager
    auto dm = ::create_domain_manager(n, dx);
    dolfin_assert(dm);
    const narwal::GenericSurface& surface = *(dm->surface(0));

    // Test number of intersected cells
    const dolfin::CellFunction<bool> cf
      = narwal::GeometryTools::compute_intersected_cells(dm->mesh(), surface);
    std::size_t num_intersected_cells = 0;
    for (dolfin::CellIterator c(*dm->mesh()); !c.end(); ++c)
      if (cf[*c])
        ++num_intersected_cells;
    EXPECT_EQ(6*num_edges*num_edges, num_intersected_cells);
  }
}


// Test number of boundary facets for a unit cube intersected by a
// square surface. Test fully and partially cases.
TEST(GeometryTools, compute_boundary_facets_simple_cube)
{
  // Loop over different mesh tests
  for (auto data : ::test_data)
  {
    // Test data
    const std::size_t n = data.first;
    const double dx = data.second;

    // Compute number of fully intersected 'edges' in each direction
    const double delta = 1.0/n;
    const std::size_t num_edges
      = std::min(n, static_cast<std::size_t>(2.0*dx/delta));

    // Create domain manager
    auto dm = ::create_domain_manager(n, dx);
    dolfin_assert(dm);

    // Test number of boundary facets (facets between fully
    // intersected and partially intersected cells)
    dolfin::FacetFunction<bool> ff
      = narwal::GeometryTools::compute_boundary_facets(dm->mesh(),
                                                       *(dm->surface(0)));
    std::size_t num_boundary_facets = 0;
    for (dolfin::FacetIterator f(*dm->mesh()); !f.end(); ++f)
      if (ff[*f])
        ++num_boundary_facets;

    // Hamdle partially and fully intersected cases
    if (num_edges < n)
      EXPECT_EQ(static_cast<std::size_t>(2*4*num_edges), num_boundary_facets);
    else
      EXPECT_EQ(static_cast<std::size_t>(0), num_boundary_facets);
  }
}


// Test GeometryTools::is_boundary_facet
TEST(GeometryTools, is_boundary_facet_simple_cube)
{
  // Loop over different mesh tests
  for (auto data : ::test_data)
  {
    // Test data
    const std::size_t n = data.first;
    const double dx = data.second;

    // Compute number of fully intersected 'edges' in each direction
    const double delta = 1.0/n;
    const std::size_t num_edges
      = std::min(n, static_cast<std::size_t>(2.0*dx/delta));

    // Test is only for full intersected domains
    if (num_edges != n)
      continue;

    // Create domain manager
    auto dm = ::create_domain_manager(n, dx);
    dolfin_assert(dm);

    // Check is_boundary_facet for fully intersected meshes
    for (dolfin::FacetIterator f(*dm->mesh()); !f.end(); ++f)
    {
      ASSERT_FALSE(narwal::GeometryTools::is_boundary_facet(*f,
                                                            *(dm->surface(0))));
    }
  }
}


// Test GeometryTools::volume_ratio
TEST(GeometryTools, volume_ratio)
{
  // TODO: implement test

  /*
  // TODO: implement a numerical check
  dolfin::UnitCubeMesh mesh(4, 4, 4);
  SquareSurface surface(10.0);
  for (dolfin::CellIterator c(mesh); !c.end(); ++c)
  {
    const double ratio = narwal::GeometryTools::volume_ratio(surface, *c, mesh);
    EXPECT_TRUE(ratio < 1.0);
    EXPECT_TRUE(ratio > 0.0);
  }
  */
}
