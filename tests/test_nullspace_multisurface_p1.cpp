// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

// Unit tests for function in narwal::XFEMTools

#include "gtest_tools.h"

#include <dolfin.h>
#include <Narwal.h>
#include "xamg_elasticity_multisurface.h"
#include <Elasticity3D_P1.h>

namespace
{
  typedef Elasticity3D_P1::FunctionSpace FuncSpace;
  static const int polynomialDegree = 1;
  static const int numNode = 4;
  static const int numField = 3;
  static const int numEnr = 4;
  static const int max_element_dim = numField*numNode*numEnr;

  // Sub domain for Dirichlet boundary condition
  class EmptyDirichletBoundary : public dolfin::SubDomain
  {
    bool inside(const dolfin::Array<double>& /*x*/, bool /*on_boundary*/) const
    { return false; }
  };

  std::shared_ptr<dolfin::GenericVector> Ax(
      std::shared_ptr<const dolfin::GenericMatrix> A,
      std::shared_ptr<const dolfin::GenericVector> x
      )
  {
    std::shared_ptr<dolfin::GenericVector> b = narwal::la::initSystemVector();
    A->transpmult(*x, *b);
    return b;
  }
  // Put any class, functions or static data specific to tests in this file her
}


// Test dimension of elasticity nullspace basis
TEST(XFEMNullSpace, multisurface_dim_elastic)
{
  // TODO
}


// Test dimension of Poisson nullspace basis
TEST(XFEMNullSpace, multisurface_dim_poisson)
{
  // TODO
}


// Test that elasticity nullspace vectors are orthogonal
TEST(XFEMNullSpace, multisurface_orthogonal_elastic)
{
  // TODO
}


// Test that Poisson nullspace vectors are orthogonal
TEST(XFEMNullSpace, multisurface_orthogonal_poisson)
{
  // TODO
}


// Test that elasticity nullspace vectors are in nullspace (check
// Ax=0)
TEST(XFEMNullSpace, inNullspaceElasticMultiSurfaceP1)
{
  dolfin::parameters["allow_extrapolation"] = true;
  dolfin::parameters["refinement_algorithm"] = "plaza_with_parent_facets";
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  ::test::xamg_elasticity_ms::ElasticityTestSetup<FuncSpace>
    s("output/" + gtest_nameroot(), polynomialDegree, max_element_dim, false);

  auto dofmap = s.V->function_space()->dofmap();
  auto tdofmap = std::dynamic_pointer_cast<const narwal::DofMap>(dofmap);

  // Create null space
  auto null_space
    = narwal::xamg::NullSpace3DDisplacement2(s.V->function_space(),
                                             tdofmap, s.b, s.domain_manager->mesh()).build();

//  ASSERT_TRUE(null_space->is_orthonormal());
//  ASSERT_TRUE(null_space->is_orthogonal());

  EXPECT_NEAR(std::round((*null_space)[0]->sum()), s.domain_manager->mesh()->num_vertices(), 1.0e-2);
  EXPECT_NEAR(std::round((*null_space)[1]->sum()), s.domain_manager->mesh()->num_vertices(), 1.0e-2);
  EXPECT_NEAR(std::round((*null_space)[2]->sum()), s.domain_manager->mesh()->num_vertices(), 1.0e-2);

  // check sum(x) != 0
  ASSERT_DOUBLE_EQ((*null_space)[0]->norm("linf"), 1.0);
  ASSERT_DOUBLE_EQ((*null_space)[1]->norm("linf"), 1.0);
  ASSERT_DOUBLE_EQ((*null_space)[2]->norm("linf"), 1.0);
  if (null_space->dim() >=6) { // test rotational modes
    ASSERT_GT((*null_space)[3]->norm("linf"), 0.0);
    ASSERT_GT((*null_space)[4]->norm("linf"), 0.0);
    ASSERT_GT((*null_space)[5]->norm("linf"), 0.0);
  }

  // check Ax=0, make sure, no Dirichlet BC hav been applied to A
  // if these tests fail, either A or the nullspace is wrong
  ASSERT_NEAR( ::Ax( s.A, (*null_space)[0] )->norm("l2"), 0.0, 1.0e-8);
  ASSERT_NEAR( ::Ax( s.A, (*null_space)[1] )->norm("l2"), 0.0, 1.0e-8);
  ASSERT_NEAR( ::Ax( s.A, (*null_space)[2] )->norm("l2"), 0.0, 1.0e-8);
  if (null_space->dim() >=6) { // test rotational modes
    ASSERT_NEAR( ::Ax( s.A, (*null_space)[3] )->norm("l2"), 0.0, 1.0e-8);
    ASSERT_NEAR( ::Ax( s.A, (*null_space)[4] )->norm("l2"), 0.0, 1.0e-8);
    ASSERT_NEAR( ::Ax( s.A, (*null_space)[5] )->norm("l2"), 0.0, 1.0e-8);
  }

  std::ofstream f("output/" + gtest_nameroot() + "_nullSpace.txt");
  narwal::la::outputNullSpace(*null_space, f);

}
