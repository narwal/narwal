// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "gtest_tools.h"

#include <dolfin.h>
#include "Narwal.h"
#include "gtest_tools.h"

// FIXME: Add tests for narwal::SubTriangulation interface, and move
// tests from elsewhere that belong here

namespace
{
  // Surface representation class
  class EllipticalSurface : public narwal::ImplicitSurface
  {
  public:
    EllipticalSurface(double theta_) :
      narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1),
      a(0.4), b(0.3), beta(0.05), theta(theta_) {}

    double f0(const dolfin::Point& p) const
    {
      const double x =  p.x()*cos(theta) + p.y()*sin(theta);
      const double y = -p.x()*sin(theta) + p.y()*cos(theta);
      const double z = p.z();
      const double d = (x/a)*(x/a) + (z/b)*(z/b) - 1.0;
      return (y - beta*d);
    }

    double f1(const dolfin::Point& p) const
    {
      const double x = p.x()*cos(theta) + p.y()*sin(theta);
      const double z = p.z();
      const double d = (x/a)*(x/a) + (z/b)*(z/b);
      if (d < 1.0)
        return -1.0;
      else
        return 1.0;
    }

  private:
    const double a, b, beta, theta;
  };
}

//-----------------------------------------------------------------------------
TEST(SubTriangulation, construction0)
{
  const int n = 15;
  dolfin::BoxMesh mesh(dolfin::Point(-1, -1, -1), dolfin::Point(1, 1, 1),
                       n, n, n);
  ::EllipticalSurface surface(0.0);

  std::size_t tet_count = 0;
  for(dolfin::CellIterator cell(mesh); !cell.end(); ++cell)
  {
    narwal::SubTriangulation t(*cell, surface);
    tet_count += t.mesh().num_cells();
    double vol = 0.0;
    for(dolfin::CellIterator c(t.mesh()); !c.end(); ++c)
      vol += c->volume();
    ASSERT_NEAR(cell->volume(), vol, n*n*n*1.0-10);
  }
  ASSERT_TRUE(tet_count > mesh.num_cells());
}
//-----------------------------------------------------------------------------
TEST(SubTriangulation, construction1)
{
  const int n = 15;
  dolfin::BoxMesh mesh(dolfin::Point(-1, -1, -1), dolfin::Point(1, 1, 1),
                       n, n, n);
  ::EllipticalSurface surface(0.0);
  narwal::SubTriangulation t(mesh, surface);

  dolfin::XDMFFile x(mesh.mpi_comm(), "a.xdmf");
  x.write(t.mesh());

  dolfin::XDMFFile x2(mesh.mpi_comm(), "a_surf.xdmf");
  x2.write(*t.surface_mesh());

  double vol0 = 1.0;
  for(dolfin::CellIterator c(t.mesh()); !c.end(); ++c)
    vol0 += c->volume();

  double vol1 = 1.0;
  for(dolfin::CellIterator c(mesh); !c.end(); ++c)
    vol1 += c->volume();

  ASSERT_NEAR(vol1, vol0, n*n*n*1.0e-10);
}
//-----------------------------------------------------------------------------
TEST(SubTriangulation, mesh_multiple_intersections)
{
  const int n = 15;
  dolfin::UnitCubeMesh mesh(n, n, n);
  const double th0 =  M_PI/6.0;
  const double th1 = -M_PI/6.0;

  ::EllipticalSurface surface0(th0);
  ::EllipticalSurface surface1(th1);
  narwal::SubTriangulation t0(mesh, surface0);
  narwal::SubTriangulation t1(t0.mesh(), surface1);

  // Test volume
  double vol = 1.0;
  for(dolfin::CellIterator c(mesh); !c.end(); ++c)
    vol += c->volume();

  double vol0 = 1.0;
  for(dolfin::CellIterator c(t0.mesh()); !c.end(); ++c)
    vol0 += c->volume();

  double vol1 = 1.0;
  for(dolfin::CellIterator c(t1.mesh()); !c.end(); ++c)
    vol1 += c->volume();

  ASSERT_NEAR(vol0, vol, n*n*n*1.0e-10);
  ASSERT_NEAR(vol1, vol, n*n*n*1.0e-10);


  // Check that sub-triangulation has 'cells' on both sides surfaces
  for(dolfin::CellIterator c(mesh); !c.end(); ++c)
  {
    // Chek intersections
    const std::pair<bool, bool> i0 = surface0.entity_intersection(*c);
    const std::pair<bool, bool> i1 = surface1.entity_intersection(*c);

    // Sub-trianguate cell
    narwal::SubTriangulation t0(*c, surface0);
    narwal::SubTriangulation t1(t0.mesh(), surface1);

    // Check for surface 0
    if (i0.first and !i0.second)
    {
      int side_counter = 0;
      for(dolfin::CellIterator c_sub(t1.mesh()); !c_sub.end(); ++c_sub)
      {
        const dolfin::Point x_mid = c_sub->midpoint();
        side_counter += surface0.side(x_mid);
      }
      ASSERT_NE(std::abs(side_counter), static_cast<int>(t1.mesh().num_cells()));
    }

    // Check for surface 1
    if (i1.first and !i1.second)
    {
      int side_counter = 0;
      for(dolfin::CellIterator c_sub(t1.mesh()); !c_sub.end(); ++c_sub)
      {
        const dolfin::Point x_mid = c_sub->midpoint();
        side_counter += surface1.side(x_mid);
      }
      ASSERT_NE(std::abs(side_counter), static_cast<int>(t1.mesh().num_cells()));
    }
  }
}
//-----------------------------------------------------------------------------
TEST(SubTriangulation, cell_multiple_intersections)
{
  const double th1 =  M_PI/12.0;
  const double th2 = -M_PI/15.0;

  // Make a simple tetrahedral cell around (0, 0, 0)
  // in sphere of radius sqrt(3)*a
  dolfin::Mesh mesh;
  dolfin::MeshEditor editor;
  editor.open(mesh, 3, 3);
  editor.init_vertices(4);
  const double a = 0.1;
  editor.add_vertex(0, -a, -a,  a);
  editor.add_vertex(1,  a,  a,  a);
  editor.add_vertex(2, -a,  a, -a);
  editor.add_vertex(3,  a, -a, -a);
  editor.init_cells(1);
  editor.add_cell(0, 0, 1, 2, 3);
  editor.close();

  dolfin::Cell c0(mesh, 0);

  std::vector<std::shared_ptr<const narwal::GenericSurface>> surfs;
  surfs.push_back(std::shared_ptr<const narwal::GenericSurface>
                  (new ::EllipticalSurface(th1)));
  surfs.push_back(std::shared_ptr<const narwal::GenericSurface>
                  (new ::EllipticalSurface(th2)));

  narwal::SubTriangulation t1(c0, surfs);

  double vol1 = 0.0;
  for(dolfin::CellIterator c(t1.mesh()); !c.end(); ++c)
    vol1 += c->volume();

  ASSERT_NEAR(vol1, c0.volume(), t1.mesh().num_cells()*1e-10);

}
//-----------------------------------------------------------------------------
TEST(SubTriangulation, mesh)
{
  // TODO: Add test for SubTriangulation::mesh
}
//-----------------------------------------------------------------------------
TEST(SubTriangulation, surface_mesh)
{
  const double th1 =  M_PI/12.0;
  const double th2 = -M_PI/3.0;

  // Make a simple tetrahedral cell around (0, 0, 0)
  // in sphere of radius sqrt(3)*a

  dolfin::Mesh mesh;
  dolfin::MeshEditor editor;
  editor.open(mesh, 3, 3);
  editor.init_vertices(4);
  const double a = 0.1;
  editor.add_vertex(0, -a, -a,  a);
  editor.add_vertex(1,  a,  a,  a);
  editor.add_vertex(2, -a,  a, -a);
  editor.add_vertex(3,  a, -a, -a);
  editor.init_cells(1);
  editor.add_cell(0, 0, 1, 2, 3);
  editor.close();

  dolfin::Cell c0(mesh, 0);

  std::vector<std::shared_ptr<const narwal::GenericSurface>> surfs;
  surfs.push_back(std::shared_ptr<const narwal::GenericSurface>
                  (new ::EllipticalSurface(th1)));
  surfs.push_back(std::shared_ptr<const narwal::GenericSurface>
                  (new ::EllipticalSurface(th2)));

  narwal::SubTriangulation t1(c0, surfs);

  dolfin::XDMFFile x0(mesh.mpi_comm(), "s0.xdmf");
  x0.write(*t1.surface_mesh(0));

  dolfin::XDMFFile x1(mesh.mpi_comm(), "s1.xdmf");
  x1.write(*t1.surface_mesh(1));

  dolfin::XDMFFile x2(mesh.mpi_comm(), "v.xdmf");
  x2.write(t1.mesh());

  ASSERT_EQ(t1.surface_mesh(0)->num_cells(), std::size_t(6));
  ASSERT_EQ(t1.surface_mesh(1)->num_cells(), std::size_t(8));
}
//-----------------------------------------------------------------------------
