// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "gtest_tools.h"

#include <dolfin.h>
#include <Narwal.h>

namespace
{
  // Surface representation class
  class EllipticalSurface : public narwal::ImplicitSurface
  {
  public:
    EllipticalSurface(double h, double theta)
      : narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1), _h(h), a(0.3),
        b(0.3), beta(0.05), _theta(theta) {}

    // Normal distance from the crack surface
    double f0(const dolfin::Point& p) const
    {
      const double x = std::cos(_theta)*p.x() - std::sin(_theta)*p.y();
      const double y = std::sin(_theta)*p.x() + std::cos(_theta)*p.y();
      const double z = p.z();
      const double d = (x/a)*(x/a) + (z/b)*(z/b) - 1.0;
      return (y - _h - beta*d);
    }

    // If f0 = 0, then if f1 < 1 -> on surface and f1 >= = -> not on surface
    double f1(const dolfin::Point& p) const
    {
      const double x = std::cos(_theta)*p.x() - std::sin(_theta)*p.y();
      const double z = p.z();
      const double d = (x/a)*(x/a) + (z/b)*(z/b);
      if (d < 1.0)
        return -1.0;
      else
        return 1.0;
    }

    // Surface height position (average y)
    const double _h;

    // Ellipse major and minor radii
    const double a, b;

    // Ellipse curvature
    const double beta;

    // Rotate axis
    const double _theta;

  };
}

TEST(Demo, DISABLED_ConformingMesh)
{
  // Create mesh
  auto mesh
    = std::make_shared<dolfin::BoxMesh>(dolfin::Point(-0.4, -0.2, -0.4),
                                        dolfin::Point(0.4, 0.2, 0.4),
                                        25, 25, 25);

  // Create fracture surfaces
  auto surface0 = std::make_shared<EllipticalSurface>(-0.1, 0.0);
  auto surface1 = std::make_shared<EllipticalSurface>(0.1, 0.0);
  auto surface2 = std::make_shared<EllipticalSurface>(0.0, 0.0);
  auto surface3 = std::make_shared<EllipticalSurface>(0.0, DOLFIN_PI/2);

  // Create DomainManager and register surfaces
  auto domain_manager = std::make_shared<narwal::DomainManager>(mesh);
  dolfin_assert(domain_manager);
  domain_manager->register_surface(surface0, "surface0");
  domain_manager->register_surface(surface1, "surface1");
  domain_manager->register_surface(surface2, "surface2");
  domain_manager->register_surface(surface3, "surface3");

  // Create mesh that conforms to crack surfaces
  auto conforming_mesh = narwal::ConformingMeshGenerator::build(*domain_manager);

  // Write mesh to file
  dolfin::XDMFFile conforming_mesh_file(conforming_mesh->mpi_comm(),
                                        "conforming_mesh.xdmf");
  conforming_mesh_file.write(*conforming_mesh);
}
