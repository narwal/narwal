// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

// Unit tests for function in narwal::XFEMTools

#include "gtest_tools.h"

#include <dolfin.h>
#include <Narwal.h>
#include "xamg_elasticity.h"
#include "xamg_poisson.h"
#include <Elasticity3D_P1.h>
#include <Poisson3D_P1.h>

namespace
{
  typedef Elasticity3D_P1::FunctionSpace FuncSpace;
  static const int polynomialDegree = 1;
  static const int numNode = 4;
  static const int numField = 3;
  static const int numEnr = 4;
  static const int max_element_dim = numField*numNode*numEnr;

  // Sub domain for Dirichlet boundary condition
  class EmptyDirichletBoundary : public dolfin::SubDomain
  {
    bool inside(const dolfin::Array<double>& /*x*/, bool /*on_boundary*/) const
    { return false; }
  };

  std::unique_ptr<dolfin::GenericVector> Ax(
      const dolfin::GenericMatrix &A,
      const dolfin::GenericVector &x
      )
  {
    auto b = narwal::la::initSystemVector();
    A.transpmult(x, *b);
    return b;
  }
  // Put any class, functions or static data specific to tests in this file her
}


// Test dimension of elasticity nullspace basis
TEST(XFEMNullSpace, dim_elastic_P1)
{
  // TODO
}


// Test dimension of Poisson nullspace basis
TEST(XFEMNullSpace, dim_poisson_P1)
{
  // TODO
}


// Test that elasticity nullspace vectors are orthogonal
TEST(XFEMNullSpace, orthogonal_elastic_P1)
{
  // TODO
}


// Test that Poisson nullspace vectors are orthogonal
TEST(XFEMNullSpace, orthogonal_poisson_P1)
{
  // TODO
}


// Test that elasticity nullspace vectors are in nullspace (check
// Ax=0)
TEST(XFEMNullSpace, inNullspaceElasticNoCutP1)
{
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  // Mesh that contains surface
  auto mesh = test::xamg_elasticity::createTestBoxMesh();

  // Create standard DOLFIN function space
  auto V_s = std::make_shared<FuncSpace>(mesh);

  // Define variational problem
  auto a = std::make_shared<Elasticity3D_P1::BilinearForm>(V_s, V_s);
  auto L = std::make_shared<Elasticity3D_P1::LinearForm>(V_s);

  // Elasticity parameters (as coefficients to allow spatial variation)
  const double E = 1.0;
  const double nu = 0.2;
  auto mu = std::make_shared<dolfin::Constant>(E/(2.0*(1.0 + nu)));
  auto lambda = std::make_shared<dolfin::Constant>(E*nu/((1.0 + nu)*(1.0 - 2.0*nu)));

  // Attach parameters and force functions
  a->mu = mu;
  a->lmbda = lambda;
  // Create right-hand side
  auto f = std::make_shared<dolfin::Constant>(0.0, -10.0, 0.0);
  L->f = f;

  // Create Dirichlet boundary conditions
  auto db = std::make_shared<::EmptyDirichletBoundary>();
  auto dbv = std::make_shared<test::xamg_elasticity::BoundaryValue>();
  auto dbc = std::make_shared<dolfin::DirichletBC>(V_s, dbv, db);

  // Assemble system
  std::shared_ptr<dolfin::GenericMatrix> A = narwal::la::initSystemMatrix();
  std::shared_ptr<dolfin::GenericVector> b = narwal::la::initSystemVector();
  dolfin::assemble_system(*A, *b, *a, *L, {dbc});
  narwal::la::toMatrixMarketFile(*A, "output/" + gtest_nameroot() + "_A");

  // Create solution Function
  auto u = std::make_shared<dolfin::Function>(V_s);

  // Create null space
  auto null_space = narwal::xamg::NullSpace3DDisplacement(V_s, b, mesh).build();

  EXPECT_NEAR(std::round((*null_space)[0]->sum()), mesh->num_vertices(), 1.0e-2);
  EXPECT_NEAR(std::round((*null_space)[1]->sum()), mesh->num_vertices(), 1.0e-2);
  EXPECT_NEAR(std::round((*null_space)[2]->sum()), mesh->num_vertices(), 1.0e-2);

  // check Ax=0, make sure, no Dirichlet BC have been applied to A
  EXPECT_NEAR( ::Ax( *A, *(*null_space)[0] )->norm("l2"), 0.0, 1.0e-8);
  EXPECT_NEAR( ::Ax( *A, *(*null_space)[1] )->norm("l2"), 0.0, 1.0e-8);
  EXPECT_NEAR( ::Ax( *A, *(*null_space)[2] )->norm("l2"), 0.0, 1.0e-8);
  EXPECT_NEAR( ::Ax( *A, *(*null_space)[3] )->norm("l2"), 0.0, 1.0e-8);
  EXPECT_NEAR( ::Ax( *A, *(*null_space)[4] )->norm("l2"), 0.0, 1.0e-8);
  EXPECT_NEAR( ::Ax( *A, *(*null_space)[5] )->norm("l2"), 0.0, 1.0e-8);

}


// Test that elasticity nullspace vectors are in nullspace (check
// Ax=0)
TEST(XFEMNullSpace, inNullspaceElasticOneCutP1)
{
  // TODO
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  test::xamg_elasticity::ElasticityTestSetup<FuncSpace>
    s("output/" + gtest_nameroot(), polynomialDegree, max_element_dim, false);

  auto dofmap = s.V->function_space()->dofmap();
  auto tdofmap = std::dynamic_pointer_cast<const narwal::DofMap>(dofmap);

  // Create null space
  auto null_space
    = narwal::xamg::NullSpace3DDisplacement2(s.V->function_space(), tdofmap,
                                             s.b, s.domainManager->mesh()).build();

  EXPECT_NEAR(std::round((*null_space)[0]->sum()), s.domainManager->mesh()->num_vertices(), 1.0e-2);
  EXPECT_NEAR(std::round((*null_space)[1]->sum()), s.domainManager->mesh()->num_vertices(), 1.0e-2);
  EXPECT_NEAR(std::round((*null_space)[2]->sum()), s.domainManager->mesh()->num_vertices(), 1.0e-2);

  // check Ax=0, make sure, no Dirichlet BC have been applied to A
  EXPECT_NEAR( ::Ax( *s.A, *(*null_space)[0] )->norm("l2"), 0.0, 1.0e-8);
  EXPECT_NEAR( ::Ax( *s.A, *(*null_space)[1] )->norm("l2"), 0.0, 1.0e-8);
  EXPECT_NEAR( ::Ax( *s.A, *(*null_space)[2] )->norm("l2"), 0.0, 1.0e-8);
  EXPECT_NEAR( ::Ax( *s.A, *(*null_space)[3] )->norm("l2"), 0.0, 1.0e-8);
  EXPECT_NEAR( ::Ax( *s.A, *(*null_space)[4] )->norm("l2"), 0.0, 1.0e-8);
  EXPECT_NEAR( ::Ax( *s.A, *(*null_space)[5] )->norm("l2"), 0.0, 1.0e-8);
}


// Test that Poisson nullspace vectors are in nullspace (check Ax=0)
TEST(XFEMNullSpace, DISABLED_in_nullspace_poisson_P1)
{
  // TODO
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  test::xamg_poisson::PoissonTestSetup<Poisson3D_P1::FunctionSpace> s(
      "output/" + gtest_nameroot(), polynomialDegree, max_element_dim);

  auto dofmap = s.V->function_space()->dofmap();
  auto tdofmap = std::dynamic_pointer_cast<const narwal::DofMap>(dofmap);

  // Create system matrix and rhs vector
  narwal::la::SolverXAMG xSolver(s.A, s.b, s.u, s.V->function_space(),
    s.domainManager, tdofmap, s.xfem_forms, true, true,
    "output/" + gtest_nameroot());

  // Create null space
  xSolver.null_space =
      narwal::xamg::NullSpace3DScalarSolution(s.V->function_space(), s.b).build();

  EXPECT_NEAR(std::round((*xSolver.null_space)[0]->sum()), s.domainManager->mesh()->num_vertices(), 1.0e-2);
}
