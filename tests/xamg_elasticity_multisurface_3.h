// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//         Axel Gerstenberger
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#ifndef NARWAL_TEST_XAMG_ELASTICITY_MULTISURFACE_H
#define NARWAL_TEST_XAMG_ELASTICITY_MULTISURFACE_H

#include <set>
#include <dolfin.h>
#include <ElasticityProblem3D.h>
#include <Narwal.h>

namespace test
{
  namespace xamg_elasticity_ms_3
  {
/*
    static std::shared_ptr<dolfin::Mesh> buildOneCellMesh()
    {
      auto mesh = std::make_shared<dolfin::Mesh>();

      dolfin::MeshEditor me;
      me.open(*mesh, 3, 3);
      me.init_vertices(4);

      dolfin::Point(-1.0, -1.0, -0.5);
      dolfin::Point(1.0, 1.0, 0.5);
      int index = 0;
      me.add_vertex(index++, -1.0, -1.0,  0.5);
      me.add_vertex(index++,  1.0, -1.0,  0.5);
      me.add_vertex(index++, -1.0,  1.0,  0.5);
      me.add_vertex(index++, -1.0, -1.0, -0.5);
      me.init_cells(1);

      int eleIndex = 0;
      me.add_cell(eleIndex++, 0, 1, 2, 3);
      me.close();

      dolfin::File file("oneCellMesh.pvd");
      file << *mesh;

      return mesh;
    }
*/
    // Pressure on crack surface
    class CrackPressure : public dolfin::Expression
    {
      void eval(dolfin::Array<double>& values, const dolfin::Array<double>& /*x*/) const
      { values[0] = 0.0; }
    };

    const double MX = 1.0;
    const double MY = 1.0;
    const double MZ = 1.0;

    const double DZone = 0.3; // Dirichlet zone

    template<int PX, int PY, int PZ>
    class DomainDirichlet : public dolfin::SubDomain { public:
      bool inside(const dolfin::Array<double>& x, bool) const {
        return (
            (PX*x[0] > ((MX)-(DZone))) &&
            (PY*x[1] > ((MY)-(DZone))) &&
            (PZ*x[2] > ((MZ)-(DZone)))
               );
      }
    };

    template<int PX, int PY, int PZ>
    std::shared_ptr<DomainDirichlet<PX,PY,PZ>> BDom() {
      return std::make_shared<DomainDirichlet<PX,PY,PZ>>();
    }

    std::shared_ptr<dolfin::Constant> BVal(double dx, double dy, double dz) {
      return std::make_shared<dolfin::Constant>(dx, dy, dz);
    }

    template<int PX, int PY, int PZ>
    std::shared_ptr<dolfin::DirichletBC> BCond(
        std::shared_ptr<dolfin::FunctionSpace> V_s,
        double dx, double dy, double dz) {
      return std::make_shared<dolfin::DirichletBC>(
          V_s, BVal( dx, dy, dz), BDom<PX,PY,PZ>());
    }

    // Surface representation class
    class FlatSurfaceRotateAroundZ : public narwal::ImplicitSurface
    {
    public:
      FlatSurfaceRotateAroundZ(double theta_, double shift_) :
        narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1),
        pi(DOLFIN_PI),
        theta(theta_*pi/180.0), shift(shift_),
        sin_theta(std::sin(theta)), cos_theta(std::cos(theta)) {}

      double f0(const dolfin::Point& p) const
      {
        const double y = (p.x()-shift)*sin_theta + (p.y()-shift)*cos_theta;
        return y;
      }

      double f1(const dolfin::Point& /*p*/) const { return -1.0; }

      /// Determine normal vector of surface pointing in plus direction
      std::vector<double> normal(const dolfin::Point /*p*/,
                                 double /*eps*/) const
      {
        std::vector<double> n(3);
        n[0] = 1.0*sin_theta;
        n[1] = 1.0*cos_theta;
        n[2] = 0.0;
        return n;
      }

    private:
      const double pi;
      const double theta, shift;
      const double sin_theta, cos_theta;
    };

    class FlatSurfaceRotateAroundX : public narwal::ImplicitSurface
    {
    public:
      FlatSurfaceRotateAroundX(double theta_, double shift_) :
        narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1),
        pi(DOLFIN_PI),
        theta(theta_*pi/180.0), shift(shift_),
        sin_theta(std::sin(theta)), cos_theta(std::cos(theta)) {}

      double f0(const dolfin::Point& p) const
      {
        const double y = (p.z()-shift)*sin_theta + (p.y()-shift)*cos_theta;
        return y;
      }

      double f1(const dolfin::Point& /*p*/) const { return -1.0; }

      /// Determine normal vector of surface pointing in plus direction
      std::vector<double> normal(const dolfin::Point /*p*/,
                                 double /*eps*/) const
      {
        std::vector<double> n(3);
        n[0] = 0.0;
        n[1] = 1.0*cos_theta;
        n[2] = 1.0*sin_theta;
        return n;
      }

    private:
      const double pi;
      const double theta, shift;
      const double sin_theta, cos_theta;
    };

    template<class FuncSpace>
    class ElasticityTestSetup
    {
    public:

      std::shared_ptr<narwal::DomainManager> domain_manager;
      std::shared_ptr<dolfin::FunctionSpace> V_s;
      std::shared_ptr<narwal::XFEMFunctionSpace> V;
      std::shared_ptr<narwal::XFEMForms> xfem_forms;
      std::shared_ptr<narwal::XFEMProblem> elasticity_problem;
      std::shared_ptr<dolfin::GenericMatrix> A;
      std::shared_ptr<dolfin::GenericVector> b;
      std::shared_ptr<dolfin::Function> u;

      ElasticityTestSetup(
        const std::string& testname,
        const int polynomialDegree,
        const int max_element_dim,
        const bool applyDBC = true)
      {
        // Create mesh
        auto mesh //= buildOneCellMesh();
          = std::make_shared<dolfin::BoxMesh>(dolfin::Point(-MX, -MY, -MZ),
                                              dolfin::Point( MX,  MY,  MZ),
                                              7, 7, 7);

        // Create fracture surfaces
        auto surface0 = std::make_shared<test::xamg_elasticity_ms_3::FlatSurfaceRotateAroundZ>(0.0, 0.02);
        auto surface1 = std::make_shared<test::xamg_elasticity_ms_3::FlatSurfaceRotateAroundZ>(90.0, 0.05);
        auto surface2 = std::make_shared<test::xamg_elasticity_ms_3::FlatSurfaceRotateAroundX>(90.0, 0.03);

        // Create DomainManager
        domain_manager = std::make_shared<narwal::DomainManager>(mesh);
        domain_manager->register_surface(surface0, "surface0");
        domain_manager->register_surface(surface1, "surface1");
        domain_manager->register_surface(surface2, "surface2");

        // Create mesh that conforms to crack surfaces
        std::shared_ptr<dolfin::Mesh> conforming_mesh
          = narwal::ConformingMeshGenerator::build(*domain_manager);

        // Write mesh to file
        dolfin::XDMFFile conforming_mesh_file(mesh->mpi_comm(),
                                              testname+"conforming_mesh.xdmf");
        conforming_mesh_file.write(*conforming_mesh);

        std::cout << "create function space" << std::endl;
        // Create standard DOLFIN function space
        V_s = std::make_shared<FuncSpace>(mesh);

        // Create XFEM space (this computes the enriched dofmap)
        V = std::make_shared<narwal::XFEMFunctionSpace>(V_s, domain_manager,
                                                        max_element_dim);

        // Elasticity parameters (as coefficients to allow spatial variation)
        const double E = 100.0;
        const double nu = 0.0;
        auto mu = std::make_shared<dolfin::Constant>(E/(2.0*(1.0 + nu)));
        auto lambda = std::make_shared<dolfin::Constant>(E*nu/((1.0 + nu)*(1.0 - 2.0*nu)));

        // Create pressure loading
        auto pressure = std::make_shared<const dolfin::Constant>(0.0);

        std::cout << "Create XFEM elasticity forms" << std::endl;
        // Create XFEM elasticity forms
        xfem_forms = std::make_shared<narwal::ElasticityProblem3D>(V, lambda,
                                                                   mu, pressure,
                                                                   polynomialDegree);

        // Create Dirichlet boundary conditions
        std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs;
        if (applyDBC) {
          bcs.push_back(BCond< 1, 1, 1>(V_s,  1.0,  1.0,  1.0));
          bcs.push_back(BCond<-1, 1, 1>(V_s,  0.0,  0.5,  0.5));
          bcs.push_back(BCond< 1,-1, 1>(V_s,  1.0,  0.0,  1.0));
          bcs.push_back(BCond<-1,-1, 1>(V_s,  0.0,  0.0,  0.0));
          bcs.push_back(BCond< 1, 1,-1>(V_s,  1.0,  1.0,  0.0));
          bcs.push_back(BCond<-1, 1,-1>(V_s,  0.0,  1.5,  0.0));
          bcs.push_back(BCond< 1,-1,-1>(V_s,  1.0,  0.0,  0.0));
          bcs.push_back(BCond<-1,-1,-1>(V_s,  0.0,  0.0,  0.0));
        }

        std::cout << "Create XFEM problem" << std::endl;
        // Create XFEM problem
        elasticity_problem = std::make_shared<narwal::XFEMProblem>(xfem_forms, bcs);

        // Assemble system
        A = narwal::la::initSystemMatrix();
        b = narwal::la::initSystemVector();
        std::cout << "Assemble system" << std::endl;
        elasticity_problem->assemble(*A, *b);
        narwal::la::toMatrixMarketFile(*A, testname + "_A");

        // Create solution Function
        u = std::make_shared<dolfin::Function>(V->function_space());
      }
    };
  }
}

#endif
