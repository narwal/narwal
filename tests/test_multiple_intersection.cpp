// Copyright (C) 2016 Melior Innovations
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "gtest_tools.h"

#include <dolfin.h>
#include "Narwal.h"
#include "gtest_tools.h"

namespace
{
  // Surface representation class
  class EllipticalSurface : public narwal::ImplicitSurface
  {
  public:
    EllipticalSurface(double theta_) :
      narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1),
      a(0.4), b(0.3), beta(0.05), theta(theta_) {}

    double f0(const dolfin::Point& p) const
    {
      const double x =  p.x()*cos(theta) + p.y()*sin(theta);
      const double y = -p.x()*sin(theta) + p.y()*cos(theta);
      const double z = p.z();
      const double d = (x/a)*(x/a) + (z/b)*(z/b) - 1.0;
      return (y - beta*d);
    }

    double f1(const dolfin::Point& p) const
    {
      const double x = p.x()*cos(theta) + p.y()*sin(theta);
      const double z = p.z();
      const double d = (x/a)*(x/a) + (z/b)*(z/b);
      if (d < 1.0)
        return -1.0;
      else
        return 1.0;
    }

  private:
    const double a, b, beta, theta;
  };
}


TEST(MultipleIntersection, block)
{
  // Create mesh
  const int n = 15;
  std::shared_ptr<dolfin::Mesh>
    mesh(new dolfin::BoxMesh(dolfin::Point(-1, -1, -1), dolfin::Point(1, 1, 1),
                             n, n, n));
  const double th1 =  M_PI/16.0;
  const double th2 = -M_PI/16.0;

  // Create surfaces
  std::shared_ptr<narwal::GenericSurface> surface0(new ::EllipticalSurface(th1));
  std::shared_ptr<narwal::GenericSurface> surface1(new ::EllipticalSurface(th2));

  // Compute in-cell surface itersections
  std::size_t intersect = 0, non_intersect = 0;
  for (dolfin::CellIterator c(*mesh); !c.end(); ++c)
  {
    const std::pair<bool, bool> intersect_data0 = surface0->entity_intersection(*c);
    const std::pair<bool, bool> intersect_data1 = surface1->entity_intersection(*c);
    bool intersect0 = intersect_data0.first and !intersect_data0.second;
    bool intersect1 = intersect_data1.first and !intersect_data1.second;

    if (intersect0 && intersect1)
    {
      bool intersect_in_cell = narwal::GeometryTools::surfaces_intersect(*c, *surface0, *surface1);
      if (intersect_in_cell)
        intersect++;
      else
        non_intersect++;
    }
  }

  // Test in-cell itersections
  ASSERT_EQ(intersect, std::size_t(12));
  ASSERT_EQ(non_intersect, std::size_t(47));
}
