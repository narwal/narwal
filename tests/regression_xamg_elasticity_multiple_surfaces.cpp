// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//         Axel Gerstenberger
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.


#include <ElasticityFunction.h>
#include "xamg_elasticity_multisurface.h"
#include <Elasticity3D_P1.h>

#include "gtest_tools.h"

namespace
{
  const double solution_l2 = 36.551333771855035;

  static const int polynomialDegree = 1;
  static const int max_element_dim = 48;
  typedef Elasticity3D_P1::FunctionSpace FuncSpace;
}

TEST(xamg, MultiSurfaceElasticityPetscDirectSolve)
{
  dolfin::parameters["allow_extrapolation"] = true;
  dolfin::parameters["refinement_algorithm"] = "plaza_with_parent_facets";
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  test::xamg_elasticity_ms::ElasticityTestSetup<Elasticity3D_P1::FunctionSpace>
    s("output/" + gtest_nameroot(), polynomialDegree, max_element_dim);

  dolfin::LUSolver solver;
  solver.solve(*s.A, *s.u->vector(), *s.b);

  ASSERT_NEAR(solution_l2, s.u->vector()->norm("l2"), 1.0e-8);
}

TEST(xamg, MultiSurfaceElasticityPetscGeneralAMG)
{
  dolfin::parameters["allow_extrapolation"] = true;
  dolfin::parameters["refinement_algorithm"] = "plaza_with_parent_facets";
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  test::xamg_elasticity_ms::ElasticityTestSetup<Elasticity3D_P1::FunctionSpace>
    s("output/" + gtest_nameroot(), polynomialDegree, max_element_dim);

  auto dofmap = s.V->function_space()->dofmap();
  auto tdofmap = std::dynamic_pointer_cast<const narwal::DofMap>(dofmap);

  // Create null space
  auto null_space =
      narwal::xamg::NullSpace3DDisplacement2(
        s.V->function_space(), tdofmap, s.b, s.domain_manager->mesh()).build();

  // Create preconditioner and attach null space
  auto pc = std::make_shared<dolfin::PETScPreconditioner>("petsc_amg"); // use "ilu", "petsc_amg", or "ml_amg"
  pc->set_nullspace(*null_space);
  pc->parameters["report"] = true;

  // Create solver
  dolfin::PETScKrylovSolver solver("gmres", pc);
  solver.parameters["report"] = true;
  solver.parameters["monitor_convergence"] = true;
  solver.parameters["relative_tolerance"] = 1.0e-8;
  solver.parameters["convergence_norm_type"] = "true";

  solver.solve(*s.A, *s.u->vector(), *s.b);

  ASSERT_NEAR(solution_l2, s.u->vector()->norm("l2"), 1.0e-4);
}

TEST(xamg, MultiSurfaceElasticityPetscTransformPetscAMG)
{
  dolfin::parameters["allow_extrapolation"] = true;
  dolfin::parameters["refinement_algorithm"] = "plaza_with_parent_facets";
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  test::xamg_elasticity_ms::ElasticityTestSetup<Elasticity3D_P1::FunctionSpace>
    s("output/" + gtest_nameroot(), polynomialDegree, max_element_dim);

  auto dofmap = s.V->function_space()->dofmap();
  auto tdofmap = std::dynamic_pointer_cast<const narwal::DofMap>(dofmap);

  // Create system matrix and rhs vector
  narwal::la::SolverXAMG xSolver(s.A, s.b, s.u, s.V->function_space(),
                                 s.domain_manager, tdofmap, s.xfem_forms,
                                 true, true, "output/" + gtest_nameroot());

  // Create null space
  xSolver.null_space =
      narwal::xamg::NullSpace3DDisplacement2(
        s.V->function_space(), tdofmap, s.b, s.domain_manager->mesh()).build();

  // Create preconditioner & solver
  xSolver.setupDefaultPreCondAndSolver("petsc_amg"); // use "ilu", "petsc_amg", or "ml_amg"

  // Solve
  EXPECT_LT(xSolver.solve(), std::size_t(100));

  EXPECT_NEAR(solution_l2, s.u->vector()->norm("l2"), 1.0e-5);

  // Create mesh that conforms to crack surface and write to file
  auto conforming_mesh = narwal::ConformingMeshGenerator::build(*s.domain_manager);
  dolfin::XDMFFile conforming_mesh_file(MPI_COMM_WORLD,
                                        "output/" + gtest_nameroot()
                                        + "-conforming-mesh.xdmf");
  conforming_mesh_file.write(*conforming_mesh);

//  narwal::ElasticFunction::write_crack_conforming_solution<Elasticity3D_P1::FunctionSpace>(
//    *s.domain_manager, *s.xfem_forms, *s.u, gtest_nameroot());
}
