#!/usr/bin/env python

# Cuthill-McKee ordering of matrices
# The reverse Cuthill-McKee algorithm gives a sparse matrix ordering that
# reduces the matrix bandwidth.
# Requires NumPy
# Copyright (C) 2011 by
# Aric Hagberg <aric.hagberg@gmail.com>
# BSD License
import networkx as nx
from networkx.utils import reverse_cuthill_mckee_ordering
import numpy as np
import numpy.linalg as la
import matplotlib.pylab as pl
import scipy.sparse as sps
import apgl
from apgl.graph import SparseGraph
import random
import argparse
import scipy.io


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("filename", help="filename with matrix in Matrix Market format")
    parser.add_argument("-r", "--reorder", action='store_const', const=True, help="reorder matrix to minimal bandwidth")
    parser.add_argument("-t", type=float, 
                        help="thresholding epsilon", default=1.0e-12)
    args = parser.parse_args()
    print(args.filename)
    
    A = scipy.io.mmread(args.filename)
    
    if args.reorder:
        print("Reordering matrix:", A.shape[0] ,"x", A.shape[1])
        
        print(" Thresholding...")
        print args.t
        t = args.t
        B = A.tocoo()
        A = sps.lil_matrix((B.shape[0], B.shape[1]))
        for i,j,v in zip(B.row, B.col, B.data):
            if abs(v) > t:
                A[i,j] = v
        
        print(" Compute Reverse Cuthill-McGee ordering...")
        graph = SparseGraph(A.shape[0], W=A.tocsr())
        rcm = list(reverse_cuthill_mckee_ordering(graph.toNetworkXGraph()))    
        print(" Transform matrix for output...")
        Adense = A.todense()
        if la.norm(Adense.transpose() - Adense, np.inf) < 1.0e-8:
            print("Matrix is symmetric before transformation")
        print "Max. Matrix entry:", scipy.linalg.norm(Adense,2)
        Adense[:,:] = Adense[rcm,:]
        Adense[:,:] = Adense[:,rcm]
        A = sps.csr_matrix(Adense)
    
        if la.norm(Adense.transpose() - Adense, np.inf) < 1.0e-8:
            print("Matrix is symmetric after transformation")
    
    print("Plotting matrix...")
    if A.shape[0] <= 128:
        Adense = A.transpose().todense()
        fig, ax = pl.subplots()
        count = 0;
        x = []
        y = []
        close = []
        volume = []
        for i in range(A.shape[0]):
            for j in range(A.shape[1]):
                if abs(Adense[i,j]) > abs(args.t):
                    x.append(i)
                    y.append(A.shape[0] - j)
                    if Adense[i,j] > abs(args.t):
                        close.append('b')
                    else:
                        close.append('r')
                    volume.append(20)
                
        ax.scatter(x, y, c=close, s=20, marker='o', alpha=1.0)
        ax.grid(True)
        fig.tight_layout()
        pl.axis('equal')
    else:
        pl.spy(A, precision=1.0e-12, markersize=1)
    pl.show()

if __name__ == "__main__":
    main()
