# Cuthill-McKee ordering of matrices
# The reverse Cuthill-McKee algorithm gives a sparse matrix ordering that
# reduces the matrix bandwidth.
# Requires NumPy
# Copyright (C) 2011 by
# Aric Hagberg <aric.hagberg@gmail.com>
# BSD License
import networkx as nx
from networkx.utils import reverse_cuthill_mckee_ordering
import numpy as np
import matplotlib.pylab as pl
import scipy.sparse as sps
import apgl
from apgl.graph import SparseGraph
import random



numVal = 4
A = sps.csr_matrix((numVal,numVal))
A[0,0] = 1.0
A[2,2] = 1.0
A[2,0] = 1.0
A[0,2] = 1.0
A[1,1] = 2.0
A[3,3] = 2.0
A[3,1] = 2.0
A[1,3] = 2.0

graph = SparseGraph(numVal, W=A)
rcm = list(reverse_cuthill_mckee_ordering(graph.toNetworkXGraph()))    
print("ordering",rcm)

Adense = A.todense()
Adense[:,:] = Adense[rcm,:]
Adense[:,:] = Adense[:,rcm]
pl.spy(sps.csr_matrix(Adense))
pl.show()

# 2 blocks or irregular matrix entries
A00 = sps.rand(20,20, density=0.4)
A11 = sps.rand(10,10, density=0.4)
AAA = sps.block_diag((A00,A11)).todense()
# shuffle blocks
a = list(range(30))
random.shuffle(a)
AAA[:,:] = AAA[a,:]
AAA[:,:] = AAA[:,a]

pl.spy(AAA)
pl.show()

# reorder again
graph = SparseGraph(30, W=sps.csr_matrix(AAA))
rcm = list(reverse_cuthill_mckee_ordering(graph.toNetworkXGraph()))    
print("ordering",rcm)

AAA[:,:] = AAA[rcm,:]
AAA[:,:] = AAA[:,rcm]

pl.spy(sps.csr_matrix(AAA))
pl.show()

