#!/bin/bash -e

set -x

WORKSPACE=$1

chmod +x ${WORKSPACE}/tpl/gcovr/gcovr
cd ${WORKSPACE}
${WORKSPACE}/tpl/gcovr/gcovr -r . -e "${WORKSPACE}/tpl" -e "${WORKSPACE}/build" -e "${WORKSPACE}/tests" -x > coverage.xml

