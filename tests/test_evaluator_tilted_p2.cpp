// Copyright (C) 2016 Melior Innovations
// Author: Axel Gerstenberger
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "gtest_tools.h"

#include <dolfin.h>
#include <Elasticity3D_P2.h>
#include <ElasticityFunction.h>
#include <ElasticityProblem3D.h>
#include <Narwal.h>
#include <Evaluator.h>



namespace
{
  static const double modelShiftY = 0.02;
  static const double ly = 1.0;

  typedef Elasticity3D_P2::FunctionSpace FuncSpace;
  static const int polynomialDegree = 2;
  static const int numNode = 10;
  static const int numField = 3;
  static const int numEnr = 4;
  static const int max_element_dim = numField*numNode*numEnr;

  // Pressure on crack surface
  class CrackPressure : public dolfin::Expression
  {
    void eval(dolfin::Array<double>& values, const dolfin::Array<double>& /*x*/) const
    {
      values[0] = 0.0; //-2.0*(x[0]*x[0] + x[2]*x[2] + 1.0);
    }
  };

  // Dirichlet boundary condition function
  class BoundaryValue : public dolfin::Expression
  {
  public:

    BoundaryValue() : Expression(3) {}

    void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
    {
      if (x[1] < 0.0)
      {
        // move lower part down
        values[0] = 0.0;
        values[1] = -0.5;
        values[2] = 0.0;
      }
      else
      {
        // move upper part up and to the right
        values[0] = 1.0;
        values[1] = 0.5 + 0.1*x[2];
        values[2] = 0.0;
      }
    }
  };

  // Sub domain for Dirichlet boundary condition
  class DirichletBoundary : public dolfin::SubDomain
  {
    bool inside(const dolfin::Array<double>& x, bool /*on_boundary*/) const
    {
      return std::abs(x[1] - modelShiftY + ly) < DOLFIN_EPS  // bottom
          || std::abs(x[1] - modelShiftY - ly) < DOLFIN_EPS; // top
    }
  };


  // Surface representation class
  class EllipticalSurface : public narwal::ImplicitSurface
  {
  public:
    EllipticalSurface()
      : narwal::ImplicitSurface(dolfin::Point(0.0, 0.0, 0.0), 3.1), a(5.0),
        b(5.0), beta(0.0) {}
//        b(0.5), beta(0.05) {}

    // Normal distance from the crack surface
    double f0(const dolfin::Point& p) const
    { return p.y(); }

    // If f0 = 0, then if f1<1 -> on surface and f1 >= = -> not on surface
    double f1(const dolfin::Point& p) const
    {
      const double x = p.x();
      const double z = p.z();
      const double d = (x/a)*(x/a) + (z/b)*(z/b);
      if (d < 1.0)
        return -1.0;
      else
        return 1.0;
    }

    /// Determine normal vector of surface pointing in plus direction
    std::vector<double> normal(const dolfin::Point /*p*/,
                               double /*eps*/) const
    {
      std::vector<double> n(3);
      n[0] = 0.0;
      n[1] = 1.0;
      n[2] = 0.0;
      return n;
    }

    // Ellipse major and minor radii
    const double a, b;

    // Ellipse curvature
    const double beta;
  };
}

TEST(Evaluator, evaluateTiltedP1)
{
  // NOTE: Make sure that the number of cells in the y-direction is
  //       odd, otherwise surface lies precisely on cell boundary.
  //       Create box domain mesh that contains surface
  auto mesh
    = std::make_shared<dolfin::BoxMesh>(dolfin::Point(-2.0, -ly + modelShiftY, -2.0),
                                        dolfin::Point( 2.0,  ly + modelShiftY,  2.0),
                                        1, 5, 1);

  // Create fracture surface
  auto surface = std::make_shared<::EllipticalSurface>();

  // Create DomainManager
  auto domain_manager = std::make_shared<narwal::DomainManager>(mesh);
  domain_manager->register_surface(surface, "surface0");

  // Create standard function space
  auto V_s = std::make_shared<FuncSpace>(mesh);

  // Create XFEM space holder (this computes the enriched dofmap)
  auto V = std::make_shared<narwal::XFEMFunctionSpace>(V_s, domain_manager,
                                                       max_element_dim);

  std::cout << "Dimension of regular function space: "
            << V_s->dim() << std::endl;

  std::cout << "Dimension of enriched function space: "
            << V->dim() << std::endl;

  // Elasticity parameters (as coefficients to allow spatial variation)
  const double E = 1.0;
  const double nu = 0.2;
  auto mu = std::make_shared<dolfin::Constant>(E/(2.0*(1.0 + nu)));
  auto lambda
    = std::make_shared<dolfin::Constant>(E*nu/((1.0 + nu)*(1.0 - 2.0*nu)));

  // Crack pressure function
  auto crack_pressure = std::make_shared<::CrackPressure>();

  // Create XFEM elasticity forms
  auto xfem_forms =
    std::make_shared<narwal::ElasticityProblem3D>(V, lambda, mu,
                                                  crack_pressure,
                                                  polynomialDegree);

  // Create Dirichlet boundary conditions
  auto db = std::make_shared<::DirichletBoundary>();
  auto dbv = std::make_shared<::BoundaryValue>();
  auto dbc = std::make_shared<dolfin::DirichletBC>(V_s, dbv, db);
  std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs;
  bcs.push_back(dbc);

  // Create XFEM problem
  auto problem = std::make_shared<narwal::XFEMProblem>(xfem_forms, bcs);

  // Assemble system
  dolfin::PETScMatrix A;
  dolfin::PETScVector b;
  problem->assemble(A, b);

  std::cout << "LHS norm: " << A.norm("frobenius")  << std::endl;
  std::cout << "RHS norm: " << b.norm("l2")  << std::endl;

  // Create solution Function
  auto u = std::make_shared<dolfin::Function>(V->function_space());

  // Solve
  #ifdef PETSC_HAVE_SUPERLU
  dolfin::LUSolver solver("superlu");
  #else
  dolfin::LUSolver solver;
  #endif
  std::cout << "Size: " << u->vector()->size() << std::endl;
  std::cout << "Solver Ax=b. This can take some time for large problems . . ."
    << std::endl;
  solver.solve(A, *u->vector(), b);
  std::cout << "Solution vector norm: " << u->vector()->norm("l2")
            << std::endl;
  std::cout << "Max vector entry: " << u->vector()->max() << std::endl;
  std::cout << "Min vector entry: " << u->vector()->min() << std::endl;

  EXPECT_NEAR( 1.2, u->vector()->max(), 1.0e-8);
  EXPECT_NEAR(-1.2, u->vector()->min(), 1.0e-8);

  // Write (nodal) solution to PVD file to visualise with ParaView
  dolfin::XDMFFile file_u(mesh->mpi_comm(),
                          "output/"+gtest_nameroot()+"-u.xdmf");
  file_u.write(*u);

  narwal::ElasticFunction::write_crack_conforming_solution<FuncSpace>(
    *domain_manager, *xfem_forms, *u, gtest_nameroot());

  auto surface_mesh = std::make_shared<dolfin::Mesh>("data/PlanarCircleY_gmshisosurface.xml");
  {
    dolfin::XDMFFile file1(surface_mesh->mpi_comm(), "output/"
                           + gtest_nameroot()+"-surface_mesh.xdmf");
    file1.write(*surface_mesh);
  }


  { // evaluate gap with mesh
    u->set_allow_extrapolation(true);
    narwal::Evaluator evaluator(u, V->dofmap(), domain_manager, xfem_forms);
    const std::unique_ptr<dolfin::Function> gap_function
      = evaluator.evaluate_discontinuity(surface_mesh, 0);
    EXPECT_NEAR(1.0499874478810001 , gap_function->vector()->max(), 1.0e-8);
    EXPECT_NEAR(0.95000767473129977, gap_function->vector()->min(), 1.0e-8);

    dolfin::XDMFFile file1(surface_mesh->mpi_comm(), "output/" +
                           gtest_nameroot()+"-surface_gap_function.xdmf");
    file1.write(*gap_function);
  }


}
