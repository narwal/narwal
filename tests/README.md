# Unit tests for Narwal

This directory contains unit and integration tests for the Narwal
library. Narwal uses the Google Test (gtest) framework
(<https://code.google.com/p/googletest/>).


## Building the tests

Change into the `./tests` directory and build the tests using
`./cmake.local`.


## Running the tests

Change into the resulting `./tests/build` directory and execute the
tests either all tests using

    ./narwal_google_tests

or only a subset of the tests using

    ./narwal_google_tests --gtest_filter=DomainManager*

To skip long running tests, we mark such tests with the keyword 'Slow'.
That way you can skip them by negative filtering (note the '-' in front),
e.g. during development, with

    ./narwal_google_tests --gtest_filter=-*Slow*
    

    
You can use `--help` to see more options or search the GoogleTest
online help for details.


## Creating tests

Creating tests is usually as simple as replacing `int main(...) {...}`
with `TEST(YourTestName, YourSpecificTestname) {...}`.  In addition,
you need to include the Google Test header, for which you can use a
convinience function `#include "gtest_tools.h"`.

If you want to run just your test, you call the test executable as

    ./build/narwal_google_test --gtest_filter=YourTestName.YourSpecificTestname

You can have as many `TEST()` functions as you like in your .cpp file.

## Naming conventions for unit tests

For unit tests file names, please use `Tests` followed by the class or
filename you want to test. For example, use `TestsDomainManager.cpp`
to write tests for the DomainManager class in
`DomainManager.cpp`. Inside the the test file, name the tests as

    TEST(DomainManager, mesh)
    {
      // Test body
    }

to write a test for the `mesh()` method.
