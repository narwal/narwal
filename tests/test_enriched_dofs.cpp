// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "gtest_tools.h"

#include <dolfin.h>
#include <Narwal.h>
#include <ElasticityProblem3D.h>
#include <Elasticity3D_P1.h>

namespace
{
  // Flat surface with normal (0, 0, 1) and passing through (0.5, 0.5,
  // 0.5)
  class SimpleSurface : public narwal::ImplicitSurface
  {
  public:
    SimpleSurface(double r)
      : narwal::ImplicitSurface(dolfin::Point(0.5, 0.5, 0.51), 5.0*5), _r0(r) {}

    // f0 is normal distance from the crack surface. If f0 = 0, then
    // if f1<1 -> on surface and f1 >= = -> not on surface
    double f0(const dolfin::Point& p) const { return p.z() - 0.51; }
    double f1(const dolfin::Point& p) const
    {
      //const double r2 = (p.x()-0.5)*(p.x()-0.5) + (p.y()-0.5)*(p.y()-0.5);
      //return std::sqrt(r2) - _r0;
      if (std::abs(p.x()-0.5) > _r0 or std::abs(p.y()-0.5) > _r0)
        return 1.0;
      else
        return -1.0;
    }
  private:
    const double _r0;
  };

  std::shared_ptr<narwal::XFEMFunctionSpace>
  create_xfem_space(std::shared_ptr<dolfin::Mesh> mesh, double r)
  {
    dolfin_assert(mesh);
    std::shared_ptr<SimpleSurface> surface(new SimpleSurface(r));
    auto domain_manager = std::make_shared<narwal::DomainManager>(mesh);
    domain_manager->register_surface(surface, "surface0");

    // Create standard DOLFIN function space and XFEM space
    std::shared_ptr<dolfin::FunctionSpace>
      V_s(new Elasticity3D_P1::FunctionSpace(mesh));
    EXPECT_EQ(V_s->dim(), 3*mesh->num_vertices());

    std::shared_ptr<narwal::XFEMFunctionSpace>
      V(new narwal::XFEMFunctionSpace(V_s, domain_manager, 24));
    return V;
  }
}

// Test dimension of enriched space when fully intersected
TEST(enriched_dofs, cube_simple_full_intersection)
{
  // Create XFEM space for surface that extends through entire domain
  std::size_t n = 1;
  std::shared_ptr<dolfin::Mesh> mesh(new dolfin::UnitCubeMesh(n, n, n));
  auto V = create_xfem_space(mesh, 10.0);
  std::size_t dim = 3*(n+1)*(n+1)*(n+1) + 3*(n+1)*(n+1)*(1+1);
  EXPECT_EQ(dim, V->dim());

  n = 12;
  mesh.reset(new dolfin::UnitCubeMesh(n, n, n));
  V = create_xfem_space(mesh, 10.0);
  dim = 3*(n+1)*(n+1)*(n+1) + 3*(n+1)*(n+1)*(1+1);
  EXPECT_EQ(dim, V->dim());
}

// Test dimension of enriched space when partially intersected
TEST(enriched_dofs, cube_simple_partial_intersection)
{
  // Create XFEM space for surface that extends through part of domain
  // such that dofs should be enriched
  std::shared_ptr<dolfin::Mesh> mesh(new dolfin::UnitCubeMesh(1, 1, 1));
  auto V = create_xfem_space(mesh, 0.2);
  EXPECT_EQ((std::size_t)24, V->dim());

  mesh.reset(new dolfin::UnitCubeMesh(12, 12, 12));
  //const double delta = 1.0/12.0;
  V = create_xfem_space(mesh, 0.2);
  //const std::size_t num_intersected cells = 1;

  std::size_t n = 2;
  std::size_t dim = 3*(n+1)*(n+1)*(n+1) + 3*(n+1)*(n+1)*(1+1);
  std::cout << "Dim is: " << V->dim() << ", " << dim << std::endl;
  //EXPECT_EQ(dim, V->dim());

  //std::shared_ptr<dolfin::Mesh> mesh(new dolfin::UnitCubeMesh(12, 12, 12));
  //auto V = create_xfem_space(mesh, 0.2);
  //EXPECT_EQ(V->dim(),  (std::size_t) 24);
}
