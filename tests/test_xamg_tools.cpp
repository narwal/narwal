// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//         Axel Gerstenberger
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "gtest_tools.h"

#include <dolfin.h>
#include "Narwal.h"
#include "Poisson1D_P1.h"
#include "Poisson3D_P1.h"

// FIXME: Add more assertion tests

namespace
{
  // Source term (right-hand side)
  class Source : public dolfin::Expression
  {
    void eval(dolfin::Array<double>& values, const dolfin::Array<double>& /*x*/) const
    { values[0] = 0.0; }
  };
}


TEST(XAMGTools, MatPtAP1d)
{
  dolfin::parameters["linear_algebra_backend"] = "PETSc";
  dolfin::parameters["reorder_dofs_serial"] = false;

  // use a 1d Poisson element to create the system matrix
  auto mesh = std::make_shared<dolfin::IntervalMesh>(1, 0, 1);
  auto V = std::make_shared<Poisson1D_P1::FunctionSpace>(mesh);

  // Define variational problem
  Poisson1D_P1::BilinearForm a(V, V);
  Poisson1D_P1::LinearForm L(V);

  auto f = std::make_shared<::Source>();
  L.f = f;

  auto A = narwal::la::initSystemMatrix();
  auto b = narwal::la::initSystemVector();

  dolfin::assemble_system(*A, *b, a, L, {});

  std::shared_ptr<dolfin::GenericMatrix> G(A->copy());
  G->zero();
  G->ident_zeros();

  G->setitem(std::make_pair(0, 1), -1.0);
  G->apply("insert");

  auto PtAP = narwal::la::MatPtAP(*G, *A);
  std::cout << PtAP->str(true) << std::endl;
  ASSERT_TRUE(PtAP->is_symmetric(1.0e-12));

  ASSERT_DOUBLE_EQ(1.0, PtAP->getitem(std::make_pair(0,0)));
  ASSERT_DOUBLE_EQ(-2.0, PtAP->getitem(std::make_pair(0,1)));
  ASSERT_DOUBLE_EQ(-2.0, PtAP->getitem(std::make_pair(1,0)));
  ASSERT_DOUBLE_EQ(4.0, PtAP->getitem(std::make_pair(1,1)));
}


TEST(XAMGTools, MatPtAP3d)
{
  dolfin::parameters["linear_algebra_backend"] = "PETSc";
  dolfin::parameters["reorder_dofs_serial"] = false;

  // use a 1d Poisson element to create the system matrix
  auto mesh = std::make_shared<dolfin::UnitCubeMesh>(2, 2, 2);
  auto V = std::make_shared<Poisson3D_P1::FunctionSpace>(mesh);

  // Define variational problem
  Poisson3D_P1::BilinearForm a(V, V);
  Poisson3D_P1::LinearForm L(V);

  auto f = std::make_shared<::Source>();
  L.f = f;

  auto A = narwal::la::initSystemMatrix();
  auto b = narwal::la::initSystemVector();

  dolfin::assemble_system(*A, *b, a, L, {});

  std::shared_ptr<dolfin::GenericMatrix> G(A->copy());
  G->zero();
  G->ident_zeros();

  G->setitem(std::make_pair(0, 1), -1.0);
  G->apply("insert");

  auto PtAP = narwal::la::MatPtAP(*G, *A);
  ASSERT_TRUE(PtAP->is_symmetric(1.0e-12));
}
