// Copyright (C) 2016 Melior Innovations
// Author: Garth N. Wells
//         Axel Gerstenberger
//
// This file is part of Narwal.
//
// Narwal is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Narwal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Narwal. If not, see <http://www.gnu.org/licenses/>.

#include "xamg_poisson.h"
#include <Poisson3D_P1.h>

#include "gtest_tools.h"

// FIXME: Add test assertions  to each function, including on solution norm
namespace
{
  static const int polynomialDegree = 1;
  static const int max_element_dim = 1*2*2;
  typedef Poisson3D_P1::FunctionSpace FuncSpace;
}

TEST(xamg, PoissonPetscGeneralAMG)
{
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  test::xamg_poisson::PoissonTestSetup<FuncSpace> s(
      "output/" + gtest_nameroot(), polynomialDegree, max_element_dim);

  // Create null space
  auto null_space =
      narwal::xamg::NullSpace3DScalarSolution(s.V->function_space(), s.b).build();

  // Create preconditioner and attach null space
  auto pc = std::make_shared<dolfin::PETScPreconditioner>("petsc_amg"); // use "petsc_amg" or "ml_amg"
  pc->set_nullspace(*null_space);
  pc->parameters["report"] = false;

  // Create solver
  dolfin::PETScKrylovSolver solver("gmres", pc);
  solver.set_operator(s.A);
  solver.parameters["report"] = false;
  solver.parameters["monitor_convergence"] = true;
  solver.parameters["relative_tolerance"] = 1.0e-8;
  solver.parameters["convergence_norm_type"] = "true";

  solver.solve(*s.A, *s.u->vector(), *s.b);
}


TEST(xamg, PoissonPetscGeneraLU)
{
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  test::xamg_poisson::PoissonTestSetup<FuncSpace> s(
      "output/" + gtest_nameroot(), polynomialDegree, max_element_dim);

  // Solve
  #ifdef PETSC_HAVE_SUPERLU
  dolfin::LUSolver solver("superlu");
  #else
  dolfin::LUSolver solver;
  #endif
  solver.solve(*s.A, *s.u->vector(), *s.b);
}

TEST(xamg, DISABLED_PoissonPetscTransformPetscAMG)
{
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  test::xamg_poisson::PoissonTestSetup<FuncSpace> s(
      "output/" + gtest_nameroot(), polynomialDegree, max_element_dim);

  auto dofmap = s.V->function_space()->dofmap();
  auto tdofmap = std::dynamic_pointer_cast<const narwal::DofMap>(dofmap);

  // Create system matrix and rhs vector
  narwal::la::SolverXAMG xSolver(s.A, s.b, s.u, s.V->function_space(),
                                 s.domainManager, tdofmap, s.xfem_forms,
                                 true, true, "output/" + gtest_nameroot());

  // Create null space
  xSolver.null_space =
      narwal::xamg::NullSpace3DScalarSolution(s.V->function_space(), s.b).build();

  // Create preconditioner & solver
  xSolver.setupDefaultPreCondAndSolver("petsc_amg");

  // Solve
  EXPECT_LT(xSolver.solve(), std::size_t(20));
}


TEST(xamg, DISABLED_PoissonPetscTransformPetscML)
{
  dolfin::parameters["linear_algebra_backend"] = "PETSc";

  test::xamg_poisson::PoissonTestSetup<FuncSpace> s(
      "output/" + gtest_nameroot(), polynomialDegree, max_element_dim);

  auto dofmap = s.V->function_space()->dofmap();
  auto tdofmap = std::dynamic_pointer_cast<const narwal::DofMap>(dofmap);

  // Create system matrix and rhs vector
  narwal::la::SolverXAMG xSolver(s.A, s.b, s.u, s.V->function_space(),
                                 s.domainManager, tdofmap, s.xfem_forms,
                                 true, true, "output/" + gtest_nameroot());

  // Create null space
  xSolver.null_space =
      narwal::xamg::NullSpace3DScalarSolution(s.V->function_space(), s.b).build();

  // Create preconditioner & solver
  xSolver.setupDefaultPreCondAndSolver("ml_amg");

  // Solve
  EXPECT_LT(xSolver.solve(), std::size_t(20));
}
